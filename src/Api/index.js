import { AsyncStorage } from 'AsyncStorage';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { API_BASE, API_BASE1 } from "../configs/costant.config";

export const ApiExecute = async (url, params = {}) => {
    const ImageKit = require('imagekit');
    const imagekit = new ImageKit({ urlEndpoint: '<YOUR_IMAGEKIT_URL_ENDPOINT>', publicKey: '<YOUR_IMAGEKIT_PUBLIC_KEY>', privateKey: '<YOUR_IMAGEKIT_PRIVATE_KEY>' });
    let api_params = {
        cache: 'no-cache',
        mode: 'cors'
    };
    api_params.method = params.method ?? 'GET';
    api_params.headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
    if (params.data) {
        api_params.body = JSON.stringify(params.data);
    }
    if (params.auth) {
        let token = await AsyncStorage.getItem('@token');
        api_params.headers["x-access-token"] = token;
    }

    return fetch(`${API_BASE}${url}`, api_params)
        .then(response => response.json())
        .then(json => {
            return { status: true, data: json };
        })
        .catch(err => {
            // console.error('API Error: ' + err);
            return { status: false, data: err };
        })
}
export const ApiExecute1 = async (url, params = {}) => {
    let api_params = {
        cache: 'no-cache',
        mode: 'cors'
    };
    api_params.method = params.method ?? 'GET';
    api_params.headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
    if (params.data) {
        api_params.body = JSON.stringify(params.data);
    }
    if (params.auth) {
        let token = await AsyncStorage.getItem('@token');
        api_params.headers["x-access-token"] = token;
    }

    return fetch(`${API_BASE1}${url}`, api_params)
        .then(response => response.json())
        .then(json => {
            return { status: true, data: json };
        })
        .catch(err => {
            // console.error('API Error: ' + err);
            return { status: false, data: err };
        })
}

export const ToastMessage = (status, msg) => {
    if (status === 'success') {
        toast.success(msg);
    } else if (status === 'info') {
        toast.info(msg);
    } else if (status === 'warning   ') {
        toast.warning(msg);
    } else if (status === 'error') {
        toast.error(msg);
    } else if (status === 'dark') {
        toast.dark(msg);
    } else {
        toast(msg);
    }
}

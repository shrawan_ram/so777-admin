import $ from "jquery";

export const ThemeColor = (color) => {
    if (color === 'dark') {
        $('html').attr('data-theme', 'dark');
    } else if (color === 'blue') {
        $('html').attr('data-theme', 'blue');
    } else if (color === 'light') {
        $('html').attr('data-theme', 'light');
    } else {
        $('html').attr('data-theme', 'dark');
    }
}

const API_BASE = '/node/api/';
// const API_BASE = 'http:///localhost:3000/api/';
const API_BASE1 = 'http://139.162.213.154:8040/demo-api/';
const IMG_BASE = 'http://localhost:3000/';

const Admin_prifix = '';

const COLOR = {
    primary: '#3498db'
}

module.exports = {
    API_BASE,
    COLOR,
    Admin_prifix,
    IMG_BASE,
    API_BASE1
}

import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import $ from "jquery";
import MainLayout from "./Screens/Layout";
// import FrontLayout from "./Screens/FrontLayout";

export default class App extends Component {
  state = {
    routes: [
      {
        name: 'Home',
        path: '/',
        component: 'AdminScreen'
      },
      {
        name: 'Dashboard',
        path: '/dashboard',
        component: 'DashboardScreen'
      }
    ]
  }

  async componentDidMount() {
    $('body').addClass('animate-on');
  }

  render() {
    return (
      <Router>
        <Switch>
          <MainLayout {...this.props} />
          {/* <FrontLayout  {...this.props} /> */}
        </Switch>
      </Router >
    );
  }
}


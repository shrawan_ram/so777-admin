import React, { Component } from "react";
// import { Collapse } from "react-collapse";
import Slider from "react-slick";
import Popup from 'reactjs-popup';
// import { Button } from 'react-bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import $ from "jquery";
import { ApiExecute, ToastMessage } from '../Api';
import { Link, useParams } from "react-router-dom";
import AdminHeader from '../Components/AdminHeader';
import AdminSidebar from '../Components/AdminSidebar';
import { Admin_prifix } from "../configs/costant.config";


export default class DashboardScreen extends Component {
    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        id: null,
        name: '',
        image: '',
    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }

    onChangename = e => {
        // this.state.name = text;
        this.setState({ name: e.target.value });
    }

    saveSlider = async () => {
        let {
            id,
            name,
            image
        } = this.state;
        console.log('name', name);
        console.log('id', id);
        console.log('image', image);

        if (!name || name.trim() === '') {

            ToastMessage('error', 'Please enter name!');
        } else {
            if (id) {
                let data = {
                    id: id,
                    name: name,
                    image: image,
                }
                let api_response = await ApiExecute(`slider/${id}`, { method: 'PUT', data: data });

                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.image = null;
                    this.state.id = null;
                    let url = `/${Admin_prifix}slider`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    ToastMessage('error', api_response.data.errors[0]);
                }
            } else {
                let data = {
                    name: name,
                    image: image,
                }
                let api_response = await ApiExecute("add-slider", { method: 'POST', data: data });

                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.image = null;
                    this.state.id = null;
                    let url = `/${Admin_prifix}slider`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    ToastMessage('error', api_response.data.errors[0]);
                }
            }
        }
    }

    getSlider = async () => {
        let slider_id = this.state.id;
        console.log(slider_id);
        let api_response = await ApiExecute(`slider/${this.state.id}`, { method: 'GET' });
        console.log('api_response', api_response);
        this.setState({ image: api_response.data.image });
        this.setState({ name: api_response.data.name });
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        if (id) {
            this.state.id = id;
            this.getSlider();
        }
    }


    render() {

        let { Router } = this.props;
        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");
        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        return (

            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">{this.state.id ? "Update" : "Create"} Slider</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}slider`} class="router-link-active" target="_self">Slider</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Create Slider</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">General Information</h4>
                                            <div class="form-group">
                                                <label>Name:</label>
                                                <input placeholder="Name" type="text" name="name" value={this.state.name} onInput={this.onChangename} data-vv-as="Name" autocomplete="Name" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>

                                            <div class="d-flex justify-content-end align-items-center">
                                                <button type="submit" onClick={this.saveSlider} id="spinner-dark-8" class="btn btn-primary ml-2">
                                                    Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
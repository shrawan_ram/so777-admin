import React, { Component } from "react";
import { ApiExecute, ToastMessage } from '../Api';
import { Link } from "react-router-dom";
import { Admin_prifix } from "../configs/costant.config";


export default class GameTypeCreate extends Component {
    state = {
        categories: [],
        id: null,
        name: '',
        slug: '',
        icon: '',
        category_id: '',
        image: '',
    }

    onChangename = e => {
        this.setState({ name: e.target.value });
    }
    onChangeslug = e => {
        this.setState({ slug: e.target.value });
    }
    onChangeicon = e => {
        this.setState({ icon: e.target.value });
    }
    onChangecategory = e => {
        this.setState({ category_id: e.target.value });
    }
    onChangeimage = e => {
        this.setState({ image: e.target.value });
    }

    saveGametype = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log('user', user);
        let admin_id = user.id;
        let {
            id,
            name,
            slug,
            icon,
            category_id,
            image,
        } = this.state;

        if (!name || name.trim() === '') {
            ToastMessage('error', 'Please enter name !');
        } else {
            if (id) {
                let data = {
                    id: id,
                    name: name,
                    slug: slug,
                    icon: icon,
                    category_id: category_id,
                    image: image,
                }
                let api_response = await ApiExecute(`gametype/${id}`, { method: 'PUT', data: data });

                console.log('login_response', api_response.data.errors);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.slug = null;
                    this.state.icon = null;
                    this.state.category_id = null;
                    this.state.id = null;
                    this.state.image = null;
                    let url = `/${Admin_prifix}gametype`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            } else {
                let data = {
                    name: name,
                    slug: slug,
                    icon: icon,
                    category_id: category_id,
                    image: image,
                }
                let api_response = await ApiExecute("add-gametype", { method: 'POST', data: data });

                console.log('login_response', api_response);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.slug = null;
                    this.state.icon = null;
                    this.state.category_id = null;
                    this.state.id = null;
                    this.state.image = null;
                    let url = `/${Admin_prifix}gametype`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            }
        }
    }

    getGametype = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log(user.role_id);

        let api_response = await ApiExecute(`all-category`, { method: 'GET' });
        this.setState({ categories: api_response.data });
        console.log(api_response.data);
    }

    getadmins = async () => {
        let api_response = await ApiExecute(`gametype/${this.state.id}`, { method: 'GET' });
        console.log('api_response', api_response);
        this.setState({ name: api_response.data.name });
        this.setState({ slug: api_response.data.slug });
        this.setState({ icon: api_response.data.icon });
        this.setState({ image: api_response.data.image });
        this.setState({ category_id: api_response.data.category_id });
    }

    componentDidMount() {
        this.getGametype();
        let id = this.props.match.params.id;
        if (id) {
            this.state.id = id;
            this.getadmins();
        }
    }


    render() {
        let self = this;
        let { Router } = this.props;
        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">{this.state.id ? "Update" : "Create"} Game Type</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}gametype`} class="router-link-active" target="_self">Game Type</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Create Game Type</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">General Information</h4>
                                            <div class="form-group">
                                                <label>Name :</label>
                                                <input placeholder="Name" type="text" name="name" value={this.state.name} onInput={this.onChangename} data-vv-as="Name" autocomplete="Name" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Slug :</label>
                                                <input placeholder="Slug" type="text" data-vv-as="Slug" name="slug" value={this.state.slug} onInput={this.onChangeslug} class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Category :</label>
                                                <select name="category" defaultValue={this.state.category_id} value={this.state.category_id} onChange={this.onChangecategory} class="form-control animation">
                                                    <option value="">Select Category</option>
                                                    {
                                                        this.state.categories.map((category) => {
                                                            return (
                                                                <option value={category.id}>{category.name}</option>
                                                            );
                                                        })
                                                    }
                                                </select>
                                            </div>
                                            <div class="d-flex justify-content-end align-items-center">
                                                <button type="submit" onClick={this.saveGametype} id="spinner-dark-8" class="btn btn-primary ml-2">
                                                    Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
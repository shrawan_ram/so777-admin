import React, { Component } from "react";
import { ApiExecute, ToastMessage } from '../Api';
import { Link } from "react-router-dom";
import { Admin_prifix } from "../configs/costant.config";


export default class AdminScreen extends Component {
    state = {
        roles: [],
        id: null,
        name: '',
        mobile: '',
        email: '',
        password: '',
        role_id: '4',
        user_name: '',
        domain: '',
    }

    onChangename = e => {
        this.setState({ name: e.target.value });
    }
    onChangeusername = e => {
        this.setState({ user_name: e.target.value });
    }
    onChangemobile = e => {
        this.setState({ mobile: e.target.value });
    }
    onChangeemail = e => {
        this.setState({ email: e.target.value });
    }
    onChangepassword = e => {
        this.setState({ password: e.target.value });
    }
    onChangerole = e => {
        this.setState({ role_id: e.target.value });
    }
    onChangedomain = e => {
        this.setState({ domain: e.target.value });
    }

    saveAdmin = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log('user', user);
        let admin_id = user.id;
        let {
            id,
            name,
            user_name,
            email,
            password,
            role_id,
            mobile,
            domain,
        } = this.state;
        console.log(admin_id);

        if (!name || name.trim() === '') {
            ToastMessage('error', 'Please enter name !');
        } else if (!user_name || user_name.trim() === '') {
            ToastMessage('error', 'Please enter user name !');
        } else if (!role_id === '') {
            ToastMessage('error', 'Please select role !');
        } else if (!domain === '') {
            ToastMessage('error', 'Please enter domain !');
        } else {
            if (id) {
                let data = {
                    id: id,
                    name: name,
                    user_name: user_name,
                    mobile: mobile,
                    email: email,
                    role_id: role_id,
                    admin_id: admin_id,
                    password: password,
                    domain: domain,
                }
                let api_response = await ApiExecute(`admin/${id}`, { method: 'PUT', data: data });

                console.log('login_response', api_response.data.errors);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.user_name = null;
                    this.state.mobile = null;
                    this.state.mobile = null;
                    this.state.password = null;
                    this.state.role_id = null;
                    this.state.id = null;
                    this.state.domain = null;
                    this.props.history.push('/admin');
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            } else {
                let data = {
                    name: name,
                    user_name: user_name,
                    mobile: mobile,
                    email: email,
                    role_id: role_id,
                    admin_id: admin_id,
                    domain: domain,
                    password: password,
                }
                let api_response = await ApiExecute("add-admin", { method: 'POST', data: data });

                console.log('login_response', api_response);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.user_name = null;
                    this.state.email = null;
                    this.state.mobile = null;
                    this.state.password = null;
                    this.state.role_id = null;
                    this.state.id = null;
                    this.state.domain = null;
                    this.props.history.push('/admin');
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            }
        }
    }

    getRoles = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log(user.role_id);

        let api_response = await ApiExecute(`role?role_id=${user.role_id}`, { method: 'GET' });
        this.setState({ roles: api_response.data });
        console.log(api_response.data);
    }

    getadmins = async () => {
        let user_id = this.state.id;
        console.log(user_id);
        let api_response = await ApiExecute(`admin/${this.state.id}`, { method: 'GET' });
        console.log('api_response', api_response);
        this.setState({ mobile: api_response.data.mobile });
        this.setState({ name: api_response.data.name });
        this.setState({ user_name: api_response.data.user_name });
        this.setState({ email: api_response.data.email });
        this.setState({ role_id: api_response.data.role_id });
    }

    componentDidMount() {
        console.log('sdkfdfkgdfk', Admin_prifix);
        this.getRoles();
        let id = this.props.match.params.id;
        if (id) {
            this.state.id = id;
            this.getadmins();
        }
    }


    render() {
        let self = this;
        let { Router } = this.props;
        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">{this.state.id ? "Update" : "Create"} Admin</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}admin`} class="router-link-active" target="_self">Admins</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Create Admin</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">General Information</h4>
                                            <div class="form-group">
                                                <label>User Name :</label>
                                                <input placeholder="User Name" type="text" name="user_name" required value={this.state.user_name} onInput={this.onChangeusername} data-vv-as="User Name" autocomplete="username" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Full Name :</label>
                                                <input placeholder="Full Name" type="text" name="name" value={this.state.name} onInput={this.onChangename} data-vv-as="Name" autocomplete="Name" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Email :</label>
                                                <input placeholder="Email" type="email" data-vv-as="Email" name="email" value={this.state.email} onInput={this.onChangeemail} class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Mobile Number :</label>
                                                <input placeholder="mobile" type="text" name="mobile" value={this.state.mobile} onInput={this.onChangemobile} data-vv-as="mobile" autocomplete="mobile" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Password :</label>
                                                <input placeholder="Password" type="password" data-vv-as="Password" name="password" value={this.state.password} onInput={this.onChangepassword} class="form-control animation" aria-required="false" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Domain :</label>
                                                <input placeholder="Domain" type="domain" data-vv-as="Domain" name="domain" value={this.state.domain} onInput={this.onChangedomain} class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            {/* <div class="form-group">
                                                            <label>Confirm Password:</label>
                                                            <input placeholder="Confirm Password" type="password" data-vv-as="Confirm Password" name="cpassword" class="form-control animation" aria-required="true" aria-invalid="false" />
                                                        </div> */}
                                            <div class="d-flex justify-content-end align-items-center">
                                                <button type="submit" onClick={this.saveAdmin} id="spinner-dark-8" class="btn btn-primary ml-2">
                                                    Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
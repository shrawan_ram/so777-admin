import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';
import Moment from 'react-moment';


export default class AccountStatement extends Component {

    state = {
        user: {},
        accountHistory: {},
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        user = JSON.parse(user);
        this.setState({
            user: user
        });
        this.accountHistory(user.id);

        $('header.header').addClass('header-casino');

    }
    accountHistory = async (id) => {
        let uId = id ? id : this.state.user.id;

        let api_response = await ApiExecute(`wallet-history/${uId}`, { method: 'GET' });
        this.setState({ accountHistory: api_response.data });
    }

    render() {
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="center-main-content report-container">
                            <div class="news-bar d-none-desktop">
                                <marquee>Launched Best Ever 8 Deck Casino 1 Card ONE-DAY.</marquee>
                                <div class="news-title"><img src="https://sitethemedata.com/v3/static/front/img/icons/speaker.svg" /></div>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel2" data-ride="carousel" data-interval="3000" class="carousel vert slide"></div>
                                </div>
                            </div>
                            <div class="report-box">
                                <div class="report-title">
                                    <div class="report-name">Account Statement</div>
                                    <div class="report-search search-box">
                                        <div class="form-group mb-0"><input type="text" placeholder="Search" class="form-control" /> <img src="https://sitethemedata.com/v6/static/front/img/search.svg" class="search-icon" /></div>
                                    </div>
                                </div>
                                <div class="report-form">
                                    <div class="form-group from-date">
                                        <label>From</label>
                                        <div class="mx-datepicker">
                                            <div class="mx-input-wrapper">
                                                <input name="date" type="text" autocomplete="off" placeholder="" class="mx-input" />
                                                <i class="mx-icon-clear">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" width="1em" height="1em">
                                                        <path d="M810.005333 274.005333l-237.994667 237.994667 237.994667 237.994667-60.010667 60.010667-237.994667-237.994667-237.994667 237.994667-60.010667-60.010667 237.994667-237.994667-237.994667-237.994667 60.010667-60.010667 237.994667 237.994667 237.994667-237.994667z"></path>
                                                    </svg>
                                                </i>
                                                <i class="mx-icon-calendar">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" width="1em" height="1em">
                                                        <path d="M940.218182 107.054545h-209.454546V46.545455h-65.163636v60.50909H363.054545V46.545455H297.890909v60.50909H83.781818c-18.618182 0-32.581818 13.963636-32.581818 32.581819v805.236363c0 18.618182 13.963636 32.581818 32.581818 32.581818h861.090909c18.618182 0 32.581818-13.963636 32.581818-32.581818V139.636364c-4.654545-18.618182-18.618182-32.581818-37.236363-32.581819zM297.890909 172.218182V232.727273h65.163636V172.218182h307.2V232.727273h65.163637V172.218182h176.872727v204.8H116.363636V172.218182h181.527273zM116.363636 912.290909V442.181818h795.927273v470.109091H116.363636z"></path>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group to-date">
                                        <label>To</label>
                                        <div class="mx-datepicker">
                                            <div class="mx-input-wrapper">
                                                <input name="date" type="text" autocomplete="off" placeholder="" class="mx-input" />
                                                <i class="mx-icon-clear">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" width="1em" height="1em">
                                                        <path d="M810.005333 274.005333l-237.994667 237.994667 237.994667 237.994667-60.010667 60.010667-237.994667-237.994667-237.994667 237.994667-60.010667-60.010667 237.994667-237.994667-237.994667-237.994667 60.010667-60.010667 237.994667 237.994667 237.994667-237.994667z"></path>
                                                    </svg>
                                                </i>
                                                <i class="mx-icon-calendar">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" width="1em" height="1em">
                                                        <path d="M940.218182 107.054545h-209.454546V46.545455h-65.163636v60.50909H363.054545V46.545455H297.890909v60.50909H83.781818c-18.618182 0-32.581818 13.963636-32.581818 32.581819v805.236363c0 18.618182 13.963636 32.581818 32.581818 32.581818h861.090909c18.618182 0 32.581818-13.963636 32.581818-32.581818V139.636364c-4.654545-18.618182-18.618182-32.581818-37.236363-32.581819zM297.890909 172.218182V232.727273h65.163636V172.218182h307.2V232.727273h65.163637V172.218182h176.872727v204.8H116.363636V172.218182h181.527273zM116.363636 912.290909V442.181818h795.927273v470.109091H116.363636z"></path>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control">
                                            <option value="1">All</option>
                                            <option value="2">Deposit/Withdraw Report</option>
                                            <option value="3">Game Report</option>
                                        </select>
                                    </div>
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                                <div class="report-page-count">
                                    <div class="form-group mb-0">
                                        <label>Show</label>
                                        <select class="form-control">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="200">200</option>
                                            <option value="300">300</option>
                                            <option value="400">400</option>
                                            <option value="500">500</option>
                                        </select>
                                        <label>Entries</label>
                                    </div>
                                    <div class="file-icons">
                                        <div><i class="fas fa-file-pdf disabled"></i></div>
                                        <div id="export_1633686148796"><i class="fas fa-file-excel disabled"></i></div>
                                    </div>
                                </div>
                                <div class="report-table ac-statement table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="bet-date">
                                                    <div>Date</div>
                                                </th>
                                                <th class="bet-user-rate">
                                                    <div>Sr no</div>
                                                </th>
                                                <th class="bet-amount">
                                                    <div>Credit</div>
                                                </th>
                                                <th class="bet-amount">
                                                    <div>Debit</div>
                                                </th>
                                                <th class="bet-remark">
                                                    <div>Remark</div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.accountHistory && this.state.accountHistory.length ?
                                                    <>
                                                        {
                                                            this.state.accountHistory && this.state.accountHistory.length && this.state.accountHistory.map((element, index) => {
                                                                return (
                                                                    <>
                                                                        <tr>
                                                                            <td class="bet-date">
                                                                                <div>
                                                                                    <Moment format="DD MMM YYYY">
                                                                                        {element.created_at}
                                                                                    </Moment>
                                                                                </div>
                                                                            </td>
                                                                            <td class="bet-user-rate">
                                                                                <div>{index + 1}</div>
                                                                            </td>
                                                                            <td class="bet-amount">
                                                                                <div>{element.type == 'deposit' ? element.amount : '-'}</div>
                                                                            </td>
                                                                            <td class="bet-amount">
                                                                                <div>{element.type != 'deposit' ? element.amount : '-'}</div>
                                                                            </td>
                                                                            <td class="bet-remark">
                                                                                <div class="ifTooltip">
                                                                                    {element.remark}
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </>
                                                                )

                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <tr class="no-record">
                                                        <td colspan="6">No records found</td>
                                                    </tr>
                                            }
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <FrontFooter {...this.props} />

                    </div>
                </div>

            </div >

        );
    }
}
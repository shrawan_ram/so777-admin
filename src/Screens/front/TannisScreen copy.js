import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ApiExecute1, ToastMessage } from '../../Api';
import { ThemeColor } from '../../assets/js/customreact';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Carousel } from 'react-responsive-carousel';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontFooter from '../../Components/FrontFooter';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import Moment from 'react-moment';
// const kFormatter = async (num) => {
//     // console.log(num);
//     return num;
//     // return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(1)) + 'k' : Math.sign(num) * Math.abs(num)
// }
export default class TannisScreen extends Component {
    state = {
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        banners: [
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16298546640188872.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/162971585470715.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16294605190635378.jpeg'
            }
        ],
        topHeaderRemove: '',
        user: {},
        subcategory: {},
        match_id: '',
        subcategories: [],
        matches: [],
        match_detail: {},
        odds_market: [],
        markets: {},
        loading: true,
    }

    userdropdown(e) {
        if ($(e.target).is('.user-dropdown')) {
            $('.user-dropdown.collapse').toggleClass('show');
        }
    }

    async componentDidunMount() {
        clearInterval(this.odds_details);
    }
    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        console.log('front_token', user);
        this.setState({
            user: JSON.parse(user)
        });

        let match_id = this.props.match.params.slug;
        if (match_id) {
            // this.state.match_id = match_id;
            console.log('klfjglkdf odfj giodfgfde r ewrr', match_id);
            this.setState({
                match_id: match_id
            });
            // this.match_details(match_id);
            this.checkGame(match_id);
        }

        $(document).on('click', '.bet-table .bet-table-header', e => {
            $(e.target).closest(".bet-table").children('.bet-table-body').toggleClass('show');

            if ($(e.target).closest(".bet-table-header").attr('aria-expanded')) {
                $(e.target).closest(".bet-table-header").attr('aria-expanded', 'false')
            } else {
                $(e.target).closest(".bet-table-header").attr('aria-expanded', 'true')
            }
            $(e.target).closest(".bet-table-header").toggleClass('collapsed');
        });

        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });
        $(document).on('click', '.accordion-box .has-arrow', e => {
            $('.user-dropdown.collapse').toggleClass('show');
        })
        $('button.slick-arrow').html('');
        $('.sport-tabs .slick-arrow').html('');

        $(document).on('click', '.sport-tabs .nav-tabs .nav-item', e => {
            $(e.target).closest(".slick-slide").children('div').children('.nav-item').children('.nav-link').toggleClass('active');
            let slug = $(e.target).closest(".slick-slide").children('div').children('.nav-item').data('id');

            // this.getCategoryDetail(slug);

            $(e.target).closest(".slick-slide").siblings().children('div').children('.nav-item').children('.nav-link').removeClass('active');
        });

    }

    match_details = async (id) => {
        let api_response = await ApiExecute1(`oddslist?sport_id=2`, { method: 'GET' });
        api_response.data.data.forEach(element => {
            if (element.eventId == id) {
                console.log('sdfkjjm d d f dfffff', element);
                this.setState({ match_detail: element });
                this.odds_details(element.marketId);

                setInterval(this.odds_details, 1000);
            }
        });
    }
    checkGame = async (id) => {
        let api_response = await ApiExecute1(`gettennismatches`, { method: 'GET' });
        console.log('status check', api_response.data);
        console.log(id);
        api_response.data.forEach(element => {

            this.setState({ loading: true });

            if (element.gameId == id) {
                console.log('sdfkjjm d d f dfffff', element);
                this.setState({ match_detail: element });
                this.odds_details(element.marketId);

                setInterval(this.odds_details, 1000);

                console.log('status chesdfghfhe rrck fyhtg', element);
                this.fancy_details(this.state.match_detail.eventId);

                setInterval(this.fancy_details, 1000);
            }
        });
    }
    odds_details = async (id = null) => {
        let mId = id ? id : this.state.match_detail.marketId;
        if (mId != undefined) {
            let market_response = await ApiExecute1(`getdata?market_id=${mId}`, { method: 'GET' });
            this.setState({ odds_market: market_response.data[0][0] });
        }
    }

    fancy_details = async (id = null) => {
        let eId = id ? id : this.state.match_detail.eventId;
        if (eId != undefined) {

            console.log('fancy_details');
            // let market_response = await ApiExecute1(`getfancydata?event_id=${eId}`, { method: 'GET' });
            // console.log('fancy_details', market_response);
            // console.log('sdklffdll dg l eid : ');
            // market_response.data.forEach(element => {
            // this.setState({ markets: market_response.data });
            // this.setState({ loading: false });
            // });
        }
    }

    render() {
        let self = this;

        var settings = {
            speed: 3000,
            autoplay: true,
            autoplaySpeed: 0,
            centerMode: true,
            // cssEase: 'linear',
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical: true,
            // variableWidth: true,
            infinite: true,
            initialSlide: 1,
            arrows: true,
            buttons: true
        };

        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">
                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />
                    <FrontSidebar {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="center-main-content">
                            <div class="news-bar d-none-desktop">
                                <div class="news-title"><i class="fas fa-bell mr-2"></i> News</div>
                                <marquee>We are happy to announce that we are bringing you a new world of gambling.</marquee>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel3" data-ride="carousel" data-interval="3000" class="carousel vert slide">

                                    </div>
                                </div>
                            </div>
                            <div class="center-container">
                                <div class="detail-page-container cricket-detail">

                                    <div class="banner scorecard-banner" style={{ backgroundImage: "url(https://sitethemedata.com/v4/static/front/img/events-banner/4.png)" }} ></div>
                                    {/* {
                                        !this.state.loading ? */}
                                    <>
                                        <div class="game-header d-none-mobile mt-2 sport4">
                                            <span class="game-header-name">{this.state.match_detail.eventName}</span>
                                            <span class="game-header-date">
                                                <Moment format="DD/MM/YYYY HH:mm:ss">
                                                    {this.state.match_detail.eventDate}
                                                </Moment>
                                            </span>
                                        </div>
                                        <div class="game-header d-none-desktop mt-2 sport4">
                                            <span class="game-header-name">
                                                {this.state.match_detail.eventName}
                                                <div><small> {this.state.match_detail.eventDate}</small></div>
                                            </span>
                                        </div>
                                        <div class="search-box d-none-big w-100">
                                            <div class="form-group mb-0">
                                                <input type="text" placeholder="Search Market" class="form-control" style={{ textTransform: 'lowercase' }} />
                                                <img src="https://sitethemedata.com/v4/static/front/img/search.svg" class="search-icon" />
                                            </div>
                                        </div>
                                        <div class="all-markets d-none-small">
                                            <div>
                                                <a href="javascript:void(0)"><small>#TOURNAMENT_WINNER</small></a>
                                                <a href="javascript:void(0)"><small>#Bookmaker</small></a>
                                                <a href="javascript:void(0)"><small>#Normal</small></a>
                                            </div>
                                            <div class="search-box">
                                                <div class="form-group mb-2">
                                                    <input type="text" placeholder="Search Market" class="form-control" style={{ textTransform: 'lowercase' }} />
                                                    <img src="https://sitethemedata.com/v4/static/front/img/search.svg" class="search-icon" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="market-4" id="goto-0">
                                            <div class="bet-table">
                                                <div data-toggle="collapse" data-target="#market0" aria-expanded="true" class="bet-table-header">
                                                    <div class="nation-name">
                                                        <span title="TOURNAMENT_WINNER">
                                                            <a href="javascript:void(0)" title="">
                                                                <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                            </a>
                                                            TOURNAMENT_WINNER
                                                        </span>
                                                        <span class="max-bet"><span title="Max : 1">1</span></span>
                                                    </div>
                                                    <div class="back back-title bl-title d-none-mobile">Back</div>
                                                    <div class="lay lay-title bl-title d-none-mobile">Lay</div>
                                                </div>
                                                <div id="market0" data-title="open" class="bet-table-body collapse show">
                                                    {
                                                        this.state.match_detail ?
                                                            <>
                                                                {
                                                                    this.state.match_detail.status == 'ACTIVE' ?
                                                                        <>
                                                                            <div class="bet-table-mobile-row d-none-desktop">
                                                                                <div class="bet-table-mobile-team-name"><span>{this.state.match_detail.selectionId}</span> <span></span></div>
                                                                            </div>
                                                                            <div data-title="ACTIVE" class="bet-table-row">
                                                                                <div class="nation-name d-none-mobile">
                                                                                    <p><span>{this.state.match_detail.nat}</span> <span class="float-right"></span></p>
                                                                                    <p class="mb-0"></p>
                                                                                </div>

                                                                                <>
                                                                                    <div class={`point-title ${this.state.match_detail.back1 == 0 && this.state.match_detail.lay1 == 0 ? 'suspended' : ''}`}>
                                                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                                                        <div class="back bl-box no-odds"><span class="d-block odds">{this.state.match_detail.back1 == 0 ? '-' : this.state.match_detail.back1}</span></div>
                                                                                        <div class="lay bl-box no-odds"><span class="d-block odds">{this.state.match_detail.lay1 == 0 ? '-' : this.state.match_detail.lay1}</span></div>
                                                                                    </div>

                                                                                    <div class="point-title">
                                                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                                                        {
                                                                                            this.state.match_detail.back12 == 0 ?
                                                                                                <div class="no-val bl-box"><span class="d-block odds">—</span></div>
                                                                                                :
                                                                                                <div class="back bl-box no-odds"><span class="d-block odds">{this.state.match_detail.back12 == 0 ? '-' : this.state.match_detail.back12}</span></div>
                                                                                        }{
                                                                                            this.state.match_detail.lay12 == 0 ?
                                                                                                <div class="no-val bl-box"><span class="d-block odds">—</span></div>
                                                                                                :
                                                                                                <div class="lay bl-box no-odds"><span class="d-block odds">{this.state.match_detail.lay12 == 0 ? '-' : this.state.match_detail.lay12}</span></div>
                                                                                        }
                                                                                    </div>

                                                                                    <div class={`point-title ${this.state.match_detail.back11 == 0 && this.state.match_detail.lay11 == 0 ? 'suspended' : ''}`}>
                                                                                        <div class="text-center d-none-desktop point-title-header">2</div>
                                                                                        <div class="back bl-box no-odds"><span class="d-block odds">{this.state.match_detail.back11 == 0 ? '-' : this.state.match_detail.back11}</span></div>
                                                                                        <div class="lay bl-box no-odds"><span class="d-block odds">{this.state.match_detail.lay11 == 0 ? '-' : this.state.match_detail.lay11}</span></div>
                                                                                    </div>
                                                                                </>
                                                                            </div>
                                                                        </>
                                                                        :
                                                                        <>
                                                                            <div class="bet-table-mobile-row d-none-desktop">
                                                                                <div class="bet-table-mobile-team-name"><span>{this.state.match_detail.selectionId}</span> <span></span></div>
                                                                            </div>
                                                                            <div data-title="REMOVED" class="bet-table-row suspendedtext">
                                                                                <div class="nation-name d-none-mobile">
                                                                                    <p><span>{this.state.match_detail.nat}</span> <span class="float-right"></span></p>
                                                                                    <p class="mb-0"></p>
                                                                                </div>
                                                                                <div class="bl-box back back2 no-val">
                                                                                    <span class="d-block odds">—</span>
                                                                                </div>
                                                                                <div class="bl-box back back1 no-val">
                                                                                    <span class="d-block odds">—</span>
                                                                                </div>
                                                                                <div class="bl-box back back no-val">
                                                                                    <span class="d-block odds">—</span>
                                                                                </div>
                                                                                <div class="bl-box lay lay no-val">
                                                                                    <span class="d-block odds">—</span>
                                                                                </div>
                                                                                <div class="bl-box lay lay1 no-val">
                                                                                    <span class="d-block odds">—</span>
                                                                                </div>
                                                                                <div class="bl-box lay lay2 no-val">
                                                                                    <span class="d-block odds">—</span>
                                                                                </div>
                                                                            </div>
                                                                        </>

                                                                }
                                                            </>
                                                            : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.markets && this.state.markets.t2 && this.state.markets.t2.length ?
                                                <div class="market-4" id="goto-1">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market1" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Bookmaker">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    Bookmaker
                                                                </span>
                                                                <span class="max-bet">Min:<span>100</span> Max:<span>50K</span></span>
                                                            </div>
                                                            <div class="back back-title bl-title d-none-mobile">Back</div>
                                                            <div class="lay lay-title bl-title d-none-mobile">Lay</div>
                                                        </div>

                                                        <div id="market1" data-title="" class="bet-table-body collapse show">
                                                            {
                                                                this.state.markets && this.state.markets.t2 && this.state.markets.t2.map((element) => {
                                                                    return (
                                                                        <>
                                                                            {
                                                                                element && element.bm1 && element.bm1.map((e) => {
                                                                                    return (
                                                                                        <>
                                                                                            <div class="bet-table-mobile-row d-none-desktop">
                                                                                                <div class="bet-table-mobile-team-name"><span>Punjab Kings</span> <span></span></div>
                                                                                            </div>
                                                                                            <div data-title={e.s} class={`bet-table-row ${e.s != 'ACTIVE' ? 'suspendedtext' : ''}`}>
                                                                                                <div class="nation-name d-none-mobile">
                                                                                                    <p><span>{e.nat}</span> <span class="float-right"></span></p>
                                                                                                    <p class="mb-0"></p>
                                                                                                </div>
                                                                                                {
                                                                                                    e.b3s == 'True' ?
                                                                                                        <div class="bl-box back back2">
                                                                                                            <span class="d-block odds">{e.b3}</span>
                                                                                                            <span class="d-block">{e.bs3}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box back back2 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.b2s == 'True' ?
                                                                                                        <div class="bl-box back back1">
                                                                                                            <span class="d-block odds">{e.b2}</span>
                                                                                                            <span class="d-block">{e.bs2}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box back back1 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.b1s == 'True' ?
                                                                                                        <div class="bl-box back back">
                                                                                                            <span class="d-block odds">{e.b1}</span>
                                                                                                            <span class="d-block">{e.bs1}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box back back no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.l3s == 'True' ?
                                                                                                        <div class="bl-box lay lay">
                                                                                                            <span class="d-block odds">{e.l3}</span>
                                                                                                            <span class="d-block">{e.ls3}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box lay lay no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.l2s == 'True' ?
                                                                                                        <div class="bl-box lay lay1">
                                                                                                            <span class="d-block odds">{e.l2}</span>
                                                                                                            <span class="d-block">{e.ls2}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box lay lay1 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.l1s == 'True' ?
                                                                                                        <div class="bl-box lay lay2">
                                                                                                            <span class="d-block odds">{e.l1}</span>
                                                                                                            <span class="d-block">{e.ls1}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box lay lay2 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                            </div>
                                                                                        </>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </>
                                                                    )
                                                                })
                                                            }
                                                        </div>

                                                    </div>
                                                </div>

                                                : ''
                                        }
                                        {
                                            this.state.markets && this.state.markets.t3 && this.state.markets.t3.length ?
                                                <div class="market-6" id="goto-2">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market2" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Normal">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    {
                                                                        this.state.markets && this.state.markets.t3 && this.state.markets.t3.map((element, index) => {
                                                                            if (index == 0) {
                                                                                return (
                                                                                    <>
                                                                                        {element.gtype}
                                                                                    </>
                                                                                )
                                                                            }
                                                                        })
                                                                    }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="market2" data-title="OPEN" class="bet-table-body collapse show container-fluid container-fluid-5">
                                                            <div class="row row5">
                                                                {
                                                                    this.state.markets && this.state.markets.t3 && this.state.markets.t3.map((element) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-12 col-md-6">
                                                                                    <div class="fancy-tripple">
                                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                                            <div class="bet-table-mobile-team-name">
                                                                                                <span>{element.nat}</span> <span></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-title="" class="bet-table-row">
                                                                                            <div class="nation-name d-none-mobile">
                                                                                                <p>
                                                                                                    {element.nat}
                                                                                                </p>
                                                                                                <p class="mb-0"></p>
                                                                                            </div>
                                                                                            <div class="bl-box lay">
                                                                                                <span class="d-block odds">{element.l1}</span>
                                                                                                <span class="d-block">{element.ls1}</span>
                                                                                            </div>
                                                                                            <div class="bl-box back">
                                                                                                <span class="d-block odds">{element.b1}</span>
                                                                                                <span class="d-block">{element.bs1}</span>
                                                                                            </div>
                                                                                            <div class="fancy-min-max">
                                                                                                Min:<span>100</span> Max:<span>50K</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        {/* <div class="market-message">
                                                                                        Total Matches 1st over : 136 (Matches Played: 29)
                                                                                    </div> */}
                                                                                    </div>
                                                                                </div>

                                                                            </>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                : ''
                                        }
                                        {
                                            this.state.markets && this.state.markets.t4 && this.state.markets.t4.length ?
                                                <div class="market-6" id="goto-2">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market2" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Normal">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    {
                                                                        this.state.markets && this.state.markets.t4 && this.state.markets.t4.map((element, index) => {
                                                                            if (index == 0) {
                                                                                return (
                                                                                    <>
                                                                                        {element.gtype}
                                                                                    </>
                                                                                )
                                                                            }
                                                                        })
                                                                    }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="market2" data-title="OPEN" class="bet-table-body collapse show container-fluid container-fluid-5">
                                                            <div class="row row5">
                                                                {
                                                                    this.state.markets && this.state.markets.t4 && this.state.markets.t4.map((element) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-12 col-md-6">
                                                                                    <div class="fancy-tripple">
                                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                                            <div class="bet-table-mobile-team-name">
                                                                                                <span>{element.nat}</span> <span></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-title="" class="bet-table-row">
                                                                                            <div class="nation-name d-none-mobile">
                                                                                                <p>
                                                                                                    {element.nat}
                                                                                                </p>
                                                                                                <p class="mb-0"></p>
                                                                                            </div>
                                                                                            <div class="bl-box lay">
                                                                                                <span class="d-block odds">{element.l1}</span>
                                                                                                <span class="d-block">{element.ls1}</span>
                                                                                            </div>
                                                                                            <div class="bl-box back">
                                                                                                <span class="d-block odds">{element.b1}</span>
                                                                                                <span class="d-block">{element.bs1}</span>
                                                                                            </div>
                                                                                            <div class="fancy-min-max">
                                                                                                Min:<span>100</span> Max:<span>50K</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        {/* <div class="market-message">
                                                                                        Total Matches 1st over : 136 (Matches Played: 29)
                                                                                    </div> */}
                                                                                    </div>
                                                                                </div>

                                                                            </>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                : ''
                                        }
                                        {
                                            this.state.markets && this.state.markets.t5 && this.state.markets.t5.length ?
                                                <div class="market-6" id="goto-2">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market2" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Normal">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    {
                                                                        this.state.markets && this.state.markets.t5 && this.state.markets.t5.map((element, index) => {
                                                                            if (index == 0) {
                                                                                return (
                                                                                    <>
                                                                                        {element.gtype}
                                                                                    </>
                                                                                )
                                                                            }
                                                                        })
                                                                    }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="market2" data-title="OPEN" class="bet-table-body collapse show container-fluid container-fluid-5">
                                                            <div class="row row5">
                                                                {
                                                                    this.state.markets && this.state.markets.t5 && this.state.markets.t5.map((element) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-12 col-md-6">
                                                                                    <div class="fancy-tripple">
                                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                                            <div class="bet-table-mobile-team-name">
                                                                                                <span>{element.nat}</span> <span></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-title="" class="bet-table-row">
                                                                                            <div class="nation-name d-none-mobile">
                                                                                                <p>
                                                                                                    {element.nat}
                                                                                                </p>
                                                                                                <p class="mb-0"></p>
                                                                                            </div>
                                                                                            <div class="bl-box lay">
                                                                                                <span class="d-block odds">{element.l1}</span>
                                                                                                <span class="d-block">{element.ls1}</span>
                                                                                            </div>
                                                                                            <div class="bl-box back">
                                                                                                <span class="d-block odds">{element.b1}</span>
                                                                                                <span class="d-block">{element.bs1}</span>
                                                                                            </div>
                                                                                            <div class="fancy-min-max">
                                                                                                Min:<span>100</span> Max:<span>50K</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        {/* <div class="market-message">
                                                                                        Total Matches 1st over : 136 (Matches Played: 29)
                                                                                    </div> */}
                                                                                    </div>
                                                                                </div>

                                                                            </>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                : ''
                                        }
                                    </>
                                    {/* : <div style={{ textAlign: 'center', lineHeight: '10px' }}>Loading...</div>
                                    } */}

                                </div>
                                <FrontFooter {...this.props} />
                                <div id="right-sidebar-id" class="right-sidebar home-right-sidebar sticky">
                                    <div class="home-casiono-icons d-none-mobile">
                                        <Slider {...settings} >
                                            {
                                                this.state.upcoming_fixtures.map((winner, index) => {
                                                    return (
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="19" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '318px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/fantasy/ludo-lands" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629367059.451188.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    );
                                                })
                                            }
                                        </Slider>
                                    </div>
                                </div>
                            </div >
                        </div>
                    </div>
                </div>
            </div >

        );
    }
}
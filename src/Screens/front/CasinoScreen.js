import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Nav } from 'react-bootstrap';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';


export default class CasinoScreen extends Component {
    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        live_casinos: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/ezugi.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/superspade.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/qt.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/evolution.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/cockfight.jpg'
            }
        ],
        fantasy_games: [
            {
                image: 'https://sitethemedata.com/casino_icons/other/diam11.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/playerbattle.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/poptheball.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/ludoclub.jpg'
            },
        ],
        other_games: [
            {
                image: 'https://sitethemedata.com/casino_icons/other/binary.jpg'
            }
        ],
        sports: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/4-color.svg',
                name: 'Cricket'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/2-color.svg',
                name: 'Tennis'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/1-color.svg',
                name: 'Football'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/8-color.svg',
                name: 'Table Tennis'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/19-color.svg',
                name: 'Ice Hockey'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/55-color.svg',
                name: 'E Games'
            }, {
                image: 'https://sitethemedata.com/v3/static/front/img/events/11-color.svg',
                name: 'Rugby League'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/3-color.svg',
                name: 'Boxing'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/7-color.svg',
                name: 'Beach Volleyball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/9-color.svg',
                name: 'Mixed Martial Arts'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/10-color.svg',
                name: 'Futsal'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/12-color.svg',
                name: 'Horse Racing'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/15-color.svg',
                name: 'Greyhounds'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/16-color.svg',
                name: 'Basketball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/17-color.svg',
                name: 'MotoGP'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/18-color.svg',
                name: 'Chess'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/22-color.svg',
                name: 'Volleyball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/29-color.svg',
                name: 'Badminton'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/25-color.svg',
                name: 'Hockey'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/32-color.svg',
                name: 'Cycling'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/35-color.svg',
                name: 'Motorbikes'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/37-color.svg',
                name: 'Athletics'
            },

        ],
        top_winners: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        banners: [
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16298546640188872.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/162971585470715.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16294605190635378.jpeg'
            }
        ],
        topHeaderRemove: '',
        gametypes: [],
        user: {},
        category: {},
        category_slug: '',
        category_id: '',
        gametype_id: 5,
        games: [],
        subcategories: [],
        refreshComponet: true,
    }

    onClickGametype = async (id) => {
        await this.setState({ gametype_id: id });
        this.gameList();
        this.allcategoryList();
        this.setState({ refreshComponet: !this.state.refreshComponet })
    }
    onClickCategory = async (id) => {
        await this.setState({ category_id: id });
        this.gameList();
    }

    async componentDidMount() {

        console.log('props', this.props);

        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');

        this.setState({
            user: JSON.parse(user)
        });

        let category_slug = this.props.slug;

        if (category_slug) {
            this.state.category_slug = category_slug;
            this.getCategory(category_slug);
        }

        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });
        $(document).on('click', '.accordion-box .has-arrow', e => {
            $('.user-dropdown.collapse').toggleClass('show');
        })
        $(document).click(function (e) {
            if ($(e.target).is('.user-dropdown')) {
                $('.user-dropdown.collapse').toggleClass('show');
            }
        });
        let self = this;

        $(document).on('click', '.casino-tabs-menu .nav-item', e => {
            $(e.target).closest(".slick-slide").children('div').children('.nav').children('.nav-item').children('.nav-link').addClass('active');
            $(e.target).closest(".slick-slide").siblings().children('div').children('.nav').children('.nav-item').children('.nav-link').removeClass('active');
        });

        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });

        $('.casino-box-tabs .slick-slider').attr('style', 'width: 100%');
        $('.casino-box-tabs .slick-list').attr('style', 'margin: 0 40px');
    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        this.setState({ categories: api_response.data.results });
    }

    getCategory = async (slug) => {
        let category = await ApiExecute(`category/info/${slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.categoryList();
        this.gametypeList(category.data.id);
        this.allcategoryList(category.data.id);
    }

    allcategoryList = async (category_id = null, game_type_id = null) => {
        let id = category_id ? category_id : this.state.category.id;
        let game_id = game_type_id ? game_type_id : this.state.gametype_id;

        let api_response = await ApiExecute(`category?category_id=${id}&&game_type_id=${game_id}`, { method: 'GET' });
        this.setState({ subcategories: api_response.data.results });
    }

    gametypeList = async (id) => {
        let api_response = await ApiExecute(`gametype?category_id=${id}`, { method: 'GET' });
        this.setState({ gametypes: api_response.data.results });
        let lists = api_response.data.results;
        let gametype;
        for (let i in lists) {
            gametype = lists[0];
        }
        // lists.forEach(element => {
        //     gametype = element;
        // });

        this.allcategoryList(id, gametype.id);
        this.gameList(gametype.id);
    }

    gameList = async (gtype_id = null) => {
        let gametype = gtype_id ? gtype_id : this.state.gametype_id;
        let category = this.state.category_id;
        console.log('catid', category, 'gtypeid', gametype);

        let api_response = await ApiExecute(`game?category_id=${category}&&game_type_id=${gametype}`, { method: 'GET' });
        console.log('sdkjf iwjfi jsfkeed', api_response.data.results);

        this.setState({ games: api_response.data.results })
    }

    render() {
        var settings = {
            speed: 3000,
            autoplay: false,
            autoplaySpeed: 0,
            centerMode: true,
            // cssEase: 'linear',
            slidesToShow: 5,
            slidesToScroll: 1,
            // vertical: true,
            // variableWidth: true,
            infinite: true,
            initialSlide: 1,
            arrows: true,
            buttons: true
        };

        var cate = {
            autoplay: false,
            autoplaySpeed: 0,
            // cssEase: 'linear',
            slidesToShow: 8,
            // slidesToScroll: 1,
            // variableWidth: true,
            infinite: false,
            initialSlide: 0,
            arrows: true,
            buttons: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 8,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 8,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        };

        var banner = {
            loop: false,
            accessibility: false,
            draggable: false,
            showsButtons: true,
            arrows: true,
            buttons: true,
        }
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />
                    {
                        this.state.refreshComponet
                            ?
                            <FrontSidebar {...this.props} gametype_id={this.state.gametype_id} />
                            :
                            <FrontSidebar {...this.props} gametype_id={this.state.gametype_id} />
                    }

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="center-main-content">
                            <div class="news-bar d-none-desktop">
                                <div class="news-title"><i class="fas fa-bell mr-2"></i> News</div>
                                <marquee>We are happy to announce that we are bringing you a new world of gambling.</marquee>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel3" data-ride="carousel" data-interval="3000" class="carousel vert slide">

                                    </div>
                                </div>
                            </div>
                            <div class="report-container">
                                <div class="report-box casino-box">
                                    <div class="casino-box-tabs">
                                        <div class="casino-tabs-menu" style={{ width: 'calc(100% - 0px)' }}>
                                            <Slider {...cate} >
                                                {
                                                    this.state.gametypes
                                                        .sort((a, b) => a.id > b.id ? 1 : -1)
                                                        .map((c, index) => {
                                                            if (index == 0) {
                                                                return (
                                                                    <div class="nav nav-pills">
                                                                        <div class="nav-item">
                                                                            <a href="javascript:void(0)" onClick={() => this.onClickGametype(c.id)} class="nav-link active"><img src="https://sitethemedata.com/v3/static/front/img/icons/4.png" /> <span>{c.name}</span></a>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            } else {
                                                                return (
                                                                    <div class="nav nav-pills">
                                                                        <div class="nav-item">
                                                                            <a href="javascript:void(0)" onClick={() => this.onClickGametype(c.id)} class="nav-link"><img src="https://sitethemedata.com/v3/static/front/img/icons/4.png" /> <span>{c.name}</span></a>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            }
                                                        })
                                                }
                                            </Slider>
                                            {/* <Nav fill variant="tabs" class="nav nav-pills" defaultActiveKey="link_0" style={{ margin: '0 50px' }}>
                                                {
                                                    this.state.gametypes
                                                        .sort((a, b) => a.id > b.id ? 1 : -1)
                                                        .map((c, index) => {
                                                            return (
                                                                <Nav.Item>
                                                                    <Nav.Link href="javascript:void(0)" onClick={() => this.onClickGametype(c.id)} eventKey={`link_${index}`}><img src="https://sitethemedata.com/v3/static/front/img/icons/4.png" /> <span>{c.name}</span></Nav.Link>
                                                                </Nav.Item>
                                                            )
                                                        })
                                                }
                                            </Nav> */}
                                        </div>

                                    </div>
                                    <div class="casino-tabs d-none">
                                        <div class="casino-tabs-menu">
                                            <a href="javascript:void(0)" class="arrow-tabs arrow-left"><i class="fas fa-chevron-left"></i></a>
                                            <ul id="tabMobile2" class="nav nav-tabs">
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link active">
                                                    All Casino
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Teenpatti
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Poker
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Baccarat
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Dragon Tiger
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    32 Cards
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Andar Bahar
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Lucky 7
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    3 Card Judgement
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Casino War
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Worli
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Bollywood
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Lottery
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Queen
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Race
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Sports
                                                </a>
                                                </li>
                                                <li class="nav-item"><a href="javascript:void(0)" class="nav-link">
                                                    Others
                                                </a>
                                                </li>
                                            </ul>
                                            <a href="javascript:void(0)" class="arrow-tabs arrow-right"><i class="fas fa-chevron-right"></i></a>
                                        </div>
                                        <div class="casino-search close-search">
                                            <input type="text" placeholder="Search for Casino" class="form-control" style={{ textTransform: 'lowercase' }} />
                                            <div class="search-icon"><i class="fas fa-search"></i></div>
                                        </div>
                                    </div>
                                    <div id="own-casino" class="casino-banners live-casino-banners">
                                        {
                                            this.state.games
                                                .map((g, index) => {
                                                    return (
                                                        <div class="casino-banner-item">
                                                            <Link to={`/casino/${g.slug}`} class="">
                                                                <div style={{ backgroundSize: 'cover', backgroundPosition: 'center center', paddingTop: '53.33%', backgroundImage: "url(https://sitethemedata.com/casino_icons/lc/teen120.jpg), url(https://sitethemedata.com/v3/static/front/img/casino/default-2.jpeg)" }}></div>
                                                            </Link>
                                                            <div class="text-center">{g.name}</div>
                                                        </div>
                                                    )
                                                })
                                        }
                                    </div>

                                </div>
                                <FrontFooter {...this.props} />
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        );
    }
}
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';
import Moment from 'react-moment';

export default class CurrentBets extends Component {
    state = {
        bets: {},
        type: 'all',
    }

    onChangetype = e => {
        console.log('fjkgdfjg', e.target.value);
        this.setState({ type: e.target.value });

        setTimeout(() => {
            this.betList();
        }, 500);
    }
    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        this.betList();

        $('header.header').addClass('header-casino');
    }

    betList = async () => {
        let user = sessionStorage.getItem('@front_user');
        user = JSON.parse(user);
        console.log('user', user);
        let type = '';
        if (this.state.type == 'all') {
            type = '';
        } else {
            type = this.state.type;
        }
        let api_response = await ApiExecute(`userbats/${user.id}?type=${type}`, { method: 'GET' });
        console.log('lists', api_response.data);

        this.setState({ bets: api_response.data });
    }

    render() {
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="center-main-content report-container">
                            <div class="news-bar d-none-desktop">
                                <marquee>Launched Best Ever 8 Deck Casino 1 Card ONE-DAY.</marquee>
                                <div class="news-title"><img src="https://sitethemedata.com/v3/static/front/img/icons/speaker.svg" /></div>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel2" data-ride="carousel" data-interval="3000" class="carousel vert slide"></div>
                                </div>
                            </div>
                            <div class="report-box">
                                <div class="report-title">
                                    <div class="report-name">Current Bets</div>
                                    <div class="report-search search-box">
                                        <div class="form-group mb-0"><input type="text" placeholder="Search" class="form-control" /> <img src="https://sitethemedata.com/v6/static/front/img/search.svg" class="search-icon" /></div>
                                    </div>
                                </div>
                                <div class="casino-report-tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item"><a data-toggle="tab" href="javascript:void(0)" class="nav-link active">Sports</a></li>
                                        {/* <li class="nav-item"><a data-toggle="tab" href="javascript:void(0)" class="nav-link">Casino</a></li> */}
                                    </ul>
                                </div>
                                {/* <div class="report-form">
                                    <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="customRadio" name="example" value="matchbet" class="custom-control-input" /> <label for="customRadio" class="custom-control-label">Matched</label></div>
                                    <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="customRadio2" name="example" value="deletebet" class="custom-control-input" /> <label for="customRadio2" class="custom-control-label">Deleted</label></div>
                                </div> */}
                                <div class="report-page-count">
                                    <div class="form-group mb-0">
                                        <label>Show</label>
                                        <select class="form-control">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="200">200</option>
                                            <option value="300">300</option>
                                            <option value="400">400</option>
                                            <option value="500">500</option>
                                        </select>
                                        <label>Entries</label>
                                    </div>
                                    <div class="bet-types-container">
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="soda-all" name="betType" value="all" checked={this.state.type == 'all' ? 'checked' : ''} onChange={this.onChangetype} class="custom-control-input" /> <label for="soda-all" class="custom-control-label">All</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="soda-back" name="betType" value="back" checked={this.state.type == 'back' ? 'checked' : ''} onChange={this.onChangetype} class="custom-control-input" /> <label for="soda-back" class="custom-control-label">Back</label></div>
                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="soda-lay" name="betType" value="lay" checked={this.state.type == 'lay' ? 'checked' : ''} onChange={this.onChangetype} class="custom-control-input" /> <label for="soda-lay" class="custom-control-label">Lay</label></div>
                                    </div>
                                    <div class="custom-control-inline">
                                        <div>Total Bets: <span class="mr-2">{this.state.bets ? this.state.bets.length : 0}</span> Total Amount: <span>0</span></div>
                                    </div>
                                    <div class="file-icons">
                                        <div><i class="fas fa-file-pdf disabled"></i></div>
                                        <div id="export_1633688129510"><i class="fas fa-file-excel disabled"></i></div>
                                    </div>
                                </div>
                                <div class="report-table current-bets table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="bet-sport">Sports</th>
                                                <th class="bet-event-name">Event Name</th>
                                                <th class="bet-market-name">Remark</th>
                                                <th class="bet-nation">Market Name</th>
                                                <th class=" bet-user-rate">User Rate</th>
                                                <th class=" bet-amount">Amount</th>
                                                <th class="bet-date">Place Date</th>
                                                {/* <th class=" bet-user-rate">Action</th> */}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.bets && this.state.bets.length ?
                                                    <>
                                                        {
                                                            this.state.bets.map((element, index) => {
                                                                return (
                                                                    <tr>
                                                                        <td class="">{element.event_type}</td>
                                                                        <td class="bet-event-name">{element.event}</td>
                                                                        <td class="bet-market-name">{element.event_name}</td>
                                                                        <td class="bet-nation">{element.type}</td>
                                                                        <td class=" bet-user-rate">{element.rate}</td>
                                                                        <td class=" bet-amount">{element.amount}</td>
                                                                        <td class="bet-date">
                                                                            <Moment format="DD MMM YYYY hh:mm A">
                                                                                {element.created_at}
                                                                            </Moment>
                                                                        </td>
                                                                        {/* <td class=" bet-user-rate">Action</td> */}
                                                                    </tr>
                                                                );
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <tr class="no-record">
                                                        <td colspan="8">No records found</td>
                                                    </tr>
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <FrontFooter {...this.props} />

                    </div>
                </div>

            </div>

        );
    }
}
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import $ from "jquery";
import Popup from 'reactjs-popup';
import Slider from "react-slick";
import FrontFooter from '../../Components/FrontFooter';
import Front1Footer from '../../Components/Front1Footer';


export default class HomeScreen extends Component {
    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        live_casinos: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/ezugi.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/superspade.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/qt.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/evolution.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/cockfight.jpg'
            }
        ],
        fantasy_games: [
            {
                image: 'https://sitethemedata.com/casino_icons/other/diam11.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/playerbattle.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/poptheball.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/ludoclub.jpg'
            },
        ],
        other_games: [
            {
                image: 'https://sitethemedata.com/casino_icons/other/binary.jpg'
            }
        ],
        sports: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/4-color.svg',
                name: 'Cricket'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/2-color.svg',
                name: 'Tennis'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/1-color.svg',
                name: 'Football'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/8-color.svg',
                name: 'Table Tennis'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/19-color.svg',
                name: 'Ice Hockey'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/55-color.svg',
                name: 'E Games'
            }, {
                image: 'https://sitethemedata.com/v3/static/front/img/events/11-color.svg',
                name: 'Rugby League'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/3-color.svg',
                name: 'Boxing'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/7-color.svg',
                name: 'Beach Volleyball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/9-color.svg',
                name: 'Mixed Martial Arts'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/10-color.svg',
                name: 'Futsal'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/12-color.svg',
                name: 'Horse Racing'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/15-color.svg',
                name: 'Greyhounds'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/16-color.svg',
                name: 'Basketball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/17-color.svg',
                name: 'MotoGP'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/18-color.svg',
                name: 'Chess'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/22-color.svg',
                name: 'Volleyball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/29-color.svg',
                name: 'Badminton'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/25-color.svg',
                name: 'Hockey'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/32-color.svg',
                name: 'Cycling'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/35-color.svg',
                name: 'Motorbikes'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/37-color.svg',
                name: 'Athletics'
            },

        ],
        top_winners: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        banners: [
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16309369650142864.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16308312682979087.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16305698917289915.jpeg'
            }
        ],
        topHeaderRemove: '',
        username: '',
        password: '',
        privancy: false,
        categories: [],
        casino_games: [],
    }

    onChangeUsername = e => {
        this.setState({ username: e.target.value });
    }
    onChangePassword = e => {
        this.setState({ password: e.target.value });
    }
    onChangePrivancy = e => {
        this.setState({ privancy: e.target.checked });
    }
    loginSubmit = async () => {
        let {
            username,
            password,
            privancy
        } = this.state;

        if (!username || username.trim() === '') {
            ToastMessage('error', 'Please enter username !');
        } else if (!password || password.trim() === '') {
            ToastMessage('error', 'Please enter password !');
        } else if (!privancy) {
            ToastMessage('error', 'Please agree Term & Condition !');
        } else {
            let data = {
                user_name: username,
                password: password,
            }
            let login_response = await ApiExecute("user/login", { method: 'POST', data: data });

            console.log('login_response', login_response);
            if (login_response.data.status) {
                sessionStorage.setItem('@front_user', JSON.stringify(login_response.data.data));
                sessionStorage.setItem('@front_token', login_response.data.token);

                // this.props.history.push('sport');
                window.location = 'sport';

                ToastMessage('success', login_response.data.msg);
            }
            else {
                ToastMessage('error', login_response.data.msg);
            }
        }
    }
    loginPopup() {
        console.log('asjnfdsfjk');
        $('.modal-login-new').toggleClass('show');
        $('.modal-login-new').toggleClass('d-none d-block');
        // $('.modal-login-new').attr('style', 'dispaly:none');
    }
    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ categories: api_response.data.results })

        let casino_response = await ApiExecute(`game?category_id=8`, { method: 'GET' });
        console.log('lists', casino_response.data.results);
        this.setState({ casino_games: casino_response.data.results })
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        if (user_token) {
            this.props.history.push('sport');
        }
        this.categoryList();

        // $(document).on('click', '.modal.modal-login-new', e => {
        //     $('.modal-login-new').toggleClass('show');
        //     $('.modal-login-new').toggleClass('d-none d-block');
        // })
    }

    closetopheaderr() {
        this.setState({ topHeaderRemove: 'none' });
    }
    render() {
        let { Router } = this.props;
        var settings = {
            speed: 5000,
            autoplay: true,
            autoplaySpeed: 0,
            centerMode: true,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            infinite: true,
            initialSlide: 1,
            arrows: false,
            buttons: false
        };

        var banner = {
            loop: false,
            accessibility: false,
            draggable: true,
            showsButtons: false,
            arrows: false,
            buttons: false,
        }
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div className="wrapper home-new">
                    <div className="animated-header" style={{ display: this.state.topHeaderRemove ? 'none' : 'block' }}>
                        <section id="animationLogin" className="header-animated-banner">
                            <div>
                                <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-bg.png" alt="" className="bg" />
                                <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-10.png" alt="" />
                                <div className="item">
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-1.png" alt="" />
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-2.png" alt="" />
                                </div>
                                <div className="item">
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-3.png" alt="" />
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-4.png" alt="" />
                                </div>
                                <div className="text">
                                    <span>
                                        <ReactTypingEffect
                                            cursor=" "
                                            text={["Launched MUFLIS TEENPAT", "Enjoy our New Casino", "Launched RACE to 17"]}
                                        />
                                    </span>
                                </div>
                                <div className="item">
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-5.png" alt="" />
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-6.png" alt="" />
                                </div>
                                <div className="item">
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-7.png" alt="" />
                                    <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-8.png" alt="" />
                                </div>
                                <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-10.png" alt="" />
                            </div>
                        </section>
                        <span className="fas fa-times" onClick={this.closetopheaderr.bind(this)}></span>
                    </div>
                    <div className="home-new-header container-fluid container-fluid-5">
                        <div className="row row5">
                            <div className="col-5 col-md-2">
                                <div className="home-new-logo">
                                    <img src="https://sitethemedata.com/sitethemes/world777.com/admin/logo.png" />
                                </div>
                            </div>
                            <div className="col-7 col-md-10">
                                <div className="home-new-header-bottom">
                                    <nav className="navbar navbar-expand-sm justify-content-center"></nav>
                                    <div>
                                        <div className="btn btn-primary ml-2" onClick={() => this.loginPopup()}>Login</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="upcoming-fixure">
                        <div className="fixure-title">Upcoming Fixtures</div>
                        <div className="fixure-box-container" style={{ width: 'calc(100% - 95px)' }}>
                            <marquee scrollamount="4" loop="infinite">

                                <div data-v-307f1144="" className="marquee">
                                    <div data-v-307f1144="" className="marquee-inner">
                                        <div data-v-307f1144="" className="marquee-content" style={{ transform: 'translateX(-1621px)' }}>
                                            <div data-v-307f1144="" className="login-fixture">
                                                {
                                                    this.state.upcoming_fixtures.map((fixture, index) => {
                                                        return (
                                                            <div data-v-307f1144="" className="fixure-box">
                                                                <div data-v-307f1144=""><i data-v-307f1144="" className="d-icon mr-2 icon-2"></i>
                                                                    {fixture.name}</div>
                                                                <div data-v-307f1144="">{fixture.date}</div>
                                                            </div>
                                                        );
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </marquee>
                        </div>
                    </div>

                    <div role="region" aria-busy="false" className="carousel slide" id="__BVID__14">
                        <Slider {...banner} >
                            {
                                this.state.banners.map((banner, index) => {
                                    return (
                                        <div className="carousel-item">
                                            <div>
                                                <img src={banner.image} className="img-logo" />
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </Slider>
                    </div>
                    <div className="w-100">
                        <h4 className="sport-list-title">Our Live Casinos</h4>
                        <div className="casino-banners-list mt-2">
                            {
                                this.state.casino_games.map((live_casino, index) => {
                                    return (
                                        <div className="casino-banner-item login-hover" onClick={() => this.loginPopup()}>
                                            <a href="javascript:void(0);">
                                                <img src={live_casino.image} className="img-fluid" />
                                                <div role="button" tabindex="0">Login</div>
                                            </a>
                                        </div>
                                    );
                                })
                            }
                        </div>
                        <div className="container-fluid container-fluid-5">
                            <div className="row row5">
                                <div className="col-12 col-md-4">
                                    <h4 className="sport-list-title">Live Casinos</h4>
                                    <div className="casino-banners-list live-casinos mt-2">
                                        {
                                            this.state.live_casinos.map((live_casino, index) => {
                                                return (
                                                    <div className="casino-banner-item login-hover" onClick={() => this.loginPopup()}>
                                                        <a href="javascript:void(0);">
                                                            <img src={live_casino.image} className="img-fluid" />
                                                            <div role="button" tabindex="0">Login</div>
                                                        </a>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <h4 className="sport-list-title">Fantasy Games</h4>
                                    <div className="casino-banners-list fantasy-games mt-2">
                                        {
                                            this.state.fantasy_games.map((fantasy_game, index) => {
                                                return (
                                                    <div className="casino-banner-item login-hover" onClick={() => this.loginPopup()}>
                                                        <a href="javascript:void(0);">
                                                            <img src={fantasy_game.image} className="img-fluid" />
                                                            <div role="button" tabindex="0">Login</div>
                                                        </a>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <h4 className="sport-list-title">Others</h4>
                                    <div className="casino-banners-list fantasy-games mt-2">
                                        {
                                            this.state.other_games.map((other_game, index) => {
                                                return (
                                                    <div className="casino-banner-item login-hover" onClick={() => this.loginPopup()}>
                                                        <a href="javascript:void(0);">
                                                            <img src={other_game.image} className="img-fluid" />
                                                            <div role="button" tabindex="0">Login</div>
                                                        </a>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 className="sport-list-title">Sports</h4>
                        <div className="all-sports-list mt-2">
                            {
                                this.state.sports.map((sport, index) => {
                                    return (
                                        <div className="sport-list-item" onClick={() => this.loginPopup()}>
                                            <img src={sport.image} role="button" tabindex="0" />
                                            <div className="mt-1 text-center">{sport.name}</div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                        <h4 className="sport-list-title">Top Winners</h4>
                        <div className="top-winners-list-container mt-2" style={{ height: '230px' }}>
                            <div className="top-winner-list-box-container1">
                                <Slider {...settings} >
                                    {
                                        this.state.top_winners.map((winner, index) => {
                                            return (
                                                <div data-v-e4caeaf8="" tabindex="-1" data-index="18" aria-hidden="true" className="slick-slide slick-cloned">
                                                    <div data-v-e4caeaf8="">
                                                        <div data-v-e4caeaf8="" tabindex="-1" className="top-winner-list-box" style={{ width: '100%', display: 'inline-block' }}>
                                                            <div data-v-e4caeaf8="" className="w-100 text-center">
                                                                <img data-v-e4caeaf8="" src={winner.image} />
                                                            </div>
                                                            <div data-v-e4caeaf8="" className="w-100">
                                                                <div data-v-e4caeaf8="" className="player-detail">
                                                                    <div data-v-e4caeaf8=""><b data-v-e4caeaf8="">Player</b>
                                                                    </div>
                                                                    <div data-v-e4caeaf8="">{winner.player}</div>
                                                                </div>
                                                                <div data-v-e4caeaf8="" className="player-detail">
                                                                    <div data-v-e4caeaf8=""><b data-v-e4caeaf8="">Time</b>
                                                                    </div>
                                                                    <div data-v-e4caeaf8="">{winner.time}</div>
                                                                </div>
                                                                <div data-v-e4caeaf8="" className="player-detail">
                                                                    <div data-v-e4caeaf8=""><b data-v-e4caeaf8="">Win Amount</b>
                                                                    </div>
                                                                    <div data-v-e4caeaf8="">{winner.amount}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            );
                                        })
                                    }
                                </Slider>
                            </div>
                        </div>
                    </div>

                    <Front1Footer {...this.props} />

                    <div id="login___BV_modal_outer_" >
                        <div id="login" role="dialog" aria-describedby="login___BV_modal_body_" class="modal fade modal-login-new" aria-modal="true">
                            <div class="modal-dialog modal-md"><span tabindex="0"></span>
                                <div id="login___BV_modal_content_" tabindex="-1" class="modal-content">
                                    <header id="login___BV_modal_header_" class="modal-header">
                                        <div aria-label="Close" class="close-login-modal" onClick={() => this.loginPopup()}>
                                            <img src="https://sitethemedata.com/v3/static/front/img/close.svg" />
                                        </div>
                                    </header>
                                    <div id="login___BV_modal_body_" class="modal-body">
                                        <h5>Login</h5>
                                        <div class="form-group">
                                            <label class="user-email-text">Username</label>
                                            <input type="text" value={this.state.username} onInput={this.onChangeUsername} placeholder="Enter Username" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label class="user-email-text">Password</label>
                                            <input type="password" value={this.state.password} onInput={this.onChangePassword} placeholder="Enter Password" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox d-inline-block">
                                                <input type="checkbox" value={this.state.privancy} onInput={this.onChangePrivancy} id="customCheck" name="example1" class="custom-control-input" />
                                                <label for="customCheck" class="custom-control-label">I am at least <a href="javascript:void(0)" class="text-danger" role="button">18 years</a>
                                                    of age and I have read, accept and agree to the <a href="/game-rules" class="" target="_blank">Game Rules</a>,
                                                    <a href="https://www.gamcare.org.uk/" target="_blank">GamCare</a>, <a href="https://www.gamblingtherapy.org/en" target="_blank">Gambling Therapy</a>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" onClick={this.loginSubmit} class="btn btn-primary btn-block">Login</button>
                                        </div>
                                    </div>
                                </div><span tabindex="0"></span>
                            </div>
                        </div>
                    </div></div>
            </div >

        );
    }
}
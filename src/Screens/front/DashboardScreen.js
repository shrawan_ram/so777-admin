import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ApiExecute1, ToastMessage } from '../../Api';
import { ThemeColor } from '../../assets/js/customreact';
import Moment from 'react-moment';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Carousel } from 'react-responsive-carousel';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';

export default class HomeScreen extends Component {
    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        live_casinos: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/ezugi.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/superspade.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/qt.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/evolution.jpg'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/cockfight.jpg'
            }
        ],
        fantasy_games: [
            {
                image: 'https://sitethemedata.com/casino_icons/other/diam11.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/playerbattle.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/poptheball.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/other/ludoclub.jpg'
            },
        ],
        other_games: [
            {
                image: 'https://sitethemedata.com/casino_icons/other/binary.jpg'
            }
        ],
        sports: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/4-color.svg',
                name: 'Cricket'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/2-color.svg',
                name: 'Tennis'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/1-color.svg',
                name: 'Football'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/8-color.svg',
                name: 'Table Tennis'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/19-color.svg',
                name: 'Ice Hockey'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/55-color.svg',
                name: 'E Games'
            }, {
                image: 'https://sitethemedata.com/v3/static/front/img/events/11-color.svg',
                name: 'Rugby League'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/3-color.svg',
                name: 'Boxing'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/7-color.svg',
                name: 'Beach Volleyball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/9-color.svg',
                name: 'Mixed Martial Arts'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/10-color.svg',
                name: 'Futsal'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/12-color.svg',
                name: 'Horse Racing'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/15-color.svg',
                name: 'Greyhounds'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/16-color.svg',
                name: 'Basketball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/17-color.svg',
                name: 'MotoGP'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/18-color.svg',
                name: 'Chess'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/22-color.svg',
                name: 'Volleyball'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/29-color.svg',
                name: 'Badminton'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/25-color.svg',
                name: 'Hockey'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/32-color.svg',
                name: 'Cycling'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/35-color.svg',
                name: 'Motorbikes'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/events/37-color.svg',
                name: 'Athletics'
            },

        ],
        top_winners: [
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
            {
                image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
                player: 'ga****',
                time: '11/07/2021 23:48',
                amount: '46,34,000'
            },
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        banners: [
            {
                image: 'https://sitethemedata.com/sitethemes/world777.com/front/banners/16345847173510984.jpg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/world777.com/front/banners/16345847173510984.jpg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/world777.com/front/banners/16345847173510984.jpg'
            }
        ],
        topHeaderRemove: '',
        user: {},
        subcategory: {},
        category_slug: '',
        subcategories: [],
        matches: [],
        tab_theme: '1',
        tabId: '4',
        matchType: 'cricket',
        loading: true,
    }

    userdropdown(e) {
        if ($(e.target).is('.user-dropdown')) {
            $('.user-dropdown.collapse').toggleClass('show');
        }
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        console.log('front_token', user);
        this.setState({
            user: JSON.parse(user)
        });

        let category_slug = this.props.slug;
        if (category_slug) {
            this.state.category_slug = category_slug;
            this.getCategory(category_slug);
        }

        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });
        $(document).on('click', '.accordion-box .has-arrow', e => {
            $('.user-dropdown.collapse').toggleClass('show');
        })
        $('button.slick-arrow').html('');
        $('.sport-tabs .slick-arrow').html('');

        $(document).on('click', '.sport-tabs .nav-tabs .nav-item', e => {
            $(e.target).closest(".slick-slide").children('div').children('.nav-item').children('.nav-link').addClass('active');
            let slug = $(e.target).closest(".slick-slide").children('div').children('.nav-item').data('id');
            let index = $(e.target).closest(".slick-slide").children('div').children('.nav-item').data('theme');
            // console.log('sdfjk fdjg', index);

            this.setState({ tab_theme: index });
            // this.getCategoryDetail(slug);

            this.setState({ loading: true });
            this.setState({ tabId: slug });
            if (slug == '4') {
                this.setState({ matchType: 'cricket' });
            } else if (slug == '2') {
                this.setState({ matchType: 'tannis' });
            } else {
                this.setState({ matchType: 'soccer' });
            }


            this.getMatchlist(slug);


            $(e.target).closest(".slick-slide").siblings().children('div').children('.nav-item').children('.nav-link').removeClass('active');
        });

    }
    theme(color) {
        ThemeColor(color);
    }

    getgames = async () => {
        let category = await ApiExecute1(`category/info`, { method: 'GET' });
        console.log('sdjkfhndsjknlsdjfio oisdjsdf', category);
    }

    getCategory = async (slug) => {
        let category = await ApiExecute(`category/info/${slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.allcategoryList(category.data.id);
    }
    getCategoryDetail = async (slug) => {
        let category = await ApiExecute(`category/info/${slug}`, { method: 'GET' });
        this.setState({ subcategory: category.data });

        this.getMatchlist(category.data.id);
    }
    getMatchlist = async (id) => {
        this.setState({ loading: true });
        // let api_response = await ApiExecute1(`oddslist?sport_id=${id}`, { method: 'GET' });
        let api_response;
        if (id == 4) {
            api_response = await ApiExecute1(`getcricketmatches`, { method: 'GET' });
        } else if (id == 2) {
            api_response = await ApiExecute1(`gettennismatches`, { method: 'GET' });
        } else {
            api_response = await ApiExecute1(`getsoccermatches`, { method: 'GET' });
        }
        console.log('matches', api_response.data);
        this.setState({ matches: api_response.data });
        this.setState({ loading: false });

        // api_response.data.data.forEach(element => {
        // element.push = this.checkGame(element.eventId);
        // console.log('asdkjsad', element.other);
        // setInterval(this.checkGame(element.eventId), 1000);
        // });
    }

    checkGame = async (id) => {

        this.setState({ loading: true });
        let api_response;
        if (id == 4) {
            api_response = await ApiExecute1(`getcricketmatches`, { method: 'GET' });
        } else if (id == 2) {
            api_response = await ApiExecute1(`gettennismatches`, { method: 'GET' });
        } else {
            api_response = await ApiExecute1(`getsoccermatches`, { method: 'GET' });
        }
        console.log('check', api_response.data);
        api_response.data.forEach(element => {

            if (element.gameId == id) {
                console.log('status chesdfghfhe rrck fyhtg', element);
                // return element;
            }
        });

        this.setState({ loading: false });
    }

    allcategoryList = async (category_id = null) => {
        let id = category_id ? category_id : this.state.category.id;

        let api_response = await ApiExecute(`category?category_id=${id}`, { method: 'GET' });
        this.setState({ subcategories: api_response.data.results });
        console.log('subcategories', api_response.data.results);
        let lists = api_response.data.results;
        for (let i in lists) {
            let li = lists[0];
            // this.getCategoryDetail(li.slug);
            this.getMatchlist(4);
        }
    }

    closetopheaderr() {

        this.setState({ topHeaderRemove: 'none' });
        $('body').removeClass('animate-on');
    }
    render() {
        let linkSlug;
        if (this.state.tabId == 4) {
            linkSlug = 'cricket';
        } else if (this.state.tabId == 2) {
            linkSlug = 'tannis';
        } else if (this.state.tabId == 1) {
            linkSlug = 'soccer';
        }

        var settings = {
            speed: 3000,
            autoplay: true,
            autoplaySpeed: 0,
            centerMode: true,
            // cssEase: 'linear',
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical: true,
            // variableWidth: true,
            infinite: true,
            initialSlide: 1,
            arrows: true,
            buttons: true
        };

        var cate = {
            autoplay: false,
            autoplaySpeed: 0,
            // cssEase: 'linear',
            slidesToShow: 3,
            slidesToScroll: 1,
            // variableWidth: true,
            infinite: false,
            initialSlide: 1,
            arrows: true,
            buttons: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        };

        var banner = {
            loop: false,
            accessibility: false,
            draggable: false,
            showsButtons: true,
            arrows: true,
            buttons: true,
        }
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">
                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />
                    <FrontSidebar {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="center-main-content">
                            <div class="news-bar d-none-desktop">
                                <div class="news-title"><i class="fas fa-bell mr-2"></i> News</div>
                                <marquee>We are happy to announce that we are bringing you a new world of gambling.</marquee>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <Carousel showArrows={false} autoPlay={true} showThumbs={false} infiniteLoop={true} axis='vertical' showIndicators={false} showStatus={false}>
                                        {
                                            this.state.upcoming_fixtures.map((upcoming_fixture, index) => {
                                                return (
                                                    <div>
                                                        <p class="fixure-box" style={{ paddingTop: '0px', textAlign: 'left' }}>
                                                            <a href="#" class="">
                                                                <div>
                                                                    <i class="d-icon mr-2 icon-2"></i>{upcoming_fixture.name}
                                                                </div>
                                                                <div>{upcoming_fixture.date}</div>
                                                            </a>
                                                        </p>
                                                    </div>
                                                );
                                            })
                                        }
                                    </Carousel>
                                </div>
                            </div>
                            <div class="center-container">
                                <div class="home-container">
                                    <div class="new-event">
                                        <div class="container-fluid container-fluid-5">
                                            <div class="row row5"></div>
                                        </div>
                                    </div>
                                    <div class="banner">
                                        <Carousel showArrows={false} autoPlay={true} showThumbs={false} infiniteLoop={true} showIndicators={true} showStatus={false}>
                                            {
                                                this.state.banners.map((banner, index) => {
                                                    return (
                                                        <div>
                                                            <img src={banner.image} />
                                                        </div>
                                                    );
                                                })
                                            }
                                        </Carousel>
                                    </div >
                                    <div class="sport-tabs">
                                        {/* <a href="javascript:void(0)" class="arrow-tabs arrow-left">
                                            <img src="https://sitethemedata.com/v3/static/front/img/arrow-down.svg" />
                                        </a> */}
                                        <ul class="nav nav-tabs">
                                            <Slider {...cate} >
                                                {
                                                    this.state.subcategories
                                                        .sort((a, b) => a.id > b.id ? 1 : -1)
                                                        .map((c, index) => {
                                                            if (index == 0) {
                                                                return (
                                                                    <li class="nav-item" data-id='4' data-theme={index + 1} ><a href="javascript:void(0)" id="" class={`nav-link sport${index + 1} active`}><i class={`d-icon icon-${index + 1}`}></i> <span>{c.name}</span></a></li>
                                                                )
                                                            } else {
                                                                if (c.slug == 'tannis') {
                                                                    return (
                                                                        <li class="nav-item" data-id='2' data-theme={index + 1} ><a href="javascript:void(0)" id="" class={`nav-link sport${index + 1}`}><i class={`d-icon icon-${index + 1}`}></i> <span>{c.name}</span></a></li>
                                                                    )
                                                                } else if (c.slug == 'soccer') {
                                                                    return (
                                                                        <li class="nav-item" data-id='1' data-theme={index + 1} ><a href="javascript:void(0)" id="" class={`nav-link sport${index + 1}`}><i class={`d-icon icon-${index + 1}`}></i> <span>{c.name}</span></a></li>
                                                                    )
                                                                }
                                                            }
                                                        })
                                                }
                                            </Slider>
                                        </ul>
                                        {/* <a href="javascript:void(0)" class="arrow-tabs arrow-right">
                                            <img src="https://sitethemedata.com/v3/static/front/img/arrow-down.svg" />
                                        </a> */}
                                    </div>
                                    <div class="bet-table">
                                        <div class={`bet-table-header sport${this.state.tab_theme}`}>
                                            <div class="game-title"><i class={`d-icon icon-${this.state.tab_theme}`}></i>  <span>{this.state.subcategory.name}</span>
                                            </div>
                                            <div class="point-title d-none-mobile">1</div>
                                            <div class="point-title d-none-mobile">X</div>
                                            <div class="point-title d-none-mobile">2</div>
                                        </div>
                                        <div class="bet-table-body">
                                            {
                                                !this.state.loading ?
                                                    this.state.matches.length != 0 ?
                                                        this.state.matches
                                                            .map((c, index) => {
                                                                if (c.vir == 1) {
                                                                    return (
                                                                        <div class="bet-table-box">
                                                                            <div class="bet-table-row-header-mobile d-none-desktop">
                                                                                <div class="game-title">
                                                                                    <div class="game-date">
                                                                                        <p class="day text-left">{c.srno}</p>
                                                                                        <p class="mb-0 day text-left">
                                                                                            <Moment format="DD MMM HH:mm">
                                                                                                {c.eventDate}
                                                                                            </Moment>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="game-name d-inline-block">
                                                                                        <Link to={`/sport/${linkSlug}/${c.gameId}`} class="" >
                                                                                            <p class="team-name text-left">{c.eventName}</p>
                                                                                            <p class="team-name text-left team-event">({c.SeriesName})</p>
                                                                                        </Link>
                                                                                        {
                                                                                            this.state.tabId == 4 ?
                                                                                                <div class="game-icons">
                                                                                                    <div class="game-icon">
                                                                                                        {/* <span class="f-bm-icon">F1</span> */}
                                                                                                    </div>
                                                                                                    <div class="game-icon">
                                                                                                        {c.f == 'True' ?
                                                                                                            <span class="f-bm-icon">F</span>
                                                                                                            : ''
                                                                                                        }
                                                                                                    </div>
                                                                                                    <div class="game-icon">
                                                                                                        {c.m1 == 'True' ?
                                                                                                            <span class="f-bm-icon">BM</span>
                                                                                                            : ''
                                                                                                        }
                                                                                                    </div>
                                                                                                    <div class="game-icon">
                                                                                                        {c.tv == 'True' ?
                                                                                                            <i class="icon-tv d-icon"></i>
                                                                                                            : ''
                                                                                                        }
                                                                                                    </div>
                                                                                                </div>
                                                                                                : ''
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="bet-table-row">
                                                                                <div class="game-title d-none-mobile">
                                                                                    {c.inPlay == 'False' ?
                                                                                        <div class="game-date">
                                                                                            <p class="day text-left">
                                                                                                <Moment format="DD MMM">
                                                                                                    {c.eventDate}
                                                                                                </Moment></p>
                                                                                            <p class="mb-0 day text-left">
                                                                                                <Moment format="HH:mm">
                                                                                                    {c.eventDate}
                                                                                                </Moment>
                                                                                            </p>
                                                                                        </div>
                                                                                        : <div class="game-date inplay"><span>Live</span></div>
                                                                                    }
                                                                                    <div class="game-name d-inline-block">
                                                                                        {
                                                                                            c.back1 == 0 && c.back11 == 0 && c.lay1 == 0 && c.lay12 == 0 ?
                                                                                                <p class="team-name text-left">{c.eventName}</p>
                                                                                                :
                                                                                                <Link to={`/sport/${linkSlug}/${c.gameId}`} class="" >
                                                                                                    <p class="team-name text-left">{c.eventName}</p>
                                                                                                    {/* <p class="team-name text-left team-event">({c.SeriesName})</p> */}
                                                                                                </Link>
                                                                                        }
                                                                                    </div>
                                                                                    {
                                                                                        this.state.tabId == 4 ?
                                                                                            <div class="game-icons">
                                                                                                <div class="game-icon">
                                                                                                    {/* <span class="f-bm-icon">F1</span> */}
                                                                                                </div>
                                                                                                <div class="game-icon">
                                                                                                    {c.f == 'True' ?
                                                                                                        <span class="f-bm-icon">F</span>
                                                                                                        : ''
                                                                                                    }
                                                                                                </div>
                                                                                                <div class="game-icon">
                                                                                                    {c.m1 == 'True' ?
                                                                                                        <span class="f-bm-icon">BM</span>
                                                                                                        : ''
                                                                                                    }
                                                                                                </div>
                                                                                                <div class="game-icon">
                                                                                                    {c.tv == 'True' ?
                                                                                                        <i class="icon-tv d-icon"></i>
                                                                                                        : ''
                                                                                                    }
                                                                                                </div>
                                                                                            </div>
                                                                                            : ''
                                                                                    }
                                                                                </div>
                                                                                {
                                                                                    c.market_runner_count > 2 ?
                                                                                        <>
                                                                                            <div class="point-title suspended">
                                                                                                <div class="text-center d-none-desktop point-title-header">1</div>
                                                                                                <div class="back bl-box no-odds"><span class="d-block odds">—</span>
                                                                                                </div>

                                                                                                <div class="lay bl-box no-odds"><span class="d-block odds">—</span>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="point-title">
                                                                                                <div class="text-center d-none-desktop point-title-header">X</div>
                                                                                                <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                                                                </div>
                                                                                                <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="point-title suspended">
                                                                                                <div class="text-center d-none-desktop point-title-header">2</div>
                                                                                                <div class="back bl-box no-odds"><span class="d-block odds">—</span>
                                                                                                </div>

                                                                                                <div class="lay bl-box no-odds"><span class="d-block odds">—</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </>
                                                                                        :
                                                                                        <>
                                                                                            <div class={`point-title ${c.back1 == 0 && c.lay1 == 0 ? 'suspended' : ''}`}>
                                                                                                <div class="text-center d-none-desktop point-title-header">1</div>
                                                                                                <div class="back bl-box no-odds" data-price={c.back1} data-size='' data-sid={c.sid} data-name='MATCH_ODDS' data-type='back' data-team={c.eventName} data-eventid={c.gameId} data-eventtype={this.state.matchType}><span class="d-block odds">{c.back1 == 0 ? '-' : c.back1}</span></div>
                                                                                                <div class="lay bl-box no-odds" data-price={c.lay1} data-size='' data-sid={c.sid} data-name='MATCH_ODDS' data-type='lay' data-team={c.eventName} data-eventid={c.gameId} data-eventtype={this.state.matchType}><span class="d-block odds">{c.lay1 == 0 ? '-' : c.lay1}</span></div>
                                                                                            </div>

                                                                                            <div class="point-title">
                                                                                                <div class="text-center d-none-desktop point-title-header">X</div>
                                                                                                {
                                                                                                    c.back12 == 0 ?
                                                                                                        <div class="no-val bl-box"><span class="d-block odds">—</span></div>
                                                                                                        :
                                                                                                        <div class="back bl-box no-odds" data-price={c.back12} data-size='' data-sid={c.sid} data-name='MATCH_ODDS' data-type='back' data-team={c.eventName} data-eventid={c.gameId} data-eventtype={this.state.matchType}><span class="d-block odds">{c.back12 == 0 ? '-' : c.back12}</span></div>
                                                                                                }{
                                                                                                    c.lay12 == 0 ?
                                                                                                        <div class="no-val bl-box"><span class="d-block odds">—</span></div>
                                                                                                        :
                                                                                                        <div class="lay bl-box no-odds" data-price={c.lay12} data-size='' data-sid={c.sid} data-name='MATCH_ODDS' data-type='lay' data-team={c.eventName} data-eventid={c.gameId} data-eventtype={this.state.matchType}><span class="d-block odds">{c.lay12 == 0 ? '-' : c.lay12}</span></div>
                                                                                                }
                                                                                            </div>

                                                                                            <div class={`point-title ${c.back11 == 0 && c.lay11 == 0 ? 'suspended' : ''}`}>
                                                                                                <div class="text-center d-none-desktop point-title-header">2</div>
                                                                                                <div class="back bl-box no-odds" data-price={c.back12} data-size='' data-sid={c.sid} data-name='MATCH_ODDS' data-type='back' data-team={c.eventName} data-eventid={c.gameId} data-eventtype={this.state.matchType}><span class="d-block odds">{c.back11 == 0 ? '-' : c.back11}</span></div>
                                                                                                <div class="lay bl-box no-odds" data-price={c.lay11} data-size='' data-sid={c.sid} data-name='MATCH_ODDS' data-type='lay' data-team={c.eventName} data-eventid={c.gameId} data-eventtype={this.state.matchType}><span class="d-block odds">{c.lay11 == 0 ? '-' : c.lay11}</span></div>
                                                                                            </div>
                                                                                        </>
                                                                                }


                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }
                                                            })
                                                        :
                                                        <div class="text-center mt-4 ">No Records Found.</div>
                                                    :
                                                    <div class="text-center mt-4">Loading....</div>
                                            }
                                            {/* <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">01 Jun</p>
                                                            <p class="mb-0 day text-left">19:00</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/npupYuU6HzWQDqyks7yaMQ==" class="">
                                                                <p class="team-name text-left">Testing A v Testing B</p>
                                                                <p class="team-name text-left team-event">(Testing Match)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">BM</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">01 Jun</p>
                                                            <p class="mb-0 day text-left">19:00</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/npupYuU6HzWQDqyks7yaMQ==" class="">
                                                                <p class="team-name text-left">Testing A v Testing B</p>
                                                                <p class="team-name text-left team-event">(Testing Match)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">BM</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title suspended">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                        <div class="back bl-box no-odds"><span class="d-block odds">—</span>
                                                        </div>


                                                        <div class="lay bl-box no-odds"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title suspended">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>
                                                        <div class="back bl-box no-odds"><span class="d-block odds">—</span>
                                                        </div>


                                                        <div class="lay bl-box no-odds"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Yesterday</p>
                                                            <p class="mb-0 day text-left">15:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/NDLE7qrL8aKJ4AnJ2CDfLw==" class="">
                                                                <p class="team-name text-left">England v India</p>
                                                                <p class="team-name text-left team-event">(Test Matches)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">BM</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Yesterday</p>
                                                            <p class="mb-0 day text-left">15:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/NDLE7qrL8aKJ4AnJ2CDfLw==" class="">
                                                                <p class="team-name text-left">England v India</p>
                                                                <p class="team-name text-left team-event">(Test Matches)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">BM</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>



                                                        <div class="back bl-box"><span class="d-block odds">1.11</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">1.12</span>
                                                        </div>

                                                    </div>
                                                    <div class="point-title">

                                                        <div class="text-center d-none-desktop point-title-header">X</div>


                                                        <div class="back bl-box"><span class="d-block odds">15</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">15.5</span>
                                                        </div>

                                                    </div>
                                                    <div class="point-title">


                                                        <div class="text-center d-none-desktop point-title-header">2</div>
                                                        <div class="back bl-box"><span class="d-block odds">26</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">27</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Today</p>
                                                            <p class="mb-0 day text-left">19:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/obY9VkNScWSezlhRL5wgcQ==" class="">
                                                                <p class="team-name text-left">Guyana Amazon Warriors v Trinbago Knight Riders</p>
                                                                <p class="team-name text-left team-event">(Caribbean Premier League)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">BM</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Today</p>
                                                            <p class="mb-0 day text-left">19:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/obY9VkNScWSezlhRL5wgcQ==" class="">
                                                                <p class="team-name text-left">Guyana Amazon Warriors v Trinbago Knight Riders</p>
                                                                <p class="team-name text-left team-event">(Caribbean Premier League)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">BM</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                        <div class="back bl-box"><span class="d-block odds">2.22</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">2.3</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>

                                                        <div class="back bl-box"><span class="d-block odds">1.77</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">1.82</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Today</p>
                                                            <p class="mb-0 day text-left">23:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/ZXDHSPgEUKLSV9ILIIXvJQ==" class="">
                                                                <p class="team-name text-left">Somerset v Lancashire</p>
                                                                <p class="team-name text-left team-event">(T20 Blast)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Today</p>
                                                            <p class="mb-0 day text-left">23:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/ZXDHSPgEUKLSV9ILIIXvJQ==" class="">
                                                                <p class="team-name text-left">Somerset v Lancashire</p>
                                                                <p class="team-name text-left team-event">(T20 Blast)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                        <div class="back bl-box"><span class="d-block odds">1.78</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">1.82</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>

                                                        <div class="back bl-box"><span class="d-block odds">2.22</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">2.3</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">04:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/3r+maA+MthyZLcPRyER5qw==" class="">
                                                                <p class="team-name text-left">Barbados Royals v St Kitts and Nevis Patriots</p>
                                                                <p class="team-name text-left team-event">(Caribbean Premier League)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">04:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/3r+maA+MthyZLcPRyER5qw==" class="">
                                                                <p class="team-name text-left">Barbados Royals v St Kitts and Nevis Patriots</p>
                                                                <p class="team-name text-left team-event">(Caribbean Premier League)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                        <div class="back bl-box"><span class="d-block odds">2.12</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">2.22</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>

                                                        <div class="back bl-box"><span class="d-block odds">1.82</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">1.9</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">16:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/MnRsRA8KedwJFZnM0J2jLA==" class="">
                                                                <p class="team-name text-left">Ireland v Zimbabwe</p>
                                                                <p class="team-name text-left team-event">(International Twenty20 Matches)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">16:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/MnRsRA8KedwJFZnM0J2jLA==" class="">
                                                                <p class="team-name text-left">Ireland v Zimbabwe</p>
                                                                <p class="team-name text-left team-event">(International Twenty20 Matches)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>

                                                        <div class="back bl-box"><span class="d-block odds">1.84</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">1.87</span>
                                                        </div>

                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>
                                                        <div class="back bl-box"><span class="d-block odds">2.16</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">2.2</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">19:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/pw%2F4sGKsalBO5ko0bSVfJA==" class="">
                                                                <p class="team-name text-left">Jamaica Tallawahs v St Lucia Kings</p>
                                                                <p class="team-name text-left team-event">(Caribbean Premier League)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">19:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/pw%2F4sGKsalBO5ko0bSVfJA==" class="">
                                                                <p class="team-name text-left">Jamaica Tallawahs v St Lucia Kings</p>
                                                                <p class="team-name text-left team-event">(Caribbean Premier League)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                        <div class="back bl-box"><span class="d-block odds">1.87</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">1.95</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>

                                                        <div class="back bl-box"><span class="d-block odds">2.06</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">2.16</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bet-table-box">
                                                <div class="bet-table-row-header-mobile d-none-desktop">
                                                    <div class="game-title">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">23:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/pFAexdwn6wrkNIiwxywwEA==" class="">
                                                                <p class="team-name text-left">Kent v Birmingham Bears</p>
                                                                <p class="team-name text-left team-event">(T20 Blast)</p>
                                                            </a>
                                                            <div class="game-icons">

                                                                <div class="game-icon"><span class="f-bm-icon">F</span>
                                                                </div>
                                                                <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                                <div class="game-icon">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-table-row">
                                                    <div class="game-title d-none-mobile">
                                                        <div class="game-date">
                                                            <p class="day text-left">Tomorrow</p>
                                                            <p class="mb-0 day text-left">23:30</p>
                                                        </div>
                                                        <div class="game-name d-inline-block">
                                                            <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/pFAexdwn6wrkNIiwxywwEA==" class="">
                                                                <p class="team-name text-left">Kent v Birmingham Bears</p>
                                                                <p class="team-name text-left team-event">(T20 Blast)</p>
                                                            </a>
                                                        </div>
                                                        <div class="game-icons">

                                                            <div class="game-icon"><span class="f-bm-icon">F1</span>
                                                            </div>
                                                            <div class="game-icon"><span class="f-bm-icon">F</span>
                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                            <div class="game-icon">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">1</div>
                                                        <div class="back bl-box"><span class="d-block odds">1.72</span>
                                                        </div>


                                                        <div class="lay bl-box"><span class="d-block odds">1.73</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">X</div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                        <div class="no-val bl-box"><span class="d-block odds">—</span>
                                                        </div>
                                                    </div>

                                                    <div class="point-title">
                                                        <div class="text-center d-none-desktop point-title-header">2</div>

                                                        <div class="back bl-box"><span class="d-block odds">2.36</span>
                                                        </div>
                                                        <div class="lay bl-box"><span class="d-block odds">2.4</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                    <div class="home-casiono-icons d-none-desktop container-fluid">
                                        <h4>Latest Launches</h4>
                                        <div class="w-100">
                                            <div data-v-3d1a4f76="" dir="ltr" class="slick-slider slick-initialized">
                                                <div data-v-3d1a4f76="" class="slick-list">
                                                    <div data-v-e4caeaf8="" data-v-3d1a4f76="" class="slick-track" style={{ opacity: 1, transform: 'translate3d(0px, 0px, 0px)' }}>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="-2" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/lottery" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627739798.542282.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="-1" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/fantasy/ludo-lands" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629367059.451188.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="0" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/1card2020" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629301159.789691.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="1" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/kbc" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1628858391.068698.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="2" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/cmeter" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629557614.311075.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="3" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/NDLE7qrL8aKJ4AnJ2CDfLw==" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629941784.348418.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="4" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/fantasy/rummy" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1628672765.107342.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="5" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/teen2024" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627638997.35112.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="6" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/instantworli" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627739781.994477.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="7" aria-hidden="false" class="slick-slide slick-active slick-current" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/teenpattit20b" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1626620871.751465.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="8" aria-hidden="false" class="slick-slide slick-active" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/lottery" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627739798.542282.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="9" aria-hidden="true" class="slick-slide" style={{ outline: 'none', width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/fantasy/ludo-lands" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629367059.451188.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="10" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/1card2020" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629301159.789691.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="11" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/kbc" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1628858391.068698.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="12" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/cmeter" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629557614.311075.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="13" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/sport/cricket/SeK7puKGhm+IDlF%2FzygDVg==/NDLE7qrL8aKJ4AnJ2CDfLw==" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629941784.348418.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="14" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/fantasy/rummy" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1628672765.107342.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="15" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/teen2024" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627638997.35112.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="16" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/instantworli" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627739781.994477.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="17" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/teenpattit20b" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1626620871.751465.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="18" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/casino/lottery" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1627739798.542282.png)" }}></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-v-e4caeaf8="" tabindex="-1" data-index="19" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '0px' }}>
                                                            <div data-v-e4caeaf8="">
                                                                <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                    <a href="/fantasy/ludo-lands" class="" data-v-e4caeaf8="">
                                                                        <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629367059.451188.png)" }}></div>
                                                                        <div class="new-laucnh-icon">
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div >
                                <FrontFooter {...this.props} />
                            </div >
                            <div id="right-sidebar-id" class="right-sidebar home-right-sidebar sticky">
                                <div class="home-casiono-icons d-none-mobile">
                                    <Slider {...settings} >
                                        {
                                            this.state.top_winners.map((winner, index) => {
                                                return (
                                                    <div data-v-e4caeaf8="" tabindex="-1" data-index="19" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '318px' }}>
                                                        <div data-v-e4caeaf8="">
                                                            <div data-v-e4caeaf8="" tabindex="-1" class="home-casino-icon-item" style={{ width: '100%', display: 'inline-block' }}>
                                                                <a href="/fantasy/ludo-lands" class="" data-v-e4caeaf8="">
                                                                    <div class="carousal-63" style={{ backgroundImage: "url(https://sitethemedata.com/common/sliders/1629367059.451188.png)" }}></div>
                                                                    <div class="new-laucnh-icon">
                                                                        <img src="https://sitethemedata.com/v3/static/front/img/new-launch.png" />
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                        }
                                    </Slider>
                                </div>
                            </div>
                        </div >
                    </div>
                </div>
            </div >

        );
    }
}
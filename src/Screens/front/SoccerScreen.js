import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ApiExecute1, ToastMessage } from '../../Api';
import { ThemeColor } from '../../assets/js/customreact';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Carousel } from 'react-responsive-carousel';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontFooter from '../../Components/FrontFooter';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import Moment from 'react-moment';
import Front2Footer from '../../Components/Front2Footer';
// const kFormatter = async (num) => {
//     // console.log(num);
//     return num;
//     // return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(1)) + 'k' : Math.sign(num) * Math.abs(num)
// }
export default class SoccerScreen extends Component {
    state = {
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        banners: [
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16298546640188872.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/162971585470715.jpeg'
            },
            {
                image: 'https://sitethemedata.com/sitethemes/common/front/banners/16294605190635378.jpeg'
            }
        ],
        topHeaderRemove: '',
        user: {},
        subcategory: {},
        match_id: '',
        subcategories: [],
        matches: [],
        match_detail: {},
        odds_market: [],
        markets: {},
        loading: true,
        wallet: {},
        exposure: {},

        match_status: {},
        setPrice: '',
        setSize: '',
        setTeam: '',
        setType: '',
        setValue: '',
        event: '',
        eventType: 'soccer',
        type: '',
        profit: '',
        loss: '',
        ip: '',
        browser: '',
    }

    userdropdown(e) {
        if ($(e.target).is('.user-dropdown')) {
            $('.user-dropdown.collapse').toggleClass('show');
        }
    }

    async componentDidunMount() {
        clearInterval(this.odds_details);
    }

    getIp = async () => {
        let self = this;
        await $.get('https://www.cloudflare.com/cdn-cgi/trace', function (data) {
            data = data.trim().split('\n').reduce(function (obj, pair) {
                pair = pair.split('=');
                return obj[pair[0]] = pair[1], obj;
            }, {});

            var ua = data.uag;
            var b;
            var browser;
            if (ua.indexOf("Opera") != -1) {

                b = browser = "Opera";
            }
            if (ua.indexOf("Firefox") != -1 && ua.indexOf("Opera") == -1) {
                b = browser = "Firefox";
                // Opera may also contains Firefox
            }
            if (ua.indexOf("Chrome") != -1) {
                b = browser = "Chrome";
            }
            if (ua.indexOf("Safari") != -1 && ua.indexOf("Chrome") == -1) {
                b = browser = "Safari";
                // Chrome always contains Safari
            }
            if (ua.indexOf("MSIE") != -1 && (ua.indexOf("Opera") == -1 && ua.indexOf("Trident") == -1)) {
                b = "MSIE";
                browser = "Internet Explorer";
                //user agent with MSIE and Opera or MSIE and Trident may exist.
            }
            if (ua.indexOf("Trident") != -1) {
                b = "Trident";
                browser = "Internet Explorer";
            }

            self.setState({ ip: data.ip });
            self.setState({ browser: browser });
        });
    }

    async componentDidMount() {

        this.getIp();
        this.getWallet();

        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        console.log('front_token', user);
        this.setState({
            user: JSON.parse(user)
        });

        let match_id = this.props.match.params.slug;
        if (match_id) {
            // this.state.match_id = match_id;
            console.log('klfjglkdf odfj giodfgfde r ewrr', match_id);
            this.setState({
                match_id: match_id
            });
            // this.match_details(match_id);
            this.checkGame(match_id);
        }

        $(document).on('click', '.bet-table .bet-table-header', e => {
            $(e.target).closest(".bet-table").children('.bet-table-body').toggleClass('show');

            if ($(e.target).closest(".bet-table-header").attr('aria-expanded')) {
                $(e.target).closest(".bet-table-header").attr('aria-expanded', 'false')
            } else {
                $(e.target).closest(".bet-table-header").attr('aria-expanded', 'true')
            }
            $(e.target).closest(".bet-table-header").toggleClass('collapsed');
        });

        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });
        $(document).on('click', '.accordion-box .has-arrow', e => {
            $('.user-dropdown.collapse').toggleClass('show');
        })
        $('button.slick-arrow').html('');
        $('.sport-tabs .slick-arrow').html('');

        $(document).on('click', '.sport-tabs .nav-tabs .nav-item', e => {
            $(e.target).closest(".slick-slide").children('div').children('.nav-item').children('.nav-link').toggleClass('active');
            let slug = $(e.target).closest(".slick-slide").children('div').children('.nav-item').data('id');

            // this.getCategoryDetail(slug);

            $(e.target).closest(".slick-slide").siblings().children('div').children('.nav-item').children('.nav-link').removeClass('active');
        });

        $(document).on('click', 'a.close-bet', e => {
            $('.bet-slip-container').toggleClass('d-none d-block');
            this.setState({ setPrice: '' });
            this.setState({ setSize: '' });
            this.setState({ setTeam: '' });
            this.setState({ setType: '' });
            this.setState({ setName: '' });
            this.setState({ setValue: '' });
        });


        $(document).on('click', '.bet-table-row .bl-box', e => {
            let price = $(e.target).closest(".bl-box").data('price');
            let size = $(e.target).closest(".bl-box").data('size');
            let team = $(e.target).closest(".bl-box").data('team');
            let type = $(e.target).closest(".bl-box").data('type');
            let name = $(e.target).closest(".bl-box").data('name');
            this.setState({ setPrice: price });
            this.setState({ setSize: size });
            this.setState({ setTeam: team });
            this.setState({ setType: type });
            this.setState({ setName: name });
            this.setState({ setValue: null });

            $('.bet-slip-container').removeClass('d-none');
            $('.bet-slip-container').addClass('d-block');
        });


        $(document).on('click', '.bet-buttons button', e => {
            console.log(this.state.setValue);
            let value = $(e.target).closest("button").data('value');
            console.log('value', value);
            this.setState({ setValue: value });
            if (this.state.setType && this.state.setType == 'back') {
                if (this.state.setName == 'MATCH_ODDS') {
                    let profit = this.state.setPrice * value - value;
                    let loss = value;
                    this.setState({ profit: profit });
                    this.setState({ loss: loss });
                } else if (this.state.setName == 'Bookmaker') {
                    let profit = (value / 100) * this.state.setPrice;
                    let loss = value;
                    this.setState({ profit: profit });
                    this.setState({ loss: loss });
                } else {
                    let profit = value;
                    let loss = value;
                    this.setState({ profit: profit });
                    this.setState({ loss: loss });
                }
            }
            if (this.state.setType && this.state.setType == 'lay') {
                if (this.state.setName == 'MATCH_ODDS') {
                    let loss = this.state.setPrice * value - value;
                    let profit = value;
                    this.setState({ profit: profit });
                    this.setState({ loss: loss });
                } else if (this.state.setName == 'Bookmaker') {
                    let profit = (value / 100) * this.state.setPrice;
                    let loss = value;
                    this.setState({ profit: profit });
                    this.setState({ loss: loss });
                } else {
                    let profit = value;
                    let loss = value;
                    this.setState({ profit: profit });
                    this.setState({ loss: loss });
                }
            }
        });

    }

    match_details = async (id) => {
        let api_response = await ApiExecute1(`oddslist?sport_id=2`, { method: 'GET' });
        api_response.data.data.forEach(element => {
            if (element.eventId == id) {
                console.log('sdfkjjm d d f dfffff', element);
                this.setState({ match_detail: element });
                this.odds_details(element.marketId);

                setInterval(this.odds_details, 1000);
            }
        });
    }
    checkGame = async (id) => {
        let api_response = await ApiExecute1(`getsoccermatches`, { method: 'GET' });
        api_response.data.forEach(element => {

            this.setState({ loading: true });

            if (element.gameId == id) {
                this.setState({ match_detail: element });
                this.odds_details(element.marketId);

                setInterval(this.odds_details, 1000);

                this.fancy_details(element.gameId);
                this.setState({ match_status: element });

                setInterval(this.fancy_details, 1000);
            }
        });
    }
    odds_details = async (id = null) => {
        let mId = id ? id : this.state.match_detail.marketId;
        if (mId != undefined) {
            let market_response = await ApiExecute1(`getdata?market_id=${mId}`, { method: 'GET' });
            this.setState({ odds_market: market_response.data[0][0] });
        }
    }

    fancy_details = async (id = null) => {
        let eId = id ? id : this.state.match_detail.eventId;
        if (eId != undefined) {

            console.log('fancy_details');
            let market_response = await ApiExecute1(`getfancydata?event_id=${eId}`, { method: 'GET' });
            // console.log('fancy_details', market_response);
            // console.log('sdklffdll dg l eid : ');
            // market_response.data.forEach(element => {
            this.setState({ markets: market_response.data });
            this.setState({ loading: false });
            // });
        }
    }

    getWallet = async () => {
        let user = sessionStorage.getItem('@front_user');
        user = JSON.parse(user);
        console.log('user', user);
        console.log(user.id);

        let api_response = await ApiExecute(`wallet/${user.id}`, { method: 'GET' });
        if (api_response.data && api_response.data.length) {
            this.setState({ wallet: api_response.data[0] });
            this.setState({ exposure: api_response.data[1] });
        }
    }

    setBat = async () => {
        let user_id = this.state.user.id;
        let event = this.state.match_detail.eventName;
        let nation = '';
        let {
            setPrice,
            setTeam,
            setType,
            setValue,
            eventType,
            setName,
            profit,
            loss,
            ip,
            browser,
        } = this.state;
        console.log(user_id);

        let balance = $('#balance-point').find('.wallet-point').data('value');

        if (setValue === '') {
            ToastMessage('error', 'Please select amount !');
        } else if (parseInt(balance) < parseInt(setValue)) {
            ToastMessage('error', 'Insaficient points in account !');
        } else {
            let data = {
                rate: setPrice,
                event_name: setTeam,
                bat_type: setType,
                amount: setValue,
                user: user_id,
                event: event,
                event_type: eventType,
                type: setName,
                profit: profit,
                loss: loss,
                ip: ip,
                browser: browser,
                nation: nation,
            }

            console.log('data', data);
            let api_response = await ApiExecute("add-bat", { method: 'POST', data: data });

            console.log('api_response', api_response);
            if (api_response.data.status) {
                $('.bet-slip-container').toggleClass('d-none d-block');
                this.updateWallet();
                this.state.setPrice = null;
                this.state.setSize = null;
                this.state.setTeam = null;
                this.state.setType = null;
                this.state.setValue = null;
                this.state.setName = null;
                this.state.type = null;
                this.state.profit = null;
                this.state.loss = null;
                this.state.setValue = null;
                ToastMessage('success', api_response.data.message);
            }
            else {
                if (api_response.data.errors == undefined && api_response.data.errors == null) {
                    ToastMessage('error', api_response.data.message);
                } else {
                    ToastMessage('error', api_response.data.errors[0]);
                }
            }

        }
    }

    updateWallet = async () => {
        let data = {
            user_id: this.state.user.id,
            amount: parseInt(this.state.wallet.amount) - parseInt(this.state.loss),
            type: 'main',
        }
        let data1 = {
            user_id: this.state.user.id,
            amount: parseInt(this.state.exposure.amount) + parseInt(this.state.loss),
            type: 'exposure',
        }
        console.log('data', data);
        console.log('data1', data1);

        let api_response = await ApiExecute(`wallet/${this.state.user.id}`, { method: 'PUt', data: data });
        if (api_response.data.status) {
            // ToastMessage('success', api_response.data.message);
        }
        else {
            if (api_response.data.errors == undefined && api_response.data.errors == null) {
                ToastMessage('error', api_response.data.message);
            } else {
                ToastMessage('error', api_response.data.errors[0]);
            }
        }

        let api1_response = await ApiExecute(`wallet/${this.state.user.id}`, { method: 'PUt', data: data1 });
        if (api1_response.data.status) {
            // ToastMessage('success', api1_response.data.message);
        }
        else {
            if (api1_response.data.errors == undefined && api1_response.data.errors == null) {
                ToastMessage('error', api1_response.data.message);
            } else {
                ToastMessage('error', api1_response.data.errors[0]);
            }
        }

    }

    render() {
        let self = this;

        var settings = {
            speed: 3000,
            autoplay: true,
            autoplaySpeed: 0,
            centerMode: true,
            // cssEase: 'linear',
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical: true,
            // variableWidth: true,
            infinite: true,
            initialSlide: 1,
            arrows: true,
            buttons: true
        };

        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">
                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />
                    <FrontSidebar {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="center-main-content">
                            <div class="news-bar d-none-desktop">
                                <div class="news-title"><i class="fas fa-bell mr-2"></i> News</div>
                                <marquee>We are happy to announce that we are bringing you a new world of gambling.</marquee>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel3" data-ride="carousel" data-interval="3000" class="carousel vert slide">

                                    </div>
                                </div>
                            </div>
                            <div class="center-container">
                                <div class="detail-page-container cricket-detail">

                                    <div class="banner scorecard-banner" style={{ backgroundImage: "url(https://sitethemedata.com/v4/static/front/img/events-banner/4.png)" }} ></div>
                                    {/* {
                                        !this.state.loading ? */}
                                    <>
                                        <div class="game-header d-none-mobile mt-2 sport4">
                                            <span class="game-header-name">{this.state.match_detail.eventName}</span>
                                            <span class="game-header-date">
                                                <Moment format="DD/MM/YYYY HH:mm:ss">
                                                    {this.state.match_detail.eventDate}
                                                </Moment>
                                            </span>
                                        </div>
                                        <div class="game-header d-none-desktop mt-2 sport4">
                                            <span class="game-header-name">
                                                {this.state.match_detail.eventName}
                                                <div><small> {this.state.match_detail.eventDate}</small></div>
                                            </span>
                                        </div>
                                        <div class="search-box d-none-big w-100">
                                            <div class="form-group mb-0">
                                                <input type="text" placeholder="Search Market" class="form-control" style={{ textTransform: 'lowercase' }} />
                                                <img src="https://sitethemedata.com/v4/static/front/img/search.svg" class="search-icon" />
                                            </div>
                                        </div>
                                        <div class="all-markets d-none-small">
                                            <div>
                                                <a href="javascript:void(0)"><small>#TOURNAMENT_WINNER</small></a>
                                                <a href="javascript:void(0)"><small>#Bookmaker</small></a>
                                                <a href="javascript:void(0)"><small>#Normal</small></a>
                                            </div>
                                            <div class="search-box">
                                                <div class="form-group mb-2">
                                                    <input type="text" placeholder="Search Market" class="form-control" style={{ textTransform: 'lowercase' }} />
                                                    <img src="https://sitethemedata.com/v4/static/front/img/search.svg" class="search-icon" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="market-4" id="goto-0">
                                            <div class="bet-table">
                                                <div data-toggle="collapse" data-target="#market0" aria-expanded="true" class="bet-table-header">
                                                    <div class="nation-name">
                                                        <span title="TOURNAMENT_WINNER">
                                                            <a href="javascript:void(0)" title="">
                                                                <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                            </a>
                                                            TOURNAMENT_WINNER
                                                        </span>
                                                        <span class="max-bet"><span title="Max : 1">1</span></span>
                                                    </div>
                                                    <div class="back back-title bl-title d-none-mobile">Back</div>
                                                    <div class="lay lay-title bl-title d-none-mobile">Lay</div>
                                                </div>
                                                <div id="market0" data-title="open" class="bet-table-body collapse show">
                                                    {
                                                        this.state.odds_market && this.state.odds_market.runners && this.state.odds_market.runners.map((element, index) => {
                                                            if (element.status == 'ACTIVE') {
                                                                return (
                                                                    <>
                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                            <div class="bet-table-mobile-team-name"><span>{element.selectionId}</span> <span></span></div>
                                                                        </div>
                                                                        <div data-title="ACTIVE" class="bet-table-row">
                                                                            <div class="nation-name d-none-mobile">
                                                                                {/* <p><span>{this.state.match_detail[`runnerName${index + 1}`]}</span> <span class="float-right"></span></p> */}
                                                                                <p><span>{element.selectionId}</span> <span class="float-right"></span></p>
                                                                                <p class="mb-0"></p>
                                                                            </div>
                                                                            {
                                                                                element && element.ex && Object.entries(element.ex).map((e, i) => {
                                                                                    if (e[0] == 'availableToBack') {
                                                                                        return (
                                                                                            <>
                                                                                                {
                                                                                                    e && e[1] && e[1]
                                                                                                        .sort((a, b) => a.id > b.id ? 1 : -1)
                                                                                                        .map((c, i1) => {
                                                                                                            return (
                                                                                                                <div class="bl-box back back2" data-price={c.price} data-size={c.size} data-name={'MATCH_ODDS'} data-type='back' data-team={element.selectionId}>
                                                                                                                    <span class="d-block odds">{c.price}</span>
                                                                                                                    <span class="d-block">{c.size}</span>
                                                                                                                </div>
                                                                                                            )
                                                                                                        })
                                                                                                }
                                                                                            </>
                                                                                        )
                                                                                    } else {
                                                                                        return (
                                                                                            <>
                                                                                                {
                                                                                                    e && e[1] && e[1].map((c, i1) => {
                                                                                                        return (
                                                                                                            <div class="bl-box lay lay1" data-price={c.price} data-size={c.size} data-name={'MATCH_ODDS'} data-type='lay' data-team={element.selectionId}>
                                                                                                                <span class="d-block odds">{c.price}</span>
                                                                                                                <span class="d-block">{c.size}</span>
                                                                                                            </div>
                                                                                                        )
                                                                                                    })
                                                                                                }
                                                                                            </>
                                                                                        )
                                                                                    }
                                                                                })
                                                                            }
                                                                        </div>
                                                                    </>
                                                                )
                                                            } else {
                                                                return (
                                                                    <>
                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                            <div class="bet-table-mobile-team-name"><span>{element.selectionId}</span> <span></span></div>
                                                                        </div>
                                                                        <div data-title="REMOVED" class="bet-table-row suspendedtext">
                                                                            <div class="nation-name d-none-mobile">
                                                                                <p><span>{this.state.match_detail[`runnerName${index + 1}`]}</span> <span class="float-right"></span></p>
                                                                                <p class="mb-0"></p>
                                                                            </div>
                                                                            <div class="bl-box back back2 no-val">
                                                                                <span class="d-block odds">—</span>
                                                                            </div>
                                                                            <div class="bl-box back back1 no-val">
                                                                                <span class="d-block odds">—</span>
                                                                            </div>
                                                                            <div class="bl-box back back no-val">
                                                                                <span class="d-block odds">—</span>
                                                                            </div>
                                                                            <div class="bl-box lay lay no-val">
                                                                                <span class="d-block odds">—</span>
                                                                            </div>
                                                                            <div class="bl-box lay lay1 no-val">
                                                                                <span class="d-block odds">—</span>
                                                                            </div>
                                                                            <div class="bl-box lay lay2 no-val">
                                                                                <span class="d-block odds">—</span>
                                                                            </div>
                                                                        </div>
                                                                    </>
                                                                )
                                                            }
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.markets && this.state.markets.t2 && this.state.markets.t2.length ?
                                                <div class="market-4" id="goto-1">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market1" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Bookmaker">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    Bookmaker
                                                                </span>
                                                                <span class="max-bet">Min:<span>100</span> Max:<span>50K</span></span>
                                                            </div>
                                                            <div class="back back-title bl-title d-none-mobile">Back</div>
                                                            <div class="lay lay-title bl-title d-none-mobile">Lay</div>
                                                        </div>

                                                        <div id="market1" data-title="" class="bet-table-body collapse show">
                                                            {
                                                                this.state.markets && this.state.markets.t2 && this.state.markets.t2.map((element) => {
                                                                    return (
                                                                        <>
                                                                            {
                                                                                element && element.bm1 && element.bm1.map((e) => {
                                                                                    return (
                                                                                        <>
                                                                                            <div class="bet-table-mobile-row d-none-desktop">
                                                                                                <div class="bet-table-mobile-team-name"><span>Punjab Kings</span> <span></span></div>
                                                                                            </div>
                                                                                            <div data-title={e.s} class={`bet-table-row ${e.s != 'ACTIVE' ? 'suspendedtext' : ''}`}>
                                                                                                <div class="nation-name d-none-mobile">
                                                                                                    <p><span>{e.nat}</span> <span class="float-right"></span></p>
                                                                                                    <p class="mb-0"></p>
                                                                                                </div>
                                                                                                {
                                                                                                    e.b3s == 'True' ?
                                                                                                        <div class="bl-box back back2">
                                                                                                            <span class="d-block odds">{e.b3}</span>
                                                                                                            <span class="d-block">{e.bs3}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box back back2 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.b2s == 'True' ?
                                                                                                        <div class="bl-box back back1">
                                                                                                            <span class="d-block odds">{e.b2}</span>
                                                                                                            <span class="d-block">{e.bs2}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box back back1 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.b1s == 'True' ?
                                                                                                        <div class="bl-box back back">
                                                                                                            <span class="d-block odds">{e.b1}</span>
                                                                                                            <span class="d-block">{e.bs1}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box back back no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.l3s == 'True' ?
                                                                                                        <div class="bl-box lay lay">
                                                                                                            <span class="d-block odds">{e.l3}</span>
                                                                                                            <span class="d-block">{e.ls3}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box lay lay no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.l2s == 'True' ?
                                                                                                        <div class="bl-box lay lay1">
                                                                                                            <span class="d-block odds">{e.l2}</span>
                                                                                                            <span class="d-block">{e.ls2}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box lay lay1 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                                {
                                                                                                    e.l1s == 'True' ?
                                                                                                        <div class="bl-box lay lay2">
                                                                                                            <span class="d-block odds">{e.l1}</span>
                                                                                                            <span class="d-block">{e.ls1}</span>
                                                                                                        </div>
                                                                                                        :
                                                                                                        <div class="bl-box lay lay2 no-val">
                                                                                                            <span class="d-block odds">—</span>
                                                                                                        </div>
                                                                                                }
                                                                                            </div>
                                                                                        </>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </>
                                                                    )
                                                                })
                                                            }
                                                        </div>

                                                    </div>
                                                </div>

                                                : ''
                                        }
                                        {
                                            this.state.markets && this.state.markets.t3 && this.state.markets.t3.length ?
                                                <div class="market-6" id="goto-2">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market2" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Normal">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    {
                                                                        this.state.markets && this.state.markets.t3 && this.state.markets.t3.map((element, index) => {
                                                                            if (index == 0) {
                                                                                return (
                                                                                    <>
                                                                                        {element.gtype}
                                                                                    </>
                                                                                )
                                                                            }
                                                                        })
                                                                    }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="market2" data-title="OPEN" class="bet-table-body collapse show container-fluid container-fluid-5">
                                                            <div class="row row5">
                                                                {
                                                                    this.state.markets && this.state.markets.t3 && this.state.markets.t3.map((element) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-12 col-md-6">
                                                                                    <div class="fancy-tripple">
                                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                                            <div class="bet-table-mobile-team-name">
                                                                                                <span>{element.nat}</span> <span></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-title="" class="bet-table-row">
                                                                                            <div class="nation-name d-none-mobile">
                                                                                                <p>
                                                                                                    {element.nat}
                                                                                                </p>
                                                                                                <p class="mb-0"></p>
                                                                                            </div>
                                                                                            <div class="bl-box lay">
                                                                                                <span class="d-block odds">{element.l1}</span>
                                                                                                <span class="d-block">{element.ls1}</span>
                                                                                            </div>
                                                                                            <div class="bl-box back">
                                                                                                <span class="d-block odds">{element.b1}</span>
                                                                                                <span class="d-block">{element.bs1}</span>
                                                                                            </div>
                                                                                            <div class="fancy-min-max">
                                                                                                Min:<span>100</span> Max:<span>50K</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        {/* <div class="market-message">
                                                                                        Total Matches 1st over : 136 (Matches Played: 29)
                                                                                    </div> */}
                                                                                    </div>
                                                                                </div>

                                                                            </>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                : ''
                                        }
                                        {
                                            this.state.markets && this.state.markets.t4 && this.state.markets.t4.length ?
                                                <div class="market-6" id="goto-2">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market2" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Normal">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    {
                                                                        this.state.markets && this.state.markets.t4 && this.state.markets.t4.map((element, index) => {
                                                                            if (index == 0) {
                                                                                return (
                                                                                    <>
                                                                                        {element.gtype}
                                                                                    </>
                                                                                )
                                                                            }
                                                                        })
                                                                    }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="market2" data-title="OPEN" class="bet-table-body collapse show container-fluid container-fluid-5">
                                                            <div class="row row5">
                                                                {
                                                                    this.state.markets && this.state.markets.t4 && this.state.markets.t4.map((element) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-12 col-md-6">
                                                                                    <div class="fancy-tripple">
                                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                                            <div class="bet-table-mobile-team-name">
                                                                                                <span>{element.nat}</span> <span></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-title="" class="bet-table-row">
                                                                                            <div class="nation-name d-none-mobile">
                                                                                                <p>
                                                                                                    {element.nat}
                                                                                                </p>
                                                                                                <p class="mb-0"></p>
                                                                                            </div>
                                                                                            <div class="bl-box lay">
                                                                                                <span class="d-block odds">{element.l1}</span>
                                                                                                <span class="d-block">{element.ls1}</span>
                                                                                            </div>
                                                                                            <div class="bl-box back">
                                                                                                <span class="d-block odds">{element.b1}</span>
                                                                                                <span class="d-block">{element.bs1}</span>
                                                                                            </div>
                                                                                            <div class="fancy-min-max">
                                                                                                Min:<span>100</span> Max:<span>50K</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        {/* <div class="market-message">
                                                                                        Total Matches 1st over : 136 (Matches Played: 29)
                                                                                    </div> */}
                                                                                    </div>
                                                                                </div>

                                                                            </>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                : ''
                                        }
                                        {
                                            this.state.markets && this.state.markets.t5 && this.state.markets.t5.length ?
                                                <div class="market-6" id="goto-2">
                                                    <div class="bet-table">
                                                        <div data-toggle="collapse" data-target="#market2" aria-expanded="true" class="bet-table-header">
                                                            <div class="nation-name">
                                                                <span title="Normal">
                                                                    <a href="javascript:void(0)" title="">
                                                                        <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="mr-1" />
                                                                    </a>
                                                                    {
                                                                        this.state.markets && this.state.markets.t5 && this.state.markets.t5.map((element, index) => {
                                                                            if (index == 0) {
                                                                                return (
                                                                                    <>
                                                                                        {element.gtype}
                                                                                    </>
                                                                                )
                                                                            }
                                                                        })
                                                                    }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="market2" data-title="OPEN" class="bet-table-body collapse show container-fluid container-fluid-5">
                                                            <div class="row row5">
                                                                {
                                                                    this.state.markets && this.state.markets.t5 && this.state.markets.t5.map((element) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-12 col-md-6">
                                                                                    <div class="fancy-tripple">
                                                                                        <div class="bet-table-mobile-row d-none-desktop">
                                                                                            <div class="bet-table-mobile-team-name">
                                                                                                <span>{element.nat}</span> <span></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-title="" class="bet-table-row">
                                                                                            <div class="nation-name d-none-mobile">
                                                                                                <p>
                                                                                                    {element.nat}
                                                                                                </p>
                                                                                                <p class="mb-0"></p>
                                                                                            </div>
                                                                                            <div class="bl-box lay">
                                                                                                <span class="d-block odds">{element.l1}</span>
                                                                                                <span class="d-block">{element.ls1}</span>
                                                                                            </div>
                                                                                            <div class="bl-box back">
                                                                                                <span class="d-block odds">{element.b1}</span>
                                                                                                <span class="d-block">{element.bs1}</span>
                                                                                            </div>
                                                                                            <div class="fancy-min-max">
                                                                                                Min:<span>100</span> Max:<span>50K</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        {/* <div class="market-message">
                                                                                        Total Matches 1st over : 136 (Matches Played: 29)
                                                                                    </div> */}
                                                                                    </div>
                                                                                </div>

                                                                            </>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                : ''
                                        }
                                    </>
                                    {/* : <div style={{ textAlign: 'center', lineHeight: '10px' }}>Loading...</div>
                                    } */}

                                </div>
                                <Front2Footer {...this.props} />
                                <div id="right-sidebar-id" class="right-sidebar home-right-sidebar sticky">
                                    {
                                        this.state.match_status.tv == 'True' ?
                                            <div class="tv-container">
                                                <div class="tv-title">
                                                    <h4 class="mb-0 bet-slip-title">TV</h4> <i class="float-right fas fa-angle-up"></i>
                                                </div>
                                                <div class="video-tv mt-1 hide-tv">
                                                    <iframe src="https://livestream11.com/user/662520618/Microsoft Windows/49.36.235.64/d706a354-03e3-4af0-a88e-da0245eba2cd"></iframe>
                                                </div>
                                            </div>
                                            : ''
                                    }

                                    <span>
                                        <div class="bet-slip-container d-none">
                                            <div>
                                                <h4 class="mb-0 bet-slip-title">Bet Slip</h4>
                                            </div>
                                            <div class="bet-slip-box">

                                                <div class="bet-slip">
                                                    <div class="bet-nation"><span>{this.state.match_detail.eventName}</span>
                                                        <a href="javascript:void(0)" class="close-bet float-right">
                                                            <img src="https://sitethemedata.com/v1/static/front/img/close.svg" />
                                                        </a>
                                                    </div>
                                                    <div class="match-result">
                                                        {this.state.setName}
                                                    </div>
                                                    <div class="bet-team">
                                                        <span title={this.state.setTeam} class="bet-team-name">
                                                            {this.state.setTeam}
                                                        </span>
                                                        <div class="odds-box float-right"><input type="number" value={this.state.setPrice} readonly="readonly" class="form-control" />
                                                            <a href="javascript:void(0)">
                                                                <img src="https://sitethemedata.com/v1/static/front/img/arrow-down.svg" class="arrow-up" />
                                                            </a>
                                                            <a href="javascript:void(0)">
                                                                <img src="https://sitethemedata.com/v1/static/front/img/arrow-down.svg" class="arrow-down" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bet-input back-border"><input type="text" value={this.state.setValue} id="placebetAmountWeb1" maxlength="9" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" placeholder="Amount" class="form-control" /></div>
                                                {
                                                    this.state.profit ?
                                                        <div class="possible-win"><span>Profit:</span> <h1 class="mb-0">{this.state.profit}</h1></div>
                                                        : ''
                                                }
                                                <div class="bet-buttons">
                                                    <button class="btn btn-primary" data-value="1000"><span>1k</span></button>
                                                    <button class="btn btn-primary" data-value="2000"><span>2k</span></button>
                                                    <button class="btn btn-primary" data-value="5000"><span>5k</span></button>
                                                    <button class="btn btn-primary" data-value="10000"><span>10k</span></button>
                                                    <button class="btn btn-primary" data-value="20000"><span>20k</span></button>
                                                    <button class="btn btn-primary" data-value="25000"><span>25k</span></button>
                                                    <button class="btn btn-primary" data-value="50000"><span>50k</span></button>
                                                    <button class="btn btn-primary" data-value="75000"><span>75k</span></button>
                                                </div>
                                                <div class="confirm-bets">
                                                    <div class="custom-control custom-switch">
                                                        <input id="autocon-1" type="checkbox" name="autocon-1" class="custom-control-input" value="true" /><label for="autocon-1" class="custom-control-label" >
                                                            Auto Confirm Bet
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="place-bet-btn"><button class="btn btn-primary btn-block" onClick={() => this.setBat()}><span>Place bet</span></button></div>
                                        </div>
                                    </span>
                                    <span></span>
                                </div>
                            </div >
                        </div>
                    </div>
                </div>
            </div >

        );
    }
}
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Nav } from 'react-bootstrap';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';


export default class FantasyScreen extends Component {
    state = {
        topHeaderRemove: '',
        gametypes: [],
        user: {},
        category: {},
        category_slug: '',
        category_id: '',
        gametype_id: 5,
        games: [],
        subcategories: [],
        refreshComponet: true,
    }

    onClickGametype = async (id) => {
        await this.setState({ gametype_id: id });
        this.gameList();
        this.allcategoryList();
        this.setState({ refreshComponet: !this.state.refreshComponet })
    }
    onClickCategory = async (id) => {
        await this.setState({ category_id: id });
        this.gameList();
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        // console.log('front_token', user);
        this.setState({
            user: JSON.parse(user)
        });

        let category_slug = this.props.slug;
        // console.log('category slug', category_slug);
        if (category_slug) {
            this.state.category_slug = category_slug;
            this.getCategory();
        }

        this.gameList();


        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });
        $(document).on('click', '.accordion-box .has-arrow', e => {
            $('.user-dropdown.collapse').toggleClass('show');
        })
        $(document).click(function (e) {
            if ($(e.target).is('.user-dropdown')) {
                $('.user-dropdown.collapse').toggleClass('show');
            }
        });
        let self = this;
        $(document).on('click', '.menu-box .navbar-nav .nav-item', e => {
            let categoryId = '';
            categoryId = $(e.target).closest(".nav-item").data('id');
            this.setState({ category_id: categoryId });
            // console.log('category_id', this.state.category_id);
            self.gameList();
            $(e.target).closest(".nav-item").children('a').addClass('active');
            $(e.target).closest(".nav-item").siblings().children('a').removeClass('active');
        });
    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        this.setState({ categories: api_response.data.results });
    }
    allcategoryList = async () => {
        let id = this.state.category.id;
        let game_id = this.state.gametype_id;
        console.log('gametype1', game_id, 'category1', id);
        let api_response = await ApiExecute(`category?category_id=${id}&&game_type_id=${game_id}`, { method: 'GET' });
        this.setState({ subcategories: api_response.data.results });
        console.log('subcategory', api_response.data.results);
    }

    getCategory = async () => {
        let category = await ApiExecute(`category/info/${this.state.category_slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.categoryList();
        this.allcategoryList();
        this.gametypeList();
    }

    gametypeList = async () => {
        let api_response = await ApiExecute(`gametype?category_id=${this.state.category.id}`, { method: 'GET' });
        // console.log('lists', api_response.data.results);
        this.setState({ gametypes: api_response.data.results });


        // let casino_response = await ApiExecute(`game?gametype_id=8`, { method: 'GET' });
        // console.log('lists', casino_response.data.results);
        // this.setState({ casino_games: casino_response.data.results })
    }

    gameList = async () => {
        let gametype = this.state.gametype_id;
        let category = this.state.category_id;
        console.log('gametype', gametype, 'category', category);
        let api_response = await ApiExecute(`game?category_id=${this.state.category_id}&&game_type_id=${this.state.gametype_id}`, { method: 'GET' });
        console.log('games', api_response.data.results);
        this.setState({ games: api_response.data.results })
    }

    render() {
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="logo-box d-none-mobile logo-casino">
                            <div class="logo"><a href="/sport" class=""><img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" class="img-fluid" /></a></div>
                        </div>
                        <div class="center-main-content report-container">
                            <div class="news-bar d-none-desktop">
                                <marquee>Launched Best Ever 8 Deck Casino 1 Card ONE-DAY.</marquee>
                                <div class="news-title">
                                    <img src="https://sitethemedata.com/v3/static/front/img/icons/speaker.svg" />
                                </div>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel2" data-ride="carousel" data-interval="3000" class="carousel vert slide"></div>
                                </div>
                            </div>

                            <div class="report-box">
                                <div class="report-title">
                                    <div class="report-name">Casino Results</div>
                                    <div class="report-search search-box">
                                        <div class="form-group mb-0"><input type="text" placeholder="Search" class="form-control" /> <img src="https://sitethemedata.com/v4/static/front/img/search.svg" class="search-icon" /></div>
                                    </div>
                                </div>
                                <div class="report-form">
                                    <div class="form-group from-date">
                                        <label>Date</label>
                                        <div class="mx-datepicker">
                                            <div class="mx-input-wrapper">
                                                <input name="date" type="text" autocomplete="off" placeholder="" class="mx-input" />
                                                <i class="mx-icon-clear">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" width="1em" height="1em">
                                                        <path d="M810.005333 274.005333l-237.994667 237.994667 237.994667 237.994667-60.010667 60.010667-237.994667-237.994667-237.994667 237.994667-60.010667-60.010667 237.994667-237.994667-237.994667-237.994667 60.010667-60.010667 237.994667 237.994667 237.994667-237.994667z"></path>
                                                    </svg>
                                                </i>
                                                <i class="mx-icon-calendar">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" width="1em" height="1em">
                                                        <path d="M940.218182 107.054545h-209.454546V46.545455h-65.163636v60.50909H363.054545V46.545455H297.890909v60.50909H83.781818c-18.618182 0-32.581818 13.963636-32.581818 32.581819v805.236363c0 18.618182 13.963636 32.581818 32.581818 32.581818h861.090909c18.618182 0 32.581818-13.963636 32.581818-32.581818V139.636364c-4.654545-18.618182-18.618182-32.581818-37.236363-32.581819zM297.890909 172.218182V232.727273h65.163636V172.218182h307.2V232.727273h65.163637V172.218182h176.872727v204.8H116.363636V172.218182h181.527273zM116.363636 912.290909V442.181818h795.927273v470.109091H116.363636z"></path>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group to-date">
                                        <label>Type</label>
                                        <select class="form-control">
                                            <option value="">Select Type</option>
                                            <option value="teen">Teenpatti 1-day</option>
                                            <option value="poker">Poker 1-Day</option>
                                            <option value="3cardj">3 Cards Judgement</option>
                                            <option value="aaa">Amar Akbar Anthony</option>
                                            <option value="ab20">Andar Bahar</option>
                                            <option value="abj">Andar Bahar 2</option>
                                            <option value="baccarat">Baccarat</option>
                                            <option value="baccarat2">Baccarat 2</option>
                                            <option value="btable">Bollywood Casino</option>
                                            <option value="card32">32 Cards A</option>
                                            <option value="card32eu">32 Cards B</option>
                                            <option value="cmatch20">Cricket Match 20-20</option>
                                            <option value="cmeter">Casino Meter</option>
                                            <option value="cricketv">Cricket V</option>
                                            <option value="cricketv2">Cricket V2</option>
                                            <option value="cricketv3">5Five Cricket</option>
                                            <option value="dt20">20-20 Dragon Tiger</option>
                                            <option value="dt202">20-20 Dragon Tiger 2</option>
                                            <option value="dt6">1 Day Dragon Tiger</option>
                                            <option value="dtl20">20-20 D T L</option>
                                            <option value="lottcard">Lottery</option>
                                            <option value="lucky7">Lucky 7 - A</option>
                                            <option value="lucky7eu">Lucky 7 - B</option>
                                            <option value="poker20">20-20 Poker</option>
                                            <option value="poker6">Poker 6 Players</option>
                                            <option value="teen20">20-20 Teenpatti</option>
                                            <option value="teen8">Teenpatti Open</option>
                                            <option value="teen9">Teenpatti Test</option>
                                            <option value="war">Casino War</option>
                                            <option value="worli">Worli Matka</option>
                                            <option value="worli2">Instant Worli</option>
                                            <option value="teen6">Teenpatti - 2.0</option>
                                            <option value="queen">Queen</option>
                                            <option value="race20">Race20</option>
                                            <option value="lucky7eu2">Lucky 7 - C</option>
                                            <option value="superover">Super Over</option>
                                            <option value="trap">The Trap</option>
                                            <option value="patti2">2 Cards Teenpatti</option>
                                            <option value="teensin">29Card Baccarat</option>
                                            <option value="teenmuf">Muflis Teenpatti</option>
                                            <option value="race17">Race to 17</option>
                                            <option value="teen20b">20-20 Teenpatti B</option>
                                            <option value="trio">Trio</option>
                                            <option value="notenum">Note Number</option>
                                            <option value="teen2024">Teen 20 24</option>
                                            <option value="kbc">K.B.C</option>
                                            <option value="teen120">1 CARD 20-20</option>
                                            <option value="teen1">1 CARD ONE-DAY</option>
                                            <option value="vteen20">V-20-20 Teenpatti</option>
                                            <option value="vteen">V-Teenpatti 1-day</option>
                                        </select>
                                    </div>
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                                <div class="report-page-count">
                                    <div class="form-group mb-0">
                                        <label>Show</label>
                                        <select class="form-control">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="200">200</option>
                                            <option value="300">300</option>
                                            <option value="400">400</option>
                                            <option value="500">500</option>
                                        </select>
                                        <label>Entries</label>
                                    </div>
                                    <div class="file-icons">
                                        <div><i class="fas fa-file-pdf"></i></div>
                                        <div id="export_1631335450912"><i class="fas fa-file-excel"></i></div>
                                    </div>
                                </div>
                                <div class="report-table d-none-mobile">

                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="bet-amount">
                                                    <div>Round ID</div>
                                                </th>
                                                <th class="bet-remark">
                                                    <div>Winner</div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>6949828676055</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5686707825600</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8441133940682</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>4666948789289</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8323157598522</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Tie</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5671273244240</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>6289567365068</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>7312024436897</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Tie</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>7640864425175</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5173169919090</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>7209478165947</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>7539406967265</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5828992191788</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5281985049515</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8074772373517</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8615179984301</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5964980792705</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>4970553703460</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8997243151587</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>5628732460717</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>7028862715654</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>6135754600714</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>9204572111446</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8360238315736</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Dealer</div>
                                                </td>
                                            </tr>
                                            <tr class="pointer">
                                                <td class="bet-event-name">
                                                    <div>8846375072650</div>
                                                </td>
                                                <td class="bet-date">
                                                    <div>Player</div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="report-table d-none-desktop">

                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>6949828676055</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5686707825600</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8441133940682</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>4666948789289</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8323157598522</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Tie</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5671273244240</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>6289567365068</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>7312024436897</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Tie</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>7640864425175</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5173169919090</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>7209478165947</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>7539406967265</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5828992191788</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5281985049515</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8074772373517</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8615179984301</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5964980792705</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>4970553703460</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8997243151587</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>5628732460717</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>7028862715654</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>6135754600714</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>9204572111446</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8360238315736</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Dealer</span></div>
                                    </div>
                                    <div class="report-row">
                                        <div><span class="bet-heading">Round ID</span> <span>8846375072650</span></div>
                                        <div><span class="bet-heading">Winner</span> <span>Player</span></div>
                                    </div>
                                </div>
                                <ul data-v-82963a40="" class="pagination justify-content-center mt-2">
                                    <li data-v-82963a40="" class="page-item disabled"><a data-v-82963a40="" tabindex="-1" class="page-link">&lt;&lt;</a></li>
                                    <li data-v-82963a40="" class="page-item disabled"><a data-v-82963a40="" tabindex="-1" class="page-link">&lt;</a></li>
                                    <li data-v-82963a40="" class="page-item active"><a data-v-82963a40="" tabindex="0" class="page-link">1</a></li>
                                    <li data-v-82963a40="" class="page-item"><a data-v-82963a40="" tabindex="0" class="page-link">2</a></li>
                                    <li data-v-82963a40="" class="page-item"><a data-v-82963a40="" tabindex="0" class="page-link">3</a></li>
                                    <li data-v-82963a40="" class="page-item disabled"><a data-v-82963a40="" tabindex="0" class="page-link">…</a></li>
                                    <li data-v-82963a40="" class="page-item"><a data-v-82963a40="" tabindex="0" class="page-link">29</a></li>
                                    <li data-v-82963a40="" class="page-item"><a data-v-82963a40="" tabindex="0" class="page-link">&gt;</a></li>
                                    <li data-v-82963a40="" class="page-item"><a data-v-82963a40="" tabindex="0" class="page-link">&gt;&gt;</a></li>
                                </ul>

                            </div>
                        </div>

                        <FrontFooter {...this.props} />

                    </div>
                </div>

            </div>

        );
    }
}
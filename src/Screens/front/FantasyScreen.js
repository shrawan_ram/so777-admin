import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Nav } from 'react-bootstrap';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';


export default class FantasyScreen extends Component {
    state = {
        topHeaderRemove: '',
        gametypes: [],
        user: {},
        category: {},
        category_slug: '',
        category_id: '',
        gametype_id: 5,
        games: [],
        subcategories: [],
        refreshComponet: true,
    }

    onClickGametype = async (id) => {
        await this.setState({ gametype_id: id });
        this.gameList();
        this.allcategoryList();
        this.setState({ refreshComponet: !this.state.refreshComponet })
    }
    onClickCategory = async (id) => {
        await this.setState({ category_id: id });
        this.gameList();
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        // console.log('front_token', user);
        this.setState({
            user: JSON.parse(user)
        });

        let category_slug = this.props.slug;
        // console.log('category slug', category_slug);
        if (category_slug) {
            this.state.category_slug = category_slug;
            this.getCategory();
        }

        this.gameList();


        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest(".accordion-box").toggleClass('mm-active');
            $(e.target).closest(".accordion-box").children('.mm-collapse').slideToggle();
            $(e.target).closest(".accordion-box").children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest(".accordion-box").siblings().children(".mm-collapse").slideUp();
            $(e.target).closest(".accordion-box").siblings().removeClass('mm-show');
        });
        $(document).on('click', '.accordion-box .has-arrow', e => {
            $('.user-dropdown.collapse').toggleClass('show');
        })
        $(document).click(function (e) {
            if ($(e.target).is('.user-dropdown')) {
                $('.user-dropdown.collapse').toggleClass('show');
            }
        });
        let self = this;
        $(document).on('click', '.menu-box .navbar-nav .nav-item', e => {
            let categoryId = '';
            categoryId = $(e.target).closest(".nav-item").data('id');
            this.setState({ category_id: categoryId });
            // console.log('category_id', this.state.category_id);
            self.gameList();
            $(e.target).closest(".nav-item").children('a').addClass('active');
            $(e.target).closest(".nav-item").siblings().children('a').removeClass('active');
        });
    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        this.setState({ categories: api_response.data.results });
    }
    allcategoryList = async () => {
        let id = this.state.category.id;
        let game_id = this.state.gametype_id;
        console.log('gametype1', game_id, 'category1', id);
        let api_response = await ApiExecute(`category?category_id=${id}&&game_type_id=${game_id}`, { method: 'GET' });
        this.setState({ subcategories: api_response.data.results });
        console.log('subcategory', api_response.data.results);
    }

    getCategory = async () => {
        let category = await ApiExecute(`category/info/${this.state.category_slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.categoryList();
        this.allcategoryList();
        this.gametypeList();
    }

    gametypeList = async () => {
        let api_response = await ApiExecute(`gametype?category_id=${this.state.category.id}`, { method: 'GET' });
        // console.log('lists', api_response.data.results);
        this.setState({ gametypes: api_response.data.results });


        // let casino_response = await ApiExecute(`game?gametype_id=8`, { method: 'GET' });
        // console.log('lists', casino_response.data.results);
        // this.setState({ casino_games: casino_response.data.results })
    }

    gameList = async () => {
        let gametype = this.state.gametype_id;
        let category = this.state.category_id;
        console.log('gametype', gametype, 'category', category);
        let api_response = await ApiExecute(`game?category_id=${this.state.category_id}&&game_type_id=${this.state.gametype_id}`, { method: 'GET' });
        console.log('games', api_response.data.results);
        this.setState({ games: api_response.data.results })
    }

    render() {
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="logo-box d-none-mobile logo-casino">
                            <div class="logo"><a href="/sport" class=""><img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" class="img-fluid" /></a></div>
                        </div>
                        <div class="center-main-content report-container">
                            <div class="news-bar d-none-desktop">
                                <marquee>Launched Best Ever 8 Deck Casino 1 Card ONE-DAY.</marquee>
                                <div class="news-title">
                                    <img src="https://sitethemedata.com/v3/static/front/img/icons/speaker.svg" />
                                </div>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel2" data-ride="carousel" data-interval="3000" class="carousel vert slide"></div>
                                </div>
                            </div>
                            <div class="report-box casino-box w-100 casino-fantasy">
                                <div class="slot-note">
                                    <div class="note-title">Note:</div>
                                    <div class="note-desc d-none-mobile">Follow FAQ when none of these games work.</div>
                                    <div class="note-desc d-none-desktop">
                                        <marquee>Follow FAQ when none of these games work.</marquee>
                                    </div>
                                    <a href="/faq" class="btn btn-primary mr-2">Click Here</a>
                                </div>
                                <div class="casino-banners mt-0 ">
                                    <div class="casino-banner-item">
                                        <img src="https://sitethemedata.com/casino_icons/other/diam11.png" class="img-fluid" />
                                    </div>
                                    <div class="casino-banner-item"><img src="https://sitethemedata.com/casino_icons/other/rummy.jpg" class="img-fluid" /></div>
                                    <div class="casino-banner-item"><img src="https://sitethemedata.com/casino_icons/other/playerbattle.jpeg" class="img-fluid" /></div>
                                    <div class="casino-banner-item"><img src="https://sitethemedata.com/casino_icons/other/poptheball.jpg" class="img-fluid" /></div>
                                    <div class="casino-banner-item"><img src="https://sitethemedata.com/casino_icons/other/ludoclub.jpg" class="img-fluid" /></div>
                                    <div class="casino-banner-item"><img src="https://sitethemedata.com/casino_icons/other/ludo-lands.jpg" class="img-fluid" /></div>
                                </div>
                                <div class="fantasy-desc-container mt-1">
                                    <div class="container-fluid container-fluid-5">
                                        <div class="row row5">
                                            <div class="col-12 col-md-6"><img src="https://sitethemedata.com/casino_icons/other/diam11.png" class="img-fluid w-100" /></div>
                                            <div class="col-12 col-md-6">
                                                <h1 class="fantasy-detail-title"><span>Diam11</span></h1>
                                                <div><button class="btn btn-lg btn-primary">Play</button></div>
                                                <div>
                                                    <div class="casino-tabs">
                                                        <ul class="nav nav-tabs">
                                                            <li class="nav-item"><a href="#description" data-toggle="tab" class="nav-link active">Description</a></li>
                                                            <li class="nav-item"><a href="#howtoplay" data-toggle="tab" class="nav-link">How to play</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div id="description" class="tab-pane active">
                                                            <ol class="list-decimal">
                                                                <li>Any participant in Diam-11 can form a team on his own, however the team must have 11 players of
                                                                    which a maximum of 7 players can be selected from the same team of the actual teams. For
                                                                    example, one can select a maximum of 7 players from India in an IND vs AUS match.
                                                                </li>
                                                                <li>
                                                                    The team can be formed in any different combination without exceeding the 100 credit,
                                                                    additionally the same team must have the following combination in order to get qualified.
                                                                    <table class="m-1">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Player Type</th>
                                                                                <th>Min</th>
                                                                                <th>Max</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Wicket Keeper - WK</td>
                                                                                <td>1</td>
                                                                                <td>4</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Batsman - BAT</td>
                                                                                <td>3</td>
                                                                                <td>6</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>All Rounder - AR</td>
                                                                                <td>1</td>
                                                                                <td>4</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Bowler - BWL</td>
                                                                                <td>3</td>
                                                                                <td>6</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </li>
                                                                <li>Captain and Vice-captain has to be assigned once the team is selected.</li>
                                                                <li>
                                                                    Once the line-up flag has been announced:
                                                                    <ol class="list-outside">
                                                                        <li>Changes in the existing team can be done, however captain and vice-captain cannot be
                                                                            changed.
                                                                        </li>
                                                                        <li>New teams cannot be formed.</li>
                                                                        <li>Teams already formed can join any contest if the same haven’t joined any.</li>
                                                                    </ol>
                                                                </li>
                                                                <li>
                                                                    Once the live flag is announced:
                                                                    <ol class="list-outside">
                                                                        <li>No changes will be permitted.</li>
                                                                        <li>No team can join any new contest, even if the team has been created before live flag.
                                                                        </li>
                                                                    </ol>
                                                                </li>
                                                                <li>Substitutes will not be awarded any points for their contribution.</li>
                                                                <li>If a player gets replaced by substitute, but later comes back on field, he will get points for
                                                                    his contributions. However, if a player, who was not a part of the announced lineups, comes to
                                                                    the field as a substitute, he will not get points for any of his contributions.
                                                                </li>
                                                                <li>Super over is not included in our point system. Therefore, no points will be awarded for any
                                                                    contribution in super over.
                                                                </li>
                                                                <li>The points awarded in live are subjected to changes based on the changes in live, for example a
                                                                    wicket given, and the decision has been reversed based on DRS or any other fact.
                                                                </li>
                                                                <li>Winners will be declared post completion of the match.</li>
                                                                <li>These rules doesn’t apply to any other Fancy or Fancy-1 markets.</li>
                                                                <li>Winning of any mentioned pots will be decided based on the number of participants joined. For example, the winning of a 100 participant pot will be different if the entire 100 participants has joined and if only 60 participants has joined.</li>
                                                            </ol>
                                                        </div>
                                                        <div id="howtoplay" class="tab-pane fade">
                                                            <ol class="list-decimal">
                                                                <li class="mb-1"><b>What is Diam11?</b>
                                                                    Diam11 is a Game of Skill where you create a team of real players for an upcoming match and
                                                                    compete with other fans for big prizes. Your team earns points based on your players’
                                                                    performances in the real-life match, so make sure you make the right choices!
                                                                </li>
                                                                <li class="mb-1">
                                                                    <b>How to play Diam11?</b>
                                                                    The first step will be to create a user ID in our website. The next step is to create your Diam
                                                                    XI team. and hence, it is imperative to understand the rules you know to score the maximum
                                                                    points.
                                                                    <ol class="list-outside">
                                                                        <li><b>Step 1:</b> Log into your user ID and then select any of the matches which will be
                                                                            visible from the current or upcoming cricket tournaments under Diam11 tab. Now, before
                                                                            you proceed towards creating your Fantasy XI team – and you have to keep in mind that
                                                                            you put into mind your knowledge and expertise and judgement and pick the players who
                                                                            you think will play a key role in scoring points. Diam11 hosts games across many
                                                                            different sports. Click on an upcoming match you want to play and keep an eye on the
                                                                            match deadline. Every cricket team you build on Diam11 has to have 11 players, of which
                                                                            a maximum of 7 players can be from any one team playing the real-life match. Player
                                                                            Combinations: Your Diam11 team can have different combinations of players, but has to be
                                                                            within the 100 credit cap and must qualify the team selection criteria Create Your
                                                                            Teams.
                                                                        </li>
                                                                        <li><b>Step 2:</b> Now, the time has come when you can create your team with a budget of 100
                                                                            credits! All you need to do now is to select a game and then click the option ‘Create
                                                                            Team’. All you need to do is to pick a minimum of 1 wicket-keeper, between 3 to 5
                                                                            batsmen, and then 1 to 3 all-rounders as well as 3 to 5 bowlers – the summation of all
                                                                            players should be equal to 100 credits. The different combinations are already listed
                                                                            there. You can pick and choose what you think is the best combination for the particular
                                                                            match. Use your knowledge of sports to pick a team within a budget of 100 credits who
                                                                            you think will score the most points for the selected match. Remember, you can create
                                                                            and join contests with up to any number of teams for every match, so try different
                                                                            combinations to improve your chances of winning.
                                                                        </li>
                                                                        <li><b>Step 3:</b> After picking the team, you have to select your team’s Captain and
                                                                            Vice-Captain. The captain gets two times the points scored by him in the actual game.
                                                                            Also, the vice-captain gets one and a half times the points.
                                                                        </li>
                                                                    </ol>
                                                                </li>
                                                                <li class="mb-1"><b>What are credits?</b>
                                                                    Credits are the cost of a player. In-form and star players typically cost more credits, while
                                                                    inexperienced or non-regular players cost less.
                                                                </li>
                                                                <li class="mb-1"><b>What are Diam11Points?</b>
                                                                    Diam11 Points are earned by real-life players on the basis of their performances in a match. For
                                                                    example, players earn points for scoring runs, taking wickets, etc. (cricket), scoring goals,
                                                                    making assists, etc. (football) and similar actions for other sports. You can check out the
                                                                    Diam11 Points System under each sport for more details.
                                                                </li>
                                                                <li class="mb-1"><b>Can I edit my team after I create it?</b>
                                                                    You can edit your team as many times as you like before the match deadline.
                                                                </li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <FrontFooter {...this.props} />

                    </div>
                </div>

            </div>

        );
    }
}
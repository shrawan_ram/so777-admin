import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ApiExecute1, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Nav } from 'react-bootstrap';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';
import { API_BASE1 } from '../../configs/costant.config';
import Teen20 from '../../Components/games/Teen20';
import Aaa from '../../Components/games/Aaa';
import Ab from '../../Components/games/Ab';


export default class CasinoGameScreen extends Component {
    state = {
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        gametypes: [],
        user: {},
        category: {},
        game_slug: '',
        category_id: '',
        gametype_id: 5,
        games: [],
        subcategories: [],
        game_detail: {},
        gameCommponet: '',
        t1: {},
        t3: [],
        ar: [],
        br: [],
        loading: true,
    }

    onClickGametype = async (id) => {
        await this.setState({ gametype_id: id });
        this.gameList();
        this.allcategoryList();
    }
    onClickCategory = async (id) => {
        await this.setState({ category_id: id });
        this.gameList();
    }

    async componentDidMount() {

        // console.log('props', this.props);
        // console.log('slugs dfg', this.props.match.params.slug);
        // this.getGame(this.props.match.params.slug);
        let string = this.props.match.params.slug;
        let slug = '';
        slug = string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        console.log('sdfhsdj sgdifj dfsjdfn', slug);
        this.setState({ gameCommponet: slug });
        // setInterval(this.getGame, 1000);
        setTimeout(() => {
            this.setState({ loading: false });
        }, 2000);
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');

        this.setState({
            user: JSON.parse(user)
        });

        let game_slug = this.props.match.params.slug;

        if (game_slug) {
            this.state.game_slug = game_slug;
            // this.getCategory(game_slug);
        }

        $(document).on('click', '.casino-video-cards .casino-cards-shuffle', e => {
            $(e.target).closest(".casino-video-cards").toggleClass('hide-cards');
        })

        $(document).on('click', '.casino-video-right-icons .casino-video-rules-icon .fa-info-circle', e => {
            $('#casino-vieo-rules').removeClass('d-none');
            $('#casino-vieo-rules').addClass('d-block');
        })

        $(document).on('click', '.casino-vieo-rules .fa-times', e => {
            $('#casino-vieo-rules').addClass('d-none');
            $('#casino-vieo-rules').removeClass('d-block');
        })
        $(document).on('click', '#right-sidebar-id .casino-video-last-results .resulta', e => {
            $('#resultdemo').children('.modal').toggleClass('d-none d-block');
            $('#resultdemo_backdrop').toggleClass('d-block  d-none');
        })
        $(document).on('click', '#resultdemo .modal-header .close', e => {
            $('#resultdemo').children('.modal').toggleClass('d-none d-block');
            $('#resultdemo_backdrop').toggleClass('d-block  d-none');
        })

        $(document).on('click', '#resultdemo #resultdemo_backdrop', e => {
            $(e.target).closest('#resultdemo').children('.modal').toggleClass('d-none d-block');
            $(e.target).closest('#resultdemo_backdrop').toggleClass('d-block  d-none');
        })

    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        this.setState({ categories: api_response.data.results });
    }

    getCategory = async (slug) => {
        let category = await ApiExecute(`category/info/${slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.categoryList();
        this.gametypeList(category.data.id);
        this.allcategoryList(category.data.id);
    }

    allcategoryList = async (category_id = null, game_type_id = null) => {
        let id = category_id ? category_id : this.state.category.id;
        let game_id = game_type_id ? game_type_id : this.state.gametype_id;

        let api_response = await ApiExecute(`category?category_id=${id}&&game_type_id=${game_id}`, { method: 'GET' });
        this.setState({ subcategories: api_response.data.results });
    }

    gametypeList = async (id) => {
        let api_response = await ApiExecute(`gametype?category_id=${id}`, { method: 'GET' });
        this.setState({ gametypes: api_response.data.results });
        let lists = api_response.data.results;
        let gametype;
        for (let i in lists) {
            gametype = lists[0];
        }
    }


    getGame = async (slug = null) => {
        let s = slug ? slug : this.state.game_slug;
        let api_response = await ApiExecute1(`getdata/${s}`, { method: 'GET' });
        console.log('game_detail', api_response.data.data);
        // this.setState({
        //     game_detail: api_response.data.data
        // });
        // this.setState({ loading: true });

        api_response.data.data.t1.forEach((element, index) => {
            // console.log('game_detail', element);
            this.setState({ t1: element });

            // if (element.autotime != '0') {
            // console.log(element.autotime);
            // this.toggleClassTimer();
            // this.toggleClassTimer();
            // }
        });
        if (api_response.data.data.t3) {
            api_response.data.data.t3.forEach((element, index) => {
                var ar = [];
                ar = element.ar.split(',');
                this.setState({ ar: ar });
                var br = [];
                br = element.br.split(',');
                this.setState({ br: br });
            });
        }

    }

    gameList = async (gtype_id = null) => {
        let gametype = gtype_id ? gtype_id : this.state.gametype_id;
        let category = this.state.category_id;
        console.log('catid', category, 'gtypeid', gametype);

        let api_response = await ApiExecute(`game?category_id=${category}&&game_type_id=${gametype}`, { method: 'GET' });
        console.log('sdkjf iwjfi jsfkeed', api_response.data.results);

        this.setState({ games: api_response.data.results })
    }

    render() {
        return (

            <div id="app" >

                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />
                    {
                        this.state.refreshComponet
                            ?
                            <FrontSidebar {...this.props} gametype_id={this.state.gametype_id} />
                            :
                            <FrontSidebar {...this.props} gametype_id={this.state.gametype_id} />
                    }
                    {
                        !this.state.loading ?
                            <div class="main-container">
                                <FrontHeader {...this.props} />
                                <div class="center-main-content">
                                    <div class="news-bar d-none-desktop">
                                        <div class="news-title"><i class="fas fa-bell mr-2"></i> News</div>
                                        <marquee>We are happy to announce that we are bringing you a new world of gambling.</marquee>
                                    </div>
                                    <div class="upcoming-fixure d-none-desktop">
                                        <div class="fixure-title">Upcoming Fixtures</div>
                                        <div class="fixure-box-container">
                                            <div id="carousel3" data-ride="carousel" data-interval="3000" class="carousel vert slide">

                                            </div>
                                        </div>
                                    </div>

                                    <Teen20 />
                                    {/* <Aaa /> */}
                                </div>
                            </div>
                            : <div>Loading....</div>
                    }
                </div>
            </div >
        );
    }
}
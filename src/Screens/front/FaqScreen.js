import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

import ReactTypingEffect from 'react-typing-effect';
import { ApiExecute, ToastMessage } from '../../Api';

import $ from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Popup from 'reactjs-popup';
import Slider from "react-slick";
import { Nav } from 'react-bootstrap';
import TopHeader from '../../Components/TopHeader';
import FrontSidebar from '../../Components/FrontSidebar';
import FrontHeader from '../../Components/FrontHeader';
import AnimatedHeader from '../../Components/AnimatedHeader';
import FrontFooter from '../../Components/FrontFooter';


export default class FaqScreen extends Component {


    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');

        $('header.header').addClass('header-casino');

    }

    render() {
        return (
            <div id="app">
                <ToastContainer
                    position="top-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <div class="wrapper">

                    <AnimatedHeader {...this.props} />
                    <TopHeader {...this.props} />

                    <div class="main-container">
                        <FrontHeader {...this.props} />
                        <div class="logo-box d-none-mobile logo-casino">
                            <div class="logo"><a href="/sport" class=""><img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" class="img-fluid" /></a></div>
                        </div>
                        <div class="center-main-content report-container">
                            <div class="news-bar d-none-desktop">
                                <marquee>Launched Best Ever 8 Deck Casino 1 Card ONE-DAY.</marquee>
                                <div class="news-title"><img src="https://sitethemedata.com/v3/static/front/img/icons/speaker.svg" /></div>
                            </div>
                            <div class="upcoming-fixure d-none-desktop">
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container">
                                    <div id="carousel2" data-ride="carousel" data-interval="3000" class="carousel vert slide"></div>
                                </div>
                            </div>
                            <div class="report-box faq-container">
                                <div class="report-title">
                                    <div class="report-name">Frequently asked questions (FAQ)</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="faq-question-box">
                                            <h5>1. Follow this video tutorial when the game does not work in safari browser.</h5>
                                            <div>
                                                <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="faq-question-box">
                                            <h5>2. Follow this video tutorial when the game does not work in private browser</h5>
                                            <div class="casino-tabs">
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item"><a href="#chrome" data-toggle="tab" class="nav-link active">Chrome</a></li>
                                                    <li class="nav-item"><a href="#firefox" data-toggle="tab" class="nav-link">Firefox</a></li>
                                                    <li class="nav-item"><a href="#edge" data-toggle="tab" class="nav-link">Edge</a></li>
                                                    <li class="nav-item"><a href="#safari" data-toggle="tab" class="nav-link">Safari</a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div id="chrome" class="tab-pane active">
                                                    <div>
                                                        <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                                    </div>
                                                </div>
                                                <div id="firefox" class="tab-pane fade">
                                                    <div>
                                                        <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                                    </div>
                                                </div>
                                                <div id="edge" class="tab-pane fade">
                                                    <div>
                                                        <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                                    </div>
                                                </div>
                                                <div id="safari" class="tab-pane fade">
                                                    <div>
                                                        <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="faq-question-box">
                                            <h5>3.  Follow this video tutorial when the game does not work in apple product.</h5>
                                            <div class="casino-tabs">
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item"><a href="#mobile" data-toggle="tab" class="nav-link active">Settings for Mobile </a></li>
                                                    <li class="nav-item"><a href="#web" data-toggle="tab" class="nav-link">Settings for Desktop</a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div id="mobile" class="tab-pane active">
                                                    <div>
                                                        <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                                    </div>
                                                </div>
                                                <div id="web" class="tab-pane fade">
                                                    <div>
                                                        <p><a href="javascript:void(0);" role="button">Click here</a> to watch the video tutorial!</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <FrontFooter {...this.props} />

                    </div>
                </div>

            </div>

        );
    }
}
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';



export default class HomeScreen extends Component {


    render() {
        return (
            <div id="app">
                <div class="wrapper">
                    <div class="about-us-container">
                        <div class="container">
                            <div class="text-center logo">
                                <img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" />
                            </div>
                            <h2 class="page-title">New to casido777</h2>
                            <h4>Which are the games I can play on casido777?</h4>
                            <h5>
                                You can bet on any live sports. There in one single wallet. Once you deposit amount, you can choose from any live casino, sports betting or slots games. We have curated selection of Indian Casino Games like Andar Bahar, 20-20 Cricket, Bollywood Casino, Teen Patti, and much more
                            </h5>
                            <h4>Navigating the casido777 Website</h4>
                            <h5>Finding your way around at casido777 is easy. Whether on a mobile device or desktop, you can navigate to any part of the website via the menu bar at the top of your screen. On casido777, all sports are listed in the top navigation bar of the screen, with key events highlighted in the ‘Quick Links’ section, or ‘Highlights’ area
                                on the home page. All ‘Live’ or ‘In Play’ events are indicated by the ‘Live’ symbol, meaning you can bet on
                                the action as it unfolds. Use any of our side navigation links or top navigation links to choose your
                                favourite sports for betting or your favourite live casino games
                            </h5>
                            <h4>How to contact customer support</h4>
                            <h5>We at casido777 Casino are proud for our live customer support. If you need any help understanding on how to play on casido777 or have any query regarding our services kindly contact us on our
                                <a href="https://wa.me/">Whatsapp Customer Care</a>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
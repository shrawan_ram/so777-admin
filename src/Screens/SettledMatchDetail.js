import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { ApiExecute, ApiExecute1, ToastMessage } from '../Api';
import { Admin_prifix } from "../configs/costant.config";

export default class SettledMatchDetail extends Component {

    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        allselected: 0,
        users: [],
        userIds: [],
        matchType: null,
        s: '',
        matches: [],
        match_detail: {},
        odds_market: [],
        markets: {},
        bats: null,
        settled_bats: null,
        loading: true,
    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }
    userList = async (id = null) => {
        let user_id
        if (id) {
            user_id = id;
        } else {
            let user = sessionStorage.getItem('@user');
            user = JSON.parse(user);
            user_id = user.id;
        }
        let api_response = await ApiExecute(`user?admin_id=${user_id}`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }

    userListwithSearch = async () => {
        console.log('sdfjhdj', this.state.s);
        let api_response = await ApiExecute(`user?s=${this.state.s}`, { method: 'GET' });
        console.log('search', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }

    componentDidUpdate(prevProps) {
        // console.log('props:', this.props.type, prevProps.type);
        if (this.props.match.params.type !== prevProps.match.params.type) {
            let id = this.props.match.params.id;
            let type = this.props.match.params.type;
            console.log(id);

            this.getMatchDetail(id);
            this.settle_bats(id);
            this.getMatchBats(id);

            // setTimeout(() => {
            //     this.refresh();
            // }, 2000);
        }
    }
    refresh() {
        let id = this.props.match.params.id;
        this.getMatchDetail(id);
        this.settle_bats(id);
        this.getMatchBats(id);
    }
    selectId(id) {
        if (this.state.userIds.includes(id)) {
            var index = this.state.userIds.indexOf(id);
            this.state.userIds.splice(index, 1);
        } else {
            this.setState({ userIds: this.state.userIds.concat(id) });
        }
    }
    selectAllId = (e) => {
        console.log('sdlkfjdlkj : ', e.target.checked);
        let array = [];
        if (e.target.checked) {
            this.state.users.forEach(element => {
                console.log('element', element.id);
                array.push(element.id);
            });
            this.setState({ userIds: array });
        } else {
            this.setState({ userIds: array });
        }
        console.log('all selected', this.state.userIds);
    }

    deleteAll = async () => {
        if (this.state.userIds.length) {
            let data = this.state.userIds.join(",");
            console.log('data', data);
            let api_response = await ApiExecute("remove-user", { method: 'POST', data: data });
            console.log('api_response', api_response);
            ToastMessage('success', api_response.msg);
            this.userList();
        } else {
            ToastMessage('error', 'Please select atleast one !');
        }
    }
    searchByName(status) {
        if (status) {
            this.userListwithSearch();
        } else {
            this.setState({ s: '' });
            this.userList();
        }
    }

    settle_bats = async (id) => {
        let eId = id;
        if (eId != undefined) {
            let market_response = await ApiExecute(`settled?event_id=${eId}`, { method: 'GET' });
            console.log('settled_bats', market_response.data.results);
            this.setState({ settled_bats: market_response.data.results });
            // this.setState({ loading: false });
        }
    }

    getMatches = async (id) => {
        this.setState({ loading: true });
        let api_response;
        if (id == 4) {
            api_response = await ApiExecute1(`getcricketmatches`, { method: 'GET' });
        } else if (id == 2) {
            api_response = await ApiExecute1(`gettennismatches`, { method: 'GET' });
        } else {
            api_response = await ApiExecute1(`getsoccermatches`, { method: 'GET' });
        }
        this.setState({ matches: api_response.data });
        console.log('matches', api_response.data);
        this.setState({ loading: false });
    }

    getMatchDetail = async (id) => {
        this.setState({ loading: true });
        let api_response = await ApiExecute(`match/${id}`, { method: 'GET' });
        this.setState({ match_detail: api_response.data });
        console.log('match_detail', api_response.data);
        this.setState({ loading: false });
    }
    getMatchBats = async (id) => {
        let api_response = await ApiExecute(`bat/?event_id=${id}`, { method: 'GET' });
        this.setState({ bats: api_response.data.results });
        console.log('bats', api_response.data.results);
    }

    handleInput = (val, i) => {
        let { settled_bats } = this.state;
        settled_bats[i] = val;
        this.setState({ settled_bats });
    }

    render() {
        let self = this;
        const { isOpened } = this.state;
        const height = 100;
        const { userIds } = this.state;
        const { search } = this.state

        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");
        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }

        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Matches Details </h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Matches List</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row account-list">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            {
                                                this.state.settled_bats && this.state.settled_bats.length ?
                                                    <>
                                                        {
                                                            this.state.settled_bats && this.state.settled_bats.filter((item) => item.type == 'MATCH_ODDS').map((element, index) => {
                                                                return (
                                                                    <option value={element.sid}>{element.name}</option>
                                                                )
                                                            })
                                                        }
                                                        <div class="mb-3">
                                                            <h4 class="mb-3">MATCH_ODDS</h4>
                                                            <div class="row">
                                                                <div class="col-8">
                                                                    <select class="form-control">
                                                                        {
                                                                            this.state.settled_bats && this.state.settled_bats.filter((item) => item.type == 'MATCH_ODDS').map((element, index) => {
                                                                                return (
                                                                                    <option value={element.sid}>{element.name}</option>
                                                                                )
                                                                            })
                                                                        }
                                                                    </select>
                                                                </div>
                                                                <div class="col-4">
                                                                    <Button type="submit">Save</Button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                    : ''
                                            }
                                            {
                                                this.state.settled_bats && this.state.settled_bats.length ?
                                                    <>
                                                        {
                                                            this.state.settled_bats && this.state.settled_bats.filter((item) => item.type == 'Bookmaker').map((element, index) => {
                                                                return (
                                                                    <option value={element.sid}>{element.name}</option>
                                                                )
                                                            })
                                                        }
                                                        <div class="mb-3">
                                                            <h4 class="mb-3">Bookmaker</h4>
                                                            <div class="row">
                                                                <div class="col-8">
                                                                    <select class="form-control">
                                                                        {
                                                                            this.state.settled_bats && this.state.settled_bats.filter((item) => item.type == 'Bookmaker').map((element, index) => {
                                                                                return (
                                                                                    <option value={element.sid}>{element.name}</option>
                                                                                )
                                                                            })
                                                                        }
                                                                    </select>
                                                                </div>
                                                                <div class="col-4">
                                                                    <Button type="submit">Save</Button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                    : ''
                                            }
                                            {
                                                this.state.settled_bats && this.state.settled_bats.length ?
                                                    <>
                                                        <div class="mb-3">
                                                            <h4 class="mb-3">Fancy</h4>
                                                            <div class="row">
                                                                {
                                                                    this.state.settled_bats && this.state.settled_bats.filter((item) => item.type == 'Fancy').map((element, index) => {
                                                                        return (
                                                                            <div class="col-6 mb-3">
                                                                                <div class="row">
                                                                                    <div class="col-6">
                                                                                        {element.name}
                                                                                    </div>
                                                                                    <div class="col-2">
                                                                                        {/* <input class="form-control result" type="text" value={element.result} onInput={(val) => self.handleInput(val, i)} ></input> */}
                                                                                        <input class="form-control result" type="text" value={element.result} ></input>

                                                                                        <input type="hidden" class="event_type" value={element.gtype}></input>
                                                                                        <input type="hidden" class="sid" value={element.sid}></input>
                                                                                        <input type="hidden" class="name" value={element.nat}></input>

                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <Button type="submit" onClick={this.fancy_Submit}>Save</Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </>
                                                    : ''
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div >
        );
    }
}
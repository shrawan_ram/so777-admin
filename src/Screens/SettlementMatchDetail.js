import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { ApiExecute, ApiExecute1, ToastMessage } from '../Api';
import { Admin_prifix } from "../configs/costant.config";
import $ from "jquery";

export default class SettlementMatchDetail extends Component {

    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        allselected: 0,
        users: [],
        userIds: [],
        matchType: null,
        s: '',
        matches: [],
        settled_bats: [],
        match_detail: {},
        odds_market: [],
        markets: {},
        loading: true,
        match_type: null,
        match_id: null,
        val: '20',
    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }
    userList = async (id = null) => {
        let user_id
        if (id) {
            user_id = id;
        } else {
            let user = sessionStorage.getItem('@user');
            user = JSON.parse(user);
            user_id = user.id;
        }
        let api_response = await ApiExecute(`user?admin_id=${user_id}`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }

    userListwithSearch = async () => {
        console.log('sdfjhdj', this.state.s);
        let api_response = await ApiExecute(`user?s=${this.state.s}`, { method: 'GET' });
        console.log('search', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }
    refresh() {
        console.log('this is refresh function.');
        let id = this.props.match.params.id;
        this.settle_bats(id);
        this.checkGame(id);
    }
    selectId(id) {
        console.log('sdfjkdksl');
        if (this.state.userIds.includes(id)) {
            var index = this.state.userIds.indexOf(id);
            this.state.userIds.splice(index, 1);
        } else {
            this.setState({ userIds: this.state.userIds.concat(id) });
        }
    }
    selectAllId = (e) => {
        console.log('sdlkfjdlkj : ', e.target.checked);
        let array = [];
        if (e.target.checked) {
            this.state.users.forEach(element => {
                console.log('element', element.id);
                array.push(element.id);
            });
            this.setState({ userIds: array });
        } else {
            this.setState({ userIds: array });
        }
        console.log('all selected', this.state.userIds);
    }

    deleteAll = async () => {
        if (this.state.userIds.length) {
            let data = this.state.userIds.join(",");
            console.log('data', data);
            let api_response = await ApiExecute("remove-user", { method: 'POST', data: data });
            console.log('api_response', api_response);
            ToastMessage('success', api_response.msg);
            this.userList();
        } else {
            ToastMessage('error', 'Please select atleast one !');
        }
    }
    searchByName(status) {
        if (status) {
            this.userListwithSearch();
        } else {
            this.setState({ s: '' });
            this.userList();
        }
    }
    getMatches = async (id) => {
        this.setState({ loading: true });
        let api_response;
        if (id == 4) {
            api_response = await ApiExecute1(`getcricketmatches`, { method: 'GET' });
        } else if (id == 2) {
            api_response = await ApiExecute1(`gettennismatches`, { method: 'GET' });
        } else {
            api_response = await ApiExecute1(`getsoccermatches`, { method: 'GET' });
        }
        this.setState({ matches: api_response.data });
        console.log('matches', api_response.data);
        this.setState({ loading: false });
    }

    // match_details = async (id) => {
    //     let api_response = await ApiExecute1(`oddslist?sport_id=4`, { method: 'GET' });
    //     console.log('sdjdfj lk oidfjidfj');
    //     api_response.data.data.forEach(element => {
    //         console.log('jcheck', element);
    //         if (element.eventId == id) {
    //             this.setState({ match_detail: element });
    //             console.log('jdfdfkfk ndffj', element);
    //             this.odds_details(element.marketId);

    //             setInterval(this.odds_details, 1000);
    //         }
    //     });
    // }

    checkGame = async (id) => {
        let api_response = await ApiExecute1(`getcricketmatches`, { method: 'GET' });
        api_response.data.forEach(element => {

            this.setState({ loading: true });
            console.log('sd fjsd figjdsfgijdogi', element.gameId);
            if (element.gameId == id) {

                this.setState({ match_detail: element });
                console.log('jdfdfkfk ndffj', element);
                this.odds_details(element.marketId);

                setInterval(this.odds_details, 1000);

                this.fancy_details(element.gameId);
                this.setState({ match_status: element });
                console.log('match_status', element);

                setInterval(this.fancy_details, 1000);
            }

            this.setState({ loading: false });
        });
    }
    odds_details = async (id = null) => {
        let mId = id ? id : this.state.match_detail.marketId;
        if (mId != undefined) {
            let market_response = await ApiExecute1(`getdata?market_id=${mId}`, { method: 'GET' });
            console.log('azfksdkm skdl', market_response);
            if (market_response && market_response.data) {
                if (market_response.data && market_response.data.length) {
                    if (market_response.data[0] && market_response.data[0].length) {
                        if (market_response.data[0][0] && market_response.data[0][0].length) {
                            this.setState({ odds_market: market_response.data[0][0] });
                        }
                    }
                } else {
                    console.log('data.length, record not found.');
                }
            } else {
                console.log('market_response, record not found.');
            }
        }
    }

    fancy_details = async (id = null) => {
        let eId = id ? id : this.state.match_detail.eventId;
        if (eId != undefined) {
            let market_response = await ApiExecute1(`getfancydata?event_id=${eId}`, { method: 'GET' });
            console.log('matches', market_response.data);
            this.setState({ markets: market_response.data });
            this.setState({ loading: false });
        }
    }

    settle_bats = async (id) => {
        let eId = id;
        if (eId != undefined) {
            let market_response = await ApiExecute(`settled?event_id=${eId}`, { method: 'GET' });
            console.log('settled_bats', market_response.data.results);
            this.setState({ settled_bats: market_response.data.results });
            // this.setState({ loading: false });
        }
    }

    match_Odds_Submit = async (e) => {
        let s_id = $(e.target).closest('.col-4').siblings().children('select').val();
        let event_type = $(e.target).closest('.col-4').siblings().children('.event_type').val();
        let team1 = $(e.target).closest('.col-4').siblings().children('.team1').val();
        let team2 = $(e.target).closest('.col-4').siblings().children('.team2').val();
        let team1_sid = $(e.target).closest('.col-4').siblings().children('.team1_sid').val();
        let team2_sid = $(e.target).closest('.col-4').siblings().children('.team2_sid').val();

        let {
            match_id,
            match_type,
        } = this.state;

        if (s_id === '') {
            ToastMessage('error', 'Please select team !');
        } else {
            let data = {
                event_id: match_id,
                event_type: match_type,
                sid: s_id,
                name: '',
                team1: team1,
                team2: team2,
                team1_sid: team1_sid,
                team2_sid: team2_sid,
                result: s_id,
                type: event_type,
            }
            if (this.state.settled_bats && this.state.settled_bats.length && this.state.settled_bats.filter(bats => bats.event_id == match_id && bats.type == event_type).length) {
                this.state.settled_bats && this.state.settled_bats.length && this.state.settled_bats.filter(bats => bats.event_id == match_id && bats.type == event_type).forEach(async (ele) => {
                    if (ele.sid != s_id) {
                        console.log('ksd n ijdfoijgd fjdfigjd i');
                        let api_response = await ApiExecute(`bat?event_id=${match_id}&type=${event_type}&limit=10000`, { method: 'GET' });
                        console.log('lists', api_response.data.results);
                        api_response.data.results.forEach(async (element) => {
                            console.log('details', element);
                            console.log('user_id', element.user);

                            let wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'GET' });
                            if (wallet_response.data && wallet_response.data.length) {
                                let main_wallet = wallet_response.data[0];
                                let exposure = wallet_response.data[1];
                                let data;
                                let data1;
                                if (s_id == element.sid) {
                                    if (element.bat_type == 'back') {
                                        data = {
                                            user_id: element.user,
                                            amount: (parseInt(main_wallet.amount) + parseInt(element.amount)) + parseInt(element.profit),
                                            type: 'main',
                                        }
                                        console.log('bat_amount', data);

                                        let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                    } else {
                                        data = {
                                            user_id: element.user,
                                            amount: (parseInt(main_wallet.amount) - (parseInt(element.amount)) - parseInt(element.profit)),
                                            type: 'main',
                                        }

                                        console.log('bat_amount', data);
                                        let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                    }
                                } else {
                                    if (element.bat_type == 'back') {
                                        data = {
                                            user_id: element.user,
                                            amount: (parseInt(main_wallet.amount) - parseInt(element.amount)) - parseInt(element.profit),
                                            type: 'main',
                                        }

                                        console.log('bat_amount', data);
                                        let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                    } else {
                                        data = {
                                            user_id: element.user,
                                            amount: parseInt(main_wallet.amount) + parseInt(element.amount) + parseInt(element.loss) + parseInt(element.profit),
                                            type: 'main',
                                        }

                                        console.log('bat_amount', data);
                                        let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                    }
                                }
                                console.log('data', data);
                            }
                        });
                        console.log('data', data);
                        let settle_response = await ApiExecute(`settled/${ele.id}`, { method: 'PUT', data: data });

                        console.log('settle_response', settle_response);
                        if (settle_response.data.status) {
                            ToastMessage('success', settle_response.data.message);
                        }
                        else {
                            if (settle_response.data.errors == undefined && settle_response.data.errors == null) {
                                ToastMessage('error', settle_response.data.message);
                            } else {
                                ToastMessage('error', settle_response.data.errors[0]);
                            }
                        }
                    }
                });
            } else {
                let api_response = await ApiExecute(`bat?event_id=${match_id}&type=${event_type}&limit=10000`, { method: 'GET' });
                console.log('lists', api_response.data.results);
                api_response.data.results.forEach(async (element) => {
                    console.log('details', element);
                    console.log('user_id', element.user);

                    let wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'GET' });
                    if (wallet_response.data && wallet_response.data.length) {
                        let main_wallet = wallet_response.data[0];
                        let exposure = wallet_response.data[1];
                        let data;
                        let data1;
                        if (s_id == element.sid) {
                            if (element.bat_type == 'back') {
                                data = {
                                    user_id: element.user,
                                    amount: parseInt(main_wallet.amount) + (parseInt(element.amount) + parseInt(element.profit)),
                                    type: 'main',
                                }
                                data1 = {
                                    user_id: element.user,
                                    amount: parseInt(exposure.amount) - parseInt(element.amount),
                                    type: 'exposure',
                                }

                                let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                            } else {
                                data1 = {
                                    user_id: element.user,
                                    amount: parseInt(exposure.amount) - parseInt(element.loss),
                                    type: 'exposure',
                                }

                                let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                            }
                        } else {
                            if (element.bat_type == 'back') {
                                data = {
                                    user_id: element.user,
                                    amount: parseInt(exposure.amount) - parseInt(element.amount),
                                    type: 'exposure',
                                }
                                let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                            } else {
                                data = {
                                    user_id: element.user,
                                    amount: parseInt(main_wallet.amount) + (parseInt(element.amount) + parseInt(element.profit)),
                                    type: 'main',
                                }
                                data1 = {
                                    user_id: element.user,
                                    amount: parseInt(exposure.amount) - parseInt(element.amount),
                                    type: 'exposure',
                                }

                                let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                            }
                        }
                        console.log('data', data);
                    }
                });
                console.log('data', data);
                let settle_response = await ApiExecute("add-settled", { method: 'POST', data: data });

                console.log('settle_response', settle_response);
                if (settle_response.data.status) {
                    ToastMessage('success', settle_response.data.message);
                }
                else {
                    if (settle_response.data.errors == undefined && settle_response.data.errors == null) {
                        ToastMessage('error', settle_response.data.message);
                    } else {
                        ToastMessage('error', settle_response.data.errors[0]);
                    }
                }
            }
        }
    }

    fancy_Submit = async (e) => {
        let s_id = $(e.target).closest('.col-4').siblings().children('.sid').val();
        let result = $(e.target).closest('.col-4').siblings().children('.result').val();
        let name = $(e.target).closest('.col-4').siblings().children('.name').val();
        let event_type = $(e.target).closest('.col-4').siblings().children('.event_type').val();
        console.log('sdgjhdfj', s_id, 'sdgjhdfj', result, 'sdgjhdfj', event_type);
        let {
            match_id,
            match_type,
        } = this.state;

        if (result === '') {
            ToastMessage('error', 'Please enter value !');
        } else {
            let data = {
                event_id: match_id,
                event_type: match_type,
                sid: s_id,
                name: name,
                result: result,
                type: event_type,
            }
            console.log('data', data);
            if (this.state.settled_bats && this.state.settled_bats.length && this.state.settled_bats.filter(bats => bats.event_id == match_id && bats.type == event_type).length) {
                this.state.settled_bats && this.state.settled_bats.length && this.state.settled_bats.filter(bats => bats.event_id == match_id && bats.type == event_type).forEach(async (ele) => {
                    if (ele.sid == s_id) {
                        let api_response = await ApiExecute(`bat?event_id=${match_id}&type=${event_type}&limit=10000`, { method: 'GET' });
                        console.log('lists', api_response.data.results);
                        api_response.data.results.forEach(async (element) => {
                            console.log('details', element);
                            console.log('user_id', element.user);

                            let wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'GET' });
                            if (wallet_response.data && wallet_response.data.length) {
                                let main_wallet = wallet_response.data[0];
                                let exposure = wallet_response.data[1];
                                let data;
                                let data1;
                                if (s_id == element.sid) {
                                    if (element.bat_type == 'back') {
                                        if (element.rate < result) {
                                            data = {
                                                user_id: element.user,
                                                amount: parseInt(main_wallet.amount) - (parseInt(element.amount) + parseInt(element.profit)),
                                                type: 'main',
                                            }

                                            let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                        } else {
                                            data = {
                                                user_id: element.user,
                                                amount: parseInt(main_wallet.amount) + (parseInt(element.amount) + parseInt(element.profit)),
                                                type: 'main',
                                            }

                                            let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                        }
                                    } else {
                                        if (element.rate >= result) {
                                            data = {
                                                user_id: element.user,
                                                amount: parseInt(main_wallet.amount) - (parseInt(element.amount) + parseInt(element.profit)),
                                                type: 'main',
                                            }

                                            let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                        } else {
                                            data = {
                                                user_id: element.user,
                                                amount: parseInt(main_wallet.amount) + (parseInt(element.amount) + parseInt(element.profit)),
                                                type: 'main',
                                            }

                                            let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                        }
                                    }
                                    console.log('data', data);
                                }
                                console.log('data', data);
                            }
                        });
                        console.log('data', data);
                        let settle_response = await ApiExecute(`settled/${ele.id}`, { method: 'PUT', data: data });

                        console.log('settle_response', settle_response);
                        if (settle_response.data.status) {
                            ToastMessage('success', settle_response.data.message);
                        }
                        else {
                            if (settle_response.data.errors == undefined && settle_response.data.errors == null) {
                                ToastMessage('error', settle_response.data.message);
                            } else {
                                ToastMessage('error', settle_response.data.errors[0]);
                            }
                        }
                    }
                });
            } else {
                let api_response = await ApiExecute(`bat?event_id=${match_id}&type=${event_type}&limit=10000`, { method: 'GET' });
                console.log('lists', api_response.data.results);
                api_response.data.results.forEach(async (element) => {
                    console.log('details', element);
                    console.log('user_id', element.user);

                    let wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'GET' });
                    if (wallet_response.data && wallet_response.data.length) {
                        let main_wallet = wallet_response.data[0];
                        let exposure = wallet_response.data[1];
                        let data;
                        let data1;
                        if (s_id == element.sid) {
                            if (element.bat_type == 'back') {
                                if (element.rate < result) {
                                    data = {
                                        user_id: element.user,
                                        amount: parseInt(main_wallet.amount) + (parseInt(element.amount) + parseInt(element.profit)),
                                        type: 'main',
                                    }
                                    data1 = {
                                        user_id: element.user,
                                        amount: parseInt(exposure.amount) - parseInt(element.amount),
                                        type: 'exposure',
                                    }

                                    let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                    let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                                } else {
                                    data1 = {
                                        user_id: element.user,
                                        amount: parseInt(exposure.amount) - parseInt(element.amount),
                                        type: 'exposure',
                                    }

                                    let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                                }
                            } else {
                                if (element.rate >= result) {
                                    data = {
                                        user_id: element.user,
                                        amount: parseInt(main_wallet.amount) + (parseInt(element.amount) + parseInt(element.profit)),
                                        type: 'main',
                                    }
                                    data1 = {
                                        user_id: element.user,
                                        amount: parseInt(exposure.amount) - parseInt(element.amount),
                                        type: 'exposure',
                                    }

                                    let update_wallet_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data });
                                    let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                                } else {
                                    data1 = {
                                        user_id: element.user,
                                        amount: parseInt(exposure.amount) - parseInt(element.amount),
                                        type: 'exposure',
                                    }

                                    let update_exposure_response = await ApiExecute(`wallet/${element.user}`, { method: 'PUT', data: data1 });
                                }
                            }
                            console.log('data', data);
                        }
                    }
                });
                console.log('data', data);
                let settle_response = await ApiExecute("add-settled", { method: 'POST', data: data });

                console.log('settle_response', settle_response);
                if (settle_response.data.status) {
                    ToastMessage('success', settle_response.data.message);
                }
                else {
                    if (settle_response.data.errors == undefined && settle_response.data.errors == null) {
                        ToastMessage('error', settle_response.data.message);
                    } else {
                        ToastMessage('error', settle_response.data.errors[0]);
                    }
                }
            }
        }
    }
    handleInput = (val, i) => {
        let { settled_bats } = this.state;
        settled_bats[i] = val;
        this.setState({ settled_bats });
    }

    componentDidUpdate(prevProps) {
        // console.log('props:', this.props.type, prevProps.type);
        if (this.props.match.params.type !== prevProps.match.params.type) {
            // fetch or other component tasks necessary for rendering
            let self = this;
            let id = this.props.match.params.id;
            this.setState({ match_id: this.props.match.params.id });
            this.setState({ match_type: this.props.match.params.type });
            console.log('mounted.');
            console.log(id);
            this.settle_bats(id);
            this.checkGame(id);
        }

        // setTimeout(() => {
        //     self.refresh();
        // }, 1000);
    }

    render() {
        let self = this;

        const { isOpened } = this.state;
        const height = 100;
        const { userIds } = this.state;
        const { search } = this.state

        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");

        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }

        // console.log('Render done');

        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Matches Details</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Matches List</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row account-list">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            {
                                                this.state.markets && this.state.markets.t1 && this.state.markets.t1.length ?
                                                    <div class="mb-3">
                                                        {
                                                            this.state.markets && this.state.markets.t1 && this.state.markets.t1[0].map((element, index) => {
                                                                if (index == 0) {
                                                                    return (
                                                                        <>
                                                                            <h4 class="mb-3">{element.mname}</h4>
                                                                        </>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                        {/* <form> */}
                                                        <div class="row">
                                                            <div class="col-8">
                                                                <select class="form-control">
                                                                    <option value=''>Select Team</option>
                                                                    {
                                                                        this.state.markets && this.state.markets.t1 && this.state.markets.t1[0].map((element, index) => {
                                                                            return (
                                                                                <>
                                                                                    {
                                                                                        this.state.settled_bats && this.state.settled_bats.filter(bats => bats.type == element.mname).length ?
                                                                                            <>
                                                                                                {
                                                                                                    this.state.settled_bats && this.state.settled_bats.map(e => {
                                                                                                        if (element.mname == e.type) {
                                                                                                            return (
                                                                                                                <>
                                                                                                                    {
                                                                                                                        e.sid == element.sid
                                                                                                                            ?
                                                                                                                            <option value={element.sid} selected>{element.nat}</option>
                                                                                                                            :
                                                                                                                            <option value={element.sid}>{element.nat}</option>
                                                                                                                    }
                                                                                                                </>
                                                                                                            )
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            </>
                                                                                            :
                                                                                            <option value={element.sid}>{element.nat}</option>
                                                                                    }
                                                                                </>
                                                                            )
                                                                        })
                                                                    }
                                                                </select>
                                                                {
                                                                    this.state.markets && this.state.markets.t1 && this.state.markets.t1.map((element, index) => {
                                                                        if (index == 0) {
                                                                            return (
                                                                                <>
                                                                                    <input type="hidden" class="event_type" value={element.mname}></input>
                                                                                    <input type="hidden" class="team1" value={element.nat}></input>
                                                                                    <input type="hidden" class="team1_sid" value={element.sid}></input>
                                                                                </>
                                                                            )
                                                                        }
                                                                        if (index == 1) {
                                                                            return (
                                                                                <>
                                                                                    <input type="hidden" class="team2" value={element.nat}></input>
                                                                                    <input type="hidden" class="team2_sid" value={element.sid}></input>
                                                                                </>
                                                                            )
                                                                        }
                                                                    })
                                                                }
                                                                <input type="hidden" class="" value=""></input>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="btn" style={{ backgroundColor: '#23292e', color: '#fff' }} type="submit" onClick={this.match_Odds_Submit}>Save</div>
                                                            </div>
                                                        </div>
                                                        {/* </form> */}
                                                    </div>
                                                    : ''
                                            }
                                            {
                                                this.state.markets && this.state.markets.t2 && this.state.markets.t2.length ?
                                                    <div class="mb-3">
                                                        <h4 class="mb-3">Bookmaker</h4>
                                                        <div class="row">
                                                            <div class="col-8">
                                                                <select class="form-control">
                                                                    <option value=''>Select Team</option>
                                                                    {
                                                                        this.state.markets && this.state.markets.t2 && this.state.markets.t2.map((element) => {
                                                                            return (
                                                                                <>
                                                                                    {
                                                                                        element && element.bm1 && element.bm1.map((e) => {
                                                                                            return (
                                                                                                <>
                                                                                                    {
                                                                                                        this.state.settled_bats && this.state.settled_bats.filter(bats => bats.type == 'Bookmaker').length ?
                                                                                                            <>
                                                                                                                {
                                                                                                                    this.state.settled_bats && this.state.settled_bats.map(t => {
                                                                                                                        if ('Bookmaker' == t.type) {
                                                                                                                            return (
                                                                                                                                <>
                                                                                                                                    {
                                                                                                                                        t.sid == e.sid
                                                                                                                                            ?
                                                                                                                                            <option value={e.sid} selected>{e.nat}</option>
                                                                                                                                            :
                                                                                                                                            <option value={e.sid}>{e.nat}</option>
                                                                                                                                    }
                                                                                                                                </>
                                                                                                                            )
                                                                                                                        }

                                                                                                                    })
                                                                                                                }
                                                                                                            </>
                                                                                                            :
                                                                                                            <option value={e.sid}>{e.nat}</option>
                                                                                                    }
                                                                                                </>
                                                                                            )
                                                                                        })
                                                                                    }
                                                                                </>
                                                                            )
                                                                        })
                                                                    }
                                                                </select>
                                                                <input type="hidden" class="event_type" value="Bookmaker"></input>
                                                                <input type="hidden" class="" value=""></input>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="btn" style={{ backgroundColor: '#23292e', color: '#fff' }} type="submit" onClick={this.match_Odds_Submit}>Save</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    : ''
                                            }
                                            {
                                                this.state.markets && this.state.markets.t3 && this.state.markets.t3.length ?
                                                    <div class="mb-3">
                                                        {
                                                            this.state.markets && this.state.markets.t3 && this.state.markets.t3.map((element, index) => {
                                                                if (index == 0) {
                                                                    return (
                                                                        <>
                                                                            <h4 class="mb-3">
                                                                                {element.gtype}
                                                                            </h4>
                                                                        </>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                        <div class="row">
                                                            {
                                                                this.state.markets && this.state.markets.t3 && this.state.markets.t3.map((element) => {
                                                                    return (
                                                                        <>
                                                                            {
                                                                                this.state.settled_bats && this.state.settled_bats.filter(bats => bats.type == element.gtype).length ?
                                                                                    <>
                                                                                        {
                                                                                            this.state.settled_bats && this.state.settled_bats.map((e, i) => {
                                                                                                if (element.gtype == e.type) {
                                                                                                    return (
                                                                                                        <>
                                                                                                            {
                                                                                                                e.sid == element.sid
                                                                                                                    ?
                                                                                                                    <div class="col-6 mb-3">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-6">
                                                                                                                                {element.nat}
                                                                                                                            </div>
                                                                                                                            <div class="col-2">
                                                                                                                                {/* <input class="form-control result" type="text" value={e.result} onInput={(val) => self.handleInput(val, i)}></input> */}
                                                                                                                                <input class="form-control result" type="text" value={e.result}></input>
                                                                                                                                <input type="hidden" class="event_type" value={element.gtype}></input>
                                                                                                                                <input type="hidden" class="sid" value={element.sid}></input>
                                                                                                                                <input type="hidden" class="name" value={element.nat}></input>
                                                                                                                            </div>
                                                                                                                            <div class="col-4">
                                                                                                                                <Button type="submit" onClick={this.fancy_Submit}>Save</Button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        {/* // <option value={element.sid} selected>{element.nat}</option> */}
                                                                                                                    </div>
                                                                                                                    :
                                                                                                                    <div class="col-6 mb-3">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-6">
                                                                                                                                {element.nat}
                                                                                                                            </div>
                                                                                                                            <div class="col-2">
                                                                                                                                <input class="form-control result" type="text"></input>

                                                                                                                                <input type="hidden" class="event_type" value={element.gtype}></input>
                                                                                                                                <input type="hidden" class="sid" value={element.sid}></input>
                                                                                                                                <input type="hidden" class="name" value={element.nat}></input>

                                                                                                                            </div>
                                                                                                                            <div class="col-4">
                                                                                                                                <Button type="submit" onClick={this.fancy_Submit}>Save</Button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        {/* // <option value={element.sid}>{element.nat}</option> */}
                                                                                                                    </div>
                                                                                                            }
                                                                                                        </>
                                                                                                    )
                                                                                                }

                                                                                            })
                                                                                        }
                                                                                    </>
                                                                                    :
                                                                                    ''
                                                                            }
                                                                        </>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                    : ''
                                            }
                                            {
                                                this.state.markets && this.state.markets.t4 && this.state.markets.t4.length ?
                                                    <div class="mb-3">
                                                        {
                                                            this.state.markets && this.state.markets.t4 && this.state.markets.t4.map((element, index) => {
                                                                if (index == 0) {
                                                                    return (
                                                                        <>
                                                                            <h4 class="mb-3">
                                                                                {element.gtype}
                                                                            </h4>
                                                                        </>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                        <div class="row">
                                                            {
                                                                this.state.markets && this.state.markets.t4 && this.state.markets.t4.map((element) => {
                                                                    return (
                                                                        <>
                                                                            {
                                                                                this.state.settled_bats && this.state.settled_bats.filter(bats => bats.type == element.gtype).length ?
                                                                                    <>
                                                                                        {
                                                                                            this.state.settled_bats && this.state.settled_bats.map(e => {
                                                                                                if (element.gtype == e.type) {
                                                                                                    console.log('sdfjd', e);
                                                                                                    return (
                                                                                                        <>
                                                                                                            {
                                                                                                                e.sid == element.sid
                                                                                                                    ?
                                                                                                                    <div class="col-6 mb-3">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-6">
                                                                                                                                {element.nat}
                                                                                                                            </div>
                                                                                                                            <div class="col-2">
                                                                                                                                <input class="form-control result" type="text" value={e.result}></input>
                                                                                                                                <input type="hidden" class="event_type" value={element.gtype}></input>
                                                                                                                                <input type="hidden" class="sid" value={element.sid}></input>
                                                                                                                                <input type="hidden" class="name" value={element.nat}></input>
                                                                                                                            </div>
                                                                                                                            <div class="col-4">
                                                                                                                                <Button type="submit" onClick={this.fancy_Submit}>Save</Button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        {/* // <option value={element.sid} selected>{element.nat}</option> */}
                                                                                                                    </div>
                                                                                                                    :
                                                                                                                    <div class="col-6 mb-3">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-6">
                                                                                                                                {element.nat}
                                                                                                                            </div>
                                                                                                                            <div class="col-2">
                                                                                                                                <input class="form-control result" type="text"></input>

                                                                                                                                <input type="hidden" class="event_type" value={element.gtype}></input>
                                                                                                                                <input type="hidden" class="sid" value={element.sid}></input>
                                                                                                                                <input type="hidden" class="name" value={element.nat}></input>

                                                                                                                            </div>
                                                                                                                            <div class="col-4">
                                                                                                                                <Button type="submit" onClick={this.fancy_Submit}>Save</Button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        {/* // <option value={element.sid}>{element.nat}</option> */}
                                                                                                                    </div>
                                                                                                            }
                                                                                                        </>
                                                                                                    )
                                                                                                }

                                                                                            })
                                                                                        }
                                                                                    </>
                                                                                    :
                                                                                    <div class="col-6 mb-3">
                                                                                        <div class="row">
                                                                                            <div class="col-6">
                                                                                                {element.nat}
                                                                                            </div>
                                                                                            <div class="col-2">
                                                                                                <input class="form-control result" type="text"></input>

                                                                                                <input type="hidden" class="event_type" value={element.gtype}></input>
                                                                                                <input type="hidden" class="sid" value={element.sid}></input>
                                                                                                <input type="hidden" class="name" value={element.nat}></input>

                                                                                            </div>
                                                                                            <div class="col-4">
                                                                                                <Button type="submit" onClick={this.fancy_Submit}>Save</Button>
                                                                                            </div>
                                                                                        </div>
                                                                                        {/* // <option value={element.sid}>{element.nat}</option> */}
                                                                                    </div>
                                                                            }
                                                                        </>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                    : ''
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div >
        );
    }
}
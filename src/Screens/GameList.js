import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ApiExecute, ToastMessage } from '../Api';
import { Admin_prifix } from "../configs/costant.config";

export default class GameList extends Component {

    state = {
        allselected: 0,
        games: [],
        gameIds: [],
        s: '',
    }

    gameList = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        let api_response = await ApiExecute(`game`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ games: api_response.data.results })

    }

    gameListwithSearch = async () => {
        // console.log('sdfjhdj', this.state.s);
        let api_response = await ApiExecute(`game?s=${this.state.s}`, { method: 'GET' });
        console.log('search', api_response.data.results);
        this.setState({ games: api_response.data.results })
    }

    componentDidMount() {
        this.gameList();
    }
    selectId(id) {
        if (this.state.gameIds.includes(id)) {
            var index = this.state.gameIds.indexOf(id);
            this.state.gameIds.splice(index, 1);
        } else {
            this.setState({ gameIds: this.state.gameIds.concat(id) });
        }
    }
    selectAllId = (e) => {
        let array = [];
        if (e.target.checked) {
            this.state.games.forEach(element => {
                // console.log('element', element.id);
                array.push(element.id);
            });
            this.setState({ gameIds: array });
        } else {
            this.setState({ gameIds: array });
        }
        console.log('all selected', this.state.gameIds);
    }

    deleteAll = async () => {
        if (this.state.gameIds.length) {
            let data = this.state.gameIds.join(",");
            console.log('data', data);
            let api_response = await ApiExecute("remove-game", { method: 'POST', data: data });
            console.log('api_response', api_response);
            ToastMessage('success', api_response.msg);
            this.gameList();
        } else {
            ToastMessage('error', 'Please select atleast one !');
        }
    }
    searchByName(status) {
        if (status) {
            this.gameListwithSearch();
        } else {
            this.setState({ s: '' });
            this.gameList();
        }
    }


    render() {
        const { gameIds } = this.state;

        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">

                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Game List</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Game List</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row account-list">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row row5">
                                                <div class="col-md-6 mb-2 search-form">
                                                    <form method="post" class="ajaxFormSubmit">
                                                        <div class="d-inline-block form-group form-group-feedback form-group-feedback-right mr-2">
                                                            <input type="text" name="searchKey" value={this.state.s} onChange={(e) => this.setState({ s: e.target.value })} placeholder="Search name" class="form-control" />
                                                        </div>
                                                        <div class="d-inline-block">
                                                            <div onClick={() => this.searchByName(true)} class="btn btn-primary mr-2">Load</div>
                                                            <div onClick={() => this.searchByName(false)} class="btn btn-light">Reset</div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-6 text-right mb-2">
                                                    {/* <div class="d-inline-block mr-2">
                                                                    <div id="export_1627983356353" class="d-inline-block">
                                                                        <button type="button" class="btn mr-1 btn-success"><i class="fas fa-file-excel"></i>
                                                                        </button>
                                                                    </div>
                                                                    <button type="button" class="btn btn-danger"><i class="fas fa-file-pdf"></i>
                                                                    </button>
                                                                </div> */}
                                                    <div class="d-inline-block">
                                                        <button type="button" onClick={() => this.deleteAll()} class="btn btn-danger mr-3"><i aria-hidden="true" class="fa fa-trash"></i></button>
                                                        <Link to={`/${Admin_prifix}add_game`} class="btn btn-success"><i aria-hidden="true" class="fa fa-plus"></i> CREATE GAME</Link>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <div class="row">
                                                            <div class="col-sm-12 col-md-6">
                                                                <div id="tickets-table_length" class="dataTables_length">
                                                                    <label class="d-inline-flex align-items-center">Show&nbsp;
                                                                        <select class="custom-select custom-select-sm" id="__BVID__1022">
                                                                            <option value="25">25</option>
                                                                            <option value="50">50</option>
                                                                            <option value="100">100</option>
                                                                            <option value="250">250</option>
                                                                            <option value="500">500</option>
                                                                            <option value="750">750</option>
                                                                            <option value="1000">1000</option>
                                                                        </select>&nbsp;entries</label>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                            <div class="table-responsive mb-0 mt-3">
                                                <div class="table no-footer table-responsive-sm">
                                                    <table id="eventsListTbl" role="table" aria-busy="false" aria-colcount="12" class="table b-table">
                                                        <thead role="rowgroup" class="">
                                                            <tr role="row" class="">
                                                                <th role="columnheader" scope="col" >
                                                                    <div><input type="checkbox" name="allSelected" value="" onChange={this.selectAllId}></input> | S.N. </div>
                                                                </th>
                                                                <th role="columnheader" scope="col">
                                                                    <div>Name</div>
                                                                </th>
                                                                <th role="columnheader" scope="col">
                                                                    <div>Category</div>
                                                                </th>
                                                                <th role="columnheader" scope="col">
                                                                    <div>Game Type</div>
                                                                </th>
                                                                <th role="columnheader" scope="col">
                                                                    <div>Image</div>
                                                                </th>
                                                                {/* <th role="columnheader" scope="col">
                                                                                <div>Action</div>
                                                                            </th> */}
                                                            </tr>
                                                        </thead>
                                                        <tbody role="rowgroup">
                                                            {
                                                                this.state.games.map((game, index) => {
                                                                    return (
                                                                        <tr role="row" class="">
                                                                            <td role="cell" class="">
                                                                                <input type="checkbox" checked={gameIds.includes(game.id) ? 'checked' : ''} onChange={() => this.selectId(game.id)}></input> | {index + 1}.
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                <Link to={`/${Admin_prifix}edit_game/${game.id}`} > <i aria-hidden="true" class="fa fa-edit"></i> {game.name}</Link>
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                {game.category_name}
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                {game.game_type_name}
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                <img src={game.image} width='100px'></img>
                                                                            </td>
                                                                            {/* <td role="cell" class="">
                                                                                            <div role="group" class="btn-group">
                                                                                                <button type="button" class="btn btn-danger">E</button>
                                                                                                <button type="button" class="btn btn-info">More</button>
                                                                                            </div>
                                                                                        </td> */}
                                                                        </tr>
                                                                    );
                                                                })
                                                            }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {/* <div class="row pt-3">
                                                            <div class="col">
                                                                <div class="dataTables_paginate paging_simple_numbers float-right">
                                                                    <ul class="pagination pagination-rounded mb-0">
                                                                        <ul role="menubar" aria-disabled="false" aria-label="Pagination" class="pagination dataTables_paginate paging_simple_numbers my-0 b-pagination justify-content-end">
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to first page" aria-disabled="true" class="page-link">«</span>
                                                                            </li>
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to previous page" aria-disabled="true" class="page-link">‹</span>
                                                                            </li>


                                                                            <li role="presentation" class="page-item active">
                                                                                <button role="menuitemradio" type="button" aria-label="Go to page 1" aria-checked="true" aria-posinset="1" aria-setsize="1" tabindex="0" class="page-link">1</button>
                                                                            </li>


                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to next page" aria-disabled="true" class="page-link">›</span>
                                                                            </li>
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to last page" aria-disabled="true" class="page-link">»</span>
                                                                            </li>
                                                                        </ul>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
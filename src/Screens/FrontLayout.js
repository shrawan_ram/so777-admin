import logo from '../logo.svg';
import '../App.css';
import React, { Component } from 'react';
import {
    BrowserRouter as Router, Link, Route,
    Switch, withRouter
} from 'react-router-dom';
import '../assets/css/all.css';
import '../assets/css/bootstrap.min.css';
import '../assets/css/color.css';
import '../assets/css/control.min.css';
import '../assets/css/css2.css';
import '../assets/css/custom.css';
import '../assets/css/fonts.css';
import '../assets/css/owl.carousel.min.css';
import '../assets/css/style.css';
import '../assets/css/responsive.css';
import '../assets/front/css/style.min.css';
import '../assets/css/main.css';
import '../assets/css/responsive.min.css';
import '../assets/css/animate.css@3.5.1.css';

import HomeScreen from './front/HomeScreen';
import DashboardScreen from './front/DashboardScreen';
import AboutScreen from './front/AboutScreen';
import GameRuleScreen from './front/GameRuleScreen';

import $ from "jquery";
import ReactTypingEffect from 'react-typing-effect';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CasinoScreen from './front/CasinoScreen';
import { ThemeColor } from '../assets/js/customreact';
import { Carousel } from 'react-responsive-carousel';
import TopHeader from '../Components/TopHeader';
import FrontSidebar from '../Components/FrontSidebar';
import FrontHeader from '../Components/FrontHeader';

import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { ApiExecute } from '../Api';
import FantasyScreen from './front/FantasyScreen';
import FaqScreen from './front/FaqScreen';
import CasinoGameScreen from './front/CasinoGameScreen';
import ReportScreen from './front/ReportScreen';
import CricketScreen from './front/CricketScreen';
import TannisScreen from './front/TannisScreen';
import AccountStatement from './front/AccountStatement';
import CurrentBets from './front/CurrentBates';
import SoccerScreen from './front/SoccerScreen';


class FrontLayout extends Component {
    state = {
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        topHeaderRemove: '',
        user: {},
        users: {},
        token: null,
        loading: true,
        categories: [],
        slug: '',
        pb: 'pb'
    }

    afterLogin = async res => {
        sessionStorage.setItem('@front_user', JSON.stringify(res.data));
        sessionStorage.setItem('@front_token', res.token);

        this.setState({
            user: res.data,
            token: res.token
        });
        // <Route exact path="/" render={() => (
        //     <Redirect to={`/${Admin_prifix}dashboard`} />
        // )} />
        <Router><Route path={`/sport`} component={DashboardScreen} /></Router>
    }

    afterLogout = async () => {
        sessionStorage.removeItem('@front_user');
        sessionStorage.removeItem('@front_token');

        this.setState({
            user: null,
            token: null,

        });

        toast('Logged out successfully!');
    }

    theme(color) {
        ThemeColor(color);
    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        // console.log('lists', api_response.data.results);
        this.setState({ categories: api_response.data.results })
    }

    async componentDidMount() {
        // console.log('props', this.props.location.pathname);
        let str = this.props.location.pathname;
        // console.log('props', str.split('/').pop());
        this.setState({ slug: str.split('/').pop() })

        this.categoryList();
        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');
        if (user_token) {
            this.setState({
                user: user,
                token: user_token,
            });
        }

        // setTimeout(() => {
        //     this.setState({ loading: true })
        // }, 2000);

    }

    closetopheaderr() {

        this.setState({ topHeaderRemove: 'none' });
        $('body').removeClass('animate-on');
    }

    render() {
        if (this.state.loading) {
            let user = sessionStorage.getItem('@front_user');
            let user_token = sessionStorage.getItem('@front_token');
            if (user_token && user) {
                return (
                    <Router>
                        <Switch>
                            <Route exact path={`/`}
                                component={(props) => <HomeScreen {...props} onLoginSuccess={(res) => this.afterLogin(res)} />}>
                            </Route>
                        </Switch>
                        <Switch>
                            <Route path={`/casino/:slug`}
                                component={(props) => <CasinoGameScreen slug='casino' {...props} />}>
                            </Route>
                            <Route path={`/report/casinoresult/:slug`}
                                component={(props) => <ReportScreen slug='fantasy' {...props} />}>
                            </Route>
                            <Route path={`/sport/cricket/:slug`}
                                component={(props) => <CricketScreen slug='sport' {...props} />}>
                            </Route>
                            <Route path={`/sport/tannis/:slug`}
                                component={(props) => <TannisScreen slug='sport' {...props} />}>
                            </Route>
                            <Route path={`/sport/soccer/:slug`}
                                component={(props) => <SoccerScreen slug='sport' {...props} />}>
                            </Route>
                            <Route path={`/casino`}
                                component={(props) => <CasinoScreen slug='casino' {...props} />}>
                            </Route>
                            <Route path={`/sport`}
                                component={(props) => <DashboardScreen slug='sport' {...props} />}>
                            </Route>
                            <Route path={`/slot`}
                                component={(props) => <CasinoScreen slug='slot' {...props} />}>
                            </Route>
                            <Route path={`/fantasy`}
                                component={(props) => <FantasyScreen slug='fantasy' {...props} />}>
                            </Route>
                            <Route path={`/others`}
                                component={(props) => <FantasyScreen slug='others' {...props} />}>
                            </Route>
                            <Route path={`/faq`}
                                component={(props) => <FaqScreen {...props} />}>
                            </Route>
                            <Route path={`/report/accountstatement`}
                                component={(props) => <AccountStatement {...props} />}>
                            </Route>
                            <Route path={`/report/currentbets`}
                                component={(props) => <CurrentBets {...props} />}>
                            </Route>
                        </Switch>
                    </Router>
                );
            } else {
                return (
                    <Router>
                        <Switch>
                            <Route exact path={`/`}
                                component={(props) => <HomeScreen {...props} onLoginSuccess={(res) => this.afterLogin(res)} />}>
                            </Route>
                            <Route path={`/about-us`}
                                component={(props) => <AboutScreen {...props} />}>
                            </Route>
                            <Route path={`/game-rules`}
                                component={(props) => <GameRuleScreen {...props} />}>
                            </Route>
                        </Switch>
                    </Router>
                );
            }
        } else {
            return (
                <div className="text-center" style={{ lineHeight: 50 }}>
                    Loding....
                </div>
            );
        }
    }
}

export default withRouter(FrontLayout);
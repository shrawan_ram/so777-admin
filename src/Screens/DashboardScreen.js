import React, { Component } from "react";
import { Collapse } from "react-collapse";
import Slider from "react-slick";
import Popup from 'reactjs-popup';
import { Button } from 'react-bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import $ from "jquery";
import { Link } from "react-router-dom";
import { AsyncStorage } from "AsyncStorage";
import AdminHeader from '../Components/AdminHeader';
import AdminSidebar from '../Components/AdminSidebar';

export default class DashboardScreen extends Component {
    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        user: {}
    }
    logoutSubmit = async () => {
        sessionStorage.removeItem('@token');
        sessionStorage.removeItem('@user');


        // this.props.history.push('/');
        window.location = '/';

    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@user');
        console.log('sdkmkldfsmdflkmdflk');
        this.setState({
            user: JSON.parse(user)
        });

    }


    render() {
        const { isOpened } = this.state;
        const height = 100;

        let { Router } = this.props;

        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");

        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,


        }

        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content" onClick={this.searchoverlay.bind(this)}>

                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="" class="p-1">
                            <div class="row row5">
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">Balance</p>
                                            <h4>3,45,000.00</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">Exposure</p>
                                            <h4>0.00</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">Credit Pts</p>
                                            <h4>5,00,000</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">All Pts</p>
                                            <h4>5,00,031</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">Settlement Pts</p>
                                            <h4>31</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">Upper Pts</p>
                                            <h4>0</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="card mini-stats-wid">
                                        <div class="card-body">
                                            <p class="text-muted fw-medium">Down Pts</p>
                                            <h4>0</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="card">
                                        <div class="card-body" style={{ position: 'relative' }}>
                                            <div style={{ minHeight: '365px' }}>
                                                <div id="apexchartsdjyhdf4t" class="apexcharts-canvas apexchartsdjyhdf4t apexcharts-theme-light" style={{ width: '1581px', height: '350px' }}>
                                                    <svg id="SvgjsSvg1860" width="1581" height="350" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink" xmlnsSvgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlnsData="ApexChartsNS" transform="translate(0, 0)" style={{ background: 'transparent' }}>
                                                        <foreignObject x="0" y="0" width="1581" height="350">
                                                            <div class="apexcharts-legend apexcharts-align-center position-bottom" xmlns="http://www.w3.org/1999/xhtml" style={{ inset: 'auto 0px 1px', position: 'absolute', maxHeight: '175px' }}>
                                                                <div class="apexcharts-legend-series" rel="1" seriesname="CreditxPts" dataCollapsed="false" style={{ margin: '2px 5px' }}><span class="apexcharts-legend-marker" rel="1" dataCollapsed="false" style={{ background: 'rgb(85, 110, 230) !important', color: 'rgb(85, 110, 230)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span><span class="apexcharts-legend-text" rel="1" i="0" dataDefault-text="Credit%20Pts" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Credit Pts</span>
                                                                </div>
                                                                <div class="apexcharts-legend-series" rel="2" seriesname="AllxPts" dataCollapsed="false" style={{ margin: '2px 5px' }}><span class="apexcharts-legend-marker" rel="2" dataCollapsed="false" style={{ background: 'rgb(241, 180, 76) !important', color: 'rgb(241, 180, 76)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span><span class="apexcharts-legend-text" rel="2" i="1" dataDefault-text="All%20Pts" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>All Pts</span>
                                                                </div>
                                                                <div class="apexcharts-legend-series" rel="3" seriesname="SettlementxPts" dataCollapsed="false" style={{ margin: '2px 5px' }}><span class="apexcharts-legend-marker" rel="3" dataCollapsed="false" style={{ background: 'rgb(80, 165, 241) !important', color: 'rgb(80, 165, 241)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span><span class="apexcharts-legend-text" rel="3" i="2" dataDefault-text="Settlement%20Pts" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Settlement Pts</span>
                                                                </div>
                                                                <div class="apexcharts-legend-series" rel="4" seriesname="UpperxPts" dataCollapsed="false" style={{ margin: '2px 5px' }}><span class="apexcharts-legend-marker" rel="4" dataCollapsed="false" style={{ background: 'rgb(52, 195, 143) !important', color: 'rgb(52, 195, 143)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span><span class="apexcharts-legend-text" rel="4" i="3" dataDefault-text="Upper%20Pts" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Upper Pts</span>
                                                                </div>
                                                                <div class="apexcharts-legend-series" rel="5" seriesname="DownxPts" dataCollapsed="false" style={{ margin: '2px 5px' }}><span class="apexcharts-legend-marker" rel="5" dataCollapsed="false" style={{ background: 'rgb(52, 58, 64) !important', color: 'rgb(52, 58, 64)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span><span class="apexcharts-legend-text" rel="5" i="4" dataDefault-text="Down%20Pts" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Down Pts</span>
                                                                </div>
                                                            </div>

                                                        </foreignObject>
                                                        <g id="SvgjsG1862" class="apexcharts-inner apexcharts-graphical" transform="translate(99.046875, 30)">
                                                            <defs id="SvgjsDefs1861">
                                                                <linearGradient id="SvgjsLinearGradient1866" x1="0" y1="0" x2="0" y2="1">
                                                                    <stop id="SvgjsStop1867" stopOpacity="0.4" stopColor="rgba(216,227,240,0.4)" offset="0"></stop>
                                                                    <stop id="SvgjsStop1868" stopOpacity="0.5" stopColor="rgba(190,209,230,0.5)" offset="1"></stop>
                                                                    <stop id="SvgjsStop1869" stopOpacity="0.5" stopColor="rgba(190,209,230,0.5)" offset="1"></stop>
                                                                </linearGradient>
                                                                <clipPath id="gridRectMaskdjyhdf4t">
                                                                    <rect id="SvgjsRect1871" width="1454.765625" height="264.348" x="-2" y="0" rx="0" ry="0" opacity="1" strokeWidth="0" stroke="none" strokeDasharray="0" fill="#fff"></rect>
                                                                </clipPath>
                                                                <clipPath id="gridRectMarkerMaskdjyhdf4t">
                                                                    <rect id="SvgjsRect1872" width="1454.765625" height="268.348" x="-2" y="-2" rx="0" ry="0" opacity="1" strokeWidth="0" stroke="none" strokeDasharray="0" fill="#fff"></rect>
                                                                </clipPath>
                                                                <filter id="SvgjsFilter2034" filterUnits="userSpaceOnUse" width="200%" height="200%" x="-50%" y="-50%">
                                                                    <feComponentTransfer id="SvgjsFeComponentTransfer2035" result="SvgjsFeComponentTransfer2035Out" in="SourceGraphic">
                                                                        <feFuncR id="SvgjsFeFuncR2036" type="linear" slope="0.5"></feFuncR>
                                                                        <feFuncG id="SvgjsFeFuncG2037" type="linear" slope="0.5"></feFuncG>
                                                                        <feFuncB id="SvgjsFeFuncB2038" type="linear" slope="0.5"></feFuncB>
                                                                        <feFuncA id="SvgjsFeFuncA2039" type="identity"></feFuncA>
                                                                    </feComponentTransfer>
                                                                </filter>
                                                            </defs>
                                                            <rect id="SvgjsRect1870" width="0" height="264.348" x="1209" y="0" rx="0" ry="0" opacity="1" strokeWidth="0" strokeDasharray="3" fill="url(#SvgjsLinearGradient1866)" class="apexcharts-xcrosshairs" y2="264.348" filter="none" fillOpacity="0.9" x1="1209" x2="1209"></rect>
                                                            <g id="SvgjsG1905" class="apexcharts-yaxis apexcharts-xaxis-inversed" rel="0">
                                                                <g id="SvgjsG1906" class="apexcharts-yaxis-texts-g apexcharts-xaxis-inversed-texts-g" transform="translate(0, 0)">
                                                                    <text id="SvgjsText1907" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="28.837963636363643" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1908">Credit Pts</tspan>
                                                                        <title></title>
                                                                    </text>
                                                                    <text id="SvgjsText1909" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="81.70756363636364" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1910">All Pts</tspan>
                                                                        <title></title>
                                                                    </text>
                                                                    <text id="SvgjsText1911" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="134.57716363636365" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1912">Settlement Pts</tspan>
                                                                        <title></title>
                                                                    </text>
                                                                    <text id="SvgjsText1913" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="187.44676363636364" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1914">Upper Pts</tspan>
                                                                        <title></title>
                                                                    </text>
                                                                    <text id="SvgjsText1915" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="240.31636363636363" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1916">Down Pts</tspan>
                                                                        <title></title>
                                                                    </text>
                                                                </g>
                                                                <line id="SvgjsLine1917" x1="0" y1="1" x2="0" y2="264.348" stroke="#e0e0e0" strokeDasharray="0"></line>
                                                            </g>
                                                            <g id="SvgjsG1881" class="apexcharts-xaxis apexcharts-yaxis-inversed">
                                                                <g id="SvgjsG1882" class="apexcharts-xaxis-texts-g" transform="translate(0, -8)">
                                                                    <text id="SvgjsText1883" fontFamily="Helvetica, Arial, sans-serif" x="1450.765625" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1885">600000</tspan>
                                                                        <title>600000</title>
                                                                    </text>
                                                                    <text id="SvgjsText1886" fontFamily="Helvetica, Arial, sans-serif" x="1208.8713541666666" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1888">500000</tspan>
                                                                        <title>500000</title>
                                                                    </text>
                                                                    <text id="SvgjsText1889" fontFamily="Helvetica, Arial, sans-serif" x="966.9770833333334" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1891">400000</tspan>
                                                                        <title>400000</title>
                                                                    </text>
                                                                    <text id="SvgjsText1892" fontFamily="Helvetica, Arial, sans-serif" x="725.0828125" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1894">300000</tspan>
                                                                        <title>300000</title>
                                                                    </text>
                                                                    <text id="SvgjsText1895" fontFamily="Helvetica, Arial, sans-serif" x="483.18854166666665" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1897">200000</tspan>
                                                                        <title>200000</title>
                                                                    </text>
                                                                    <text id="SvgjsText1898" fontFamily="Helvetica, Arial, sans-serif" x="241.29427083333326" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1900">100000</tspan>
                                                                        <title>100000</title>
                                                                    </text>
                                                                    <text id="SvgjsText1901" fontFamily="Helvetica, Arial, sans-serif" x="-0.6000000000001364" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                        <tspan id="SvgjsTspan1903">0</tspan>
                                                                        <title>0</title>
                                                                    </text>
                                                                </g>
                                                                <line id="SvgjsLine1904" x1="0" y1="264.348" x2="1450.765625" y2="264.348" stroke="#e0e0e0" strokeDasharray="0" strokeWidth="1"></line>
                                                            </g>
                                                            <g id="SvgjsG1918" class="apexcharts-grid">
                                                                <g id="SvgjsG1919" class="apexcharts-gridlines-horizontal">
                                                                    <line id="SvgjsLine1928" x1="0" y1="0" x2="1450.765625" y2="0" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                    <line id="SvgjsLine1929" x1="0" y1="52.869600000000005" x2="1450.765625" y2="52.869600000000005" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                    <line id="SvgjsLine1930" x1="0" y1="105.73920000000001" x2="1450.765625" y2="105.73920000000001" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                    <line id="SvgjsLine1931" x1="0" y1="158.60880000000003" x2="1450.765625" y2="158.60880000000003" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                    <line id="SvgjsLine1932" x1="0" y1="211.47840000000002" x2="1450.765625" y2="211.47840000000002" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                    <line id="SvgjsLine1933" x1="0" y1="264.348" x2="1450.765625" y2="264.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                </g>
                                                                <g id="SvgjsG1920" class="apexcharts-gridlines-vertical"></g>
                                                                <line id="SvgjsLine1921" x1="0" y1="265.348" x2="0" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1922" x1="242.09427083333335" y1="265.348" x2="242.09427083333335" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1923" x1="484.1885416666667" y1="265.348" x2="484.1885416666667" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1924" x1="726.2828125" y1="265.348" x2="726.2828125" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1925" x1="968.3770833333333" y1="265.348" x2="968.3770833333333" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1926" x1="1210.4713541666665" y1="265.348" x2="1210.4713541666665" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1927" x1="1452.5656249999997" y1="265.348" x2="1452.5656249999997" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                <line id="SvgjsLine1935" x1="0" y1="264.348" x2="1450.765625" y2="264.348" stroke="transparent" strokeDasharray="0"></line>
                                                                <line id="SvgjsLine1934" x1="0" y1="1" x2="0" y2="264.348" stroke="transparent" strokeDasharray="0"></line>
                                                            </g>
                                                            <g id="SvgjsG1873" class="apexcharts-bar-series apexcharts-plot-series">
                                                                <g id="SvgjsG1874" class="apexcharts-series" rel="1" seriesName="seriesx1" dataRealIndex="0">
                                                                    <path id="SvgjsPath1876" d="M 0.1 17.18262L 1209.0713541666667 17.18262Q 1209.0713541666667 17.18262 1209.0713541666667 17.18262L 1209.0713541666667 35.686980000000005Q 1209.0713541666667 35.686980000000005 1209.0713541666667 35.686980000000005L 1209.0713541666667 35.686980000000005L 0.1 35.686980000000005L 0.1 35.686980000000005z" fill="rgba(85,110,230,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdjyhdf4t)" pathTo="M 0.1 17.18262L 1209.0713541666667 17.18262Q 1209.0713541666667 17.18262 1209.0713541666667 17.18262L 1209.0713541666667 35.686980000000005Q 1209.0713541666667 35.686980000000005 1209.0713541666667 35.686980000000005L 1209.0713541666667 35.686980000000005L 0.1 35.686980000000005L 0.1 35.686980000000005z" pathFrom="M 0.1 17.18262L 0.1 17.18262L 0.1 35.686980000000005L 0.1 35.686980000000005L 0.1 35.686980000000005L 0.1 35.686980000000005L 0.1 35.686980000000005L 0.1 17.18262" cy="70.05222" cx="1209.0713541666667" j="0" val="500000" barHeight="18.504360000000002" barWidth="1208.9713541666667"></path>
                                                                    <path id="SvgjsPath1877" d="M 0.1 70.05222L 1209.146914876302 70.05222Q 1209.146914876302 70.05222 1209.146914876302 70.05222L 1209.146914876302 88.55658000000001Q 1209.146914876302 88.55658000000001 1209.146914876302 88.55658000000001L 1209.146914876302 88.55658000000001L 0.1 88.55658000000001L 0.1 88.55658000000001z" fill="rgba(241,180,76,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdjyhdf4t)" pathTo="M 0.1 70.05222L 1209.146914876302 70.05222Q 1209.146914876302 70.05222 1209.146914876302 70.05222L 1209.146914876302 88.55658000000001Q 1209.146914876302 88.55658000000001 1209.146914876302 88.55658000000001L 1209.146914876302 88.55658000000001L 0.1 88.55658000000001L 0.1 88.55658000000001z" pathFrom="M 0.1 70.05222L 0.1 70.05222L 0.1 88.55658000000001L 0.1 88.55658000000001L 0.1 88.55658000000001L 0.1 88.55658000000001L 0.1 88.55658000000001L 0.1 70.05222" cy="122.92182000000001" cx="1209.146914876302" j="1" val="500031.25" barHeight="18.504360000000002" barWidth="1209.046914876302" selected="true" filter="url(#SvgjsFilter2034)"></path>
                                                                    <path id="SvgjsPath1878" d="M 0.1 122.92182000000001L 0.17495622395833332 122.92182000000001Q 0.17495622395833332 122.92182000000001 0.17495622395833332 122.92182000000001L 0.17495622395833332 141.42618000000002Q 0.17495622395833332 141.42618000000002 0.17495622395833332 141.42618000000002L 0.17495622395833332 141.42618000000002L 0.1 141.42618000000002L 0.1 141.42618000000002z" fill="rgba(80,165,241,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdjyhdf4t)" pathTo="M 0.1 122.92182000000001L 0.17495622395833332 122.92182000000001Q 0.17495622395833332 122.92182000000001 0.17495622395833332 122.92182000000001L 0.17495622395833332 141.42618000000002Q 0.17495622395833332 141.42618000000002 0.17495622395833332 141.42618000000002L 0.17495622395833332 141.42618000000002L 0.1 141.42618000000002L 0.1 141.42618000000002z" pathFrom="M 0.1 122.92182000000001L 0.1 122.92182000000001L 0.1 141.42618000000002L 0.1 141.42618000000002L 0.1 141.42618000000002L 0.1 141.42618000000002L 0.1 141.42618000000002L 0.1 122.92182000000001" cy="175.79142000000002" cx="0.17495622395833332" j="2" val="31" barHeight="18.504360000000002" barWidth="0.07495622395833333"></path>
                                                                    <path id="SvgjsPath1879" d="M 0.1 175.79142000000002L 0.1 175.79142000000002Q 0.1 175.79142000000002 0.1 175.79142000000002L 0.1 194.29578Q 0.1 194.29578 0.1 194.29578L 0.1 194.29578L 0.1 194.29578L 0.1 194.29578z" fill="rgba(52,195,143,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdjyhdf4t)" pathTo="M 0.1 175.79142000000002L 0.1 175.79142000000002Q 0.1 175.79142000000002 0.1 175.79142000000002L 0.1 194.29578Q 0.1 194.29578 0.1 194.29578L 0.1 194.29578L 0.1 194.29578L 0.1 194.29578z" pathFrom="M 0.1 175.79142000000002L 0.1 175.79142000000002L 0.1 194.29578L 0.1 194.29578L 0.1 194.29578L 0.1 194.29578L 0.1 194.29578L 0.1 175.79142000000002" cy="228.66102" cx="0.1" j="3" val="0" barHeight="18.504360000000002" barWidth="0"></path>
                                                                    <path id="SvgjsPath1880" d="M 0.1 228.66102L 0.1 228.66102Q 0.1 228.66102 0.1 228.66102L 0.1 247.16538Q 0.1 247.16538 0.1 247.16538L 0.1 247.16538L 0.1 247.16538L 0.1 247.16538z" fill="rgba(52,58,64,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdjyhdf4t)" pathTo="M 0.1 228.66102L 0.1 228.66102Q 0.1 228.66102 0.1 228.66102L 0.1 247.16538Q 0.1 247.16538 0.1 247.16538L 0.1 247.16538L 0.1 247.16538L 0.1 247.16538z" pathFrom="M 0.1 228.66102L 0.1 228.66102L 0.1 247.16538L 0.1 247.16538L 0.1 247.16538L 0.1 247.16538L 0.1 247.16538L 0.1 228.66102" cy="281.53062" cx="0.1" j="4" val="0" barHeight="18.504360000000002" barWidth="0"></path>
                                                                </g>
                                                                <g id="SvgjsG1875" class="apexcharts-datalabels" dataRealIndex="0"></g>
                                                            </g>
                                                            <line id="SvgjsLine1936" x1="0" y1="0" x2="1450.765625" y2="0" stroke="#b6b6b6" strokeDasharray="0" strokeWidth="1" class="apexcharts-ycrosshairs"></line>
                                                            <line id="SvgjsLine1937" x1="0" y1="0" x2="1450.765625" y2="0" strokeDasharray="0" strokeWidth="0" class="apexcharts-ycrosshairs-hidden"></line>
                                                            <g id="SvgjsG1938" class="apexcharts-yaxis-annotations"></g>
                                                            <g id="SvgjsG1939" class="apexcharts-xaxis-annotations"></g>
                                                            <g id="SvgjsG1940" class="apexcharts-point-annotations"></g>
                                                        </g>
                                                        <g id="SvgjsG1863" class="apexcharts-annotations"></g>
                                                    </svg>
                                                    <div class="apexcharts-tooltip apexcharts-theme-light" style={{ left: '1308.05px', top: '38.3826px' }}>
                                                        <div class="apexcharts-tooltip-title" style={{ fontFamily: 'Helvetica, Arial, sans-serif', fontSize: '12px' }}>All Pts</div>
                                                        <div class="apexcharts-tooltip-series-group apexcharts-active" style={{ order: 1, display: 'flex' }}><span class="apexcharts-tooltip-marker" style={{ backgroundColor: 'rgba(241, 180, 76, 0.85)' }}></span>
                                                            <div class="apexcharts-tooltip-text" style={{ fontFamily: 'Helvetica, Arial, sans-serif', fontSize: '12px' }}>
                                                                <div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label">series-1: </span><span class="apexcharts-tooltip-text-value">500031.25</span>
                                                                </div>
                                                                <div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                                        <div class="apexcharts-yaxistooltip-text"></div>
                                                    </div>
                                                    <div class="apexcharts-toolbar" style={{ top: '4%', right: '1%' }}>
                                                        <div class="apexcharts-menu-icon" title="Menu" onClick={this.Graphtoggle.bind(this)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0V0z"></path>
                                                                <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
                                                            </svg>
                                                        </div>
                                                        <div class={apexcharts_menu.join(' ')}>
                                                            <div class="apexcharts-menu-item exportSVG" title="Download SVG">Download SVG</div>
                                                            <div class="apexcharts-menu-item exportPNG" title="Download PNG" >Download PNG</div>
                                                            <div class="apexcharts-menu-item exportCSV" title="Download CSV">Download CSV</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row5 align-self-center text-center">
                                                <div class="col-6 col-sm">
                                                    <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-primary"></i> Credit Pts</p>
                                                    <h5>5,00,000</h5>
                                                </div>
                                                <div class="col-6 col-sm">
                                                    <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-warning"></i> All Pts</p>
                                                    <h5>5,00,031</h5>
                                                </div>
                                                <div class="col-4 col-sm">
                                                    <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-info"></i> Settlement Pts</p>
                                                    <h5>31</h5>
                                                </div>
                                                <div class="col-4 col-sm">
                                                    <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-success"></i> Upper Pts</p>
                                                    <h5>0</h5>
                                                </div>
                                                <div class="col-4 col-sm">
                                                    <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-dark"></i> Down Pts</p>
                                                    <h5>0</h5>
                                                </div>
                                            </div>
                                            <div class="resize-triggers">
                                                <div class="expand-trigger">
                                                    <div style={{ width: '1622px', height: '457px' }}></div>
                                                </div>
                                                <div class="contract-trigger"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div style={{ position: 'relative' }}>
                                                <div style={{ minHeight: '365px' }}>
                                                    <div id="apexchartsdmxeuvi" class="apexcharts-canvas apexchartsdmxeuvi apexcharts-theme-light" style={{ width: '1581px', height: '350px' }}>
                                                        <svg id="SvgjsSvg1941" width="1581" height="350" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink" xmlnsSvgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlnsData="ApexChartsNS" transform="translate(0, 0)" style={{ background: 'transparent' }}>
                                                            <foreignObject x="0" y="0" width="1581" height="350">
                                                                <div class="apexcharts-legend apexcharts-align-center position-bottom" xmlns="http://www.w3.org/1999/xhtml" style={{ inset: 'auto 0px 1px', position: 'absolute', maxHeight: '175px' }}>
                                                                    <div class="apexcharts-legend-series" rel="1" seriesname="SportsxPxL" dataCollapsed="false" style={{ margin: '2px 5px' }}>
                                                                        <span class="apexcharts-legend-marker" rel="1" dataCollapsed="false" style={{ backgroundColor: 'rgb(85, 110, 230)', color: 'rgb(85, 110, 230)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}>
                                                                        </span>
                                                                        <span class="apexcharts-legend-text" rel="1" i="0" dataDefault-text="Sports%20P%2FL" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Sports P/L</span>
                                                                    </div>
                                                                    <div class="apexcharts-legend-series" rel="2" seriesname="xCasinoxPxL" dataCollapsed="false" style={{ margin: '2px 5px' }}>
                                                                        <span class="apexcharts-legend-marker" rel="2" dataCollapsed="false" style={{ backgroundColor: 'rgb(241, 180, 76)', color: 'rgb(241, 180, 76)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span>
                                                                        <span class="apexcharts-legend-text" rel="2" i="1" dataDefault-text="%20Casino%20P%2FL" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}> Casino P/L</span>
                                                                    </div>
                                                                    <div class="apexcharts-legend-series" rel="3" seriesname="ThirdxPartyxCasinoxPxL" dataCollapsed="false" style={{ margin: '2px 5px' }}>
                                                                        <span class="apexcharts-legend-marker" rel="3" dataCollapsed="false" style={{ backgroundColor: 'rgb(80, 165, 241)', color: 'rgb(80, 165, 241)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span>
                                                                        <span class="apexcharts-legend-text" rel="3" i="2" dataDefault-text="Third%20Party%20Casino%20P%2FL" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Third Party Casino P/L</span>
                                                                    </div>
                                                                    <div class="apexcharts-legend-series" rel="4" seriesname="TotalxPxL" dataCollapsed="false" style={{ margin: '2px 5px' }}>
                                                                        <span class="apexcharts-legend-marker" rel="4" dataCollapsed="false" style={{ backgroundColor: 'rgb(52, 195, 143)', color: 'rgb(52, 195, 143)', height: '12px', width: '12px', left: '0px', top: '0px', borderWidth: '0px', borderColor: 'rgb(255, 255, 255)', borderRadius: '2px' }}></span>
                                                                        <span class="apexcharts-legend-text" rel="4" i="3" dataDefault-text="Total%20P%2FL" dataCollapsed="false" style={{ color: 'rgb(55, 61, 63)', fontize: '12px', fontWeight: 400, fontFamily: 'Helvetica, Arial, sans-serif' }}>Total P/L</span>
                                                                    </div>
                                                                </div>
                                                            </foreignObject>
                                                            <g id="SvgjsG1943" class="apexcharts-inner apexcharts-graphical" transform="translate(139.546875, 30)">
                                                                <defs id="SvgjsDefs1942">
                                                                    <linearGradient id="SvgjsLinearGradient1947" x1="0" y1="0" x2="0" y2="1">
                                                                        <stop id="SvgjsStop1948" stopOpacity="0.4" stopColor="rgba(216,227,240,0.4)" offset="0"></stop>
                                                                        <stop id="SvgjsStop1949" stopOpacity="0.5" stopColor="rgba(190,209,230,0.5)" offset="1"></stop>
                                                                        <stop id="SvgjsStop1950" stopOpacity="0.5" stopColor="rgba(190,209,230,0.5)" offset="1"></stop>
                                                                    </linearGradient>
                                                                    <clipPath id="gridRectMaskdmxeuvi">
                                                                        <rect id="SvgjsRect1952" width="1435.453125" height="264.348" x="-2" y="0" rx="0" ry="0" opacity="1" strokeWidth="0" stroke="none" strokeDasharray="0" fill="#fff"></rect>
                                                                    </clipPath>
                                                                    <clipPath id="gridRectMarkerMaskdmxeuvi">
                                                                        <rect id="SvgjsRect1953" width="1435.453125" height="268.348" x="-2" y="-2" rx="0" ry="0" opacity="1" strokeWidth="0" stroke="none" strokeDasharray="0" fill="#fff"></rect>
                                                                    </clipPath>
                                                                </defs>
                                                                <rect id="SvgjsRect1951" width="0" height="264.348" x="-99.390625" y="0" rx="0" ry="0" opacity="1" strokeWidth="0" strokeDasharray="3" fill="url(#SvgjsLinearGradient1947)" class="apexcharts-xcrosshairs" y2="264.348" filter="none" fillOpacity="0.9" x1="-99.390625" x2="-99.390625"></rect>
                                                                <g id="SvgjsG1979" class="apexcharts-yaxis apexcharts-xaxis-inversed" rel="0">
                                                                    <g id="SvgjsG1980" class="apexcharts-yaxis-texts-g apexcharts-xaxis-inversed-texts-g" transform="translate(0, 0)">
                                                                        <text id="SvgjsText1981" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="36.04745454545455" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1982">Sports P/L</tspan>
                                                                            <title></title>
                                                                        </text>
                                                                        <text id="SvgjsText1983" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="102.13445454545456" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1984">Casino P/L</tspan>
                                                                            <title></title>
                                                                        </text>
                                                                        <text id="SvgjsText1985" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="168.22145454545455" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1986">Third Party Casino ...</tspan>
                                                                            <title></title>
                                                                        </text>
                                                                        <text id="SvgjsText1987" fontFamily="Helvetica, Arial, sans-serif" x="-15" y="234.30845454545454" textAnchor="end" dominantBaseline="auto" fontSize="11px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1988">Total P/L</tspan>
                                                                            <title></title>
                                                                        </text>
                                                                    </g>
                                                                    <line id="SvgjsLine1989" x1="0" y1="1" x2="0" y2="264.348" stroke="#e0e0e0" strokeDasharray="0"></line>
                                                                </g>
                                                                <g id="SvgjsG1961" class="apexcharts-xaxis apexcharts-yaxis-inversed">
                                                                    <g id="SvgjsG1962" class="apexcharts-xaxis-texts-g" transform="translate(0, -8)">
                                                                        <text id="SvgjsText1963" fontFamily="Helvetica, Arial, sans-serif" x="1431.453125" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1965">0</tspan>
                                                                            <title>0</title>
                                                                        </text>
                                                                        <text id="SvgjsText1966" fontFamily="Helvetica, Arial, sans-serif" x="1073.48984375" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1968">-8</tspan>
                                                                            <title>-8</title>
                                                                        </text>
                                                                        <text id="SvgjsText1969" fontFamily="Helvetica, Arial, sans-serif" x="715.5265624999998" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1971">-16</tspan>
                                                                            <title>-16</title>
                                                                        </text>
                                                                        <text id="SvgjsText1972" fontFamily="Helvetica, Arial, sans-serif" x="357.5632812499998" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1974">-24</tspan>
                                                                            <title>-24</title>
                                                                        </text>
                                                                        <text id="SvgjsText1975" fontFamily="Helvetica, Arial, sans-serif" x="-0.40000000000009095" y="294.348" textAnchor="middle" dominantBaseline="auto" fontSize="12px" fontWeight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style={{ fontFamily: 'Helvetica, Arial, sans-serif' }}>
                                                                            <tspan id="SvgjsTspan1977">-32</tspan>
                                                                            <title>-32</title>
                                                                        </text>
                                                                    </g>
                                                                    <line id="SvgjsLine1978" x1="0" y1="264.348" x2="1431.453125" y2="264.348" stroke="#e0e0e0" strokeDasharray="0" strokeWidth="1"></line>
                                                                </g>
                                                                <g id="SvgjsG1990" class="apexcharts-grid">
                                                                    <g id="SvgjsG1991" class="apexcharts-gridlines-horizontal">
                                                                        <line id="SvgjsLine1998" x1="0" y1="0" x2="1431.453125" y2="0" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                        <line id="SvgjsLine1999" x1="0" y1="66.087" x2="1431.453125" y2="66.087" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                        <line id="SvgjsLine2000" x1="0" y1="132.174" x2="1431.453125" y2="132.174" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                        <line id="SvgjsLine2001" x1="0" y1="198.26100000000002" x2="1431.453125" y2="198.26100000000002" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                        <line id="SvgjsLine2002" x1="0" y1="264.348" x2="1431.453125" y2="264.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-gridline"></line>
                                                                    </g>
                                                                    <g id="SvgjsG1992" class="apexcharts-gridlines-vertical"></g>
                                                                    <line id="SvgjsLine1993" x1="0" y1="265.348" x2="0" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                    <line id="SvgjsLine1994" x1="358.16328125" y1="265.348" x2="358.16328125" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                    <line id="SvgjsLine1995" x1="716.3265624999999" y1="265.348" x2="716.3265624999999" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                    <line id="SvgjsLine1996" x1="1074.4898437499999" y1="265.348" x2="1074.4898437499999" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                    <line id="SvgjsLine1997" x1="1432.6531249999998" y1="265.348" x2="1432.6531249999998" y2="271.348" stroke="#e0e0e0" strokeDasharray="0" class="apexcharts-xaxis-tick"></line>
                                                                    <line id="SvgjsLine2004" x1="0" y1="264.348" x2="1431.453125" y2="264.348" stroke="transparent" strokeDasharray="0"></line>
                                                                    <line id="SvgjsLine2003" x1="0" y1="1" x2="0" y2="264.348" stroke="transparent" strokeDasharray="0"></line>
                                                                </g>
                                                                <g id="SvgjsG1954" class="apexcharts-bar-series apexcharts-plot-series">
                                                                    <g id="SvgjsG1955" class="apexcharts-series" rel="1" seriesName="seriesx1" dataRealIndex="0">
                                                                        <path id="SvgjsPath1957" d="M 1431.453125 23.130450000000003L 33.5496826171875 23.130450000000003Q 33.5496826171875 23.130450000000003 33.5496826171875 23.130450000000003L 33.5496826171875 42.95655000000001Q 33.5496826171875 42.95655000000001 33.5496826171875 42.95655000000001L 33.5496826171875 42.95655000000001L 1431.453125 42.95655000000001L 1431.453125 42.95655000000001z" fill="rgba(85,110,230,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdmxeuvi)" pathTo="M 1431.453125 23.130450000000003L 33.5496826171875 23.130450000000003Q 33.5496826171875 23.130450000000003 33.5496826171875 23.130450000000003L 33.5496826171875 42.95655000000001Q 33.5496826171875 42.95655000000001 33.5496826171875 42.95655000000001L 33.5496826171875 42.95655000000001L 1431.453125 42.95655000000001L 1431.453125 42.95655000000001z" pathFrom="M 1431.453125 23.130450000000003L 1431.453125 23.130450000000003L 1431.453125 42.95655000000001L 1431.453125 42.95655000000001L 1431.453125 42.95655000000001L 1431.453125 42.95655000000001L 1431.453125 42.95655000000001L 1431.453125 23.130450000000003" cy="89.21745000000001" cx="33.5496826171875" j="0" val="-31.25" barHeight="19.8261" barWidth="-1397.9034423828125"></path>
                                                                        <path id="SvgjsPath1958" d="M 1431.453125 89.21745000000001L 1431.453125 89.21745000000001Q 1431.453125 89.21745000000001 1431.453125 89.21745000000001L 1431.453125 109.04355000000001Q 1431.453125 109.04355000000001 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001z" fill="rgba(241,180,76,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdmxeuvi)" pathTo="M 1431.453125 89.21745000000001L 1431.453125 89.21745000000001Q 1431.453125 89.21745000000001 1431.453125 89.21745000000001L 1431.453125 109.04355000000001Q 1431.453125 109.04355000000001 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001z" pathFrom="M 1431.453125 89.21745000000001L 1431.453125 89.21745000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 109.04355000000001L 1431.453125 89.21745000000001" cy="155.30445000000003" cx="1431.453125" j="1" val="0" barHeight="19.8261" barWidth="0"></path>
                                                                        <path id="SvgjsPath1959" d="M 1431.453125 155.30445000000003L 1431.453125 155.30445000000003Q 1431.453125 155.30445000000003 1431.453125 155.30445000000003L 1431.453125 175.13055000000003Q 1431.453125 175.13055000000003 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003z" fill="rgba(80,165,241,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdmxeuvi)" pathTo="M 1431.453125 155.30445000000003L 1431.453125 155.30445000000003Q 1431.453125 155.30445000000003 1431.453125 155.30445000000003L 1431.453125 175.13055000000003Q 1431.453125 175.13055000000003 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003z" pathFrom="M 1431.453125 155.30445000000003L 1431.453125 155.30445000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 175.13055000000003L 1431.453125 155.30445000000003" cy="221.39145000000002" cx="1431.453125" j="2" val="0" barHeight="19.8261" barWidth="0"></path>
                                                                        <path id="SvgjsPath1960" d="M 1431.453125 221.39145000000002L 44.73291015625 221.39145000000002Q 44.73291015625 221.39145000000002 44.73291015625 221.39145000000002L 44.73291015625 241.21755000000002Q 44.73291015625 241.21755000000002 44.73291015625 241.21755000000002L 44.73291015625 241.21755000000002L 1431.453125 241.21755000000002L 1431.453125 241.21755000000002z" fill="rgba(52,195,143,0.85)" fillOpacity="1" stroke-opacity="1" stroke-linecap="round" strokeWidth="0" strokeDasharray="0" class="apexcharts-bar-area" index="0" clip-path="url(#gridRectMaskdmxeuvi)" pathTo="M 1431.453125 221.39145000000002L 44.73291015625 221.39145000000002Q 44.73291015625 221.39145000000002 44.73291015625 221.39145000000002L 44.73291015625 241.21755000000002Q 44.73291015625 241.21755000000002 44.73291015625 241.21755000000002L 44.73291015625 241.21755000000002L 1431.453125 241.21755000000002L 1431.453125 241.21755000000002z" pathFrom="M 1431.453125 221.39145000000002L 1431.453125 221.39145000000002L 1431.453125 241.21755000000002L 1431.453125 241.21755000000002L 1431.453125 241.21755000000002L 1431.453125 241.21755000000002L 1431.453125 241.21755000000002L 1431.453125 221.39145000000002" cy="287.47845" cx="44.73291015625" j="3" val="-31" barHeight="19.8261" barWidth="-1386.72021484375"></path>
                                                                    </g>
                                                                    <g id="SvgjsG1956" class="apexcharts-datalabels" dataRealIndex="0"></g>
                                                                </g>
                                                                <line id="SvgjsLine2005" x1="0" y1="0" x2="1431.453125" y2="0" stroke="#b6b6b6" strokeDasharray="0" strokeWidth="1" class="apexcharts-ycrosshairs"></line>
                                                                <line id="SvgjsLine2006" x1="0" y1="0" x2="1431.453125" y2="0" strokeDasharray="0" strokeWidth="0" class="apexcharts-ycrosshairs-hidden"></line>
                                                                <g id="SvgjsG2007" class="apexcharts-yaxis-annotations"></g>
                                                                <g id="SvgjsG2008" class="apexcharts-xaxis-annotations"></g>
                                                                <g id="SvgjsG2009" class="apexcharts-point-annotations"></g>
                                                            </g>
                                                            <g id="SvgjsG1944" class="apexcharts-annotations"></g>
                                                        </svg>
                                                        <div class="apexcharts-tooltip apexcharts-theme-light" style={{ left: '139.547px', top: '-5px' }}>
                                                            <div class="apexcharts-tooltip-title" style={{ fontFamily: 'Helvetica, Arial, sans-serif', fontSize: '12px' }}>Sports P/L</div>
                                                            <div class="apexcharts-tooltip-series-group apexcharts-active" style={{ order: 1, display: 'flex' }}><span class="apexcharts-tooltip-marker" style={{ backgroundColor: 'rgba(85, 110, 230, 0.85)' }}></span>
                                                                <div class="apexcharts-tooltip-text" style={{ fontFamily: 'Helvetica, Arial, sans-serif', fontSize: '12px' }}>
                                                                    <div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label">series-1: </span><span class="apexcharts-tooltip-text-value">-31.25</span>
                                                                    </div>
                                                                    <div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                                            <div class="apexcharts-yaxistooltip-text"></div>
                                                        </div>
                                                        <div class="apexcharts-toolbar" style={{ top: '0px', right: '3px' }}>
                                                            <div class="apexcharts-menu-icon" title="Menu" onClick={this.Graphtoggle_b.bind(this)}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                    <path fill="none" d="M0 0h24v24H0V0z"></path>
                                                                    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
                                                                </svg>
                                                            </div>
                                                            <div class={apexcharts_menu_b.join(' ')}>
                                                                <div class="apexcharts-menu-item exportSVG" title="Download SVG">Download SVG</div>
                                                                <div class="apexcharts-menu-item exportPNG" title="Download PNG" >Download PNG</div>
                                                                <div class="apexcharts-menu-item exportCSV" title="Download CSV">Download CSV</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="resize-triggers">
                                                    <div class="expand-trigger">
                                                        <div style={{ width: '1582px', height: '366px' }}></div>
                                                    </div>
                                                    <div class="contract-trigger"></div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="row row5 align-self-center text-center">
                                                    <div class="col-6 col-sm-3">
                                                        <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-primary"></i> Sports P/L</p>
                                                        <h5>-31</h5>
                                                    </div>
                                                    <div class="col-6 col-sm-3">
                                                        <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-warning"></i> Casino P/L</p>
                                                        <h5>0</h5>
                                                    </div>
                                                    <div class="col-6 col-sm-3 ">
                                                        <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-info"></i> Tp Casino P/L</p>
                                                        <h5>0</h5>
                                                    </div>
                                                    <div class="col-6 col-sm-3">
                                                        <p class="mb-2 fontSize-11"><i class="mdi mdi-circle align-middle fontSize-10 me-2 text-success"></i> Total P/L</p>
                                                        <h5>-31</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <h4 class="sport-list-title pl-2">Our Live Casino</h4>
                                    <div data-simplebar="init" style={{ maxHeight: 470 }}>
                                        <div class="simplebar-wrapper" style={{ margin: '0px' }}>
                                            <div class="simplebar-height-auto-observer-wrapper">
                                                <div class="simplebar-height-auto-observer"></div>
                                            </div>
                                            <div class="simplebar-mask">
                                                <div class="simplebar-offset" style={{ right: '0px', bottom: '0px' }}>
                                                    <div class="simplebar-content-wrapper" style={{ height: 'auto', overflow: 'hidden scroll' }}>
                                                        <div class="simplebar-content" style={{ padding: '0px' }}>
                                                            <div class="casino-banners">
                                                                {
                                                                    this.state.our_live_casinos.map((live_casino, index) => {
                                                                        return (
                                                                            <div class="casino-banner-item">
                                                                                <a href="/admin/casino/teenmuf" class="">
                                                                                    <img class="img-fluid" data-src={live_casino.image} src={live_casino.image} lazy="loading" />
                                                                                </a>
                                                                            </div>
                                                                        );
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="simplebar-placeholder" style={{ width: 'auto', height: '1894px' }}></div>
                                        </div>
                                        <div class="simplebar-track simplebar-horizontal" style={{ visibility: 'hidden' }}>
                                            <div class="simplebar-scrollbar" style={{ width: '0px', display: 'none' }}></div>
                                        </div>
                                        <div class="simplebar-track simplebar-vertical" style={{ visibility: 'visible' }}>
                                            <div class="simplebar-scrollbar" style={{ height: '116px', display: 'block', transform: 'translate3d(0px, 0px, 0px)' }}></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
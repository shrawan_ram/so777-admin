import logo from '../logo.svg';
import '../App.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import '../assets/css/all.css';
import '../assets/admin/css/app.css';


import '../assets/admin/css/theme.css';
import '../assets/admin/css/custom.css';


import { Admin_prifix } from "../configs/costant.config";
import ReactTypingEffect from 'react-typing-effect';

import Popup from 'reactjs-popup';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Slider from "react-slick";
import { ApiExecute, ToastMessage } from '../Api';
import { AsyncStorage } from 'AsyncStorage';
import '../assets/admin/css/style.css';
import '../assets/admin/css/responsive.css';

export default class AdminScreen extends Component {
  state = {
    our_live_casinos: [
      {
        image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
      }, {
        image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
      }
    ],
    live_casinos: [
      {
        image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/ezugi.jpg'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/superspade.jpg'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/qt.jpg'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/evolution.jpg'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/home-banners/live-casino/cockfight.jpg'
      }
    ],
    fantasy_games: [
      {
        image: 'https://sitethemedata.com/casino_icons/other/diam11.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/other/playerbattle.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/other/poptheball.jpg'
      },
      {
        image: 'https://sitethemedata.com/casino_icons/other/ludoclub.jpg'
      },
    ],
    other_games: [
      {
        image: 'https://sitethemedata.com/casino_icons/other/binary.jpg'
      }
    ],
    sports: [
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/4-color.svg',
        name: 'Cricket'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/2-color.svg',
        name: 'Tennis'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/1-color.svg',
        name: 'Football'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/8-color.svg',
        name: 'Table Tennis'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/19-color.svg',
        name: 'Ice Hockey'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/55-color.svg',
        name: 'E Games'
      }, {
        image: 'https://sitethemedata.com/v3/static/front/img/events/11-color.svg',
        name: 'Rugby League'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/3-color.svg',
        name: 'Boxing'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/7-color.svg',
        name: 'Beach Volleyball'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/9-color.svg',
        name: 'Mixed Martial Arts'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/10-color.svg',
        name: 'Futsal'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/12-color.svg',
        name: 'Horse Racing'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/15-color.svg',
        name: 'Greyhounds'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/16-color.svg',
        name: 'Basketball'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/17-color.svg',
        name: 'MotoGP'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/18-color.svg',
        name: 'Chess'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/22-color.svg',
        name: 'Volleyball'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/29-color.svg',
        name: 'Badminton'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/25-color.svg',
        name: 'Hockey'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/32-color.svg',
        name: 'Cycling'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/35-color.svg',
        name: 'Motorbikes'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/events/37-color.svg',
        name: 'Athletics'
      },

    ],
    top_winners: [
      {
        image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
        player: 'ga****',
        time: '11/07/2021 23:48',
        amount: '46,34,000'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
        player: 'ga****',
        time: '11/07/2021 23:48',
        amount: '46,34,000'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
        player: 'ga****',
        time: '11/07/2021 23:48',
        amount: '46,34,000'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
        player: 'ga****',
        time: '11/07/2021 23:48',
        amount: '46,34,000'
      },
      {
        image: 'https://sitethemedata.com/v3/static/front/img/user-icon.png',
        player: 'ga****',
        time: '11/07/2021 23:48',
        amount: '46,34,000'
      },
    ],
    upcoming_fixtures: [
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
      {
        name: 'Marchenko v Anderson',
        date: '13/07/2021 00:30:00'
      },
    ],
    banners: [
      {
        image: 'https://sitethemedata.com/sitethemes/common/front/banners/16253794381741858.jpeg'
      },
      {
        image: 'https://sitethemedata.com/sitethemes/common/front/banners/162591097559015.jpeg'
      },
      {
        image: 'https://sitethemedata.com/sitethemes/common/front/banners/1625577403705665.jpeg'
      }
    ],
    addClass: false,
    username: '',
    password: '',
  }
  onChangeUsername = e => {
    this.setState({ username: e.target.value });
  }
  onChangePassword = e => {
    this.setState({ password: e.target.value });
  }
  toggle() {
    this.setState({ addClass: !this.state.addClass });
  }
  loginSubmit = async () => {
    let {
      username,
      password
    } = this.state;

    if (!username || username.trim() === '') {
      ToastMessage('error', 'Please enter username!');
    } else if (!password || password.trim() === '') {
      ToastMessage('error', 'Please enter password!');
    } else {
      let data = {
        user_name: username,
        password: password,
      }
      let login_response = await ApiExecute("admin/login", { method: 'POST', data: data });

      console.log('login_response', login_response);
      if (login_response.data.status) {
        sessionStorage.setItem('@user', JSON.stringify(login_response.data.data));
        sessionStorage.setItem('@token', login_response.token);
        let url = `/${Admin_prifix}dashboard`;

        // this.props.history.push(url);
        window.location = url;

        ToastMessage('success', login_response.data.msg);
      }
      else {
        ToastMessage('error', login_response.data.msg);
      }
    }
  }

  async componentDidMount() {
    let user = sessionStorage.getItem('@user');
    let user_token = sessionStorage.getItem('@token');
    console.log('user_token', user);
    if (user_token) {
      this.props.history.push('/dashboard');
    }
  }
  render() {
    let boxClass = ["header-right"];
    let login = ["login"];
    if (this.state.addClass) {
      login.push('right-bar-enabled');
    }

    let { Router } = this.props;

    var settings = {
      speed: 5000,
      autoplay: true,
      autoplaySpeed: 0,
      centerMode: true,
      cssEase: 'linear',
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      infinite: true,
      initialSlide: 1,
      arrows: false,
      buttons: false
    };

    var banner = {
      // autoplay:true,
      loop: false,
      dots: true,
      showsButtons: true,
      arrows: false,
      buttons: true,


    }

    return (
      <div className={login.join(' ')} >

        <ToastContainer
          position="top-center"
          autoClose={4000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick={false}
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <div id="app">
          <div data-v-8f01e1aa="" class="wrapper home-new">
            <div data-v-8f01e1aa="" class="container">
              <div data-v-8f01e1aa="" class="home-new-header">
                <div data-v-8f01e1aa="" class="home-new-logo">
                  <img data-v-8f01e1aa="" data-src="https://sitethemedata.com/sitethemes/world777.com/admin/logo.png" src="https://sitethemedata.com/sitethemes/world777.com/admin/logo.png" lazy="loaded" />
                </div>
                <div data-v-8f01e1aa="" class="home-new-header-bottom">
                  <div data-v-8f01e1aa="" class="header-sport-list d-none-mobile">
                    <marquee scrollamount="4" loop="infinite">
                      <div data-v-8f01e1aa="" class="all-sports-list">
                        {
                          this.state.sports.map((sport, index) => {
                            return (
                              <div data-v-8f01e1aa="" class="sport-list-item">
                                <img data-v-8f01e1aa="" data-src={sport.image} src={sport.image} lazy="loaded" />
                                <div data-v-8f01e1aa="" class="mt-1 text-center">{sport.name}</div>
                              </div>
                            );
                          })
                        }
                      </div>
                    </marquee>
                  </div>
                </div>
                <div data-v-8f01e1aa="" class="header-right" onClick={this.toggle.bind(this)}>
                  <button data-v-8f01e1aa="" class="btn btn-primary login-btn">Login</button>
                </div>
              </div>
              <div data-v-8f01e1aa="" class="upcoming-fixure">
                <div className="fixure-title">Upcoming Fixtures</div>
                <marquee data-v-8f01e1aa="">
                  <div data-v-8f01e1aa="" class="fixure-box-container">
                    {
                      this.state.upcoming_fixtures.map((upcoming_fixture, index) => {
                        return (
                          <div data-v-8f01e1aa="" class="fixure-box">
                            <div data-v-8f01e1aa="">
                              <i data-v-8f01e1aa="" class="d-icon mr-2 icon-2"></i>
                              {upcoming_fixture.name}
                            </div>
                            <div data-v-8f01e1aa="">{upcoming_fixture.date}</div>
                          </div>
                        );
                      })
                    }
                  </div>
                </marquee>
              </div>
              <div data-v-8f01e1aa="" class="w-100 d-none-desktop">
                <marquee scrollamount="4" loop="infinite">
                  <div data-v-8f01e1aa="" class="all-sports-list">
                    {
                      this.state.sports.map((sport, index) => {
                        return (
                          <div data-v-8f01e1aa="" class="sport-list-item">
                            <img data-v-8f01e1aa="" data-src={sport.image} src={sport.image} lazy="loaded" />
                            <div data-v-8f01e1aa="" class="mt-1 text-center">{sport.name}</div>
                          </div>
                        );
                      })
                    }
                  </div>
                </marquee>
              </div>
              <div data-v-8f01e1aa="">
                <div data-v-8f01e1aa="" role="region" aria-busy="true" class="carousel slide" id="__BVID__15">
                  <Slider {...banner} >
                    {
                      this.state.banners.map((banner, index) => {
                        return (
                          <div className="carousel-item">
                            <div>
                              <img src={banner.image} className="img-logo" />
                            </div>
                          </div>
                        );
                      })
                    }
                  </Slider>
                </div>
              </div>
              <div data-v-8f01e1aa="">
                <h4 data-v-8f01e1aa="" class="sport-list-title">Our Live Casino</h4>
                <div data-v-8f01e1aa="" class="casino-banners-list mt-2">
                  {
                    this.state.our_live_casinos.map((live_casino, index) => {
                      return (
                        <div data-v-8f01e1aa="" class="casino-banner-item login-hover" onClick={this.toggle.bind(this)}>
                          <img data-v-8f01e1aa="" class="img-fluid" data-src={live_casino.image} src={live_casino.image} lazy="loaded" />
                          <div data-v-8f01e1aa="">Login</div>
                        </div>
                      );
                    })
                  }
                </div>
                <div data-v-8f01e1aa="" class="container-fluid container-fluid-5">
                  <div data-v-8f01e1aa="" class="row row5">
                    <div data-v-8f01e1aa="" class="col-12 col-md-4">
                      <h4 data-v-8f01e1aa="" class="sport-list-title">Live Casino</h4>
                      <div data-v-8f01e1aa="" class="casino-banners-list live-casinos mt-2">
                        {
                          this.state.live_casinos.map((live_casino, index) => {
                            return (
                              <div data-v-8f01e1aa="" class="casino-banner-item login-hover" onClick={this.toggle.bind(this)}>
                                <a data-v-8f01e1aa="" href="javascript:void(0);">
                                  <img data-v-8f01e1aa="" class="img-fluid" data-src={live_casino.image} src={live_casino.image} lazy="loaded" />
                                  <div data-v-8f01e1aa="" role="button" tabindex="0">Login</div>
                                </a>
                              </div>
                            );
                          })
                        }
                      </div>
                    </div>
                    <div data-v-8f01e1aa="" class="col-12 col-md-4">
                      <h4 data-v-8f01e1aa="" class="sport-list-title">Fantasy Games</h4>
                      <div data-v-8f01e1aa="" class="casino-banners-list fantasy-games mt-2">
                        {
                          this.state.fantasy_games.map((fantasy_game, index) => {
                            return (
                              <div data-v-8f01e1aa="" class="casino-banner-item login-hover" onClick={this.toggle.bind(this)}>
                                <a data-v-8f01e1aa="" href="javascript:void(0);">
                                  <img data-v-8f01e1aa="" class="img-fluid" data-src={fantasy_game.image} src={fantasy_game.image} lazy="loaded" />
                                  <div data-v-8f01e1aa="" role="button" tabindex="0">Login</div>
                                </a>
                              </div>
                            );
                          })
                        }
                      </div>
                    </div>
                    <div data-v-8f01e1aa="" class="col-12 col-md-4">
                      <h4 data-v-8f01e1aa="" class="sport-list-title">Others</h4>
                      <div data-v-8f01e1aa="" class="casino-banners-list fantasy-games mt-2">
                        {
                          this.state.other_games.map((other_game, index) => {
                            return (
                              <div data-v-8f01e1aa="" class="casino-banner-item login-hover" onClick={this.toggle.bind(this)}>
                                <a data-v-8f01e1aa="" href="javascript:void(0);">
                                  <img data-v-8f01e1aa="" src={other_game.image} class="img-fluid" />
                                  <div data-v-8f01e1aa="" role="button" tabindex="0">Login</div>
                                </a>
                              </div>
                            );
                          })
                        }
                      </div>
                    </div>
                  </div>
                </div>
                <h4 data-v-8f01e1aa="" class="sport-list-title">Top Winners</h4>
                <div data-v-8f01e1aa="" class="top-winners-list-container mt-2">
                  <div data-v-8f01e1aa="" class="top-winner-list-box-container1">
                    <Slider {...settings} >
                      {
                        this.state.top_winners.map((winner, index) => {
                          return (
                            <div data-v-e4caeaf8="" tabindex="-1" data-index="-10" aria-hidden="true" class="slick-slide slick-cloned">
                              <div data-v-e4caeaf8="">
                                <div data-v-8f01e1aa="" data-v-e4caeaf8="" tabindex="-1" style={{ width: '100%', display: 'inline-block' }}>
                                  <div data-v-8f01e1aa="" data-v-e4caeaf8="" class="top-winner-list-box">
                                    <div data-v-8f01e1aa="" data-v-e4caeaf8="" class="w-100 text-center">
                                      <img data-v-8f01e1aa="" data-v-e4caeaf8="" data-src={winner.image} src={winner.image} lazy="loading" />
                                    </div>
                                    <div data-v-8f01e1aa="" data-v-e4caeaf8="" class="w-100">
                                      <div data-v-8f01e1aa="" data-v-e4caeaf8="" class="player-detail">
                                        <div data-v-8f01e1aa="" data-v-e4caeaf8=""><b data-v-8f01e1aa="" data-v-e4caeaf8="">Player</b>
                                        </div>
                                        <div data-v-8f01e1aa="" data-v-e4caeaf8="">{winner.player}</div>
                                      </div>
                                      <div data-v-8f01e1aa="" data-v-e4caeaf8="" class="player-detail">
                                        <div data-v-8f01e1aa="" data-v-e4caeaf8=""><b data-v-8f01e1aa="" data-v-e4caeaf8="">Time</b>
                                        </div>
                                        <div data-v-8f01e1aa="" data-v-e4caeaf8="">{winner.time}</div>
                                      </div>
                                      <div data-v-8f01e1aa="" data-v-e4caeaf8="" class="player-detail">
                                        <div data-v-8f01e1aa="" data-v-e4caeaf8=""><b data-v-8f01e1aa="" data-v-e4caeaf8="">Win Amount</b>
                                        </div>
                                        <div data-v-8f01e1aa="" data-v-e4caeaf8="">{winner.amount}</div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })
                      }
                    </Slider>
                  </div>
                </div>
              </div>
              <footer data-v-8f01e1aa="" class="footer">
                <div data-v-8f01e1aa="" class="container-fluid container-fluid-5">
                  <div data-v-8f01e1aa="" class="row row5">
                    <div data-v-8f01e1aa="" class="col-lg-12 text-center">

                      <div data-v-8f01e1aa="" class="mt-2 gt">
                        <a data-v-8f01e1aa="" href="javascript:void(0)" role="button">
                          <img data-v-8f01e1aa="" data-src="https://sitethemedata.com/v3/static/front/img/18plus.png" src="https://sitethemedata.com/v3/static/front/img/18plus.png" lazy="loaded" />
                        </a>
                        <a data-v-8f01e1aa="" href="https://www.gamcare.org.uk/" target="_blank">
                          <img data-v-8f01e1aa="" data-src="https://sitethemedata.com/v3/static/front/img/gamecare.png" src="https://sitethemedata.com/v3/static/front/img/gamecare.png" lazy="loaded" />
                        </a>
                        <a data-v-8f01e1aa="" href="https://www.gamblingtherapy.org/en" target="_blank">
                          <img data-v-8f01e1aa="" data-src="https://sitethemedata.com/v3/static/front/img/gt.png" src="https://sitethemedata.com/v3/static/front/img/gt.png" lazy="loaded" />
                        </a>
                      </div>
                      <div data-v-8f01e1aa="" class="mt-3">© Copyright 2021. All Rights Reserved.</div>
                    </div>
                  </div>
                </div>
              </footer>
              <div data-v-8f01e1aa="">
                <div data-v-8f01e1aa="" class="right-bar">
                  <div data-v-8f01e1aa="">
                    <div data-v-8f01e1aa="" class="rightbar-title px-3 py-4">
                      <a data-v-8f01e1aa="" href="javascript:void(0);" class="closebtn float-right" onClick={this.toggle.bind(this)}></a>
                      <h3 data-v-8f01e1aa="" class="m-0 text-light">ADMIN LOGIN</h3>
                    </div>

                    <div data-v-8f01e1aa="" class="p-4 mt-5">
                      <div data-v-8f01e1aa="" class="overflow-hidden">
                        <div data-v-8f01e1aa="">
                          <h3 data-v-8f01e1aa="" class="text-center mt-2 mb-0 text-secondary">
                            Welcome to Admin Panel
                          </h3>
                          <p data-v-8f01e1aa="" class="text-center text-secondary">Enter your Username and Password</p>

                          <div data-v-8f01e1aa="" id="input-group-1" role="group" class="form-group">

                            <div>
                              <input data-v-8f01e1aa="" id="input-1" name="username" value={this.state.username}
                                onInput={this.onChangeUsername} type="text" placeholder="Enter Username" class="form-control-lg form-control" />
                            </div>
                          </div>
                          <div data-v-8f01e1aa="" id="input-group-2" role="group" class="form-group">
                            <div>
                              <input data-v-8f01e1aa="" id="input-2" name="password" type="password" value={this.state.password}
                                onInput={this.onChangePassword} placeholder="Enter password" class="form-control-lg form-control" />
                            </div>
                          </div>
                          <div data-v-8f01e1aa="" class="mt-3">
                            <button data-v-8f01e1aa="" onClick={this.loginSubmit} type="submit" class="btn btn-block btn-theme1 btn-lg btn-submit btn-secondary">Sign In</button>

                          </div>
                        </div>
                        <div data-v-8f01e1aa="" class="text-center text-secondary mt-2">
                          <div data-v-8f01e1aa="" class="mb-2">© Copyright 2021. All Rights Reserved.</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div data-v-8f01e1aa="" class="rightbar-overlay"></div>
              </div>
            </div>
          </div>
        </div>

      </div>

    );
  }
}
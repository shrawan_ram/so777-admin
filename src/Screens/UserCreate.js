import React, { Component } from "react";
import { ApiExecute, ToastMessage } from '../Api';
import { Link } from "react-router-dom";
import { Admin_prifix } from "../configs/costant.config";


export default class DashboardScreen extends Component {
    state = {
        roles: [],
        id: null,
        name: '',
        mobile: '',
        email: '',
        city: '',
        password: '',
        role_id: '',
        user_name: '',
        remark: '',
        transaction_code: '',
        amount: '0',
    }

    onChangename = e => {
        this.setState({ name: e.target.value });
    }
    onChangeusername = e => {
        this.setState({ user_name: e.target.value });
    }
    onChangemobile = e => {
        this.setState({ mobile: e.target.value });
    }
    onChangecity = e => {
        this.setState({ city: e.target.value });
    }
    onChangeamount = e => {
        this.setState({ amount: e.target.value });
    }
    onChangeemail = e => {
        this.setState({ email: e.target.value });
    }
    onChangepassword = e => {
        this.setState({ password: e.target.value });
    }
    onChangerole = e => {
        this.setState({ role_id: e.target.value });
    }
    onChangeremark = e => {
        this.setState({ remark: e.target.value });
    }
    onChangetransactioncode = e => {
        this.setState({ transaction_code: e.target.value });
    }

    saveUser = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log('user', user);
        let admin_id = user.id;
        let {
            id,
            name,
            user_name,
            email,
            password,
            role_id,
            mobile,
            city,
            remark,
            transaction_code,
        } = this.state;
        console.log(admin_id);

        if (!name || name.trim() === '') {
            ToastMessage('error', 'Please enter name !');
        } else if (!user_name || user_name.trim() === '') {
            ToastMessage('error', 'Please enter user name !');
        } else if (!role_id === '') {
            ToastMessage('error', 'Please select role !');
        } else if (!transaction_code === '') {
            ToastMessage('error', 'Please enter transaction password !');
        } else {
            if (id) {
                let data = {
                    id: id,
                    name: name,
                    user_name: user_name,
                    mobile: mobile,
                    email: email,
                    role_id: role_id,
                    admin_id: admin_id,
                    password: password,
                    city: city,
                    remark: remark,
                    transaction_code: transaction_code,
                }
                let api_response = await ApiExecute(`user/${id}`, { method: 'PUT', data: data });

                console.log('login_response', api_response.data.errors);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.user_name = null;
                    this.state.mobile = null;
                    this.state.mobile = null;
                    this.state.password = null;
                    this.state.role_id = null;
                    this.state.city = null;
                    this.state.remark = null;
                    this.state.transaction_code = null;
                    this.state.id = null;
                    let url = `/${Admin_prifix}user`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            } else {
                let data = {
                    name: name,
                    user_name: user_name,
                    mobile: mobile,
                    email: email,
                    role_id: role_id,
                    admin_id: admin_id,
                    password: password,
                    city: city,
                    remark: remark,
                    transaction_code: transaction_code,
                }
                let api_response = await ApiExecute("add-user", { method: 'POST', data: data });

                console.log('login_response', api_response.data.data);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.user_name = null;
                    this.state.email = null;
                    this.state.mobile = null;
                    this.state.password = null;
                    this.state.role_id = null;
                    this.state.city = null;
                    this.state.remark = null;
                    this.state.transaction_code = null;
                    this.state.id = null;

                    this.addWallet(api_response.data.data);

                    // let url = `/${Admin_prifix}user`;
                    // this.props.history.push(url);
                    // ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }

            }
        }
    }

    addWallet = async (id) => {
        let admin_id = id;
        let {
            amount,
        } = this.state;

        let data1 = {
            amount: amount,
            type: 'main',
            user_id: admin_id
        }
        let data2 = {
            amount: 0,
            type: 'exposure',
            user_id: admin_id
        }
        let api1_response = await ApiExecute("add-wallet", { method: 'POST', data: data1 });
        let api2_response = await ApiExecute("add-wallet", { method: 'POST', data: data2 });

        console.log('wallet_response', api1_response);
        if (api1_response.data.status) {
            this.state.amount = 0;

            let url = `/${Admin_prifix}user`;
            this.props.history.push(url);
            // ToastMessage('success', api1_response.data.message);
            ToastMessage('success', 'User created successfully');
        }
        else {
            if (api1_response.data.errors == undefined && api1_response.data.errors == null) {
                ToastMessage('error', api1_response.data.message);
            } else {
                ToastMessage('error', api1_response.data.errors[0]);
            }
        }
    }

    getRoles = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log(user.role_id);

        let api_response = await ApiExecute(`role?role_id=${user.role_id}`, { method: 'GET' });
        this.setState({ roles: api_response.data });
        console.log(api_response.data);
    }

    getusers = async () => {
        let user_id = this.state.id;
        console.log(user_id);
        let api_response = await ApiExecute(`user/${this.state.id}`, { method: 'GET' });
        console.log('api_response', api_response);
        this.setState({ mobile: api_response.data.mobile });
        this.setState({ name: api_response.data.name });
        this.setState({ user_name: api_response.data.user_name });
        this.setState({ email: api_response.data.email });
        this.setState({ role_id: api_response.data.role_id });
    }

    componentDidMount() {
        this.getRoles();
        let id = this.props.match.params.id;
        if (id) {
            this.state.id = id;
            this.getusers();
        }
    }


    render() {
        let self = this;
        let { Router } = this.props;
        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");
        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">{this.state.id ? "Update" : "Create"} User</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}user`} class="router-link-active" target="_self">Users</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Create User</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">General Information</h4>
                                            <div class="form-group">
                                                <label>User Name :</label>
                                                <input placeholder="User Name" type="text" name="user_name" autoComplete="off" required value={this.state.user_name} onInput={this.onChangeusername} data-vv-as="User Name" autocomplete="username" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Full Name :</label>
                                                <input placeholder="Full Name" type="text" name="name" value={this.state.name} onInput={this.onChangename} data-vv-as="Name" autocomplete="Name" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Email :</label>
                                                <input placeholder="Email" type="email" data-vv-as="Email" name="email" value={this.state.email} onInput={this.onChangeemail} class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Mobile Number :</label>
                                                <input placeholder="mobile" type="text" name="mobile" value={this.state.mobile} onInput={this.onChangemobile} data-vv-as="mobile" autocomplete="mobile" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Password :</label>
                                                <input placeholder="Password" autoComplete="off" type="password" data-vv-as="Password" name="password" value={this.state.password} onInput={this.onChangepassword} class="form-control animation" aria-required="false" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>City :</label>
                                                <input placeholder="city" type="text" name="city" value={this.state.city} onInput={this.onChangecity} data-vv-as="city" autocomplete="city" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            {/* <div class="form-group">
                                                            <label>Confirm Password:</label>
                                                            <input placeholder="Confirm Password" type="password" data-vv-as="Confirm Password" name="cpassword" class="form-control animation" aria-required="true" aria-invalid="false" />
                                                        </div> */}
                                            {/* <div class="d-flex justify-content-end align-items-center">
                                                <button type="submit" onClick={this.saveUser} id="spinner-dark-8" class="btn btn-primary ml-2">
                                                    Submit
                                                </button>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group tag-select">
                                                <label>User Type :</label>
                                                <select name="role" defaultValue={this.state.role_id} value={this.state.role_id} onChange={this.onChangerole} class="form-control animation">
                                                    <option value="">Select User Type</option>
                                                    {
                                                        this.state.roles.map((role) => {
                                                            return (
                                                                <option value={role.id}>{role.name}</option>
                                                            );
                                                        })
                                                    }
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Credit Amount:</label>
                                                {
                                                    this.state.role_id == 5 ?
                                                        <input placeholder="Credit Amount" disabled style={{ backgroundColor: '#dddddd' }} type="number" min="0" data-vv-as="Credit Amount" name="camt" value="0" onInput={this.onChangeamount} class="form-control" aria-required="false" aria-invalid="false" />
                                                        :
                                                        <input placeholder="Credit Amount" type="number" min="0" data-vv-as="Credit Amount" name="camt" value={this.state.amount} onInput={this.onChangeamount} class="form-control" aria-required="false" aria-invalid="false" />
                                                }
                                            </div>
                                            {/* <h4 class="card-title">Partnership Information</h4>
                                            <div>
                                                <div class="form-group">
                                                    <label>Partnership With No Return:</label>
                                                    <input placeholder="Partnership With No Return" type="text" name="spart1" data-vv-as="Partnership With No Return" maxlength="4" class="form-control animation" aria-required="true" aria-invalid="false" />
                                                    <p class="help is-success m-0 d-inline-block">Our: 0 | Down Line: 0</p>
                                                </div>
                                            </div> */}

                                            <div class="form-group">
                                                <label>Remark:</label>
                                                <textarea placeholder="Remark" data-vv-as="Remark" name="remark" value={this.state.remark} onInput={this.onChangeremark} class="form-control" aria-required="false" aria-invalid="false"></textarea>
                                            </div>
                                            <div class="d-flex justify-content-end align-items-center">
                                                <input placeholder="Transaction Code" type="password" name="mpassword" value={this.state.transaction_code} onInput={this.onChangetransactioncode} class="form-control" aria-required="true" aria-invalid="false" />
                                                <button type="submit" onClick={() => this.saveUser()} id="spinner-dark-8" class="btn btn-primary ml-2">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >

        );
    }
}
import React, { Component } from "react";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import { ApiExecute, ToastMessage } from '../Api';
import { Admin_prifix } from "../configs/costant.config";

export default class CurrentBet extends Component {

    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        allselected: 0,
        users: [],
        userIds: [],
        s: '',
    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }
    betList = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        let api_response = await ApiExecute(`userbats/${user.id}`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ users: api_response.data.results })

    }

    betListwithSearch = async () => {
        console.log('sdfjhdj', this.state.s);
        let api_response = await ApiExecute(`user?s=${this.state.s}`, { method: 'GET' });
        console.log('search', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }

    componentDidMount() {
        this.betList();
    }
    selectId(id) {
        console.log('sdfjkdksl');
        if (this.state.userIds.includes(id)) {
            var index = this.state.userIds.indexOf(id);
            this.state.userIds.splice(index, 1);
        } else {
            this.setState({ userIds: this.state.userIds.concat(id) });
        }
    }
    selectAllId = (e) => {
        console.log('sdlkfjdlkj : ', e.target.checked);
        let array = [];
        if (e.target.checked) {
            this.state.users.forEach(element => {
                console.log('element', element.id);
                array.push(element.id);
            });
            this.setState({ userIds: array });
        } else {
            this.setState({ userIds: array });
        }
        console.log('all selected', this.state.userIds);
    }

    deleteAll = async () => {
        if (this.state.userIds.length) {
            let data = this.state.userIds.join(",");
            console.log('data', data);
            let api_response = await ApiExecute("remove-user", { method: 'POST', data: data });
            console.log('api_response', api_response);
            ToastMessage('success', api_response.msg);
            this.betList();
        } else {
            ToastMessage('error', 'Please select atleast one !');
        }
    }

    searchByName(status) {
        if (status) {
            this.betListwithSearch();
        } else {
            this.setState({ s: '' });
            this.betList();
        }
    }

    render() {
        const { isOpened } = this.state;
        const height = 100;
        const { userIds } = this.state;
        const { search } = this.state

        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");

        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }

        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">

                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Current Bets</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link></li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Current Bets</span></li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <div class="casino-report-tabs">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a data-toggle="tab" href="javascript:void(0)" class="nav-link active">Sports</a></li>
                                    <li class="nav-item"><a data-toggle="tab" href="javascript:void(0)" class="nav-link">Casino</a></li>
                                </ul>
                            </div> */}
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="report-form mb-3 row align-items-center">
                                                <div class="col-md-4 col-lg-3">
                                                    <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="customRadio" name="example" value="matchbet" checked="checked" class="custom-control-input" /> <label for="customRadio" class="custom-control-label">Matched</label></div>
                                                    <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="customRadio2" name="example" value="deletebet" class="custom-control-input" /> <label for="customRadio2" class="custom-control-label">Deleted</label></div>
                                                </div>
                                                <div class="col-md-8  col-lg-4">
                                                    <div class="custom-control custom-radio custom-control-inline pl-0">
                                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="soda-all" name="bettype" value="all" checked="checked" class="custom-control-input" /> <label for="soda-all" class="custom-control-label">All</label></div>
                                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="soda-back" name="bettype" value="back" class="custom-control-input" /> <label for="soda-back" class="custom-control-label">Back</label></div>
                                                        <div class="custom-control custom-radio custom-control-inline"><input type="radio" id="soda-lay" name="bettype" value="lay" class="custom-control-input" /> <label for="soda-lay" class="custom-control-label">Lay</label></div>
                                                    </div>
                                                    <div class="custom-control-inline">
                                                        <button title="Refresh Data" type="button" class="btn mr-2 btn-primary">Load
                                                        </button>
                                                        <div id="export_1633328755611" class="d-inline-block disabled"><button type="button" disabled="disabled" class="btn mr-1 btn-success disabled"><i class="fas fa-file-excel"></i></button></div>
                                                        <button type="button" disabled="disabled" class="btn btn-danger disabled"><i class="fas fa-file-pdf"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-lg-5 text-right">
                                                    <div class="custom-control-inlinemr-0 mt-1">
                                                        <h5>Total Soda: <span class="mr-2">0</span> Total Amount: <span>0.00</span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row w-100">
                                                <div class="col-6">
                                                    <div id="tickets-table_length" class="dataTables_length">
                                                        <label class="d-inline-flex align-items-center">
                                                            Show&nbsp;
                                                            <select class="custom-select custom-select-sm" id="__BVID__621">
                                                                <option value="25">25</option>
                                                                <option value="50">50</option>
                                                                <option value="75">75</option>
                                                                <option value="100">100</option>
                                                                <option value="125">125</option>
                                                                <option value="150">150</option>
                                                            </select>
                                                            &nbsp;entries
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-6 text-right">
                                                    <div id="tickets-table_filter" class="dataTables_filter text-md-right"><label class="d-inline-flex align-items-center"><input type="search" placeholder="Search..." class="form-control form-control-sm ml-2 form-control" id="__BVID__622" /></label></div>
                                                </div>
                                            </div>
                                            <div class="table-responsive mb-0">
                                                <div class="table no-footer table-responsive-sm">
                                                    <table id="eventsListTbl" role="table" aria-busy="false" aria-colcount="11" class="table b-table table-bordered">

                                                        <colgroup>
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: '200px' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                            <col style={{ width: 'auto' }} />
                                                        </colgroup>
                                                        <thead role="rowgroup" class="">

                                                            <tr role="row" class="">
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="1" aria-sort="none" class="">
                                                                    <div>S.N.</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="1" aria-sort="none" class="">
                                                                    <div>Event Type</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="2" aria-sort="none" class="">
                                                                    <div>Event Name</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="2" aria-sort="none" class="">
                                                                    <div>Event Des.</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="3" aria-sort="none" class="">
                                                                    <div>User Name</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="4" aria-sort="none" class="">
                                                                    <div>Type</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="6" aria-sort="none" class="text-right">
                                                                    <div>U Rate</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="7" aria-sort="none" class="text-right">
                                                                    <div>Amount</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="5" aria-sort="none" class="">
                                                                    <div>Profit / Loss</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="8" aria-sort="none" class="">
                                                                    <div>Place Date</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" tabindex="0" aria-colindex="9" aria-sort="none" class="">
                                                                    <div>IP</div>
                                                                    <span class="sr-only"> (Click to sort Ascending)</span>
                                                                </th>
                                                                <th role="columnheader" scope="col" aria-colindex="10" class="">
                                                                    <div>Browser</div>
                                                                </th>
                                                                <th role="columnheader" scope="col" aria-colindex="11" class="">
                                                                    <div>Action</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody role="rowgroup">
                                                            {
                                                                this.state.users && this.state.users.length ?
                                                                    <>
                                                                        {
                                                                            this.state.users.map((element, index) => {
                                                                                return (
                                                                                    <tr role="row" class="">
                                                                                        <td role="cell" class="">
                                                                                            <input type="checkbox" checked={userIds.includes(element.id) ? 'checked' : ''} onChange={() => this.selectId(element.id)}></input> | {index + 1}.
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.event_type}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.event}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.event_name}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.user}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.type}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.rate}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.amount}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.profit} / {element.loss}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            <Moment format="DD MMM YYYY hh:mm A">
                                                                                                {element.created_at}
                                                                                            </Moment>
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.ip}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            {element.browser}
                                                                                        </td>
                                                                                        <td role="cell" class="">
                                                                                            <i class="bx bx-dots-horizontal"></i>
                                                                                        </td>
                                                                                        {/* <td role="cell" class="">
                                    <div role="group" class="btn-group">
                                        <button type="button" class="btn btn-danger">E</button>
                                        <button type="button" class="btn btn-info">More</button>
                                    </div>
                                </td> */}
                                                                                    </tr>
                                                                                );
                                                                            })
                                                                        }
                                                                    </>
                                                                    :
                                                                    <tr role="row" class="b-table-empty-row">
                                                                        <td colspan="11" role="cell" class="">
                                                                            <div role="alert" aria-live="polite">
                                                                                <div class="text-center my-2">There are no records to show</div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                            }

                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row pt-3">
                                                <div class="col">
                                                    <div class="dataTables_paginate paging_simple_numbers float-right">
                                                        <ul class="pagination pagination-rounded mb-0">
                                                            <ul role="menubar" aria-disabled="false" aria-label="Pagination" class="pagination dataTables_paginate paging_simple_numbers my-0 b-pagination justify-content-end">
                                                                <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to first page" aria-disabled="true" class="page-link">«</span></li>
                                                                <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to previous page" aria-disabled="true" class="page-link">‹</span></li>

                                                                <li role="presentation" class="page-item active"><button role="menuitemradio" type="button" aria-label="Go to page 1" aria-checked="true" aria-posinset="1" aria-setsize="1" tabindex="0" class="page-link">1</button></li>

                                                                <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to next page" aria-disabled="true" class="page-link">›</span></li>
                                                                <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to last page" aria-disabled="true" class="page-link">»</span></li>
                                                            </ul>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
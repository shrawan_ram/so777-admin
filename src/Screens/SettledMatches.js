import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ApiExecute, ApiExecute1, ToastMessage } from '../Api';
import { Admin_prifix } from "../configs/costant.config";

export default class SettledMatches extends Component {
    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        allselected: 0,
        users: [],
        userIds: [],
        matches: null,
        matchType: null,
        s: '',
    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }
    userList = async (id = null) => {
        let user_id
        if (id) {
            user_id = id;
        } else {
            let user = sessionStorage.getItem('@user');
            user = JSON.parse(user);
            user_id = user.id;
        }
        let api_response = await ApiExecute(`user?admin_id=${user_id}`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }

    userListwithSearch = async () => {
        console.log('sdfjhdj', this.state.s);
        let api_response = await ApiExecute(`user?s=${this.state.s}`, { method: 'GET' });
        console.log('search', api_response.data.results);
        this.setState({ users: api_response.data.results })
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        if (id == '4') {
            this.setState({ matchType: 'cricket' });
        } else if (id == '2') {
            this.setState({ matchType: 'tannis' });
        } else {
            this.setState({ matchType: 'soccer' });
        }
        this.getMatches();
    }
    selectId(id) {
        console.log('sdfjkdksl');
        if (this.state.userIds.includes(id)) {
            var index = this.state.userIds.indexOf(id);
            this.state.userIds.splice(index, 1);
        } else {
            this.setState({ userIds: this.state.userIds.concat(id) });
        }
    }
    selectAllId = (e) => {
        console.log('sdlkfjdlkj : ', e.target.checked);
        let array = [];
        if (e.target.checked) {
            this.state.users.forEach(element => {
                console.log('element', element.id);
                array.push(element.id);
            });
            this.setState({ userIds: array });
        } else {
            this.setState({ userIds: array });
        }
        console.log('all selected', this.state.userIds);
    }

    deleteAll = async () => {
        if (this.state.userIds.length) {
            let data = this.state.userIds.join(",");
            console.log('data', data);
            let api_response = await ApiExecute("remove-user", { method: 'POST', data: data });
            console.log('api_response', api_response);
            ToastMessage('success', api_response.msg);
            this.userList();
        } else {
            ToastMessage('error', 'Please select atleast one !');
        }
    }
    searchByName(status) {
        if (status) {
            this.userListwithSearch();
        } else {
            this.setState({ s: '' });
            this.userList();
        }
    }
    getMatches = async () => {
        this.setState({ loading: true });

        let api_response = await ApiExecute(`match?type="cricket"`, { method: 'GET' });
        console.log('lists', api_response.data.results);

        this.setState({ matches: api_response.data.results });
        console.log('matches', api_response.data.results);
        this.setState({ loading: false });
    }


    render() {
        const { isOpened } = this.state;
        const height = 100;
        const { userIds } = this.state;
        const { search } = this.state

        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");

        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }

        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">

                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Matches List</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Matches List</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row account-list">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row row5">
                                                <div class="col-md-6 mb-2 search-form">
                                                    <form method="post" class="ajaxFormSubmit">
                                                        <div class="d-inline-block form-group form-group-feedback form-group-feedback-right mr-2">
                                                            <input type="text" name="searchKey" value={this.state.s} onChange={(e) => this.setState({ s: e.target.value })} placeholder="Search name" class="form-control" />
                                                        </div>
                                                        <div class="d-inline-block">
                                                            <div onClick={() => this.searchByName(true)} class="btn btn-primary mr-2">Load</div>
                                                            <div onClick={() => this.searchByName(false)} class="btn btn-light">Reset</div>
                                                        </div>
                                                    </form>
                                                </div>
                                                {/* <div class="col-md-6 text-right mb-2">
                                                    <div class="d-inline-block mr-2">
                                                                    <div id="export_1627983356353" class="d-inline-block">
                                                                        <button type="button" class="btn mr-1 btn-success"><i class="fas fa-file-excel"></i>
                                                                        </button>
                                                                    </div>
                                                                    <button type="button" class="btn btn-danger"><i class="fas fa-file-pdf"></i>
                                                                    </button>
                                                                </div>
                                                    <div class="d-inline-block">
                                                        <button type="button" onClick={() => this.deleteAll()} class="btn btn-danger mr-3"><i aria-hidden="true" class="fa fa-trash"></i></button>
                                                        <Link to={`/${Admin_prifix}add_user`} class="btn btn-success"><i aria-hidden="true" class="fa fa-plus"></i> CREATE USER</Link>
                                                    </div>
                                                </div> */}
                                            </div>
                                            {/* <div class="row">
                                                            <div class="col-sm-12 col-md-6">
                                                                <div id="tickets-table_length" class="dataTables_length">
                                                                    <label class="d-inline-flex align-items-center">Show&nbsp;
                                                                        <select class="custom-select custom-select-sm" id="__BVID__1022">
                                                                            <option value="25">25</option>
                                                                            <option value="50">50</option>
                                                                            <option value="100">100</option>
                                                                            <option value="250">250</option>
                                                                            <option value="500">500</option>
                                                                            <option value="750">750</option>
                                                                            <option value="1000">1000</option>
                                                                        </select>&nbsp;entries</label>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                            <div class="table-responsive mb-0 mt-3">
                                                <div class="table no-footer table-responsive-sm">
                                                    <table id="eventsListTbl" role="table" aria-busy="false" aria-colcount="12" class="table b-table">
                                                        <thead role="rowgroup" class="">
                                                            <tr role="row" class="">
                                                                <th role="columnheader" scope="col" >
                                                                    {/* <div><input type="checkbox" name="allSelected" value="" onChange={this.selectAllId}></input> | */}
                                                                    S.N.
                                                                    {/* </div> */}
                                                                </th>
                                                                <th role="columnheader" scope="col" >
                                                                    <div>Name</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody role="rowgroup">
                                                            {
                                                                this.state.matches && this.state.matches.length && this.state.matches.map((element, index) => {
                                                                    return (
                                                                        <tr role="row" class="">
                                                                            <td role="cell" class="">
                                                                                {index + 1}.
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                <Link to={`/${Admin_prifix}settled/${this.state.matchType}/${element.event_id}`} class="">{element.name}</Link>
                                                                            </td>
                                                                        </tr>
                                                                    );
                                                                })
                                                            }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {/* <div class="row pt-3">
                                                            <div class="col">
                                                                <div class="dataTables_paginate paging_simple_numbers float-right">
                                                                    <ul class="pagination pagination-rounded mb-0">
                                                                        <ul role="menubar" aria-disabled="false" aria-label="Pagination" class="pagination dataTables_paginate paging_simple_numbers my-0 b-pagination justify-content-end">
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to first page" aria-disabled="true" class="page-link">«</span>
                                                                            </li>
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to previous page" aria-disabled="true" class="page-link">‹</span>
                                                                            </li>


                                                                            <li role="presentation" class="page-item active">
                                                                                <button role="menuitemradio" type="button" aria-label="Go to page 1" aria-checked="true" aria-posinset="1" aria-setsize="1" tabindex="0" class="page-link">1</button>
                                                                            </li>


                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to next page" aria-disabled="true" class="page-link">›</span>
                                                                            </li>
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to last page" aria-disabled="true" class="page-link">»</span>
                                                                            </li>
                                                                        </ul>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
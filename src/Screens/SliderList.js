import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ApiExecute, ToastMessage } from '../Api';
import { Admin_prifix } from "../configs/costant.config";


export default class SliderList extends Component {

    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        allselected: 0,
        sliders: [],
        sliderIds: [],
        s: '',
    }

    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }
    sliderList = async () => {
        let api_response = await ApiExecute("slider", { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ sliders: api_response.data.results })

    }

    sliderListwithSearch = async () => {
        console.log('sdfjhdj', this.state.s);
        let api_response = await ApiExecute(`slider?s=${this.state.s}`, { method: 'GET' });
        console.log('search', api_response.data.results);
        this.setState({ sliders: api_response.data.results })
    }

    componentDidMount() {
        this.sliderList();
    }
    selectId(id) {
        console.log('sdfjkdksl');
        if (this.state.sliderIds.includes(id)) {
            var index = this.state.sliderIds.indexOf(id);
            this.state.sliderIds.splice(index, 1);
        } else {
            this.setState({ sliderIds: this.state.sliderIds.concat(id) });
        }
    }
    selectAllId = (e) => {
        console.log('sdlkfjdlkj : ', e.target.checked);
        let array = [];
        if (e.target.checked) {
            this.state.sliders.forEach(element => {
                console.log('element', element.id);
                array.push(element.id);
            });
            this.setState({ sliderIds: array });
        } else {
            this.setState({ sliderIds: array });
        }
        console.log('all selected', this.state.sliderIds);
    }

    deleteAll = async () => {
        if (this.state.sliderIds.length) {
            let data = this.state.sliderIds.join(",");
            console.log('data', data);
            let api_response = await ApiExecute("remove-slider", { method: 'POST', data: data });
            console.log('api_response', api_response);
            ToastMessage('success', api_response.msg);
            this.sliderList();
        } else {
            ToastMessage('error', 'Please select atleast one !');
        }
    }
    searchByName(status) {
        if (status) {
            this.sliderListwithSearch();
        } else {
            this.setState({ s: '' });
            this.sliderList();
        }
    }


    render() {
        const { isOpened } = this.state;
        const height = 100;
        const { sliderIds } = this.state;
        const { search } = this.state

        let custom_side = ["custom_side"];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");

        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let apexcharts_menu = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass) {
            apexcharts_menu.push('apexcharts-menu-open')
        }
        let apexcharts_menu_b = ["apexcharts-menu"];
        if (this.state.GraphtoggleAddclass_b) {
            apexcharts_menu_b.push('apexcharts-menu-open')
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }

        return (

            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">

                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Slider List</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Slider List</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row account-list">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row row5">
                                                <div class="col-md-6 mb-2 search-form">
                                                    <form method="post" class="ajaxFormSubmit">
                                                        <div class="d-inline-block form-group form-group-feedback form-group-feedback-right mr-2">
                                                            <input type="text" name="searchKey" value={this.state.s} onChange={(e) => this.setState({ s: e.target.value })} placeholder="Search name" class="form-control" />
                                                        </div>
                                                        <div class="d-inline-block">
                                                            <div onClick={() => this.searchByName(true)} class="btn btn-primary mr-2">Load</div>
                                                            <div onClick={() => this.searchByName(false)} class="btn btn-light">Reset</div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-6 text-right mb-2">
                                                    {/* <div class="d-inline-block mr-2">
                                                                    <div id="export_1627983356353" class="d-inline-block">
                                                                        <button type="button" class="btn mr-1 btn-success"><i class="fas fa-file-excel"></i>
                                                                        </button>
                                                                    </div>
                                                                    <button type="button" class="btn btn-danger"><i class="fas fa-file-pdf"></i>
                                                                    </button>
                                                                </div> */}
                                                    <div class="d-inline-block">
                                                        <button type="button" onClick={() => this.deleteAll()} class="btn btn-danger mr-3"><i aria-hidden="true" class="fa fa-trash"></i></button>
                                                        <Link to={`/${Admin_prifix}add_slider`} class="btn btn-success"><i aria-hidden="true" class="fa fa-plus"></i> CREATE SLIDER</Link>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <div class="row">
                                                            <div class="col-sm-12 col-md-6">
                                                                <div id="tickets-table_length" class="dataTables_length">
                                                                    <label class="d-inline-flex align-items-center">Show&nbsp;
                                                                        <select class="custom-select custom-select-sm" id="__BVID__1022">
                                                                            <option value="25">25</option>
                                                                            <option value="50">50</option>
                                                                            <option value="100">100</option>
                                                                            <option value="250">250</option>
                                                                            <option value="500">500</option>
                                                                            <option value="750">750</option>
                                                                            <option value="1000">1000</option>
                                                                        </select>&nbsp;entries</label>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                            <div class="table-responsive mb-0 mt-3">
                                                <div class="table no-footer table-responsive-sm">
                                                    <table id="eventsListTbl" role="table" aria-busy="false" aria-colcount="12" class="table b-table">
                                                        <thead role="rowgroup" class="">
                                                            <tr role="row" class="">
                                                                <th role="columnheader" scope="col" >
                                                                    <div><input type="checkbox" name="allSelected" value="" onChange={this.selectAllId}></input> | S.N. </div>
                                                                </th>
                                                                <th role="columnheader" scope="col" >
                                                                    <div>Name</div>
                                                                </th>
                                                                <th role="columnheader" scope="col">
                                                                    <div>Image</div>
                                                                </th>
                                                                {/* <th role="columnheader" scope="col">
                                                                                <div>Action</div>
                                                                            </th> */}
                                                            </tr>
                                                        </thead>
                                                        <tbody role="rowgroup">
                                                            {
                                                                this.state.sliders.map((slider, index) => {
                                                                    return (
                                                                        <tr role="row" class="">
                                                                            <td role="cell" class="">
                                                                                <input type="checkbox" checked={sliderIds.includes(slider.id) ? 'checked' : ''} onChange={() => this.selectId(slider.id)}></input> | {index + 1}.
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                <Link to={`/${Admin_prifix}edit_slider/${slider.id}`} > <i aria-hidden="true" class="fa fa-edit"></i> {slider.name}</Link>
                                                                            </td>
                                                                            <td role="cell" class="">
                                                                                Image
                                                                            </td>
                                                                            {/* <td role="cell" class="">

                                                                                            <div role="group" class="btn-group">
                                                                                                <button type="button" class="btn btn-danger">E</button>
                                                                                                <button type="button" class="btn btn-info">More</button>
                                                                                            </div>
                                                                                        </td> */}
                                                                        </tr>
                                                                    );
                                                                })
                                                            }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {/* <div class="row pt-3">
                                                            <div class="col">
                                                                <div class="dataTables_paginate paging_simple_numbers float-right">
                                                                    <ul class="pagination pagination-rounded mb-0">
                                                                        <ul role="menubar" aria-disabled="false" aria-label="Pagination" class="pagination dataTables_paginate paging_simple_numbers my-0 b-pagination justify-content-end">
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to first page" aria-disabled="true" class="page-link">«</span>
                                                                            </li>
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to previous page" aria-disabled="true" class="page-link">‹</span>
                                                                            </li>


                                                                            <li role="presentation" class="page-item active">
                                                                                <button role="menuitemradio" type="button" aria-label="Go to page 1" aria-checked="true" aria-posinset="1" aria-setsize="1" tabindex="0" class="page-link">1</button>
                                                                            </li>


                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to next page" aria-disabled="true" class="page-link">›</span>
                                                                            </li>
                                                                            <li role="presentation" aria-hidden="true" class="page-item disabled"><span role="menuitem" aria-label="Go to last page" aria-disabled="true" class="page-link">»</span>
                                                                            </li>
                                                                        </ul>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        );
    }
}
import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    withRouter,
} from "react-router-dom";
import AdminScreen from "../Screens/AdminScreen";
import DashboardScreen from "../Screens/DashboardScreen";
import AccountListScreen from "../Screens/AccountListScreen";
import AccountCreateScreen from "../Screens/AccountCreateScreen";
import SliderCreate from "../Screens/SliderCreate";
import SliderList from "../Screens/SliderList";
import PageCreate from "../Screens/PageCreate";
import PageList from "../Screens/PageList";
import CategoryCreate from "../Screens/CategoryCreate";
import CategoryList from "../Screens/CategoryList";
import GameTypeCreate from "../Screens/GameTypeCreate";
import GameTypeList from "../Screens/GameTypeList";
import GameCreate from "../Screens/GameCreate";
import GameList from "../Screens/GameList";
import UserCreate from "../Screens/UserCreate";
import UserList from "../Screens/UserList";
import AdminCreate from "../Screens/AdminCreate";
import AdminList from "../Screens/AdminList";
import BankScreen from "../Screens/BankScreen";
import OurCasinoScreen from "../Screens/OurCasinoScreen";
import MultiLoginAccScreen from "../Screens/MultiLoginAccScreen";
import CurrentBet from "../Screens/CurrentBet";
import $ from "jquery";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AdminHeader from '../Components/AdminHeader';
import AdminSidebar from '../Components/AdminSidebar';
import NotFound from "../Screens/NotFound";

import { Admin_prifix } from "../configs/costant.config";
import SettlementMatches from "./SettlementMatches";
import SettlementMatchDetail from "./SettlementMatchDetail";
import SettledMatchDetail from "./SettledMatchDetail";
import SettledMatches from "./SettledMatches";



class App extends Component {
    state = {
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        user: null,
        users: {},
        token: null,
        loading: false,
        routes: [
            {
                name: 'Home',
                path: '/',
                component: 'AdminScreen'
            },
            {
                name: 'Dashboard',
                path: '/dashboard',
                component: 'DashboardScreen'
            }
        ]
    }

    afterLogin = async res => {
        sessionStorage.setItem('@user', JSON.stringify(res.data));
        sessionStorage.setItem('@token', res.token);

        this.setState({
            user: res.data,
            token: res.token
        });
        // <Route exact path="/" render={() => (
        //     <Redirect to={`/${Admin_prifix}dashboard`} />
        // )} />
        <Router><Route path={`/${Admin_prifix}dashboard`} component={DashboardScreen} /></Router>
    }

    afterLogout = async () => {
        sessionStorage.removeItem('@user');
        sessionStorage.removeItem('@token');

        this.setState({
            user: null,
            token: null
        });

        toast('Logged out successfully!');
    }

    async componentDidMount() {
        console.log('tjis p', this.props.data);
        let user = sessionStorage.getItem('@user');
        let user_token = sessionStorage.getItem('@token');
        if (user_token) {
            this.setState({
                user: user,
                token: user_token
            });
        }

        setTimeout(() => {
            this.setState({ loading: true })
        }, 2000);

        $(document).on('click', '.accordion-box .has-arrow', e => {
            $(e.target).closest('.accordion-box').toggleClass('mm-active');
            $(e.target).closest('.accordion-box').children('.mm-collapse').slideToggle();
            $(e.target).closest('.accordion-box').children('.mm-collapse').toggleClass('mm-show');

            $(e.target).closest('.accordion-box').siblings().removeClass('mm-active');
            $(e.target).closest('.accordion-box').siblings().children('.mm-collapse').removeClass('mm-show');
            $(e.target).closest('.accordion-box').siblings().children('.mm-collapse').slideUp();
        })

    }

    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    render() {
        let custom_side = ['custom_side'];
        if (this.state.sidebarAddclass) {
            custom_side.push('sidebar-enable vertical-collpsed');
        }
        let search_overlay = [""];
        if (this.state.searchBlock) {
            search_overlay.push('search-overlay');
        }

        if (this.state.loading) {
            let user = sessionStorage.getItem('@user');
            let user_token = sessionStorage.getItem('@token');
            if (user_token && user) {
                return (
                    <Router>
                        <Switch>
                            <Route exact path={`/${Admin_prifix}`}
                                component={(props) => <AdminScreen {...props} onLoginSuccess={(res) => this.afterLogin(res)} />}>
                            </Route>
                        </Switch>

                        <Switch>
                            <div className={custom_side.join(' ')} data-sidebar="dark">

                                <div class={search_overlay.join(' ')} onClick={this.searchoverlay.bind(this)}></div>

                                <div id="app">
                                    <div data-v-162f8f8f="" id="layout-wrapper">

                                        <AdminHeader {...this.props} />

                                        <div data-v-162f8f8f="" class="vertical-menu" style={{ display: 'd-none' }} onClick={this.searchoverlay.bind(this)}>
                                            <div class="d-none-mobile">
                                                <div settings="[object Object]" class="h-100" data-simplebar="init">
                                                    <div class="simplebar-wrapper" style={{ margin: '0px' }}>
                                                        <div class="simplebar-height-auto-observer-wrapper">
                                                            <div class="simplebar-height-auto-observer"></div>
                                                        </div>
                                                        <div class="simplebar-mask">
                                                            <div class="simplebar-offset" style={{ right: '0px', bottom: '0px' }}>
                                                                <div class="simplebar-content-wrapper" style={{ height: '100%', overflow: 'hidden scroll' }}>
                                                                    <div class="simplebar-content" style={{ padding: '0px' }}>
                                                                        <AdminSidebar />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="simplebar-placeholder" style={{ width: 'auto', height: '396px' }}></div>
                                                    </div>
                                                    <div class="simplebar-track simplebar-horizontal" style={{ visibility: 'hidden' }}>
                                                        <div class="simplebar-scrollbar simplebar-visible" style={{ width: '0px', display: 'none' }}></div>
                                                    </div>
                                                    <div class="simplebar-track simplebar-vertical simplebar-hover" style={{ visibility: 'visible' }}>
                                                        <div class="simplebar-scrollbar simplebar-visible simplebar-hover" style={{ height: '380px', display: 'block', transform: 'translate3d(0px, 0px, 0px)' }}></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-none-desktop">
                                                <div settings="[object Object]" class="h-100" data-simplebar="init">
                                                    <div class="simplebar-wrapper" style={{ margin: '0px' }}>
                                                        <div class="simplebar-height-auto-observer-wrapper">
                                                            <div class="simplebar-height-auto-observer"></div>
                                                        </div>
                                                        <div class="simplebar-mask">
                                                            <div class="simplebar-offset" style={{ right: '0px', bottom: '0px' }}>
                                                                <div class="simplebar-content-wrapper" style={{ height: '100%', overflow: 'hidden scroll' }}>
                                                                    <div class="simplebar-content" style={{ padding: '0px' }}>
                                                                        <AdminSidebar />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="simplebar-placeholder" style={{ width: 'auto', height: '396px' }}></div>
                                                    </div>
                                                    <div class="simplebar-track simplebar-horizontal" style={{ visibility: 'hidden' }}>
                                                        <div class="simplebar-scrollbar simplebar-visible" style={{ width: '0px', display: 'none' }}></div>
                                                    </div>
                                                    <div class="simplebar-track simplebar-vertical simplebar-hover" style={{ visibility: 'visible' }}>
                                                        <div class="simplebar-scrollbar simplebar-visible simplebar-hover" style={{ height: '380px', display: 'block', transform: 'translate3d(0px, 0px, 0px)' }}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <Route path={`/${Admin_prifix}dashboard`}
                                            component={(props) => <DashboardScreen {...props} />}>
                                        </Route>

                                        {/* Admin Routes */}
                                        <Route path={`/${Admin_prifix}admins`}>
                                            <AccountListScreen />
                                        </Route>
                                        <Route path={`/${Admin_prifix}add_admins`}>
                                            <AccountCreateScreen />
                                        </Route>
                                        <Route path={`/${Admin_prifix}add_admin`}
                                            component={(props) => <AdminCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}admin`}>
                                            <AdminList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_admin/:id`}
                                            component={(props) => <AdminCreate {...props} />}>
                                        </Route>

                                        {/* Slider Routes */}
                                        <Route path={`/${Admin_prifix}add_slider`}
                                            component={(props) => <SliderCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}slider`}>
                                            <SliderList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_slider/:id`}
                                            component={(props) => <SliderCreate {...props} />}>
                                        </Route>

                                        {/* Pages Routes */}
                                        <Route path={`/${Admin_prifix}add_page`}
                                            component={(props) => <PageCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}page`}>
                                            <PageList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_page/:id`}
                                            component={(props) => <PageCreate {...props} />}>
                                        </Route>

                                        {/* Category Routes */}
                                        <Route path={`/${Admin_prifix}add_category`}
                                            component={(props) => <CategoryCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}category`}>
                                            <CategoryList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_category/:id`}
                                            component={(props) => <CategoryCreate {...props} />}>
                                        </Route>

                                        {/* Game Type Routes */}
                                        <Route path={`/${Admin_prifix}add_gametype`}
                                            component={(props) => <GameTypeCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}gametype`}>
                                            <GameTypeList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_gametype/:id`}
                                            component={(props) => <GameTypeCreate {...props} />}>
                                        </Route>

                                        {/* Game Routes */}
                                        <Route path={`/${Admin_prifix}add_game`}
                                            component={(props) => <GameCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}game`}>
                                            <GameList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_game/:id`}
                                            component={(props) => <GameCreate {...props} />}>
                                        </Route>

                                        {/* User Routes */}
                                        <Route path={`/${Admin_prifix}add_user`}
                                            component={(props) => <UserCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}user`}>
                                            <UserList />
                                        </Route>
                                        <Route path={`/${Admin_prifix}user/:id`}
                                            component={(props) => <UserList {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}edit_user/:id`}
                                            component={(props) => <UserCreate {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}bank`}>
                                            <BankScreen />
                                        </Route>
                                        <Route path={`/${Admin_prifix}our_casino`}>
                                            <OurCasinoScreen />
                                        </Route>
                                        <Route path={`/${Admin_prifix}multi_login_account`}>
                                            <MultiLoginAccScreen />
                                        </Route>

                                        {/* Reports Routes */}
                                        <Route path={`/${Admin_prifix}reports/current-bets`}>
                                            <CurrentBet />
                                        </Route>

                                        {/* bat settlements */}
                                        <Route path={`/${Admin_prifix}settlement/matches/:id`}
                                            component={(props) => <SettlementMatches {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}settlement/:type/:id`}
                                            component={(props) => <SettlementMatchDetail {...props} />}>
                                        </Route>

                                        <Route path={`/${Admin_prifix}settled/matches/:id`}
                                            component={(props) => <SettledMatches {...props} />}>
                                        </Route>
                                        <Route path={`/${Admin_prifix}settled/:type/:id`}
                                            component={(props) => <SettledMatchDetail {...props} />}>
                                        </Route>
                                        {/* <Route path={`/${Admin_prifix}settlement/matches/details/:id`}
                                            component={(props) => <SettlementDetail {...props} />}>
                                        </Route> */}
                                    </div>
                                </div>
                            </div>
                        </Switch>
                    </Router >
                );
            } else {
                return (
                    <Router>
                        <div>
                            <Switch>
                                <Route exact path={`/${Admin_prifix}`}
                                    component={(props) => <AdminScreen {...props} onLoginSuccess={(res) => this.afterLogin(res)} />}>
                                </Route>
                            </Switch>
                        </div>
                    </Router>
                );
            }
        } else {
            return (
                <div className="text-center" style={{ lineHeight: 50 }}>
                    Loding....
                </div>
            );
        }
    }
}
export default withRouter(App)


import React, { Component } from "react";
import { ApiExecute, ToastMessage } from '../Api';
import { Link } from "react-router-dom";
import { Admin_prifix, IMG_BASE } from "../configs/costant.config";
import $ from "jquery";


export default class CategoryScreen extends Component {
    state = {
        categories: [],
        id: null,
        name: '',
        slug: '',
        icon: '',
        parent: '',
        image: '',
        gametypes: [],
        game_type_id: '',
    }

    onChangename = e => {
        this.setState({ name: e.target.value });
    }
    onChangeslug = e => {
        this.setState({ slug: e.target.value });
    }
    onChangeicon = e => {
        this.setState({ icon: e.target.value });
    }
    onChangeparent = e => {
        this.setState({ parent: e.target.value });
        this.getGametype(e.target.value);
    }
    onChangegametype = e => {
        this.setState({ game_type_id: e.target.value });
    }
    onChangeimage = e => {
        let abc = this.uploadImage(e);
    }

    saveCategory = async () => {
        // let image_url = $('.uploadImage img').attr('src');
        // this.setState({ image: image_url });

        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log('user', user);
        let admin_id = user.id;
        let {
            id,
            name,
            slug,
            icon,
            parent,
            game_type_id,
            image,
        } = this.state;

        if (!name || name.trim() === '') {
            ToastMessage('error', 'Please enter name !');
        } else {
            if (id) {
                let data = {
                    id: id,
                    name: name,
                    slug: slug,
                    icon: icon,
                    parent: parent,
                    game_type_id: game_type_id,
                    image: image,
                }
                let api_response = await ApiExecute(`category/${id}`, { method: 'PUT', data: data });

                console.log('login_response', api_response.data.errors);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.slug = null;
                    this.state.icon = null;
                    this.state.parent = null;
                    this.state.game_type_id = null;
                    this.state.id = null;
                    this.state.image = null;
                    let url = `/${Admin_prifix}category`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            } else {
                let data = {
                    name: name,
                    slug: slug,
                    icon: icon,
                    parent: parent,
                    game_type_id: game_type_id,
                    image: image,
                }
                console.log('sdjdf', data);
                let api_response = await ApiExecute("add-category", { method: 'POST', data: data });

                console.log('login_response', api_response);
                if (api_response.data.status) {
                    this.state.name = null;
                    this.state.slug = null;
                    this.state.icon = null;
                    this.state.parent = null;
                    this.state.game_type_id = null;
                    this.state.id = null;
                    this.state.image = null;
                    let url = `/${Admin_prifix}category`;
                    this.props.history.push(url);
                    ToastMessage('success', api_response.data.message);
                }
                else {
                    if (api_response.data.errors == undefined && api_response.data.errors == null) {
                        ToastMessage('error', api_response.data.message);
                    } else {
                        ToastMessage('error', api_response.data.errors[0]);
                    }
                }
            }
        }
    }

    getCategory = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log(user.role_id);

        let api_response = await ApiExecute(`all-category`, { method: 'GET' });
        this.setState({ categories: api_response.data });
        console.log(api_response.data);
    }

    getadmins = async () => {
        let api_response = await ApiExecute(`category/${this.state.id}`, { method: 'GET' });
        console.log('api_response', api_response);
        this.setState({ name: api_response.data.name });
        this.setState({ slug: api_response.data.slug });
        this.setState({ icon: api_response.data.icon });
        this.setState({ image: api_response.data.image });
        this.setState({ parent: api_response.data.parent });
        this.setState({ game_type_id: api_response.data.game_type_id });
        console.log(api_response.data.game_type_id);

        this.getGametype(api_response.data.game_type_id);
    }

    getGametype = async (id) => {
        let api_response = await ApiExecute(`all-gametype?category=${id}`, { method: 'GET' });
        this.setState({ gametypes: api_response.data });
        console.log(api_response.data);
    }

    dataURLToBlob(dataURL) {
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            var parts = dataURL.split(',');
            var contentType = parts[0].split(':')[1];
            var raw = parts[1];

            return new Blob([raw], { type: contentType });
        }

        parts = dataURL.split(BASE64_MARKER);
        contentType = parts[0].split(':')[1];
        raw = window.atob(parts[1]);
        var rawLength = raw.length;

        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], { type: contentType });
    }
    uploadImage(event) {
        // Read in file
        let self = this
        var file = event.target.files[0];
        if (file.type.match('image.*')) {
            var reader = new FileReader();

            reader.onload = function (readerEvent) {
                var image = new Image();
                image.onload = function () {
                    // Resize the image
                    var canvas = document.createElement('canvas'),
                        max_size = 770,// TODO : pull max size from a site config
                        width = image.width,
                        height = image.height;
                    if (width > height) {
                        if (width > max_size) {
                            height *= max_size / width;
                            width = max_size;
                        }
                    } else {
                        if (height > max_size) {
                            width *= max_size / height;
                            height = max_size;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                    var dataUrl = canvas.toDataURL('image/png');
                    // var resizedImage = dataURLToBlob(dataUrl);
                    self.setState({ image: dataUrl });
                    // $('.uploadImage img').attr('src', dataUrl);
                    // console.log({
                    //     type: "imageResized",
                    //     url: dataUrl
                    // });
                    // event.trigger({
                    //     type: "imageResized",
                    //     blob: resizedImage,
                    //     url: dataUrl
                    // });
                }
                image.src = readerEvent.target.result;
            }

            reader.readAsDataURL(file);
        }
    }

    componentDidMount() {
        this.getCategory();
        let id = this.props.match.params.id;
        if (id) {
            this.state.id = id;
            this.getadmins();
        }
    }

    render() {
        let self = this;
        let { Router } = this.props;
        return (
            <div data-v-162f8f8f="" class="main-content">
                <div data-v-162f8f8f="" class="page-content">
                    <div data-v-162f8f8f="">
                        <div data-v-162f8f8f="">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">{this.state.id ? "Update" : "Create"} Category</h4>
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}dashboard`} class="" target="_self">Home</Link>
                                                </li>
                                                <li class="breadcrumb-item"><Link to={`/${Admin_prifix}category`} class="router-link-active" target="_self">Category</Link>
                                                </li>
                                                <li class="breadcrumb-item active"><span aria-current="location">Create Category</span>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">General Information</h4>
                                            <div class="form-group">
                                                <label>Name :</label>
                                                <input placeholder="Name" type="text" name="name" value={this.state.name} onInput={this.onChangename} data-vv-as="Name" autocomplete="Name" class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Slug :</label>
                                                <input placeholder="Slug" type="text" data-vv-as="Slug" name="slug" value={this.state.slug} onInput={this.onChangeslug} class="form-control animation" aria-required="true" aria-invalid="false" />
                                            </div>
                                            <div class="form-group">
                                                <label>Parent :</label>
                                                <select name="parent" defaultValue={this.state.parent} value={this.state.parent} onChange={this.onChangeparent} class="form-control animation">
                                                    <option value="">Select Parent</option>
                                                    {
                                                        this.state.categories.map((category) => {
                                                            return (
                                                                <option value={category.id}>{category.name}</option>
                                                            );
                                                        })
                                                    }
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Game Type :</label>
                                                <select name="game_type_id" defaultValue={this.state.game_type_id} value={this.state.game_type_id} onChange={this.onChangegametype} class="form-control animation">
                                                    <option value="">Select Game Type</option>
                                                    {
                                                        this.state.gametypes.map((gametype) => {
                                                            return (
                                                                <option value={gametype.id}>{gametype.name}</option>
                                                            );
                                                        })
                                                    }
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Image :</label>
                                                <label class="uploadImage d-block">
                                                    <img src={this.state.image} alt="" style={{ width: '20%' }} />
                                                    <input type="file" id="takePictureField" accept="image/*" style={{ display: 'none' }} onChange={this.onChangeimage} />
                                                </label>
                                                <label for="takePictureField" class="my-2 btn btn-block btn-info">Upload Image</label>
                                            </div>
                                            <div class="d-flex justify-content-end align-items-center">
                                                <button type="submit" onClick={this.saveCategory} id="spinner-dark-8" class="btn btn-primary ml-2">
                                                    Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Link,
    Route,
    withRouter,
} from "react-router-dom";
import $ from "jquery";
import { ApiExecute } from "../Api";

class FrontSidebar extends Component {

    state = {
        subcategories: [],
        category: {},
    }

    getCategory = async () => {
        let category = await ApiExecute(`category/info/${this.props.slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.gametypeList(category.data.id)
    }

    allcategoryList = async (catid = null, gametypeid = null) => {
        let id = catid ? catid : this.state.category.id;
        let gtype = gametypeid ? gametypeid : this.props.gametype_id;
        let api_response;

        if (this.props.slug == 'sport') {
            api_response = await ApiExecute(`category?category_id=${id}`, { method: 'GET' });
            this.setState({ subcategories: api_response.data.results });
        } else {
            api_response = await ApiExecute(`category?category_id=${id}&&game_type_id=${gtype}`, { method: 'GET' });
            this.setState({ subcategories: api_response.data.results });
        }
    }
    gametypeList = async (id) => {

        let api_response = await ApiExecute(`gametype?category_id=${id}`, { method: 'GET' });
        let lists = api_response.data.results;
        let gametype;
        lists.forEach(element => {
            gametype = element;
        });

        if (this.props.slug == 'sport') {
            this.allcategoryList(id);
        } else {
            this.allcategoryList(id, gametype.id);

        }
    }

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
        this.setState({
            user: JSON.parse(user)
        });
        this.getCategory();

        $('li.nav-item').children('a').addClass('active');

        $(document).on('click', '.navbar-nav', e => {
            $(e.target).closest('.SideBar').toggleClass('show');
            $(e.target).closest('.SideBar').children('.dropdown-menu').toggleClass('show');

            $(e.target).closest('.SideBar').siblings().removeClass('show');
            $(e.target).closest('.SideBar').siblings().children('.dropdown-menu').removeClass('show');
            $(e.target).closest('.SideBar').siblings().children('.dropdown-menu').children('.SideBar').removeClass('show');
            $(e.target).closest('.SideBar').siblings().children('.dropdown-menu').children('.SideBar').children('.dropdown-menu').removeClass('show');
        });

        $(document).on('click', '.casino-menu .navbar-nav .nav-item', e => {
            $(e.target).closest('.nav-item').children('a').addClass('active');

            $(e.target).closest('.nav-item').siblings().children('a').removeClass('active');
        });
    }
    render() {
        return (
            <div class="sidebar-left d-none-mobile">
                <div class="logo-box d-none-mobile">
                    <div class="logo">
                        <a href="/sport" aria-current="page" class="router-link-exact-active router-link-active">
                            {/* <img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" class="img-fluid" /> */}
                            <img src="https://sitethemedata.com/v6/static/front/img/logo-blue.png?ver=159" class="img-fluid" />
                        </a>
                    </div>
                </div>
                <div class="d-none-desktop tabs-mobile">
                    <ul>
                        <li> <a href="/sport" aria-current="page" class="router-link-exact-active router-link-active">
                            Exchange
                        </a>
                        </li>
                        <li> <a href="/casino" class="">
                            Our Casino
                        </a>
                        </li>
                        <li> <a href="/slot" class="">
                            Casino + Slot
                        </a>
                        </li>
                        <li> <a href="/fantasy" class="">
                            Fantasy Games
                        </a>
                        </li>
                    </ul>
                </div>
                <div class="search-box">
                    <div class="form-group">
                        <input type="text" placeholder="Search" autocomplete="off" class="form-control" style={{ textTransform: 'lowercase' }} />
                        <img src="https://sitethemedata.com/v3/static/front/img/search.svg" class="search-icon" />
                    </div>
                </div>
                {
                    this.props.slug == 'sport'
                        ?
                        <div class="menu-box">
                            <div> <span mode="out-in">
                                <ul class="navbar-nav">
                                    {
                                        this.state.subcategories
                                            .sort((a, b) => a.id > b.id ? 1 : -1)
                                            .map((c, index) => {
                                                return (
                                                    <li class="SideBar">
                                                        <a href="javascript:void(0)" class={`dropdown-item dropdown-toggle sport${index + 1}`}>
                                                            <i class={`d-icon icon-${index + 1}`}></i>
                                                            <span class="sport-name ifTooltip">{c.name}</span>  <span> ({c.subcategories.length})</span>
                                                        </a>
                                                        {
                                                            c.subcategories.length
                                                                ?
                                                                <ul class="dropdown-menu">
                                                                    {
                                                                        c.subcategories
                                                                            .sort((a, b) => a.id > b.id ? 1 : -1)
                                                                            .map((sub, index1) => {
                                                                                return (
                                                                                    <li class="text-dark SideBar">
                                                                                        <a href="javascript:void(0)" class="dropdown-item dropdown-toggle"> <span class="ifTooltip">{sub.name}</span>  <span> ({sub.games.length})</span>
                                                                                        </a>
                                                                                        {
                                                                                            sub.games.length
                                                                                                ?
                                                                                                <ul class="dropdown-menu">
                                                                                                    {
                                                                                                        sub.games
                                                                                                            .sort((a, b) => a.id > b.id ? 1 : -1)
                                                                                                            .map((g, index1) => {
                                                                                                                return (
                                                                                                                    <li class="text-dark SideBar">
                                                                                                                        <Link to={`/sport/cricket/${g.slug}`} class="dropdown-item"> <span class="ifTooltip">{g.name}</span>
                                                                                                                        </Link>
                                                                                                                    </li>
                                                                                                                )
                                                                                                            })
                                                                                                    }
                                                                                                </ul>
                                                                                                :
                                                                                                ''
                                                                                        }
                                                                                    </li>
                                                                                )
                                                                            })
                                                                    }
                                                                </ul>

                                                                : ''
                                                        }
                                                    </li>
                                                )
                                            })
                                    }
                                </ul>
                            </span>
                            </div>
                        </div>
                        :
                        <div class="menu-box casino-menu">
                            <ul class="navbar-nav" style={{ marginBottom: '20px' }}>
                                <span mode="out-in">
                                    {
                                        this.props.slug == 'casino' ?
                                            <li class="nav-item" data-id=''><a href="javascript:void(0)" class=""><span class="sport-name">All Casino</span></a></li>
                                            :
                                            ''
                                    }
                                    {
                                        this.state.subcategories
                                            .sort((a, b) => a.id > b.id ? 1 : -1)
                                            .map((c, index) => {
                                                return (
                                                    <li class="nav-item" data-id={c.id}><a href="javascript:void(0)" class=""><span class="sport-name">{c.name}</span></a></li>
                                                )
                                            })
                                    }
                                </span>
                            </ul>
                        </div>
                }

            </div>

        );
    }
}

export default withRouter(FrontSidebar);

import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Link,
    Route,
    withRouter,
} from "react-router-dom";
import $ from "jquery";
import { ThemeColor } from '../assets/js/customreact';
import { Carousel } from "react-responsive-carousel";
import { ApiExecute, ApiExecute1 } from "../Api";

class FrontHeader extends Component {

    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        user: {},

        wallet: {},
        exposure: {},

        subcategories: [],
        category: {},
        cricket: {},
        tannis: {},
        soccer: {},
        loading: true,
        categories: [],
    }
    logoutSubmit = async () => {
        sessionStorage.removeItem('@front_token');
        sessionStorage.removeItem('@front_user');
        // let url = `/${Admin_prifix}dashboard`;
        // this.props.history.push('/');
        window.location = '/';
    }

    theme(color) {
        ThemeColor(color);
    }

    userdropdown(e) {
        $('.user-dropdown.collapse').toggleClass('show');
    }

    async componentDidMount() {
        console.log('header props', this.props.slug);
        if (this.props.slug == 'fantasy' || this.props.slug == 'others') {
            $('header.header').addClass('header-casino');
        }
        let user = sessionStorage.getItem('@front_user');
        this.setState({
            user: JSON.parse(user)
        });
        this.categoryList();
        this.getWallet();
        this.getCategory();
        this.getMatchlist();

        $(document).on('click', '.menu-button-mobile.mobile-menu', e => {
            $('#sidebar-border').toggleClass('d-none d-block');
            $('.b-sidebar-backdrop').toggleClass('d-none d-block');
        });
    }

    getWallet = async () => {
        let user = sessionStorage.getItem('@front_user');
        user = JSON.parse(user);
        console.log('user', user);
        console.log(user.id);

        let api_response = await ApiExecute(`wallet/${user.id}`, { method: 'GET' });
        if (api_response.data && api_response.data.length) {
            this.setState({ wallet: api_response.data[0] });
            this.setState({ exposure: api_response.data[1] });
        }
    }
    getCategory = async () => {

        this.setState({ loading: true });

        let category = await ApiExecute(`category/info/${this.props.slug}`, { method: 'GET' });
        this.setState({ category: category.data });

        this.gametypeList(category.data.id)

        this.setState({ loading: false });
    }

    allcategoryList = async (catid = null, gametypeid = null) => {
        let id = catid ? catid : this.state.category.id;
        let gtype = gametypeid ? gametypeid : this.props.gametype_id;
        let api_response;

        if (this.props.slug == 'sport') {
            api_response = await ApiExecute(`category?category_id=${id}`, { method: 'GET' });
            this.setState({ subcategories: api_response.data.results });
        } else {
            api_response = await ApiExecute(`category?category_id=${id}&&game_type_id=${gtype}`, { method: 'GET' });
            this.setState({ subcategories: api_response.data.results });
        }
    }
    gametypeList = async (id) => {

        let api_response = await ApiExecute(`gametype?category_id=${id}`, { method: 'GET' });
        let lists = api_response.data.results;
        let gametype;
        lists.forEach(element => {
            gametype = element;
        });

        if (this.props.slug == 'sport') {
            this.allcategoryList(id);
        } else {
            this.allcategoryList(id, gametype.id);

        }
    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ categories: api_response.data.results })
    }
    getMatchlist = async () => {
        this.setState({ loading: true });

        let api1_response = await ApiExecute1(`getcricketmatches`, { method: 'GET' });
        this.setState({ cricket: api1_response.data });
        let api2_response = await ApiExecute1(`gettennismatches`, { method: 'GET' });
        this.setState({ tannis: api2_response.data });
        let api3_response = await ApiExecute1(`getsoccermatches`, { method: 'GET' });
        this.setState({ soccer: api3_response.data });

        this.setState({ loading: false });

        // api_response.data.data.forEach(element => {
        // element.push = this.checkGame(element.eventId);
        // console.log('asdkjsad', element.other);
        // setInterval(this.checkGame(element.eventId), 1000);
        // });
    }
    render() {
        return (
            <>
                <header class="header" style={{ display: 'flex' }}>
                    <div class="menu-button-mobile mobile-menu">
                        <span class="menu-button-bar"></span>
                        <span class="menu-button-bar"></span>
                        <span class="menu-button-bar"></span>
                    </div>
                    <div tabindex="-1" class="b-sidebar-outer">

                        <div id="sidebar-border" tabindex="-1" role="dialog" aria-modal="true" aria-hidden="true" class="b-sidebar shadow bg-light text-dark d-none-desktop d-none" style={{ width: '80%' }}>

                            <div class="b-sidebar-body">
                                <div id="sidebar-left-mo" class="sidebar-left">
                                    <div class="d-none-desktop tabs-mobile">
                                        <ul>
                                            {
                                                this.state.categories
                                                    .sort((a, b) => a.id > b.id ? 1 : -1)
                                                    .map((c, index) => {
                                                        if (this.props.slug == c.slug) {
                                                            return (
                                                                <li> <Link to={`/${c.slug}`} class="router-link-exact-active router-link-active">{c.name}</Link>
                                                                </li>)
                                                        } else {
                                                            return (
                                                                <li> <Link to={`/${c.slug}`} >{c.name}</Link>
                                                                </li>)
                                                        }
                                                    })
                                            }
                                        </ul>
                                    </div>
                                    <div class="menu-scrolable">
                                        <div class="search-box">
                                            <div class="form-group d-inline-block mb-0"><input type="text" placeholder="Search" autocomplete="off" class="form-control" style={{ textTransform: 'lowercase' }} /> <img src="https://sitethemedata.com/v6/static/front/img/search.svg" class="search-icon" /></div>
                                            <div class="menu-button-mobile mobile-menu d-none-desktop"><span class="menu-button-bar"></span> <span class="menu-button-bar"></span> <span class="menu-button-bar"></span></div>
                                        </div>

                                        {
                                            this.state.loading ?
                                                <div class="menu-box text-center" style={{ fontSize: '10px' }}>Loading...</div>
                                                :
                                                <>
                                                    {
                                                        this.props.slug == 'sport'
                                                            ?
                                                            <div class="menu-box">
                                                                <div> <span mode="out-in">
                                                                    <ul class="navbar-nav">
                                                                        {
                                                                            this.state.subcategories
                                                                                .sort((a, b) => a.id > b.id ? 1 : -1)
                                                                                .map((c, index) => {
                                                                                    let cslug = this.state[c.slug];
                                                                                    return (
                                                                                        <li class="SideBar">
                                                                                            <a href="javascript:void(0)" class={`dropdown-item dropdown-toggle sport${index + 1}`}>
                                                                                                <i class={`d-icon icon-${index + 1}`}></i>
                                                                                                <span class="sport-name ifTooltip">{c.name}</span>  <span> ({cslug.length})</span>
                                                                                            </a>
                                                                                            {
                                                                                                cslug && cslug.length
                                                                                                    ?
                                                                                                    <ul class="dropdown-menu">
                                                                                                        {
                                                                                                            cslug
                                                                                                                .sort((a, b) => a.id > b.id ? 1 : -1)
                                                                                                                .map((sub, index1) => {
                                                                                                                    return (
                                                                                                                        <li class="text-dark SideBar">
                                                                                                                            <Link to={`/sport/${c.slug}/${sub.gameId}`} class="dropdown-item"> <span class="ifTooltip">{sub.eventName}</span>
                                                                                                                            </Link>
                                                                                                                        </li>

                                                                                                                    )
                                                                                                                })
                                                                                                        }
                                                                                                    </ul>

                                                                                                    : ''
                                                                                            }
                                                                                        </li>
                                                                                    )
                                                                                })
                                                                        }
                                                                    </ul>
                                                                </span>
                                                                </div>
                                                            </div>
                                                            :
                                                            <div class="menu-box casino-menu">
                                                                <ul class="navbar-nav" style={{ marginBottom: '20px' }}>
                                                                    <span mode="out-in">
                                                                        {
                                                                            this.props.slug == 'casino' ?
                                                                                <li class="nav-item" data-id=''><a href="javascript:void(0)" class=""><span class="sport-name">All Casino</span></a></li>
                                                                                :
                                                                                ''
                                                                        }
                                                                        {
                                                                            this.state.subcategories
                                                                                .sort((a, b) => a.id > b.id ? 1 : -1)
                                                                                .map((c, index) => {
                                                                                    return (
                                                                                        <li class="nav-item" data-id={c.id}><a href="javascript:void(0)" class=""><span class="sport-name">{c.name}</span></a></li>
                                                                                    )
                                                                                })
                                                                        }
                                                                    </span>
                                                                </ul>
                                                            </div>
                                                    }
                                                </>
                                        }
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="b-sidebar-backdrop bg-dark d-none"></div>
                    </div>
                    <div class="logo-box d-none-desktop">
                        <a href="/sport" aria-current="page" class="router-link-exact-active router-link-active">
                            <div class="logo">
                                <img src="https://sitethemedata.com/v6/static/front/img/logo-blue.png?ver=159" class="img-fluid" />
                                {/* <img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" class="img-fluid" /> */}
                            </div>
                        </a>
                    </div>
                    <div class="upcoming-fixure d-none-mobile">
                        <div class="fixure-title">Upcoming Fixtures</div>
                        <div class="fixure-box-container">
                            <Carousel showArrows={false} autoPlay={true} showThumbs={false} infiniteLoop={true} axis='vertical' showIndicators={false} showStatus={false}>
                                {
                                    this.state.upcoming_fixtures.map((upcoming_fixture, index) => {
                                        return (
                                            <div>
                                                <p class="fixure-box" style={{ paddingTop: '0px', textAlign: 'left' }}>
                                                    <a href="#" class="">
                                                        <div>
                                                            <i class="d-icon mr-2 icon-2"></i>{upcoming_fixture.name}
                                                        </div>
                                                        <div>{upcoming_fixture.date}</div>
                                                    </a>
                                                </p>
                                            </div>
                                        );
                                    })
                                }
                            </Carousel>
                        </div>
                    </div>
                    <div class="news-bar d-none-mobile">
                        <div class="news-title"><i class="fas fa-bell mr-2"></i> News</div>
                        <marquee>We are happy to announce that we are bringing you a new world of gambling.</marquee>
                    </div>
                    <div class="float-right header-right" id="balance-point">
                        <span class="balance d-none-mobile">Pts:
                            <span class="balance-value wallet-point" data-value={this.state.wallet.amount}>{this.state.wallet.amount}</span>  <span class="balance-value exposure-point">
                                | {this.state.exposure.amount}
                            </span>
                        </span>
                        <div data-toggle="collapse" data-target="#user-dropdown" class="username-info d-none-mobile collapsed" onClick={() => this.userdropdown(this)} aria-expanded="false"><span class="user-icon"><img src="https://sitethemedata.com/v3/static/front/img/user.svg" /></span>  <span class="username">{this.state.user.name}</span>
                            <img src="https://sitethemedata.com/v3/static/front/img/arrow-down.svg" />
                            <div id="user-dropdown" class="collapse user-dropdown" style={{ padding: '8px 0px' }}>
                                <a href="/report/accountstatement" class="">Account Statement</a>
                                <a href="/report/currentbets" class="">Current Bets</a>
                                {/* <a href="/report/activity" class="">Activity Log</a> */}
                                {/* <a href="/report/casinoresult" class="">Casino Results</a> */}
                                {/* <a href="/report/livecasinoresult" class="">Live Casino Bets</a> */}
                                <a href="javascript:void(0)">Set Button Value</a>
                                <a href="javascript:void(0)" class="changePasswordModel">Change Password</a>
                                {/* <a href="/secureauth" class="">Security Auth Verification</a> */}
                                <div class="login-seperator"></div> <a href="javascript:void(0)" onClick={() => this.logoutSubmit()}>Logout</a>
                            </div>
                        </div>
                        <div class="text-center d-none-desktop bal-point">Pts:
                            <span class="wallet-point" data-value={this.state.wallet.amount}>{this.state.wallet.amount}</span>  <span class="exposure-point" data-value={this.state.exposure.amount}>
                                | {this.state.exposure.amount}</span>
                        </div>
                        <div class="username-info d-none-desktop">
                            <div data-toggle="collapse" data-target="#user-dropdown" class="d-inline-block collapsed" onClick={() => this.userdropdown(this)} aria-expanded="false"><span class="user-icon"><img src="https://sitethemedata.com/v3/static/front/img/user.svg" /></span>  <span class="username">test1234567</span>
                                <img src="https://sitethemedata.com/v3/static/front/img/arrow-down.svg" />
                            </div>
                            <div id="user-dropdown" class="collapse user-dropdown">
                                <div class="color-box-container">
                                    <div class="w-100 select-theme-title">Pick your theme</div>
                                    <div class="color-box dark-box" onClick={() => this.theme('dark')}></div>
                                    <div class="color-box light-box" onClick={() => this.theme('light')}></div>
                                    <div class="color-box blue-box" onClick={() => this.theme('blue')}></div>
                                </div>
                                <div class="login-seperator"></div>
                                <a href="/report/accountstatement" class="">Account Statement</a>
                                <a href="/report/currentbets" class="">Current Bets</a>
                                {/* <a href="/report/activity" class="">Activity Log</a> */}
                                {/* <a href="/report/casinoresult" class="">Casino Results</a> */}
                                {/* <a href="/report/livecasinoresult" class="">Live Casino Bets</a> */}
                                <a href="javascript:void(0)">Set Button Value</a>
                                <a href="javascript:void(0)" class="changePasswordModel">Change Password</a>
                                {/* <a href="/secureauth" class="">Security Auth Verification</a> */}
                                <div class="login-seperator"></div> <a href="javascript:void(0)" onClick={() => this.logoutSubmit()}>Logout</a>
                            </div>
                        </div>
                    </div>
                </header>

            </>

        );
    }
}

export default withRouter(FrontHeader);

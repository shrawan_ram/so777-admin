import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Link,
    Route,
    withRouter,
} from "react-router-dom";

class Front1Footer extends Component {

    async componentDidMount() {
        let user = sessionStorage.getItem('@front_user');
    }
    render() {
        return (
            <footer class="footer">
                <div class="container-fluid container-fluid-5">
                    <div class="row row5">
                        <div class="col-12 col-md-3 text-center"><img src="https://sitethemedata.com/sitethemes/casido777.com/front/logo.png" class="img-logo" /></div>

                        <div class="col-12 col-sm footer-link">
                            <h4>Menu</h4>
                            <div><a href="/about-us" class="" target="_blank">About Us</a></div>
                            <div><a href="/game-rules" class="" target="_blank">Game Rules</a></div>

                        </div>
                        <div class="col-12 col-sm gt">
                            <h4>Game Therapy</h4>
                            <div>
                                <a href="javascript:void(0)" class="vm">

                                </a>
                                <a href="javascript:void(0)" role="button"><img src="https://sitethemedata.com/v4/static/front/img/18plus.png" /></a> <a href="https://www.gamcare.org.uk/" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/gamecare.png" /></a> <a href="https://www.gamblingtherapy.org/en" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/gt.png" /></a>
                            </div>

                        </div>
                        <div class="col-12 col-sm footer-social">
                            <h4>Follow Us</h4>
                            <div><a href="#" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/home-banners/social/facebook.png" /></a> <a href="#" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/home-banners/social/instagram.png" /></a> <a href="#" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/home-banners/social/telegram.png" /></a> <a href="#" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/home-banners/social/twitter.png" /></a> <a href="#" target="_blank"><img src="https://sitethemedata.com/v4/static/front/img/home-banners/social/youtube.png" /></a></div>
                            <div class="mt-1">

                            </div>
                        </div>
                        <div class="col-12 mt-4">
                            <div class="footer-bottom text-center">

                                <div class="text-center mt-2">© Copyright 2021. All Rights Reserved.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default withRouter(Front1Footer);

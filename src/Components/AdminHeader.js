import React, { Component } from "react";
import Slider from "react-slick";
import Popup from "reactjs-popup";
import {
    BrowserRouter as Router,
    Route,
    withRouter,
} from "react-router-dom";
import $ from "jquery";
import { ToastContainer } from 'react-toastify';
import { Admin_prifix } from "../configs/costant.config";
import 'react-toastify/dist/ReactToastify.css';
import DashboardScreen from "../Screens/DashboardScreen";
import { ApiExecute, ToastMessage } from "../Api";

class AdminHeader extends Component {

    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        user: {},
        wallet: {},
        exposure: {},
        user_detail: {},
        user_id: '',
        userWallet: {},
        adminWallet: {},
        wallet_type: '',

        user_main_amt: 0,
        admin_main_amt: 0,
        new_user_main_amt: 0,
        new_admin_main_amt: 0,
        deposit: 0,
        new_deposit: 0,
        amount: 0,
        remark: '',
        transation_code: '',
        accountHistory: {},

        new_password: '',
        confirm_password: '',
        name: '',
        user_name: '',
        email: '',
        mobile: '',
        city: '',
    }
    logoutSubmit = async () => {
        sessionStorage.removeItem('@token');
        sessionStorage.removeItem('@user');
        // let url = `/${Admin_prifix}dashboard`;
        // this.props.history.push(url);
        window.location = `/${Admin_prifix}`;
        // <Router><Route path={`/${Admin_prifix}dashboard`} component={DashboardScreen} /></Router>
    }
    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' });
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    onChangeremark = e => {
        this.setState({ remark: e.target.value });
    }
    onChangeamount = e => {
        this.setState({ amount: e.target.value });
        if (this.state.wallet_type == 'deposit') {
            this.setState({ new_admin_main_amt: parseInt(this.state.admin_main_amt) - parseInt(e.target.value) });
            this.setState({ new_user_main_amt: parseInt(this.state.user_main_amt) + parseInt(e.target.value) });
            this.setState({ new_deposit: parseInt(this.state.deposit) + parseInt(e.target.value) });
        }
        if (this.state.wallet_type == 'withdrawal') {
            this.setState({ new_admin_main_amt: parseInt(this.state.admin_main_amt) + parseInt(e.target.value) });
            this.setState({ new_user_main_amt: parseInt(this.state.user_main_amt) - parseInt(e.target.value) });
            this.setState({ new_deposit: parseInt(this.state.deposit) - parseInt(e.target.value) });
        }
    }

    onNewPassword = e => {
        this.setState({ new_password: e.target.value });
    }

    onConfirmPassword = e => {
        this.setState({ confirm_password: e.target.value });
    }

    onChangetransactioncode = e => {
        this.setState({ transation_code: e.target.value });
    }
    onChangeName = e => {
        this.setState({ name: e.target.value });
    }
    onChangeUserName = e => {
        this.setState({ user_name: e.target.value });
    }
    onChangeMobile = e => {
        this.setState({ mobile: e.target.value });
    }
    onChangeEmail = e => {
        this.setState({ email: e.target.value });
    }
    onChangeCity = e => {
        this.setState({ city: e.target.value });
    }

    async componentDidMount() {
        let self = this;
        let user = sessionStorage.getItem('@user');
        this.setState({
            user: JSON.parse(user)
        });
        console.log('this props dfh', this.props);
        this.getWallet();

        $(document).on('click', '.deposit-wallet', e => {
            let id = $(e.target).closest(".deposit-wallet").data('id');
            this.setState({ user_id: id });
            let type = $(e.target).closest(".deposit-wallet").data('type');
            this.setState({ wallet_type: type });

            self.getUserWallet(id);
            self.userDetails(id);

            $('#deposit_model').children('.modal').toggleClass('d-none d-block');
            $('#deposit_model').children('.modal').toggleClass('show');
            $('#deposit_model_backdrop').toggleClass('d-block d-none');
        });

        $(document).on('click', '#vertical-menu-btn', e => {
            $('.vertical-menu').toggleClass('d-none d-block');
            // $('body').toggleClass('sidebar-enable vertical-collpsed');

        });
        $(document).on('click', '#deposit_model .modal-header button.close', e => {
            $('#deposit_model').children('.modal').toggleClass('d-none d-block');
            $('#deposit_model').children('.modal').toggleClass('show');
            $('#deposit_model_backdrop').toggleClass('d-block d-none');
        });

        $(document).on('click', '.more-details', e => {
            let id = $(e.target).closest(".more-details").data('id');
            this.setState({ user_id: id });

            self.userDetails(id);
            self.getUserWalletDetail(id);
            self.accountHistory(id);

            $('#more_model').children('.modal').toggleClass('d-none d-block');
            $('#more_model').children('.modal').toggleClass('show');
            $('#more_model_backdrop').toggleClass('d-block d-none');
        });

        $(document).on('click', '#more_model .modal-header button.close', e => {
            $('#more_model').children('.modal').toggleClass('d-none d-block');
            $('#more_model').children('.modal').toggleClass('show');
            $('#more_model_backdrop').toggleClass('d-block d-none');
        });

        $(document).on('click', '#more_model .tabs .nav .nav-item', e => {
            let id = $(e.target).closest(".nav-item").children('.nav-link').data('id');
            console.log('sdfjksd sdfuer', id);

            $(e.target).closest(".nav-item").children('.nav-link').addClass('active tab-bg-primary');
            $(e.target).closest(".nav-item").siblings().children('.nav-link').removeClass('active tab-bg-primary');

            $(e.target).closest(".tabs").children('.tab-content').children(`#${id}`).addClass('active');
            $(e.target).closest(".tabs").children('.tab-content').children(`#${id}`).addClass('d-block');
            $(e.target).closest(".tabs").children('.tab-content').children(`#${id}`).siblings().removeClass('active');
            $(e.target).closest(".tabs").children('.tab-content').children(`#${id}`).siblings().addClass('d-none');
            $(e.target).closest(".tabs").children('.tab-content').children(`#${id}`).siblings().removeClass('d-block');
        });
    }

    getUserWallet = async (id) => {
        let api_response = await ApiExecute(`wallet/${id}`, { method: 'GET' });
        if (api_response.data && api_response.data.length) {
            this.setState({ userWallet: api_response.data });

            this.setState({ user_main_amt: api_response.data[0].amount });
        }

        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        let api1_response = await ApiExecute(`wallet/${user.id}`, { method: 'GET' });
        if (api1_response.data && api1_response.data.length) {
            this.setState({ adminWallet: api1_response.data });

            this.setState({ admin_main_amt: api1_response.data[0].amount });
            this.setState({ new_admin_main_amt: api1_response.data[0].amount });
        }
    }

    getWallet = async () => {
        let user = sessionStorage.getItem('@user');
        user = JSON.parse(user);
        console.log('user', user);
        console.log(user.id);

        let api_response = await ApiExecute(`wallet/${user.id}`, { method: 'GET' });
        if (api_response.data && api_response.data.length) {
            this.setState({ wallet: api_response.data[0] });
            this.setState({ exposure: api_response.data[1] });
        }
    }

    getUserWalletDetail = async (id) => {
        let api_response = await ApiExecute(`wallet/${id}`, { method: 'GET' });
        if (api_response.data && api_response.data.length) {
            this.setState({ wallet: api_response.data[0] });
            this.setState({ exposure: api_response.data[1] });
        }
    }

    userDetails = async (id) => {
        let uId = id ? id : this.state.user_id;

        let api_response = await ApiExecute(`user/${uId}`, { method: 'GET' });
        this.setState({ user_detail: api_response.data });

        this.setState({ user_name: api_response.data.user_name });
        this.setState({ email: api_response.data.email });
        this.setState({ mobile: api_response.data.mobile });
        this.setState({ name: api_response.data.name });
        this.setState({ city: api_response.data.city });
    }

    accountHistory = async (id) => {
        let uId = id ? id : this.state.user_id;

        let api_response = await ApiExecute(`wallet-history/${uId}`, { method: 'GET' });
        this.setState({ accountHistory: api_response.data });
    }

    updateWalletHistory = async () => {
        let {
            amount,
            wallet_type,
            remark,
            user_id,
        } = this.state;

        if (amount == '' && amount == 0) {
            ToastMessage('error', 'Please enter amount!');
            console.log('error', 'Please enter amount!');
        } else if (this.state.transation_code != this.state.user.transation_code) {
            ToastMessage('error', 'Please enter correct transction password!');
            console.log('error', 'Please enter correct transction password!');
        } else {
            let data = {
                user_id: user_id,
                amount: amount,
                type: wallet_type,
                remark: remark,
            }
            console.log('data', data);

            let api_response = await ApiExecute("add-wallet-history", { method: 'POST', data: data });
            console.log('api_response', api_response);
            if (api_response.data.status) {
                this.updateUserWallet();
                this.updateAdminWallet();
                // ToastMessage('success', api_response.data.message);

                this.state.wallet_type = '';
                this.state.user_id = '';

                this.state.user_main_amt = 0;
                this.state.admin_main_amt = 0;
                this.state.new_user_main_amt = 0;
                this.state.new_admin_main_amt = 0;
                this.state.deposit = 0;
                this.state.new_deposit = 0;
                this.state.amount = 0;
                this.state.remark = '';
                this.state.transation_code = '';

                this.getWallet();

                $('#deposit_model').children('.modal').toggleClass('d-none d-block');
                $('#deposit_model').children('.modal').toggleClass('show');
                $('#deposit_model_backdrop').toggleClass('d-block d-none');

                ToastMessage('success', 'Amount transfer successfull.');
            }
            else {
                if (api_response.data.errors == undefined && api_response.data.errors == null) {
                    ToastMessage('error', api_response.data.message);
                } else {
                    ToastMessage('error', api_response.data.errors[0]);
                }
            }

        }
    }

    updateUserWallet = async () => {
        let {
            new_user_main_amt,
            user_id,
        } = this.state;

        let data = {
            user_id: user_id,
            amount: new_user_main_amt,
            type: 'main',
        }
        console.log('data', data);

        let api_response = await ApiExecute(`wallet/${user_id}`, { method: 'PUt', data: data });
        console.log('api_response', api_response);
        if (api_response.data.status) {
            // ToastMessage('success', api_response.data.message);
        }
        else {
            if (api_response.data.errors == undefined && api_response.data.errors == null) {
                ToastMessage('error', api_response.data.message);
            } else {
                ToastMessage('error', api_response.data.errors[0]);
            }
        }

    }
    updateAdminWallet = async () => {
        let user_id = this.state.user.id;
        let {
            new_admin_main_amt,
        } = this.state;

        let data = {
            user_id: user_id,
            amount: new_admin_main_amt,
            type: 'main',
        }
        console.log('data', data);

        let api_response = await ApiExecute(`wallet/${user_id}`, { method: 'PUT', data: data });
        console.log('api_response', api_response);
        if (api_response.data.status) {

            // ToastMessage('success', api_response.data.message);
        }
        else {
            if (api_response.data.errors == undefined && api_response.data.errors == null) {
                ToastMessage('error', api_response.data.message);
            } else {
                ToastMessage('error', api_response.data.errors[0]);
            }
        }

    }

    changeUserPassword = async () => {
        let {
            new_password,
            confirm_password,
            user_id,
        } = this.state;

        if (new_password == '') {
            ToastMessage('error', 'Please enter new password !');
            console.log('error', 'Please enter new password !');
        } else if (confirm_password == '') {
            ToastMessage('error', 'Please enter confirm password !');
            console.log('error', 'Please enter confirm password !');
        } else if (new_password != confirm_password) {
            ToastMessage('error', 'Password not match !');
            console.log('error', 'Password not match !');
        } else if (this.state.transation_code != this.state.user.transation_code) {
            ToastMessage('error', 'Please enter correct transction password!');
            console.log('error', 'Please enter correct transction password!');
        } else {
            let data = {
                password: new_password,
            }
            console.log('data', data);

            let api_response = await ApiExecute(`user-password/${user_id}`, { method: 'PUT', data: data });
            console.log('api_response', api_response);
            if (api_response.data.status) {

                this.state.new_password = '';
                this.state.confirm_password = '';
                this.state.user_id = '';

                $('#more_model').children('.modal').toggleClass('d-none d-block');
                $('#more_model').children('.modal').toggleClass('show');
                $('#more_model_backdrop').toggleClass('d-block d-none');

                ToastMessage('success', 'User password changed successfully.');
            }
            else {
                if (api_response.data.errors == undefined && api_response.data.errors == null) {
                    ToastMessage('error', api_response.data.message);
                } else {
                    ToastMessage('error', api_response.data.errors[0]);
                }
            }

        }
    }

    updateUserProfile = async () => {
        let {
            user_name,
            email,
            mobile,
            name,
            city,
            user_id,
        } = this.state;

        if (user_name == '') {
            ToastMessage('error', 'Please enter user_name !');
            console.log('error', 'Please enter user_name !');
        } else if (name == '') {
            ToastMessage('error', 'Please enter name!');
            console.log('error', 'Please enter name!');
        } else if (email == '') {
            ToastMessage('error', 'Please enter email!');
            console.log('error', 'Please enter email!');
        } else if (mobile == '') {
            ToastMessage('error', 'Please enter mobile!');
            console.log('error', 'Please enter mobile!');
        } else if (this.state.transation_code != this.state.user.transation_code) {
            ToastMessage('error', 'Please enter correct transction password!');
            console.log('error', 'Please enter correct transction password!');
        } else {
            let data = {
                user_name: user_name,
                name: name,
                email: email,
                mobile: mobile,
                city: city,
                role_id: this.state.user_detail.role_id,
                admin_id: this.state.user_detail.admin_id,
            }
            console.log('data', data);

            let api_response = await ApiExecute(`user/${user_id}`, { method: 'PUT', data: data });
            console.log('api_response', api_response);
            if (api_response.data.status) {

                this.state.user_name = '';
                this.state.name = '';
                this.state.email = '';
                this.state.mobile = '';
                this.state.city = '';
                this.state.user_id = '';

                $('#more_model').children('.modal').toggleClass('d-none d-block');
                $('#more_model').children('.modal').toggleClass('show');
                $('#more_model_backdrop').toggleClass('d-block d-none');

                ToastMessage('success', 'User profile updated successfully.');
            }
            else {
                if (api_response.data.errors == undefined && api_response.data.errors == null) {
                    ToastMessage('error', api_response.data.message);
                } else {
                    ToastMessage('error', api_response.data.errors[0]);
                }
            }

        }
    }

    render() {
        const { Route } = this.props;

        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");

        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }
        return (
            <>
                <header data-v-162f8f8f="" id="page-topbar">
                    <ToastContainer
                        position="top-center"
                        autoClose={4000}
                        hideProgressBar
                        newestOnTop={false}
                        closeOnClick={false}
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                    />
                    <div class="navbar-header">
                        <div class="d-flex">
                            <div class="navbar-brand-box">
                                <a href="" aria-current="page" class="logo logo-light router-link-exact-active router-link-active" style={{ display: 'block' }}>
                                    <span class="logo-sm"><img src="https://sitethemedata.com/v3/static/admin/img/icon.png" alt="" height="22" /></span>
                                    <span class="logo-lg"><img src="https://sitethemedata.com/sitethemes/world777.com/admin/logo.png" alt="" class="site-logo" /></span>
                                </a>
                            </div>
                            <button id="vertical-menu-btn" type="button" class="btn btn-sm px-3 fontSize-16 header-item side-btn" onClick={this.sidebar_menu.bind(this)}><i class="fa fa-fw fa-bars"></i>
                            </button>
                            <div class="site-searchbox mt-3 d-none d-lg-inline-block">
                                <div tabIndex="-1" className={multiselect, this.state.searchInput}>
                                    <div class="multiselect__select"></div>
                                    <div class="multiselect__tags" onClick={this.search_input.bind(this)}>
                                        <div class="multiselect__tags-wrap" style={{ display: 'none' }}></div>
                                        <div class="multiselect__spinner" style={{ display: 'none' }}></div>
                                        <input name="" type="text" autoComplete="nope" placeholder="Search User" tabIndex="0" class="multiselect__input" style={{ width: !this.state.searchBlock ? '0px' : '100%', position: !this.state.searchBlock ? 'absolute' : '', padding: !this.state.searchBlock ? '0px' : '' }} />
                                        {
                                            !this.state.searchBlock
                                                ?
                                                <span class="multiselect__placeholder"> Search User </span>
                                                :
                                                <></>
                                        }


                                    </div>
                                    <div tabIndex="-1" class="multiselect__content-wrapper" style={{ maxHeight: '300px', display: !this.state.searchBlock ? 'none' : 'block' }}>
                                        <ul class="multiselect__content" style={{ display: 'block' }}>

                                            <li style={{ display: 'none' }}><span class="multiselect__option"><span>No elements found</span></span>
                                            </li>
                                            <li><span class="multiselect__option">List is empty.</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" w-100">
                            <div class="upcoming-fixure fixture-nav" onClick={this.searchoverlay.bind(this)}>
                                <div class="fixure-title">Upcoming Fixtures</div>
                                <div class="fixure-box-container1">

                                    <Slider {...banner} >
                                        {
                                            this.state.upcoming_fixtures.map((upcoming_fixture, index) => {
                                                return (
                                                    <div data-v-e4caeaf8="" tabIndex="-1" data-index="37" aria-hidden="true" class="slick-slide slick-cloned" style={{ width: '305px' }}>
                                                        <div data-v-e4caeaf8="">
                                                            <a href="" class="" tabIndex="-1" data-v-e4caeaf8="" style={{ width: '100%', display: 'inline-block' }}>
                                                                <div class="fixure-box">
                                                                    <div class="f-title"><i class="d-icon mr-2 icon-2"></i>
                                                                        {upcoming_fixture.name}</div>
                                                                    <div>{upcoming_fixture.date}</div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                        }
                                    </Slider>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex" onClick={this.searchoverlay.bind(this)}>
                            <div class="dropdown b-dropdown d-inline-block d-lg-none ml-2 btn-group" id="__BVID__35">

                                <button aria-haspopup="true" aria-expanded="false" type="button" class="btn dropdown-toggle btn-black header-item noti-icon" id="__BVID__35__BV_toggle_"><i class="mdi mdi-magnify"></i>
                                </button>
                                <ul role="menu" tabIndex="-1" class="dropdown-menu dropdown-menu-lg p-0 dropdown-menu-right" aria-labelledby="__BVID__35__BV_toggle_">
                                    <form class="p-3">
                                        <div class="form-group m-0">
                                            <div class="input-group">
                                                <div tabIndex="-1" class="multiselect">
                                                    <div class="multiselect__select"></div>
                                                    <div class="multiselect__tags">
                                                        <div class="multiselect__tags-wrap" style={{ display: 'none' }}></div>

                                                        <div class="multiselect__spinner" style={{ display: 'none' }}></div>
                                                        <input name="" type="text" autoComplete="nope" placeholder="Search User" tabIndex="0" class="multiselect__input" style={{ width: '0px', position: 'absolute', padding: '0px' }} />
                                                        <span class="multiselect__placeholder">
                                                            Search User
                                                        </span>
                                                    </div>
                                                    <div tabIndex="-1" class="multiselect__content-wrapper" style={{ maxHeight: '300px', display: 'none' }}>
                                                        <ul class="multiselect__content" style={{ display: 'block' }}>

                                                            <li style={{ display: 'none' }}><span class="multiselect__option"><span>No elements found</span></span>
                                                            </li>
                                                            <li><span class="multiselect__option">List is empty.</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </ul>
                            </div>
                            <div class="dropdown d-none d-lg-inline-block ml-1">
                                <button type="button" class="btn header-item noti-icon" onClick={this.toggleFullScreen.bind(this)}><i class="bx bx-fullscreen" ></i>
                                </button>
                            </div>
                            <div class="d-none d-sm-inline-block rules-icon nowrap">

                                <Popup
                                    trigger={<span class="main-rules"><a href="javascript:void(0)"><i class="fas fa-info-circle mr-1"></i>Rules</a></span>}
                                    modal
                                    nested >
                                    {
                                        close => (
                                            <div id="__BVID__24" role="dialog" aria-labelledby="__BVID__24___BV_modal_title_" aria-describedby="__BVID__24___BV_modal_body_" class="modal fade show" aria-modal="true" style={{ display: 'block' }}>
                                                <div class="modal-dialog modal-lg"><span tabIndex="0"></span>
                                                    <div id="__BVID__24___BV_modal_content_" tabIndex="-1" class="modal-content">
                                                        <header id="__BVID__24___BV_modal_header_" class="modal-header">

                                                            <h5 id="__BVID__24___BV_modal_title_" class="modal-title">Rules</h5>
                                                            <button type="button" aria-label="Close" class="close" onClick={() => { console.log('modal closed '); close(); }}>×</button>
                                                        </header>
                                                        <div id="__BVID__24___BV_modal_body_" class="modal-body">
                                                            <div class="main-rules-container">
                                                                <div class="dropdown rules-language-container">
                                                                    <div data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
                                                                        <img src="https://sitethemedata.com/v3/static/front/img/flag_english.png" />English <i class="fas fa-angle-down ml-1"></i>
                                                                    </div>
                                                                    <div class="dropdown-menu rules-language">
                                                                        <div>
                                                                            <img src="https://sitethemedata.com/v3/static/front/img/flag_english.png" /> <span>English</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="menu-box">
                                                                    <div id="accordion">
                                                                        <div class="card">
                                                                            <div id="eventhead0" class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event0" aria-controls="event0" class="collapsed">
                                                                                Football
                                                                            </a>
                                                                            </div>
                                                                            <div id="event0" aria-labelledby="eventhead0" data-parent="#accordion" class="collapse">
                                                                                <div id="eventaccordion0" class="card-body">
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event0game0" class="collapsed">match</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion0" id="event0game0" class="card-body collapse">
                                                                                            <div class="rule-text">If a match is abandoned after it has already kicked off, any bets where the outcome has already been decided e.g. half-time result or first team to score will stand. All other bets will be made void regardless of the score-line at the time of abandonment.</div>
                                                                                            <div class="rule-text text-danger">If confirmation of the rescheduled start time is not received within 3 hours of the original kick-off time, bets will be void.</div>
                                                                                            <div class="rule-text text-danger">If kick-off time suffers from consecutive delays which take it over the 3 hour period mentioned above, bets will still stand as long as the eventual reschedule is within 48 hours of the original kick-off time.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event0game1" class="collapsed">bookmaker</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion0" id="event0game1" class="card-body collapse">
                                                                                            <div class="rule-text">If the match will not take place within 48 hours of the original kick-off time bets will be void.</div>
                                                                                            <div class="rule-text text-danger">If the selection is in a multiple bet or accumulator any refund must be requested before kick-off of the first leg of the multiple bet.</div>
                                                                                            <div class="rule-text text-danger">Where a confirmed postponed match features as part of a multiple bet, the bet will stand on the remaining selections in the multiple.</div>
                                                                                            <div class="rule-text">Please note that games which have their kick-off altered well in advance to accommodate live TV, or to ease fixture congestion will not be classed as postponed.</div>
                                                                                            <div class="rule-text text-danger">If a match is forfeited or a team is given a walkover victory without the match having kicked off, then all bets will be void. Any subsequently awarded scoreline will not count for settlement purposes.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event0game2" class="collapsed">fancy</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion0" id="event0game2" class="card-body collapse">
                                                                                            <div class="rule-text text-danger">Tournament Total Goals, Team Total Goals goals. scored in 90 minutes or in extra-time will count.Goals scored in penalty shootouts do not count.</div>
                                                                                            <div class="rule-text text-danger">Tournament Corners - Only corners taken in 90 minutes count.</div>
                                                                                            <div class="rule-text text-danger">Tournament Penalties Missed/Converted - Penalties taken in 90 minutes, extra-time and penalty shootouts all count. If a penalty has to be re-taken the previous disallowed penalty(ies) do not count.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card">
                                                                            <div id="eventhead1" class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1" aria-controls="event1" class="collapsed">
                                                                                Cricket
                                                                            </a>
                                                                            </div>
                                                                            <div id="event1" aria-labelledby="eventhead1" data-parent="#accordion" class="collapse">
                                                                                <div id="eventaccordion1" class="card-body">
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1game0" class="collapsed">bookmaker</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion1" id="event1game0" class="card-body collapse">
                                                                                            <div class="rule-text text-danger">Due to any reason any team will be getting advantage or disadvantage we are not concerned.</div>
                                                                                            <div class="rule-text text-danger">Company reserves the right to suspend/void any id/bets if the same is found to be illegitimate. For example incase of vpn/robot-use/multiple entry from same IP/ multiple bets at the same time (Punching) and others. Note : only winning bets will be voided.</div>
                                                                                            <div class="rule-text text-danger">We will simply compare both teams 25 overs score higher score team will be declared winner in ODI (25 over comparison)</div>
                                                                                            <div class="rule-text text-danger">We will simply compare both teams 10 overs higher score team will be declared winner in T20 matches (10 over comparison)</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Any query about the result or rates should be contacted within 7 days of the specific event, the same will not be considered valid post 7 days from the event.</div>
                                                                                            <div class="rule-text">Football-Spain LaLiga winner 2019-2020 without Barcelona &amp; Real Madrid</div>
                                                                                            <div class="rule-text text-danger">Highest point scoring team in the league table excluding Barcelona &amp; Real Madrid will be considered as winner of this event</div>
                                                                                            <div class="rule-text text-danger">If two team ends up with equal points, then result will be given based on the official point table</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1game1" class="collapsed">fancy</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion1" id="event1game1" class="card-body collapse">
                                                                                            <div class="rule-text">1. All fancy bets will be validated when match has been tied.</div>
                                                                                            <div class="rule-text text-danger">2. All advance fancy will be suspended before toss or weather condition.</div>
                                                                                            <div class="rule-text text-danger">3. In case technical error or any circumstances any fancy is suspended and does not resume result will be given all previous bets will be valid (based on haar/jeet).</div>
                                                                                            <div class="rule-text text-danger">4. If any case wrong rate has been given in fancy that particular bets will be cancelled.</div>
                                                                                            <div class="rule-text text-danger">5. In any circumstances management decision will be final related to all exchange items. Our scorecard will be considered as valid if there is any mismatch in online portal.</div>
                                                                                            <div class="rule-text text-danger">6. In case customer make bets in wrong fancy we are not liable to delete. No changes will be made and bets will be consider as confirm bet.</div>
                                                                                            <div class="rule-text text-danger">7. Due to any technical error market is open and result has came all bets after result will be deleted.</div>
                                                                                            <div class="rule-text text-danger">8. Manual bets are not accepted in our exchange.</div>
                                                                                            <div class="rule-text text-danger">9.Our exchange will provide 5 second delay in our TV.</div>
                                                                                            <div class="rule-text text-danger">10. Company reserves the right to suspend/void any id/bets if the same is found to be illegitimate. For example incase of VPN/robot-use/multiple entry from same IP and others. Note : only winning bets will be voided.</div>
                                                                                            <div class="rule-text text-danger">11. Company reserves the right to void any bets (only winning bets) of any event at any point of the match if the company believes there is any cheating/wrong doing in that particular event by the players (either batsman/bowler)</div>
                                                                                            <div class="rule-text text-danger">12. Once our exchange give username and password it is your responsibility to change a password.</div>
                                                                                            <div class="rule-text text-danger">13. Penalty runs will not be counted in any fancy.</div>
                                                                                            <div class="rule-text text-danger">14. Warning:- live scores and other data on this site is sourced from third party feeds and may be subject to time delays and/or be inaccurate. If you rely on this data to place bets, you do so at your own risk. Our exchange does not accept responsibility for loss suffered as a result of reliance on this data.</div>
                                                                                            <div class="rule-text text-danger">15. Traders will be block the user ID if find any misinterpret activities, No queries accept regarding.</div>
                                                                                            <div class="rule-text text-danger">16.Our exchange is not responsible for misuse of client id.</div>
                                                                                            <div class="rule-text text-danger">TEST</div>
                                                                                            <div class="rule-text text-danger">1 Session:-</div>
                                                                                            <div class="rule-text">1.1 Complete session valid in test.</div>
                                                                                            <div class="rule-text">1.2 Session is not completed for ex:- India 60 over run session Ind is running in case India team declares or all-out at 55 over next 5 over session will be continue in England inning.</div>
                                                                                            <div class="rule-text">1.3 1st day 1st session run minimum 25 over will be played then result is given otherwise 1st day 1st session will be deleted.</div>
                                                                                            <div class="rule-text">1.4 1st day 2nd session run minimum 25 over will be played then result is given otherwise 1st day 2nd session will be deleted.</div>
                                                                                            <div class="rule-text">1.5 1st day total run minimum 80 over will be played then result is given otherwise 1st day total run will be deleted.</div>
                                                                                            <div class="rule-text">1.6 Test match both advance session is valid.</div>
                                                                                            <div class="rule-text text-danger">2 Test lambi/ Inning run:-</div>
                                                                                            <div class="rule-text">2.1 Mandatory 70 over played in test lambi paari/ Innings run. If any team all-out or declaration lambi paari/ innings run is valid.</div>
                                                                                            <div class="rule-text">2.2 In case due to weather situation match has been stopped all lambi trades will be deleted.</div>
                                                                                            <div class="rule-text">2.3 In test both lambi paari / inning run is valid in advance fancy.</div>
                                                                                            <div class="rule-text text-danger">3 Test batsman:-</div>
                                                                                            <div class="rule-text">3.1 In case batsmen is injured he/she is made 34 runs the result will be given 34 runs.</div>
                                                                                            <div class="rule-text">3.2 Batsman 50/100 run if batsman is injured or declaration the result will be given on particular run.</div>
                                                                                            <div class="rule-text">3.3 In next men out fancy if player is injured particular fancy will be deleted.</div>
                                                                                            <div class="rule-text">3.4 In advance fancy opening batsmen is only valid if same batsmen came in opening the fancy will be valid in case one batsmen is changed that particular player fancy will be deleted.</div>
                                                                                            <div class="rule-text">3.5 Test match both advance fancy batsmen run is valid.</div>
                                                                                            <div class="rule-text text-danger">4 Test partnership:-</div>
                                                                                            <div class="rule-text">4.1 In partnership one batsman is injured partnership is continued in next batsman.</div>
                                                                                            <div class="rule-text">4.2 Partnership and player runs due to weather condition or match abandoned the result will be given as per score.</div>
                                                                                            <div class="rule-text">4.3 Advance partnership is valid in case both players are different or same.</div>
                                                                                            <div class="rule-text">4.4 Test match both advance fancy partnership is valid.</div>
                                                                                            <div class="rule-text text-danger">5 Other fancy advance (test):-</div>
                                                                                            <div class="rule-text">5.1 Four, sixes, wide, wicket, extra run, total run, highest over and top batsmen is valid only if 300 overs has been played or the match has been won by any team otherwise all these fancy will be deleted. Additionally all events are valid only for 1st innings( this is applicable to all individual team events also)</div>
                                                                                            <div class="rule-text text-danger">2 Odi rule:-</div>
                                                                                            <div class="rule-text text-danger">Session:-</div>
                                                                                            <div class="rule-text">Match 1st over run advance fancy only 1st innings run will be counted.</div>
                                                                                            <div class="rule-text">Complete session is valid in case due to rain or match abandoned particular session will be deleted.</div>
                                                                                            <div class="rule-text">For example:- 35 over run team a is playing any case team A is all-out in 33 over team a has made 150 run the session result is validated on particular run.</div>
                                                                                            <div class="rule-text">Advance session is valid in only 1st innings.</div>
                                                                                            <div class="rule-text text-danger">50 over runs:-</div>
                                                                                            <div class="rule-text">In case 50 over is not completed all bet will be deleted due to weather or any condition.</div>
                                                                                            <div class="rule-text">Advance 50 over runs is valid in only 1st innings.</div>
                                                                                            <div class="rule-text text-danger">Odi batsman runs:-</div>
                                                                                            <div class="rule-text">In case batsman is injured he/she is made 34 runs the result will be given 34 runs.</div>
                                                                                            <div class="rule-text">In next men out fancy if player is injured particular fancy will be deleted.</div>
                                                                                            <div class="rule-text">In advance fancy opening batsmen is only valid if same batsmen came in opening the fancy will be valid in case one batsmen is changed that particular player fancy will be deleted.</div>
                                                                                            <div class="rule-text text-danger">Odi partnership runs:-</div>
                                                                                            <div class="rule-text">In partnership one batsman is injured partnership is continued in next batsman.</div>
                                                                                            <div class="rule-text">Advance partnership is valid in case both players are different or same.</div>
                                                                                            <div class="rule-text">Both team advance partnerships are valid in particular match.</div>
                                                                                            <div class="rule-text text-danger">Other fancy:-</div>
                                                                                            <div class="rule-text">Four, sixes, wide, wicket, extra run, total run, highest over ,top batsman,maiden over,caught-out,no-ball,run-out,fifty and century are valid only match has been completed in case due to rain over has been reduced all other fancy will be deleted.</div>
                                                                                            <div class="rule-text text-danger">T20:-</div>
                                                                                            <div class="rule-text text-danger">Session:-</div>
                                                                                            <div class="rule-text">Match 1st over run advance fancy only 1st innings run will be counted.</div>
                                                                                            <div class="rule-text">Complete session is valid in case due to rain or match abandoned particular session will be deleted.</div>
                                                                                            <div class="rule-text">For example :- 15 over run team a is playing any case team a is all-out in 13 over team A has made 100 run the session result is validated on particular run.</div>
                                                                                            <div class="rule-text">Advance session is valid in only 1st innings.</div>
                                                                                            <div class="rule-text text-danger">20 over runs:-</div>
                                                                                            <div class="rule-text">Advance 20 over run is valid only in 1st innings. 20 over run will not be considered as valid if 20 overs is not completed due to any situation.</div>
                                                                                            <div class="rule-text text-danger">T20 batsman runs:-</div>
                                                                                            <div class="rule-text">In case batsman is injured he/she is made 34 runs the result will be given 34 runs.</div>
                                                                                            <div class="rule-text">In next men out fancy if player is injured particular fancy will be deleted.</div>
                                                                                            <div class="rule-text">In advance fancy opening batsmen is only valid if same batsmen came in opening the fancy will be valid in case one batsmen is changed that particular player fancy will be deleted.</div>
                                                                                            <div class="rule-text text-danger">T20 partnership runs:-</div>
                                                                                            <div class="rule-text">In partnership one batsman is injured partnership is continued in next batsman.</div>
                                                                                            <div class="rule-text">Advance partnership is valid in case both players are different or same.</div>
                                                                                            <div class="rule-text">Both team advance partnerships are valid in particular match.</div>
                                                                                            <div class="rule-text text-danger">1st 2 &amp; 3 Wickets runs:- T20/ODI</div>
                                                                                            <div class="rule-text">Advance event is valid in only 1st Innings.</div>
                                                                                            <div class="rule-text">If over reduced due to rain or weather condition or match abandoned the result will be given as per score.</div>
                                                                                            <div class="rule-text text-danger">Other fancy:-</div>
                                                                                            <div class="rule-text">T-20 ,one day and test match in case current innings player and partnership are running in between match has been called off or abandoned that situation all current player and partnership results are valid.</div>
                                                                                            <div class="rule-text">Four, sixes, wide, wicket, extra run, total run, highest over and top batsman,maiden over,caught-out,no-ball,run-out,fifty and century are valid only match has been completed in case due to rain over has been reduced all other fancy will be deleted. 1st 6 over dot ball and 20 over dot ball fancy only valid is 1st innings.</div>
                                                                                            <div class="rule-text">1st wicket lost to any team balls meaning that any team 1st wicket fall down in how many balls that particular fancy at least minimum one ball have to be played otherwise bets will be deleted.</div>
                                                                                            <div class="rule-text">1st wicket lost to any team fancy valid both innings.</div>
                                                                                            <div class="rule-text">How many balls for 50 runs any team meaning that any team achieved 50 runs in how many balls that particular fancy at least one ball have to be played otherwise that fancy bets will be deleted.</div>
                                                                                            <div class="rule-text">How many balls for 50 runs fancy any team only first inning valid.</div>
                                                                                            <div class="rule-text">1st 6 inning boundaries runs any team fancy will be counting only according to run scored fours and sixes at least 6 over must be played otherwise that fancy will be deleted.</div>
                                                                                            <div class="rule-text">1st inning 6 over boundaries runs any team run like wide ,no-ball ,leg-byes ,byes and over throw runs are not counted this fancy.</div>
                                                                                            <div class="rule-text">How many balls face any batsman meaning that any batsman how many balls he/she played that particular fancy at least one ball have to be played otherwise that fancy bets will be deleted.</div>
                                                                                            <div class="rule-text">How many balls face by any batsman both innings valid.</div>
                                                                                            <div class="rule-text">Lowest scoring over will be considered valid only if the over is completed fully (all six deliveries has to be bowled)</div>
                                                                                            <div class="rule-text text-danger">Concussion in Test:-</div>
                                                                                            <div class="rule-text">All bets of one over session will be deleted in test scenario, in case session is incomplete. For example innings declared or match suspended to bad light or any other conditions.</div>
                                                                                            <div class="rule-text">1. All bets will be considered as valid if a player has been replaced under concussion substitute, result will be given for the runs scored by the mentioned player. For example DM Bravo gets retired hurt at 23 runs, then result will be given for 23.</div>
                                                                                            <div class="rule-text">2. Bets of both the player will be valid under concussion substitute.</div>
                                                                                            <div class="rule-text text-danger">Total Match- Events (test):-</div>
                                                                                            <div class="rule-text">Minimum of 300 overs to be bowled in the entire test match, otherwise all bets related to the particular event will get void. For example, Total match caught outs will be valid only if 300 overs been bowled in the particular test match</div>
                                                                                            <div class="rule-text text-danger">Limited over events-Test:-</div>
                                                                                            <div class="rule-text">This event will be considered valid only if the number of overs defined on the particular event has been bowled, otherwise all bets related to this event will get void. For example 0-25 over events will be valid only if 25 overs has been bowled, else the same will not be considered as valid</div>
                                                                                            <div class="rule-text">If the team gets all out prior to any of the defined overs, then balance overs will be counted in next innings. For example if the team gets all out in 23.1 over the same will be considered as 24 overs and the balance overs will be counted from next innings.</div>
                                                                                            <div class="rule-text text-danger">Bowler Wicket event's- Test:-</div>
                                                                                            <div class="rule-text">Minimum of one legal over (one complete over) has to be bowled by the bowler mentioned in the event, else the same will not be considered as valid.</div>
                                                                                            <div class="rule-text text-danger">Bowler over events- Test:-</div>
                                                                                            <div class="rule-text">The mentioned bowler has to complete the defined number of overs, else the bets related to that particular event will get void. For example if the mentioned bowler has bowled 8 overs, then 5 over run of that particular bowler will be considered as valid and the 10 over run will get void.</div>
                                                                                            <div class="rule-text text-danger">Player ball event's- Test:-</div>
                                                                                            <div class="rule-text">This event will be considered valid only if the defined number of runs made by the mentioned player, else the result will be considered as 0 (zero) balls.</div>
                                                                                            <div class="rule-text">For example if Root makes 20 runs in 60 balls and gets out on 22 runs, result for 20 runs will be 60 balls and the result for balls required for 25 run Root will be considered as 0 (Zero) and the same will be given as result</div>
                                                                                            <div class="rule-text text-danger">Limited over events-ODI:-</div>
                                                                                            <div class="rule-text">This event will be considered valid only if the number of overs defined on the particular event has been bowled, otherwise all bets related to this event will get void. 0-50 over events will be valid only if 50 over completed, if the team batting first get all out prior to 50 over the balance over will be counted from second innings. For example if team batting first gets all out in 35 over balance 15 over will be counted from second innings, the same applies for all events if team gets all out before the defined number of overs.</div>
                                                                                            <div class="rule-text">The events which remains incomplete will be voided if over gets reduced in the match due to any situation, for example if match interrupted in 15 overs due to rain/badlight and post this over gets reduced. Events for 0-10 will be valid, all other events related to this type will get deleted.</div>
                                                                                            <div class="rule-text">This events will be valid only if the defined number of over is completed. For example team batting first gets all out in 29.4 over then the same will be considered as 30 over, the team batting second must complete 20 overs only then 0-50 over events will be considered as valid. In case team batting second gets all out in 19.4 over then 0-50 over event will not be considered as valid, This same is valid for 1st Innings only.</div>
                                                                                            <div class="rule-text text-danger">Bowler event- ODI:-</div>
                                                                                            <div class="rule-text">The mentioned bowler has to complete the defined number of overs, else the bets related to that particular event will get void. For example if the mentioned bowler has bowled 8 overs, then 5 over run of that particular bowler will be considered as valid and the 10 over run will get void.</div>
                                                                                            <div class="rule-text">Both innings are valid</div>
                                                                                            <div class="rule-text text-danger">Other event:- T20</div>
                                                                                            <div class="rule-text">The events for 1-10 over and 11-20 over will be considered valid only if the number of over mentioned has been played completely. However if the over got reduced before the particular event then the same will be voided, if the team batting first get all out prior to 20 over the balance over will be counted from second innings. For example if team batting first gets all out in 17 over balance 3 over will be counted from second innings. This same is valid for 1st Innings only.</div>
                                                                                            <div class="rule-text">If over got reduced in between any running event, then the same will be considered valid and the rest will be voided. For example.., match started and due to rain/bad light or any other situation match got interrupted at 4 over and later over got reduced. Then events for 1-10 is valid rest all will be voided</div>
                                                                                            <div class="rule-text">Bowler Session: This event is valid only if the bowler has completed his maximum quota of overs, else the same will be voided. However if the match has resulted and the particular bowler has already started bowling his final over then result will be given even if he haven't completed the over. For example B Kumar is bowling his final over and at 3.4 the match has resulted then result will be given for B Kumar over runs</div>
                                                                                            <div class="rule-text">Incase of DLS, the over got reduced then the bowler who has already bowled his maximum quota of over that result will be considered as valid and the rest will be voided</div>
                                                                                            <div class="rule-text text-danger">12. Player and partnership are valid only 14 matches.</div>
                                                                                            <div class="rule-text text-danger">Boundary on Match 1st Free hit</div>
                                                                                            <div class="rule-text">Both innings are valid</div>
                                                                                            <div class="rule-text">Boundary hit on Free hit only be considered as valid</div>
                                                                                            <div class="rule-text">Bets will be deleted if there is no Free hit in the mentioned match</div>
                                                                                            <div class="rule-text">Boundary by bat will be considered as valid</div>
                                                                                            <div class="rule-text text-danger">Boundaries by Player</div>
                                                                                            <div class="rule-text">Both Four and six are valid</div>
                                                                                            <div class="rule-text text-danger">Any query regarding result or rate has to be contacted within 7 days from the event, query after 7 days from the event will not be considered as valid</div>
                                                                                            <div class="rule-text text-danger">CPL:-</div>
                                                                                            <div class="rule-text">If CPL fixture 0f 33 matches gets reduced due to any reason, then all the special fancies will be voided (Match abandoned due to rain/bad light will not be considered in this)</div>
                                                                                            <div class="rule-text">Fancy based on all individual teams are valid only for league stage</div>
                                                                                            <div class="rule-text">Total 1st over runs: Average 6 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total fours: Average 22 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total sixes: Average 13 sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average will 13 Wickets be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wides - Average 10 wides will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 18 extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total No ball - Average 1 no ball will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Fifties - Average 1 fifties will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Caught outs: Average 9 caught out will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">At any situation if result is given for any particular event based on the rates given for the same, then the particular result will be considered valid, similarly if the tournament gets canceled due to any reason the previously given result will be considered valid</div>
                                                                                            <div class="rule-text">Management decision will be final</div>
                                                                                            <div class="rule-text">Highest innings run - Only first innings is valid</div>
                                                                                            <div class="rule-text">Lowest innings run - Only first innings is valid</div>
                                                                                            <div class="rule-text">Highest over run: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest 1st over run in individual match: Both innings are valid, however for CPL we have created the fancy for 1st innings only</div>
                                                                                            <div class="rule-text">Highest Fours in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Sixes in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Extras in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Wicket in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Super over will not be included</div>
                                                                                            <div class="rule-text text-danger">Barbados Tridents</div>
                                                                                            <div class="rule-text">Opening partnership run: Average 24 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">First 6 over run: Average 45 run will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">St Kitts and Nevis Patriots</div>
                                                                                            <div class="rule-text">Opening partnership run: Average 25 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">First 6 over run: Average 45 run will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Trinbago Knight Riders</div>
                                                                                            <div class="rule-text">Opening partnership run: Average 22 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">First 6 over run: Average 46 run will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Guyana Amazon Warriors</div>
                                                                                            <div class="rule-text">Opening partnership run: Average 23 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">First 6 over run: Average 44 run will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">St Lucia Zouks</div>
                                                                                            <div class="rule-text">Opening partnership run: Average 22 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">First 6 over run: Average 43 run will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Jamaica Tallawahs</div>
                                                                                            <div class="rule-text">Opening partnership run: Average 24 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">First 6 over run: Average 46 run will be given in case match abandoned or over reduced</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1game2" class="collapsed">khado</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion1" id="event1game2" class="card-body collapse">
                                                                                            <div class="rule-text">Only First inning valid for T20 and one day matches.</div>
                                                                                            <div class="rule-text">Same will be work like Lambi. If match abandoned or over reduced, all bets will be deleted.</div>
                                                                                            <div class="rule-text">You can choose your own value in this event.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1game3" class="collapsed">fancy1</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion1" id="event1game3" class="card-body collapse">
                                                                                            <div class="rule-text text-danger">1. Even odd game betting rate rules.</div>
                                                                                            <div class="rule-text">1.1 Completed game is valid , in case due to rain over are reduced or match abandoned particular game will be deleted.</div>
                                                                                            <div class="rule-text">1.2 All bets regarding to ODD/EVEN player/partnership are valid if one legal delivery is being played, else the bets will be deleted. Player odd/even all advance bets will be valid if one legal delivery is being played in match otherwise voided.</div>
                                                                                            <div class="rule-text">1.4 In any circumstances management decision will be final.</div>
                                                                                            <div class="rule-text text-danger">2 Top batsman rules:-</div>
                                                                                            <div class="rule-text text-danger">2.1 If any player does not come as per playing eleven then all bets will be get deleted for the particular player.</div>
                                                                                            <div class="rule-text">2.2 two players done the same run in a single match (M Agarwal 30 runs and A Rayudu 30 runs, whole inning top batsmen score also 30 run) then both player settlement to be get done 50 percent (50% , 50%)rate on their original value which given by our exchange.</div>
                                                                                            <div class="rule-text">Suppose we have opened value of M Agarwal 3.75 back and customer place bets on 10000 @ 3.75 rates and A Rayudu 3.0 back and customer place bets on 10000 @ 3.0 rates.</div>
                                                                                            <div class="rule-text">Whole inning result announces 30 run by both player then</div>
                                                                                            <div class="rule-text text-danger">Rule of top batsman:-if you bet on M Agarwal you will be get half amount of this rate (10000*3.75/2=18750 you will get)</div>
                                                                                            <div class="rule-text text-danger">Rule of top batsman:-if you bet on A Rayudu you will be get half amount of this rate (10000*3.00/2=15000 you will get)</div>
                                                                                            <div class="rule-text">Top batsman only 1st inning valid.</div>
                                                                                            <div class="rule-text">For one day 50 over and for t-20 match 20 over must be played for top batsmen otherwise all bets will be deleted.</div>
                                                                                            <div class="rule-text text-danger">Man of the Match Rules</div>
                                                                                            <div class="rule-text">1. All bets will be deleted in case the match is abandoned or over reduced.</div>
                                                                                            <div class="rule-text">2. All bets will be deleted if the mentioned player is not included in playing 11.</div>
                                                                                            <div class="rule-text">3. In case Man of the Match is shared between two players then Dead heat rule will be applicable, For example K Perera and T Iqbal shares the Man of the Match, then the settlement will be done 50% of the rates accordingly.</div>
                                                                                            <div class="rule-text">4. Rules similar to our Top Batsman rules.</div>
                                                                                            <div class="rule-text text-danger">Maximum Sixes by Team</div>
                                                                                            <div class="rule-text">1. All bets will be deleted if match abandoned or over reduced</div>
                                                                                            <div class="rule-text">2. All bets will be deleted if both the teams hits same number of sixes.</div>
                                                                                            <div class="rule-text">3. Super over will not be considered.</div>
                                                                                            <div class="rule-text text-danger">Maximum 6 or 10 over runs</div>
                                                                                            <div class="rule-text">1. All bets will be deleted if match abandoned or over reduced.</div>
                                                                                            <div class="rule-text">2. All the bets will be deleted if both the teams score is same (Runs scored in 6 or 10 overs)</div>
                                                                                            <div class="rule-text">3. 6 overs for T20 and 10 overs for ODI</div>
                                                                                            <div class="rule-text">4. Both the innings are valid.</div>
                                                                                            <div class="rule-text">5. This fancy will be valid for 1st 6 overs of both innings for T20 and 1st 10 overs of both innings for ODI</div>
                                                                                            <div class="rule-text text-danger">Batsman Match</div>
                                                                                            <div class="rule-text">Batsman Match:- Bets for Favourite batsman from the two batsman matched.</div>
                                                                                            <div class="rule-text">All bets will be deleted if any one of the mentioned player is not included in playing 11.</div>
                                                                                            <div class="rule-text">All bets will be deleted unless one ball being played by both the mentioned players.</div>
                                                                                            <div class="rule-text">All bets will be deleted if over reduced or Match abandoned.</div>
                                                                                            <div class="rule-text">All bets will be deleted if both the player scored same run. For example H Amla and J Bairstow are the batsman matched, H Amla and J Bairstow both scored 38 runs then all bets will be deleted.</div>
                                                                                            <div class="rule-text">Both innings will be valid</div>
                                                                                            <div class="rule-text text-danger">Opening Pair</div>
                                                                                            <div class="rule-text">1. Bets for Favourite opening pair from the two mentioned opening pair.</div>
                                                                                            <div class="rule-text">2. Runs made by both the opening player will be added. For example:- J Roy scored 20 runs and J Bairstow scored 30 runs result will be 50 runs.</div>
                                                                                            <div class="rule-text">3. Highest run made by the pair will be declared as winner. For example: Opening pair ENG total is 70 runs and Opening pair SA is 90 runs, then SA 90 runs will be declared as winner.</div>
                                                                                            <div class="rule-text">Both innings will be valid</div>
                                                                                            <div class="rule-text text-danger">Our exchange Special</div>
                                                                                            <div class="rule-text">All bets will be deleted if the mentioned player is not included in playing 11.</div>
                                                                                            <div class="rule-text">All bets will be deleted if match abandoned or over reduced.</div>
                                                                                            <div class="rule-text">Both innings will be valid</div>
                                                                                            <div class="rule-text text-danger">Direction of First Boundary</div>
                                                                                            <div class="rule-text">All bets will be deleted if the mentioned batsman is not included in playing 11.</div>
                                                                                            <div class="rule-text">All bets will be deleted if match abandoned or over reduced.</div>
                                                                                            <div class="rule-text">The boundary hit through off side of the stump will be considered as off side four.</div>
                                                                                            <div class="rule-text">The boundary hit through leg side of the stump will be considered as leg side four.</div>
                                                                                            <div class="rule-text">Boundaries through extras (byes,leg byes,wide,overthrow) will not be considered as valid.</div>
                                                                                            <div class="rule-text">Only 1st Inning will be considered</div>
                                                                                            <div class="rule-text text-danger">Fifty &amp; Century by Batsman</div>
                                                                                            <div class="rule-text">All bets will be deleted if match abandoned or over reduced.</div>
                                                                                            <div class="rule-text">All bets will be deleted if the mentioned batsman is not included in playing 11.</div>
                                                                                            <div class="rule-text">All bets will be deleted unless the batsman faces one legal ball.</div>
                                                                                            <div class="rule-text">Both Innings will be valid.</div>
                                                                                            <div class="rule-text text-danger">1st over Fancy</div>
                                                                                            <div class="rule-text">Boundaries through extras (byes,leg byes,wide,overthrow) will not be considered.</div>
                                                                                            <div class="rule-text">Only 1st inning will be valid</div>
                                                                                            <div class="rule-text text-danger">Odd Even Fancy</div>
                                                                                            <div class="rule-text">Incompleted games will be deleted. Over reduced or abandoned all bets will be deleted.</div>
                                                                                            <div class="rule-text text-danger">For example:-275 run SL bhav must be played 50 over if rain comes or any condition otherwise 275 run SL bets will be deleted.</div>
                                                                                            <div class="rule-text text-danger">Next Man out</div>
                                                                                            <div class="rule-text">Next man out fancy advance &amp; in regular. Both inning will be valid. If any player does not come in opening then all bets will be deleted. If over reduced or abandoned then all bets will be deleted.</div>
                                                                                            <div class="rule-text text-danger">Caught out</div>
                                                                                            <div class="rule-text">Caught out fancy in advance &amp; in regular. Both inning will be valid. If over reduced or match abandoned then all bets will be deleted.</div>
                                                                                            <div class="rule-text text-danger">Wkt &amp; All out Fancy</div>
                                                                                            <div class="rule-text">5 wkt in 10 over &amp; All out in 20 over fancy is valid for both inning. If match abandoned or over reduced all bets will be deleted.</div>
                                                                                            <div class="rule-text text-danger">Test Match: Game Completed Fancy</div>
                                                                                            <div class="rule-text">1. This is the fancy for match to be won/ completed in which day &amp; session (Completed: Game has to be completed)</div>
                                                                                            <div class="rule-text">2. If match drawn then all the sessions will be considered as lost.</div>
                                                                                            <div class="rule-text text-danger">Meter Fancy</div>
                                                                                            <div class="rule-text">In case match abandoned or over reduced mid point rule will be applicable</div>
                                                                                            <div class="rule-text">For example: If Dhoni meter is 75 / 77 and the match abandoned or over reduced, then the result will be 76</div>
                                                                                            <div class="rule-text">In case of single difference result will be given for the higher rate of the final rate (eg 53/54) and match gets abandoned then the result will be given as 54</div>
                                                                                            <div class="rule-text">Midpoint rule is applicable for test match also. However for lambi meter/ inning meter 70 overs has to be played only then the same will be considered as valid</div>
                                                                                            <div class="rule-text text-danger">Maximum Boundaries:-</div>
                                                                                            <div class="rule-text">If the number of fours or sixes of both the team is equal, then all bets of the respective event will get voided</div>
                                                                                            <div class="rule-text text-danger">Khado:- Test</div>
                                                                                            <div class="rule-text">Minimum 70 over has to be played by the particular team only then the Khado of the team will be considered as valid, else the same will be voided</div>
                                                                                            <div class="rule-text text-danger">Lunch Favourite</div>
                                                                                            <div class="rule-text text-danger">1. The team which is favourite at lunch will be considered as lunch favourite or the team which is favourite after first inning last ball will be considered as lunch favourite in our exchange.</div>
                                                                                            <div class="rule-text text-danger">2. In any circumstances management decision will be final.</div>
                                                                                            <div class="rule-text text-danger">3. In case of tie in T20 or one day in lunch favourite game , all bets will be deleted in our exchange.</div>
                                                                                            <div class="rule-text text-danger">4. In case overs are reduced in a match, the team which favourite at lunch will be considered as lunch favourite.</div>
                                                                                            <div class="rule-text">4.1 For example :- if match is reduced to 18 over per side in t20 or Oneday then after 18 over the team which is favourite at lunch will be considered as lunch favourite.</div>
                                                                                            <div class="rule-text text-danger">5. In case of weather, 1st innings match overs are reduced and direct target is given to team which will bat in 2nd inning then lunch favourite will be considered after target is given at lunch.</div>
                                                                                            <div class="rule-text">5.1 For example :- in T20 match rain comes at 14 over and match is interrupted due to rain and direct target is given to 2nd batting team, then team which is favourite in match odds after target is given in match, will be considered as lunch favourite.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1game4" class="collapsed">cricket casino</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion1" id="event1game4" class="card-body collapse">
                                                                                            <div class="rule-text text-danger">1. Completed game is valid , in case due to rain over are reduced or match abandoned particular game will be deleted.</div>
                                                                                            <div class="rule-text text-danger">2. Helmet penalty run will be counted, rest other penalty run will not be counted in game of our exchange.</div>
                                                                                            <div class="rule-text text-danger">3. In any circumstances management decision will be final.</div>
                                                                                            <div class="rule-text text-danger">4. The last digit of run in all game will be valid in our exchange.</div>
                                                                                            <div class="rule-text text-danger">5. Single last digit game :-</div>
                                                                                            <div class="rule-text">5.1 For example:- 6 over run Ind : 47 run , so the result will be given as 7 for single last digit game in our exchange.</div>
                                                                                            <div class="rule-text text-danger">6. Double last digit game :-</div>
                                                                                            <div class="rule-text">6.1 For example:- 6 over run &amp; 10 over run Ind : 45 run &amp; 83 run respectively , so the result will be given as 53 for double last digit game in our exchange.</div>
                                                                                            <div class="rule-text text-danger">7. Triple last digit game :-</div>
                                                                                            <div class="rule-text">7.1 For example:- 6 over run, 10 over run &amp; 15 over run Ind : 43 run ,80 run and 125 respectively so the result will be given as 305 for triple last digit game in our exchange.</div>
                                                                                            <div class="rule-text">7.2 For example:- 6 over run, 10 over run &amp; Lambi run Ind : 43 run ,80 run and 187 respectively so the result will be given as 307 for triple last digit game in our exchange.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-header"><a href="javascript:void(0)" data-toggle="collapse" data-target="#event1game5" class="collapsed">match</a>
                                                                                        </div>
                                                                                        <div data-parent="#eventaccordion1" id="event1game5" class="card-body collapse">
                                                                                            <div class="rule-text text-danger">IPL</div>
                                                                                            <div class="rule-text text-danger">If IPL fixture of 60 matches gets reduced due to any reason, then all the special fancies will be voided (Match abandoned due to rain/bad light will not be considered in this)</div>
                                                                                            <div class="rule-text text-danger">At any situation if result is given for any particular event based on the rates given for the same, then the particular result will be considered valid, similarly if the tournament gets canceled due to any reason the previously given result will be considered valid</div>
                                                                                            <div class="rule-text text-danger">Management decision will be final</div>
                                                                                            <div class="rule-text">Total 1st over runs: Average 6 runs will be given in case match abandoned or over reduced (only 1st innings valid)</div>
                                                                                            <div class="rule-text">Total 1st 6 over run:- Average 47 runs will be given in case match abandoned or over reduced (Only 1st Innings valid)</div>
                                                                                            <div class="rule-text">Total fours: Average 27 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total sixes: Average 12 sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average will 12 Wickets be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wides - Average 8 wides will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Fifties - Average 2 fifties will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Caught outs: Average 9 caught out will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Bowled:- Average 2 Bowled out will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total LBW:- Average 1 LBW will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run out:- Average 1 Run out will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Highest innings run - Only first innings is valid</div>
                                                                                            <div class="rule-text">Lowest innings run - Only first innings is valid</div>
                                                                                            <div class="rule-text">Highest over run: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest 1st over run in individual match: only first innings is valid</div>
                                                                                            <div class="rule-text">Highest Fours in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Sixes in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Extras in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Wicket in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest 6 over run: - Both innings are valid</div>
                                                                                            <div class="rule-text text-danger">Super over will not be included</div>
                                                                                            <div class="rule-text">In fastest fifty always the first 50 runs will be considered, for example of R Sharma scores 1st fifty in 17 balls and scores 100 in next 14 balls, fastest 50 will be given based on the balls for the 1st fifty runs</div>
                                                                                            <div class="rule-text text-danger">Big Bash League</div>
                                                                                            <div class="rule-text">Total match 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Highest innings run - Only first innings is valid</div>
                                                                                            <div class="rule-text">Lowest innings run - Only first innings is valid</div>
                                                                                            <div class="rule-text">Total 1st 6 over run:- Average 46 runs will be given if total 20 overs is not played, This event is valid only for the 1st innings</div>
                                                                                            <div class="rule-text">Total Fours - Average 25 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 10 sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average will 12 Wickets be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wides - Average 8 wides will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Fifties - Average 2 fifties will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Caught out - Average 8 catch out will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Bowled out - Average 2 bowled out will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Highest 6 over run: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest run in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Fours in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest Sixes in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Total LBW:- Average 1 LBW will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Highest Wickets in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest extras in individual match: Both innings are valid</div>
                                                                                            <div class="rule-text">Highest match 1st over run in individual match: Only 1st inning will be considered as valid valid</div>
                                                                                            <div class="rule-text text-danger">All events related to bowler are valid only for the league stages, both the innings will be considered as valid. A minimum of 32 overs has to be bowled else the same will be voided. If the mentioned bowler has bowled one legal delivery then it will be considered as 1 over even if the over is not completed</div>
                                                                                            <div class="rule-text text-danger">All events based on ground are decided based on the initial fixture, in case of any changes in the venue between the series. Then average will be given based on the initial fixture for the number of games reduced, Similarly if any match is added newly to the venue will not be considered</div>
                                                                                            <div class="rule-text text-danger">Average for wickets taken will be given in case match abandoned or over reduced or the player has not bowled single legal delivery before the over got reduced</div>
                                                                                            <div class="rule-text text-danger">Fancy based on all individual teams/players/ground are valid only for league stage</div>
                                                                                            <div class="rule-text text-danger">Management decision will be final</div>
                                                                                            <div class="rule-text text-danger">Bellerive Oval:- Hobart</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 46 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 28 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 11 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 330 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Manuka Oval:- Canberra</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 5 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 44 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 24 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 10 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 13 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 318 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Bellerive Oval:- Hobart</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Aurora stadium:- Launceston</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 45 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 25 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 10 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 320 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">The Gabba:- Brisbane</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 44 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 24 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 9 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 13 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">QUEENSLAND</div>
                                                                                            <div class="rule-text">Total Run- Average 310 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 44 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 24 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 10 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 315 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Adelaide Oval</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 46 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 27 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 10 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 320 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Perth Stadium</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 46 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 26 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 12 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 9 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 16 Extras will be given in case match abandoned or over reducedTotal Extras - Average 16 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 340 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Showground Stadium</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 44 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 25 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 9 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 315 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Docklands Stadium</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 46 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 25 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 11 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 14 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 320 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Melbourne Ground</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 45 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 26 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 10 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 15 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 330 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Sydney Ground</div>
                                                                                            <div class="rule-text">Total 1st over run:- Average 6 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total 6 over run:- Average 46 runs will be given if total 20 overs is not played, only 1st innings will be considered as valid</div>
                                                                                            <div class="rule-text">Total Fours - Average 26 fours will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes - Average 12 Sixes will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets - Average 12 wickets will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide – Average 8 Wide will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras - Average 15 Extras will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run- Average 335 runs will be given in case match abandoned or over reduced</div>
                                                                                            <div class="rule-text text-danger">Average will be given for player if the mentioned player is not included in the playing 11</div>
                                                                                            <div class="rule-text text-danger">If the mentioned player is not included in playing 11 for 3 consecutive matches and the mentioned player will be deleted</div>
                                                                                            <div class="rule-text text-danger">Super over will not be included</div>
                                                                                            <div class="rule-text text-danger">World Cup</div>
                                                                                            <div class="rule-text text-danger">In case of any circumstances, management decision will be final for all the fancies under world cup.</div>
                                                                                            <div class="rule-text text-danger">WC:-WORLD CUP</div>
                                                                                            <div class="rule-text text-danger">MOM:-MAN OF THE MATCH.</div>
                                                                                            <div class="rule-text">Match 1st over run:- This fancy is valid only for first innings of the match, Average 4 runs will be given in case of match abandoned or the entire 50 over is not played.</div>
                                                                                            <div class="rule-text">Highest inning run:- This fancy is valid only for first innings of the match.</div>
                                                                                            <div class="rule-text">Lowest innings run:- This fancy is valid only for first innings of the match.</div>
                                                                                            <div class="rule-text">Total Fours:- Average 48 Fours will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Sixes:- Average 10 Sixes will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wickets:- Average 15 Wickets will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Wide:- Average 14 Wide will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Extras:- Average 25 Extras will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total No ball:- Average 2 No ball will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Fifties:- Average 3 Fifties will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Centuries:- Average 1 century will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Run outs:- Average 1 Run out will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">Total Ducks:- Average 1 Duck out will be given if the match is abandoned or over reduced. If the player is not out in the score of zero the same will not be considered as Duck out.</div>
                                                                                            <div class="rule-text">Total Caught Out:- Average 10 Caught Out will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">All fancy related to Individual teams are valid only for league matches (9 matches played by the teams in league stages)</div>
                                                                                            <div class="rule-text">In case any player mentioned in our world cup fancy doesn’t play for the first three consecutive matches all the bets will be deleted.</div>
                                                                                            <div class="rule-text">1. In case any player mentioned in our world cup fancy got ruled out or doesn’t play post few matches the bets after the last match played by the above mentioned player will be deleted. For example: U Khawaja played for first three league matches and doesn’t play after that, then bets for the first three matches will be valid. Bets after third match will be deleted.</div>
                                                                                            <div class="rule-text">2. First 10 over runs is valid for both innings for all the teams.</div>
                                                                                            <div class="rule-text">3. Total runs by team:- Average will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">4. First 10 over runs by team:- Average will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">5. Fours by team:- Average will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">6. Sixes by team:- Average will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">7. Opening wicket partnership:- Average will be given if the match is abandoned or over reduced</div>
                                                                                            <div class="rule-text">8. Runs by player:- Average will be given if the match is abandoned or over reduced, Average will be given unless one ball is being played after the player enters the crease</div>
                                                                                            <div class="rule-text">9. Wickets by player:- Average will be given if the match is abandoned or over reduced, Average will be given unless one legal delivery has been bowled by the mentioned player.</div>
                                                                                            <div class="rule-text">10. Sixes by player:- Average will be given if the match is abandoned or over reduced, Average will be given unless one ball is being played after the player enters the crease.</div>
                                                                                            <div class="rule-text text-danger">Average of every fancy follows:</div>
                                                                                            <div class="rule-text text-danger">Total runs by ENG 295 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of ENG 56 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by ENG 25 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by ENG 7 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of ENG 44 runs per game</div>
                                                                                            <div class="rule-text">J Roy runs 38 runs per game</div>
                                                                                            <div class="rule-text">J Bairstow runs 43 runs per game</div>
                                                                                            <div class="rule-text">J Root runs 43 runs per game</div>
                                                                                            <div class="rule-text">J Archer wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">C Woakes wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">A Rashid wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">J Bairstow Sixes 2 sixes per game</div>
                                                                                            <div class="rule-text">J Buttler Sixes 2 sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by IND 285 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of IND 53 runs per game</div>
                                                                                            <div class="rule-text">Total Four by IND 26 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by IND 6 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of IND 41 runs per game</div>
                                                                                            <div class="rule-text">S Dhawan runs 38 runs per game</div>
                                                                                            <div class="rule-text">R Sharma runs 43 runs per game</div>
                                                                                            <div class="rule-text">V Kohli runs 48 runs per game</div>
                                                                                            <div class="rule-text">J Bumrah wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">M Shami wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">K Yadav wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">R Sharma Sixes 2 sixes per game</div>
                                                                                            <div class="rule-text">H Pandya Sixes 1 sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by AUS 280 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of AUS 52 runs per game</div>
                                                                                            <div class="rule-text">Total Four by AUS 26 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by AUS 6 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of AUS 40 runs per game</div>
                                                                                            <div class="rule-text">D Warner runs 43 runs per game</div>
                                                                                            <div class="rule-text">A Finch runs 38 runs per game</div>
                                                                                            <div class="rule-text">S Smith runs 42 runs per game</div>
                                                                                            <div class="rule-text">M Starc wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">P Cummins wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">A Zampa wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">D Warner Sixes 2 sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by SA 270 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of SA 51 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by SA 24 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by SA 5 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of SA 37 runs per game</div>
                                                                                            <div class="rule-text">H Amla runs 38 runs per game</div>
                                                                                            <div class="rule-text">F Du plessis runs 39 runs per game</div>
                                                                                            <div class="rule-text">V Der Dussen runs Runs per game</div>
                                                                                            <div class="rule-text">Q De Kock runs 36 Runs per game</div>
                                                                                            <div class="rule-text">I Tahir wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">K Rabada wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">D Steyn wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">Q De Kock Sixes 1 sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by NZ 275 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of NZ 50 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by NZ 25 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by NZ 5 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of NZ 37 runs per game</div>
                                                                                            <div class="rule-text">C Munro runs 32 runs per game</div>
                                                                                            <div class="rule-text">M Guptill runs 38 runs per game</div>
                                                                                            <div class="rule-text">K Williamson runs 45 runs per game</div>
                                                                                            <div class="rule-text">H Nicholls runs Runs per game</div>
                                                                                            <div class="rule-text">T Boult wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">T Southee wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">M Santner wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">M Guptill Sixes 2 Sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by WI 270 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of WI 49 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by WI 23 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by WI 7 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of WI 35 runs per game</div>
                                                                                            <div class="rule-text">C Gayle runs 37 runs per game</div>
                                                                                            <div class="rule-text">E Lewis runs 32 runs per game</div>
                                                                                            <div class="rule-text">DM Bravo runs 32 runs per game</div>
                                                                                            <div class="rule-text">S Hope runs 37 runs per game</div>
                                                                                            <div class="rule-text">K Roach wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">S Cottrell wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">J holder wickets 1 wicket per game</div>
                                                                                            <div class="rule-text">A Nurse wickets 1 wickets per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by PAK 270 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of PAK 50 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by PAK 24 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by PAK 5 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of PAK 36 runs per game</div>
                                                                                            <div class="rule-text">Imam Ul Haq runs 36 runs per game</div>
                                                                                            <div class="rule-text">B Azam runs 44 runs per game</div>
                                                                                            <div class="rule-text">F Zaman runs 34 runs per game</div>
                                                                                            <div class="rule-text">H Ali wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">Shadab Khan wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">Shaheen Afridi wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">F Zaman Sixes 1 sixes per game</div>
                                                                                            <div class="rule-text text-danger">C Gayle Sixes 2 Sixes per game</div>
                                                                                            <div class="rule-text text-danger">A Russell Sixes 2 Sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by SL 250 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of SL 48 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by SL 22 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by SL 4 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of SL 32 runs per game</div>
                                                                                            <div class="rule-text">D Karunaratne runs 31 runs per game</div>
                                                                                            <div class="rule-text">L Thirimanne runs 29 runs per game</div>
                                                                                            <div class="rule-text">K Mendis runs 33 runs per game</div>
                                                                                            <div class="rule-text">L Malinga wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">S Lakmal wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">J Vandersay wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">T Perera Sixes 1 sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by BAN 245 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of BAN 48 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by BAN 22 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by BAN 4 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of BAN 32 runs per game</div>
                                                                                            <div class="rule-text">T Iqbal runs 34 runs per game</div>
                                                                                            <div class="rule-text">S Sarkar runs 29 runs per game</div>
                                                                                            <div class="rule-text">M Rahim runs 31 runs per game</div>
                                                                                            <div class="rule-text">M Hasan wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">M Rahman wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">M Mortaza wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">T Iqbal Sixes 1 sixes per game</div>
                                                                                            <div class="rule-text text-danger">Total runs by AFG 235 runs per game</div>
                                                                                            <div class="rule-text">First 10 over runs of AFG 46 runs per game</div>
                                                                                            <div class="rule-text">Total Fours by AFG 20 fours per game</div>
                                                                                            <div class="rule-text">Total Sixes by AFG 4 sixes per game</div>
                                                                                            <div class="rule-text">Opening wicket partnership runs of AFG 28 runs per game</div>
                                                                                            <div class="rule-text">R Shah runs 27 runs per game</div>
                                                                                            <div class="rule-text">H Zazai runs 26 runs per game</div>
                                                                                            <div class="rule-text">A Afghan runs Runs per game</div>
                                                                                            <div class="rule-text">M Shahzad runs 27 runs per game</div>
                                                                                            <div class="rule-text">D Zadran wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">Rashid khan wickets 2 wickets per game</div>
                                                                                            <div class="rule-text">Mujeeb ur rahman wickets 1 wickets per game</div>
                                                                                            <div class="rule-text">H Zazai Sixes 1 sixes per game</div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><span tabIndex="0"></span>
                                                </div>
                                            </div>
                                        )}
                                </Popup>
                            </div>
                            <div class="dropdown d-none d-sm-inline-block ml-1 ">
                                <button type="button" class="btn header-item noti-icon"><span class="balance nowrap">Pts:
                                    <span class="balance-value"><b> {this.state.wallet && this.state.wallet.amount ? this.state.wallet.amount : 0}</b></span>

                                </span>
                                </button>
                            </div>
                            <div class="dropdown b-dropdown btn-group" id="__BVID__39">

                                <button aria-haspopup="true" aria-expanded="false" type="button" class="btn dropdown-toggle btn-black header-item" id="__BVID__39__BV_toggle_" onClick={this.user_dropdown.bind(this)}><span class=" ml-1">{this.state.user.name}</span>  <i class="mdi mdi-chevron-down"></i>
                                </button>
                                <ul role="menu" tabIndex="-1" class={dropdown_menu.join(' ')} aria-labelledby="__BVID__39__BV_toggle_">
                                    <div class="dropdown  d-sm-none ml-1 mr-1">
                                        <div class="bal-box"><span class="balance nowrap">Pts:
                                            <span class="balance-value"><b> {this.state.wallet && this.state.wallet.amount ? this.state.wallet.amount : 0}</b></span>

                                        </span>
                                        </div>
                                    </div> <a href="javascript: void(0);" class="dropdown-item d-sm-none"><i class="fas fa-info-circle mr-1"></i> Rules
                                    </a>
                                    <li role="presentation"><a role="menuitem" href="#" target="_self" class="dropdown-item"><a href="/admin/secureauth" class=""><i class="bx bx-lock-open fontSize-16 align-middle mr-1"></i>
                                        Secure Auth
                                    </a></a>
                                    </li> <a href="javascript: void(0);" class="dropdown-item"><i class="bx bx-wallet fontSize-16 align-middle mr-1"></i> Change Password
                                    </a>
                                    <div class="dropdown-divider"></div> <a href="javascript:void(0)" class="dropdown-item text-danger" onClick={() => this.logoutSubmit()}><i class="bx bx-power-off fontSize-16 align-middle mr-1 text-danger"></i> Logout
                                    </a>
                                </ul>
                            </div>



                        </div>
                    </div>
                </header>
                <div id="deposit_model" style={{ position: 'absolute', zIndex: '1040' }}>
                    <div id="__BVID__894" role="dialog" aria-describedby="__BVID__894___BV_modal_body_" class="modal fade hide d-none" aria-modal="true" style={{ display: 'block' }}>
                        <div class="modal-dialog modal-md modal-dialog-scrollable">
                            <span tabIndex="0"></span>
                            <div id="__BVID__894___BV_modal_content_" tabIndex="-1" class="modal-content">
                                <header id="__BVID__894___BV_modal_header_" class={`modal-header ${this.state.wallet_type == 'deposit' ? 'bg-success' : ''} ${this.state.wallet_type == 'withdrawal' ? 'bg-danger' : ''}`}>
                                    <h5 class="modal-title text-uppercase text-white">{this.state.wallet_type}</h5>
                                    <button type="button" data-dismiss="modal" class="close text-white">×</button>
                                </header>
                                <div id="__BVID__894___BV_modal_body_" class="modal-body">

                                    <div class="tabs" id="__BVID__913">

                                        <div class="">
                                            <ul role="tablist" class="nav nav-tabs" id="__BVID__913__BV_tab_controls_">
                                                <li role="presentation" class="nav-item"><a role="tab" aria-selected="true" aria-setsize="1" aria-posinset="1" href="#" target="_self" class="nav-link active tab-bg-success" id="__BVID__914___BV_tab_button__" aria-controls="__BVID__914">{this.state.wallet_type}</a></li>
                                            </ul>
                                        </div>
                                        <div class="tab-content text-muted" id="__BVID__913__BV_tab_container_">
                                            <div role="tabpanel" aria-hidden="false" class="tab-pane active" id="__BVID__914" aria-labelledby="__BVID__914___BV_tab_button__">

                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">{this.state.user.name}</label>
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6"><input placeholder="Amount" type="text" value={this.state.admin_main_amt} readOnly="readOnly" name="userDipositeloginusramount" class="form-control txt-right" /></div>
                                                            <div class="col-6"><input placeholder="Amount" type="text" value={this.state.new_admin_main_amt} readOnly="readOnly" name="userDipositeloginusrNamount" class="form-control txt-right" /></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">{this.state.user_detail.name}</label>
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6"><input placeholder="Amount" type="text" value={this.state.user_main_amt} readOnly="readOnly" name="userDipositeusrnameamount" class="form-control txt-right" /></div>
                                                            <div class="col-6"><input placeholder="Amount" type="text" value={this.state.new_user_main_amt} readOnly="readOnly" name="userDipositeusrnameNamount" class="form-control txt-right" /></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Profit/Loss</label>
                                                    <div class="col-8">
                                                        <div class="row">
                                                            <div class="col-6"><input placeholder="P/L" type="text" value={this.state.deposit} readOnly="readOnly" name="userDipositepl" class="form-control txt-right" /></div>
                                                            <div class="col-6"><input placeholder="P/L" type="text" value={this.state.new_deposit} readOnly="readOnly" name="userDipositeplnew" class="form-control txt-right" /></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Amount</label>
                                                    <div class="col-8 form-group-feedback form-group-feedback-right"><input placeholder="Amount" type="number" required value={this.state.amount} onInput={this.onChangeamount} name="userDipositeamount" class="form-control txt-right" aria-required="true" aria-invalid="false" /></div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Remark</label>
                                                    <div class="col-8 form-group-feedback form-group-feedback-right"><textarea placeholder="Remark" value={this.state.remark} onInput={this.onChangeremark} name="userDipositeremark" class="form-control" aria-required="true" aria-invalid="false"></textarea></div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Transaction Code</label>
                                                    <div class="col-8 form-group-feedback form-group-feedback-right"><input placeholder="Transaction Code" required value={this.state.transation_code} onInput={this.onChangetransactioncode} name="userDipositempassword" type="password" class="form-control" aria-required="true" aria-invalid="false" /></div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-12 text-right">
                                                        <button class={`btn ${this.state.wallet_type == 'deposit' ? 'btn-success' : ''} ${this.state.wallet_type == 'withdrawal' ? 'btn-danger' : ''}`} onClick={() => this.updateWalletHistory()}>
                                                            submit
                                                            <i class="fas fa-sign-in-alt ml-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <span tabIndex="0"></span>
                        </div>
                    </div>
                    <div id="deposit_model_backdrop" class="modal-backdrop d-none"></div>
                </div>
                <div id="more_model" style={{ position: 'absolute', zIndex: '1040' }}>
                    <div id="__BVID__902" role="dialog" aria-describedby="__BVID__902___BV_modal_body_" class="modal fade hide d-none" aria-modal="true" style={{ display: 'block' }}>
                        <div class="modal-dialog modal-xl modal-dialog-scrollable">
                            <span tabindex="0"></span>
                            <div id="__BVID__902___BV_modal_content_" tabindex="-1" class="modal-content">
                                <header id="__BVID__902___BV_modal_header_" class="modal-header bg-primary">
                                    <h5 class="modal-title text-uppercase text-white">{this.state.user_detail.name}</h5>
                                    <button type="button" data-dismiss="modal" class="close text-white">×</button>
                                </header>
                                <div id="__BVID__902___BV_modal_body_" class="modal-body theme-bg">

                                    <div class="tabs" id="__BVID__919">

                                        <div class="">
                                            <ul role="tablist" class="nav nav-tabs" id="__BVID__919__BV_tab_controls_">
                                                <li role="presentation" class="nav-item"><a role="tab" aria-selected="false" aria-setsize="5" aria-posinset="1" href="#" target="_self" class="nav-link active tab-bg-primary" data-id="tab_profile" id="__BVID__920___BV_tab_button__" aria-controls="__BVID__920" tabindex="-1"><span>Profile</span></a></li>
                                                <li role="presentation" class="nav-item"><a role="tab" aria-selected="false" aria-setsize="5" aria-posinset="2" href="#" target="_self" class="nav-link" data-id="tab_changepassword" id="__BVID__922___BV_tab_button__" aria-controls="__BVID__922"><span class="d-inline-block d-sm-none">
                                                    C Pass
                                                </span> <span class="d-none d-sm-inline-block">Change Password</span></a>
                                                </li>
                                                {/* <li role="presentation" class="nav-item"><a role="tab" aria-selected="false" aria-setsize="5" aria-posinset="3" href="#" target="_self" class="nav-link" data-id="tab_profile"  id="__BVID__924___BV_tab_button__" aria-controls="__BVID__924" tabindex="-1"><span class="d-inline-block d-sm-none">
                                                    Lock
                                                </span> <span class="d-none d-sm-inline-block">User lock</span></a>
                                                </li> */}
                                                <li role="presentation" class="nav-item"><a role="tab" aria-selected="false" aria-setsize="5" aria-posinset="4" href="#" target="_self" class="nav-link" data-id="tab_history" id="__BVID__928___BV_tab_button__" aria-controls="__BVID__928" tabindex="-1"><span class="d-inline-block d-sm-none">
                                                    Acc history
                                                </span> <span class="d-none d-sm-inline-block">Account history</span></a>
                                                </li>
                                                <li role="presentation" class="nav-item"><a role="tab" aria-selected="false" aria-setsize="5" aria-posinset="5" href="#" target="_self" class="nav-link" data-id="tab_editprofile" id="__BVID__941___BV_tab_button__" aria-controls="__BVID__941" tabindex="-1"><span class="d-inline-block d-sm-none">
                                                    Edit Profile
                                                </span> <span class="d-none d-sm-inline-block">Edit Profile</span></a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content text-muted" id="__BVID__919__BV_tab_container_">
                                            <div role="tabpanel" aria-hidden="true" class="tab-pane active d-block" id="tab_profile" aria-labelledby="__BVID__920___BV_tab_button__">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="card text-center">
                                                            <div class="card-body p-2">
                                                                <div class="avatar-sm mx-auto mb-1"><span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-16 text-uppercase">R</span></div>
                                                                <h5 class="font-size-15"><a href="javascript: void(0);" class="text-dark">{this.state.user_detail.name}</a></h5>
                                                                <p class="text-muted mb-1">{this.state.user_detail.user_name}</p>
                                                            </div>
                                                            <div class="card-footer bg-transparent border-top">
                                                                <div class="contact-links d-flex font-size-20">
                                                                    <div class="flex-fill"><a title={this.state.user_detail.mobile} href="javascript: void(0);"><i class="bx bx-phone-call"></i></a></div>
                                                                    <div class="flex-fill"><a title={`City: ${this.state.user_detail.city}`} href="javascript: void(0);"><i class="bx bxs-city"></i></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card personalinfo-card">
                                                            <div class="card-body">
                                                                <h4 class="card-title mb-4">Partnership Information</h4>
                                                                <div class="table-responsive mb-0">
                                                                    <table class="table">
                                                                        <tbody>

                                                                            <tr>
                                                                                <th scope="row" class="br-0">Remark:</th>
                                                                                <td class="br-0">Hello</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="card" style={{ minHeight: '390px' }}>
                                                            <div class="card-body">
                                                                <h4 class="card-title mb-4">Additional Information</h4>
                                                                <div class="table-responsive mb-0">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th scope="row" class="br-0">User Name:</th>
                                                                                <td class="br-0">{this.state.user_detail.user_name}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Full Name:</th>
                                                                                <td class="br-0">{this.state.user_detail.name}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Mobile Number:</th>
                                                                                <td class="br-0">{this.state.user_detail.mobile}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row" class="br-0">City:</th>
                                                                                <td class="br-0">{this.state.user_detail.city}</td>
                                                                            </tr>
                                                                            {/* <tr>
                                                                                <th scope="row" class="br-0">Credit Pts:</th>
                                                                                <td class="br-0">1,000.00</td>
                                                                            </tr> */}
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Pts:</th>
                                                                                <td class="br-0"><span>{this.state.wallet.amount}</span></td>
                                                                            </tr>
                                                                            {/* <tr>
                                                                                <th scope="row" class="br-0">Client P/L:</th>
                                                                                <td class="br-0">0.00</td>
                                                                            </tr> */}
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Exposure:</th>
                                                                                <td class="br-0">{this.state.exposure.amount}</td>
                                                                            </tr>
                                                                            {/* <tr>
                                                                                <th scope="row" class="br-0">Casino Pts:</th>
                                                                                <td class="br-0">0.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Sports Pts:</th>
                                                                                <td class="br-0">0.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Third Party Pts:</th>
                                                                                <td class="br-0">0.00</td>
                                                                            </tr> */}
                                                                            <tr>
                                                                                <th scope="row" class="br-0">Created Date :</th>
                                                                                <td class="br-0"><span>{this.state.user_detail.created_at}</span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div role="tabpanel" aria-hidden="false" class="tab-pane d-none" id="tab_changepassword" aria-labelledby="__BVID__922___BV_tab_button__" >
                                                {/* <form data-vv-scope="UserChangePassword"> */}
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Password</label>
                                                    <div class="col-8 form-group-feedback form-group-feedback-right">
                                                        <input placeholder="Password" type="password" onInput={this.onNewPassword} name="userchangepasswordpassword" data-vv-as="Password" class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Confirm Password</label>
                                                    <div class="col-8 form-group-feedback form-group-feedback-right">
                                                        <input placeholder="Confirm Password" type="password" onInput={this.onConfirmPassword} name="userchangepasswordcpassword" data-vv-as="Confirm Password" class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Transaction Code</label>
                                                    <div class="col-8 form-group-feedback form-group-feedback-right">
                                                        <input placeholder="Transaction Code" type="password" onInput={this.onChangetransactioncode} name="userchangepasswordmpassword" data-vv-as="Transaction Code" class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-12 text-right">
                                                        <button type="submit" class="btn btn-primary" onClick={() => this.changeUserPassword()}>
                                                            submit
                                                            <i class="fas fa-sign-in-alt ml-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                {/* </form> */}
                                            </div>

                                            <div role="tabpanel" aria-hidden="true" class="tab-pane d-none" id="tab_history" aria-labelledby="__BVID__928___BV_tab_button__">
                                                <div class="table-responsive">
                                                    <div class="table no-footer table-responsive-sm">
                                                        <table id="eventsListTbl" role="table" aria-busy="false" aria-colcount="5" class="table b-table">

                                                            <thead role="rowgroup" class="">

                                                                <tr role="row" class="">
                                                                    <th role="columnheader" scope="col" aria-colindex="1" class="">
                                                                        <div>S.N.</div>
                                                                    </th>
                                                                    <th role="columnheader" scope="col" aria-colindex="1" class="">
                                                                        <div>Super User</div>
                                                                    </th>
                                                                    <th role="columnheader" scope="col" aria-colindex="2" class="">
                                                                        <div>User</div>
                                                                    </th>
                                                                    <th role="columnheader" scope="col" aria-colindex="3" class="">
                                                                        <div>Transfer From</div>
                                                                    </th>
                                                                    <th role="columnheader" scope="col" aria-colindex="4" class="">
                                                                        <div>Amount</div>
                                                                    </th>
                                                                    <th role="columnheader" scope="col" aria-colindex="5" class="">
                                                                        <div>Date</div>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody role="rowgroup">
                                                                {
                                                                    this.state.accountHistory && this.state.accountHistory.length ?
                                                                        <>
                                                                            {
                                                                                this.state.accountHistory && this.state.accountHistory.length && this.state.accountHistory.map((element, index) => {
                                                                                    return (
                                                                                        <>
                                                                                            <tr role="row" class="b-table-empty-row">
                                                                                                <td role="cell" class="">
                                                                                                    {index + 1}
                                                                                                </td>
                                                                                                <td role="cell" class="">
                                                                                                    {this.state.user.name}
                                                                                                </td>
                                                                                                <td role="cell" class="">
                                                                                                    {element.user_id}
                                                                                                </td>
                                                                                                <td role="cell" class="">
                                                                                                    {element.remark ? element.remark : '-   '}
                                                                                                </td>
                                                                                                <td role="cell" class="">
                                                                                                    {element.amount}
                                                                                                </td>
                                                                                                <td role="cell" class="">
                                                                                                    {element.created_at}
                                                                                                </td>
                                                                                            </tr>
                                                                                        </>
                                                                                    )

                                                                                })
                                                                            }
                                                                        </>
                                                                        :

                                                                        <tr role="row" class="b-table-empty-row">
                                                                            <td colspan="5" role="cell" class="">
                                                                                <div role="alert" aria-live="polite">
                                                                                    <div class="text-center my-2">There are no records to show</div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" aria-hidden="true" class="tab-pane d-none" id="tab_editprofile" aria-labelledby="__BVID__941___BV_tab_button__">
                                                {/* <form data-vv-scope="editprofile" method="post"> */}
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Full Name</label>
                                                    <div class="col-8 form-group-feedback-right pl-0">
                                                        <input placeholder="Full Name" type="text" name="fname" value={this.state.user_detail.name} onInput={this.onChangeName} class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">User Name</label>
                                                    <div class="col-8 form-group-feedback-right pl-0">
                                                        <input placeholder="User Name" type="text" name="user_name" value={this.state.user_detail.user_name} onInput={this.onChangeUserName} class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Email</label>
                                                    <div class="col-8 form-group-feedback-right pl-0">
                                                        <input placeholder="Email" type="text" name="email" value={this.state.user_detail.email} onInput={this.onChangeEmail} class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Mobile</label>
                                                    <div class="col-8 form-group-feedback-right pl-0">
                                                        <input placeholder="Mobile" type="text" name="mobile" value={this.state.user_detail.mobile} onInput={this.onChangeMobile} class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">City</label>
                                                    <div class="col-8 form-group-feedback-right pl-0">
                                                        <input placeholder="City" type="text" name="city" value={this.state.user_detail.city} onInput={this.onChangeCity} class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                {/* <div class="form-group row align-items-center">
                                                    <label class="col-form-label col-4">Change Password Lock</label>
                                                    <div class="mb-1 custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" value="true" id="__BVID__943" /><label class="custom-control-label" for="__BVID__943"></label></div>
                                                </div>
                                                <div class="form-group row align-items-center">
                                                    <label class="col-form-label col-4">Favorite Master</label>
                                                    <div class="mb-1 custom-control custom-switch"><input type="checkbox" class="custom-control-input" value="true" id="__BVID__960" /><label class="custom-control-label" for="__BVID__960"></label></div>
                                                </div> */}
                                                <div class="form-group row">
                                                    <label class="col-form-label col-4">Transaction Code</label>
                                                    <div class="col-8 form-group-feedback-right pl-0">
                                                        <input placeholder="Transaction Code" type="password" name="mpass" onInput={this.onChangetransactioncode} class="form-control" aria-required="true" aria-invalid="false" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-12 text-right">
                                                        <button type="submit" class="btn btn-primary" onClick={() => this.updateUserProfile()}>
                                                            submit
                                                            <i class="fas fa-sign-in-alt ml-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                {/* </form> */}
                                            </div>

                                            {/* <div role="tabpanel" aria-hidden="true" class="tab-pane" id="tab_profile" aria-labelledby="__BVID__924___BV_tab_button__">
                                                <form data-vv-scope="UserLock" method="post">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Bet lock</label>
                                                        <div class="mb-1 custom-control custom-switch"><input type="checkbox" class="custom-control-input" value="true" id="__BVID__926" /><label class="custom-control-label" for="__BVID__926"></label></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">User lock</label>
                                                        <div class="mb-1 custom-control custom-switch"><input type="checkbox" class="custom-control-input" value="true" id="__BVID__927" /><label class="custom-control-label" for="__BVID__927"></label></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-4">Transaction Code</label>
                                                        <div class="col-8 form-group-feedback-right pl-0"><input placeholder="Transaction Code" type="password" name="UserLockMpassword" class="form-control" aria-required="true" aria-invalid="false" /></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-12 text-right"><button type="submit" class="btn btn-primary">
                                                            submit
                                                            <i class="fas fa-sign-in-alt ml-1"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span tabindex="0"></span>
                        </div>
                    </div>
                    <div id="more_model_backdrop" class="modal-backdrop d-none"></div>
                </div>
            </>
        );
    }
}

export default withRouter(AdminHeader);

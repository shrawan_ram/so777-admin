import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Link,
    Route,
    withRouter,
} from "react-router-dom";
import $ from "jquery";
import { ThemeColor } from '../assets/js/customreact';
import { ApiExecute } from "../Api";
import ReactTypingEffect from "react-typing-effect";

class AnimatedHeader extends Component {

    state = {
        topHeaderRemove: '',
    }

    closetopheaderr() {
        this.setState({ topHeaderRemove: 'none' });
        $('body').removeClass('animate-on');
    }

    async componentDidMount() {

    }
    render() {
        return (
            <div className="animated-header" style={{ display: this.state.topHeaderRemove ? 'none' : 'block' }}>
                <section id="animationLogin" className="header-animated-banner">
                    <div>
                        <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-bg.png" alt="" className="bg" />
                        <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-10.png" alt="" />
                        <div className="item">
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-1.png" alt="" />
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-2.png" alt="" />
                        </div>
                        <div className="item">
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-3.png" alt="" />
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-4.png" alt="" />
                        </div>
                        <div className="text">
                            <span>
                                <ReactTypingEffect
                                    cursor=" "
                                    text={["Launched MUFLIS TEENPAT", "Enjoy our New Casino", "Launched RACE to 17"]}
                                />
                            </span>
                        </div>
                        <div className="item">
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-5.png" alt="" />
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-6.png" alt="" />
                        </div>
                        <div className="item">
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-7.png" alt="" />
                            <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-8.png" alt="" />
                        </div>
                        <img src="https://sitethemedata.com/v3/static/front/img/animation/anim-banner-10.png" alt="" />
                    </div>
                </section>
                <span className="fas fa-times" onClick={this.closetopheaderr.bind(this)}></span>
            </div>
        );
    }
}

export default withRouter(AnimatedHeader);

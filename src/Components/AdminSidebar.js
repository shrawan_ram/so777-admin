import React, { Component } from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import { Admin_prifix } from "../configs/costant.config";

class AdminHeader extends Component {
    // constructor(props) {
    //     super(props);
    //     this.props = props;
    // }
    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        user: {}
    }
    logoutSubmit = async () => {
        sessionStorage.removeItem('@token');
        sessionStorage.removeItem('@user');

        this.props.history.push('/');

    }
    sidebar_menu() {
        this.setState({ sidebarAddclass: !this.state.sidebarAddclass });
    }
    user_dropdown() {
        this.setState({ userDropdownAddclass: !this.state.userDropdownAddclass });
    }
    Graphtoggle() {
        this.setState({ GraphtoggleAddclass: !this.state.GraphtoggleAddclass });
    }
    Graphtoggle_b() {
        this.setState({ GraphtoggleAddclass_b: !this.state.GraphtoggleAddclass_b });
    }
    search_input() {
        this.setState({ searchInput: 'multiselect--active' });
        this.setState({ searchBlock: 'block' });
    }
    searchoverlay() {
        this.setState({ searchInput: '' });
        this.setState({ searchBlock: '' })
    }
    toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    } async componentDidMount() {
        let user = sessionStorage.getItem('@user');
        this.setState({
            user: JSON.parse(user)
        });
    }
    render() {
        const { isOpened } = this.state;
        let { Router } = this.props;
        let dropdown_menu = ["dropdown-menu dropdown-menu-right"];
        if (this.state.userDropdownAddclass) {
            dropdown_menu.push('show')
        }
        let multiselect = ["multiselect"];
        if (this.state.searchInput) {
            multiselect.push("multiselect--active");
        }
        var banner = {
            autoplay: true,
            vertical: true,
            loop: true,
            // accessibility: false,
            // draggable: false,
            showsButtons: true,
            arrows: false,
            buttons: false,
        }
        return (
            <div id="sidebar-menu">
                <ul id="side-menu" class="metismenu list-unstyled">
                    <li class="mm-active"><Link to={`/${Admin_prifix}dashboard`} class="side-nav-link-ref router-link-exact-active router-link-active "><i class="bx bx-home-circle"></i> <span>Dashboard</span></Link>
                    </li>
                    {/* <li><a href="/admin/market-analysis" class="side-nav-link-ref"><i class="bx bxs-bar-chart-alt-2"></i> <span>Market Analysis</span></a>
                    </li>
                    <li><Link to="/multi_login_account" class="side-nav-link-ref"><i class="bx bx-user-plus"></i> <span>Multi Login Account</span></Link>
                    </li> */}
                    {
                        this.state.user.role_id == 2
                            ?
                            <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Admins</span></a>
                                <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                    <li><Link to={`/${Admin_prifix}admin`} class="side-nav-link-ref">Admin List</Link>
                                    </li>
                                    <li><Link to={`/${Admin_prifix}add_admin`} class="side-nav-link-ref">Admin Add</Link>
                                    </li>
                                </ul>
                            </li>
                            :

                            this.state.user.role_id == 4
                                ?
                                <li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Users</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}user`} class="side-nav-link-ref">User List</Link>
                                            </li>
                                            <li><Link to={`/${Admin_prifix}add_user`} class="side-nav-link-ref">User Add</Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Slider</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}slider`} class="side-nav-link-ref">Slider List</Link>
                                            </li>
                                            <li><Link to={`/${Admin_prifix}add_slider`} class="side-nav-link-ref">Slider Add</Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Pages</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}page`} class="side-nav-link-ref">Pages List</Link>
                                            </li>
                                            <li><Link to={`/${Admin_prifix}add_page`} class="side-nav-link-ref">Pages Add</Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Category</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}category`} class="side-nav-link-ref">Category List</Link>
                                            </li>
                                            <li><Link to={`/${Admin_prifix}add_category`} class="side-nav-link-ref">Category Add</Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Game Type</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}gametype`} class="side-nav-link-ref">Game Type List</Link>
                                            </li>
                                            <li><Link to={`/${Admin_prifix}add_gametype`} class="side-nav-link-ref">Game Type Add</Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Games</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}game`} class="side-nav-link-ref">Game List</Link>
                                            </li>
                                            <li><Link to={`/${Admin_prifix}add_game`} class="side-nav-link-ref">Game Add</Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Reports</span></a>
                                        <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                            <li><Link to={`/${Admin_prifix}reports/current-bets`} class="side-nav-link-ref">Current Bets</Link>
                                            </li>
                                        </ul>
                                    </li>
                                </li>
                                :
                                <>
                                    {
                                        this.state.user.role_id == 5 ?
                                            <>
                                                <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Settlement</span></a>
                                                    <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                                        <li><Link to={`/${Admin_prifix}settlement/matches/4`} class="side-nav-link-ref">Cricket</Link></li>
                                                        <li><Link to={`/${Admin_prifix}settlement/matches/2`} class="side-nav-link-ref">Tannis</Link></li>
                                                        <li><Link to={`/${Admin_prifix}settlement/matches/1`} class="side-nav-link-ref">Soccer</Link></li>
                                                    </ul>
                                                </li>
                                                <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Settled</span></a>
                                                    <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                                        <li><Link to={`/${Admin_prifix}settled/matches/4`} class="side-nav-link-ref">Cricket</Link></li>
                                                        <li><Link to={`/${Admin_prifix}settled/matches/2`} class="side-nav-link-ref">Tannis</Link></li>
                                                        <li><Link to={`/${Admin_prifix}settled/matches/1`} class="side-nav-link-ref">Soccer</Link></li>
                                                    </ul>
                                                </li>
                                                <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Reports</span></a>
                                                    <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                                        <li><Link to={`/${Admin_prifix}reports/current-bets`} class="side-nav-link-ref">Current Bets</Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </>
                                            :
                                            <>
                                                <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Users</span></a>
                                                    <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                                        <li><Link tto={`/${Admin_prifix}user`} class="side-nav-link-ref">User List</Link>
                                                        </li>
                                                        <li><Link to={`/${Admin_prifix}add_user`} class="side-nav-link-ref">User Add</Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-user-circle"></i> <span>Reports</span></a>
                                                    <ul aria-expanded="false" class="sub-menu mm-collapse" value="5">
                                                        <li><Link to={`/${Admin_prifix}reports/current-bets`} class="side-nav-link-ref">Current Bets</Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </>
                                    }
                                </>
                    }


                    {/* <li><Link to="/bank" class="side-nav-link-ref"><i class="bx bxs-bank"></i> <span>Bank</span></Link>
                    </li> */}
                    {/* <li className="accordion-box"><a href="javascript:void(0);" class="has-arrow" aria-expanded="false"><i class="bx bx-file"></i> <span>Reports</span></a>
                        <ul aria-expanded="false" class="sub-menu mm-collapse">
                            <li><a href="/admin/reports/accountstatement" class="side-nav-link-ref">Account Statement</a>
                            </li>
                            <li><a href="/admin/reports/profitloss" class="side-nav-link-ref">Party Win Loss</a>
                            </li>
                            <li><a href="/admin/reports/currentbets" class="side-nav-link-ref">Current Bets</a>
                            </li>
                            <li><a href="/admin/reports/userhistory" class="side-nav-link-ref">User History</a>
                            </li>
                            <li><a href="/admin/settings/userlock" class="side-nav-link-ref">General Lock</a>
                            </li>
                            <li><a href="/admin/reports/casinoresult" class="side-nav-link-ref">Our Casino Result</a>
                            </li>
                            <li><a href="/admin/reports/livecasinoreport" class="side-nav-link-ref">Live Casino Result</a>
                            </li>
                            <li><a href="/admin/reports/turnover" class="side-nav-link-ref">Turn Over</a>
                            </li>
                            <li><a href="/admin/reports/authlist" class="side-nav-link-ref">User Authentication</a>
                            </li>
                        </ul>
                    </li>
                    <li><Link to="/our_casino" class="side-nav-link-ref"><span class="badge badge-pill badge-success float-right">New</span> <i class="mdi mdi-cards-playing-outline"></i> <span>Our Casino</span></Link>
                    </li>
                    <li id="event-tree" class="menu-box accordion-box"><a href="javascript: void(0);" class="has-arrow" aria-expanded="false"><i class="bx bxs-calendar-event"></i> <span>Events</span></a>
                        <ul class="sub-menu mm-collapse">
                            <li class="accordion-box"><a href="javascript:void(0)" class="has-arrow sport4"><span>Cricket</span> <span> (12)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>One Day Internationals</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/EpunR00Sw1ttcIE6PZUoig==" class="side-nav-link-ref"><span>Sri Lanka v India</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/fj7AuWV++5OTx7cJwqOnDQ==" class="side-nav-link-ref"><span>Zimbabwe v Bangladesh</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>T20 Blast</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/Hn5o51Kg0hnhKMZ0TDQBLg==" class="side-nav-link-ref"><span>Lancashire v Yorkshire</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>T5 XI</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/virtualcricket/SeK7puKGhm+IDlF%2FzygDVg==/PxQd9tcFLC98jC7xYtCseA==" class="side-nav-link-ref"><span>Hyderabad XI Vs Delhi XI</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/virtualcricket/SeK7puKGhm+IDlF%2FzygDVg==/9cNsJ+a3VwIyJYZh1vf2Tw==" class="side-nav-link-ref"><span>Mumbai XI Vs Kolkata XI</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>International Twenty20 Matches</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/8Q9R7MhfuEjoQoEJdb3iPA==" class="side-nav-link-ref"><span>England v Pakistan</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/KVsiJw3Yy+Zqewx660UsHA==" class="side-nav-link-ref"><span>Ireland v South Africa</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>Indian Premier League</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/weGz4WU2w%2FYDCH1iVVH5Ng==" class="side-nav-link-ref"><span>Indian Premier League</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>T10 XI</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/virtualcricket/SeK7puKGhm+IDlF%2FzygDVg==/b7rWfJhJM%2F6FWH6kr8Rgpg==" class="side-nav-link-ref"><span>JT XI Vs GAW XI</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/virtualcricket/SeK7puKGhm+IDlF%2FzygDVg==/Q+N5gzaOhfrLpTUzaf2D4w==" class="side-nav-link-ref"><span>JT XI Vs SNP XI</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>Testing Match</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/npupYuU6HzWQDqyks7yaMQ==" class="side-nav-link-ref"><span>Testing A v Testing B</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>Womens One Day Internationals</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/SeK7puKGhm+IDlF%2FzygDVg==/WjdBs9mDrPyIFOXq5SrqIA==" class="side-nav-link-ref"><span>West Indies Women v Pakistan Women</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="accordion-box"><a href="javascript:void(0)" class="has-arrow sport2"><span>Tennis</span> <span> (38)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHALLENGER MEN - SINGLES Todi (Italy)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/5rijayzVDD6PpfUUeU%2FAlw==" class="side-nav-link-ref"><span>Gaio v To Etcheverry</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/4%2FD26UIxZPLTAAnrdVV%2Fiw==" class="side-nav-link-ref"><span>Pucinelli De Almeida v Vilella Martinez</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ATP - SINGLES Gstaad (Switzerland) - Qualification</span> <span> (6)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/GtlJNVWS5AI26sFU1xdhgw==" class="side-nav-link-ref"><span>Couacaud v Sakamoto</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/sKnPwQsbupZfWLxTX3K6RQ==" class="side-nav-link-ref"><span>Diez v Da Wenger</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/4uh+0WbU6RSVktHLd4pTxA==" class="side-nav-link-ref"><span>Kacp Zuk v Vavassori</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/j0e5b6okmMPMAfMf%2FeExEg==" class="side-nav-link-ref"><span>Kopriva v Marterer</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/y0fa4bfOntZx%2FtmcOFbvTg==" class="side-nav-link-ref"><span>Otte v Pellegrino</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/ux1kaYZRtnkXNUlKA5QogA==" class="side-nav-link-ref"><span>Ziz Bergs v Vukic</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>WTA - SINGLES Budapest (Hungary)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/JfAuO6Vw1eZMq83xVnc+SA==" class="side-nav-link-ref"><span>Kalinina v Collins</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/MyqTOJYkltNHHC%2FnemZ7sw==" class="side-nav-link-ref"><span>Putintseva v Galfi</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>WTA - SINGLES Gdynia (Poland) - Qualification</span> <span> (7)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/fOhCbbFHjEAQrg1Apj2GDw==" class="side-nav-link-ref"><span>Kater Bondarenko v Zakharova</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/+x5JDuLCOrd0yKuDFrB0iQ==" class="side-nav-link-ref"><span>Mar Kubka v Gorgodze</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/oTKSM6fJqfeKy1c5nLbLAg==" class="side-nav-link-ref"><span>Mari Melnikova v Danilina</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/hsvy0H4IlL0S+St82vWqXw==" class="side-nav-link-ref"><span>Perrin v Bondar</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/nsRgd6MqnvwAHA%2F2ihOmIw==" class="side-nav-link-ref"><span>Talaba v Di Sarra</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/4w4PMIxg2x7r%2F+Q+m0XdTw==" class="side-nav-link-ref"><span>Var Flink v Mrdeza</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/PwnZLRBuP18n30P18a98FQ==" class="side-nav-link-ref"><span>Wer Falkowska v Loeb</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ATP - SINGLES Newport (USA)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/d1eVbUA2Rf%2Fgyf4c%2F4v0xA==" class="side-nav-link-ref"><span>Brooksby v Thompson</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/ynRG5HACcLZUAz0IlvVn4Q==" class="side-nav-link-ref"><span>Bublik v Anderson</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>WTA - SINGLES Lausanne(Switzerland)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/V342UQhIKiXQTVPpHYI+OQ==" class="side-nav-link-ref"><span>Garcia v Burel</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/PjpO6RelgwD35q1VXPJWqA==" class="side-nav-link-ref"><span>Zidansek v Zanevska</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHALLENGER MEN - SINGLES Iasi (Romania)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/hHEeiFSTzm6x01YGpYzqaA==" class="side-nav-link-ref"><span>Hug Gaston v Meligeni Rodrigues Alve</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/v46g6Ea7Ig1skX5sna3XBw==" class="side-nav-link-ref"><span>Zekic v Kolar</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ATP - SINGLES Hamburg (Germany)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/f8XfTImJVBp9VEbY%2FFo1Lg==" class="side-nav-link-ref"><span>Delbonis v Carreno Busta</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/ir4aCtyJ4sbcSbEruMO3Cw==" class="side-nav-link-ref"><span>Krajinovic v Djere</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ATP - SINGLES Bastad (Sweden)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/n7HSuVDJAsQIx2j+QPLPKw==" class="side-nav-link-ref"><span>Hanfmann v Coria</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/3swl59taRby8FYQfgdAB1Q==" class="side-nav-link-ref"><span>Ruud v Carballes Baena</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHALLENGER MEN - DOUBLES Amersfoort (Netherlands)</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/XYSH4s9OwAT2akPPUQpZLw==" class="side-nav-link-ref"><span>Galdos/Oliveira - Castelnuovo L/Guinard M</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>WTA - SINGLES Prague Open (Czech Republic)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/+HpF%2Fq7a7s6UN9RTCsAuvw==" class="side-nav-link-ref"><span>Martincova v Minnen</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/GYtoV7FVBh%2FKbfzYgBf2Bg==" class="side-nav-link-ref"><span>Xin Wang v Krejcikova</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ATP - DOUBLES Bastad (Sweden)</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/8Xm08iJjKBmdMEh9GF%2FqFA==" class="side-nav-link-ref"><span>Cuevas/Martin - S.Arends/D.Pel</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHALLENGER MEN - SINGLES Amersfoort (Netherlands)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/uNL1WuU8NTl4SUeVzHMCeg==" class="side-nav-link-ref"><span>Andreozzi v Van de Zandschulp</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/1OoTL6iferxQWs%2FWeI2uPg==" class="side-nav-link-ref"><span>T Griekspoor v Hoang</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHALLENGER MEN - SINGLES Nur-Sultan 3 (Kazakhstan)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/Imn2iNerRdd6IYQy0ZYHCg==" class="side-nav-link-ref"><span>Clarke v B Gojo</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/i+UCPCa+vC1Ys+stWqqW2A==" class="side-nav-link-ref"><span>Polansky v Purcell</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ATP - DOUBLES Hamburg (Germany)</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/bYdVkxhMuZ3y+WfeTU752A==" class="side-nav-link-ref"><span>K.Krawietz/H.Tecau - J. Cerretani/H. Hach Verdugo</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHALLENGER MEN - DOUBLES Iasi (Romania)</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/K2tSlNx+Q7556EXln8xsFg==" class="side-nav-link-ref"><span>O. LUZ/F. Meligeni Rodrigues Alves - Casanova/Orte</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>UTR Pro Tennis Series Atlanta</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/ZMsVdzACxXZhtFWzJX9BDA==/vOwPsKt9vAoLbyMKN35RIg==" class="side-nav-link-ref"><span>Lukas Greif - Aidan Kim</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport1"><span>Football</span> <span> (237)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>SCOTLAND League Cup</span> <span> (14)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Wo4dPKDbBhbVFpZfwqKAGA==" class="side-nav-link-ref"><span>Albion Rovers - Edinburgh City</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/6+r179Mb3WCpC2xl4c1yLQ==" class="side-nav-link-ref"><span>Alloa Athletic - Livingston</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/rG36TkCN6bbcosFmBIoxig==" class="side-nav-link-ref"><span>Brora Rangers - Forfar Athletic</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/cK%2FkWKV1f%2FuyjpgqYWnX1g==" class="side-nav-link-ref"><span>Clyde - Kilmarnock</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/hnPfEW+0tsls%2FC9s2YX3Ow==" class="side-nav-link-ref"><span>Cowdenbeath - Brechin City</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0n4067os831rk8sFXP4eKg==" class="side-nav-link-ref"><span>Dundee United - Arbroath</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/8Ro63EEPW05ihr6+ZdCSrQ==" class="side-nav-link-ref"><span>Dunfermline Athletic - Dumbarton</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/thG+77QsixydtIcSz9SEmw==" class="side-nav-link-ref"><span>Falkirk - Hamilton</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PMqX9lTGYRfqDkcSBV0pBg==" class="side-nav-link-ref"><span>Kelty Hearts - East Fife</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/tZfyHHotL1OVVD4HyAuzVQ==" class="side-nav-link-ref"><span>Motherwell - Queen of the South</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ykLO3rdrF5Nteq5u%2FMim4Q==" class="side-nav-link-ref"><span>Peterhead - Cove Rangers</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ASeyCnb90d4mljwh3Z3aMw==" class="side-nav-link-ref"><span>Queens Park - Airdrieonians</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/VU7+vwJSsT55WBKMenSypg==" class="side-nav-link-ref"><span>Stenhousemuir - Partick Thistle</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/5gguUgnwVba80vWXkqUYzQ==" class="side-nav-link-ref"><span>Stranraer - East Kilbride</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ARGENTINA Torneo Federal</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ALljWpdMjWb6qqaO6Mq40A==" class="side-nav-link-ref"><span>Douglas Haig - Crucero del Norte</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/7vQPgTxz5yTIcW6elKMs9w==" class="side-nav-link-ref"><span>Ferro Carril Oeste General Pico - CA Juventud Unid</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>GHANA Premier League</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/troFbTLVRbEtG2KwLiVSMQ==" class="side-nav-link-ref"><span>A. Ashanti Gold - Inter Allies</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/9yNlus7umok3ZqeUgFZkmw==" class="side-nav-link-ref"><span>Great Olympics - Dreams FC</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/x2q9Ps1at30Rvw+dkiIgYw==" class="side-nav-link-ref"><span>WAFA - Hearts of Oak</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ARGENTINA Primera Nacional</span> <span> (5)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/5w2lQnScxKsx5p2cPrf6Bg==" class="side-nav-link-ref"><span>All Boys - Guillermo Brown</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/UEsnoskZY2bCis8R5cMKjg==" class="side-nav-link-ref"><span>Barracas Central - Villa Dálmine</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/UW9dertyesWHSNewia0XNQ==" class="side-nav-link-ref"><span>Gimnasia y Esgrima de Jujuy - Defensores de Belgra</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/9a0YxJGbF%2FtEbqXW6rVbQw==" class="side-nav-link-ref"><span>Independiente Rivadavia - San Martín de San Juan</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/jg0xO0xBNwz8C3VGNUnqGg==" class="side-nav-link-ref"><span>Tristán Suárez - Santamarina</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ARGENTINA Primera D</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/eoFSEhMPyWrtkrWHZzhzIw==" class="side-nav-link-ref"><span>Centro Español - Deportivo Paraguayo</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/uITJd91NVVcJo6fN%2Fr8fag==" class="side-nav-link-ref"><span>Club Social y Deportivo Liniers - Sportivo Barraca</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AUSTRALIA NPL Queensland</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/J2XggMzaSVz8k2SSBfJ6iA==" class="side-nav-link-ref"><span>Eastern Suburbs - Gold Coast United</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/jdnqXQft40zfxcrFb1hFaw==" class="side-nav-link-ref"><span>Peninsula Power - Brisbane Olympic</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/JdJU6D1hSI28wFx83ZgV3A==" class="side-nav-link-ref"><span>Queensland Lions - Logan Lightning</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>US Major League Soccer</span> <span> (12)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0rAwSsxvqVYuI4A5aJmQAQ==" class="side-nav-link-ref"><span>Atlanta Utd v New England</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/8v%2FSMj63e63Wvf5P1G3bAQ==" class="side-nav-link-ref"><span>Colorado v San Jose Earthquakes</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/2ENFVaGVKHinKCDQXIAfYg==" class="side-nav-link-ref"><span>Columbus v New York City</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Tpkvgwx8hRjIoLgrgE2r%2Fg==" class="side-nav-link-ref"><span>Los Angeles FC v Real Salt Lake</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Vdk15V+NN97V8JJIQ8TJkQ==" class="side-nav-link-ref"><span>Minnesota Utd v Seattle Sounders</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/KJsfqGZFh10+h4f%2FX+QUGw==" class="side-nav-link-ref"><span>Montreal Impact v FC Cincinnati</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/wXp8rh3fbTDWoOODBR0z3A==" class="side-nav-link-ref"><span>Nashville SC v Chicago Fire</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/FtJhkyxFfJYv8eLbqMlj7g==" class="side-nav-link-ref"><span>New York Red Bulls v Inter Miami CF</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/5gR9FmG59YMGjm94FxflpQ==" class="side-nav-link-ref"><span>Philadelphia v DC Utd</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/gYNw2O99sFzgHVldMtsGVg==" class="side-nav-link-ref"><span>Portland Timbers v FC Dallas</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/TO3iPUGxshHGBeWdDqFNGA==" class="side-nav-link-ref"><span>Toronto FC v Orlando City</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/+9FjL9pVTSMafzUKK3ybZg==" class="side-nav-link-ref"><span>Vancouver Whitecaps v LA Galaxy</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>JAPAN J2 League</span> <span> (7)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/O7hwneHBhiyzIaHy1bJV0g==" class="side-nav-link-ref"><span>Jef United Chiba - Zwiegen Kanazawa</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0Zg8wJ6DIVz0YTnhvls9Sg==" class="side-nav-link-ref"><span>Jubilo Iwata - Montedio Yamagata</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/kYz4uH60VqrhveEsQF6Z4A==" class="side-nav-link-ref"><span>Kyoto Sanga FC - Albirex Niigata FC</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/7sbbNPCKRXFfKxJYLbrBRA==" class="side-nav-link-ref"><span>Matsumoto Yamaga FC - Mito Hollyhock</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/nR4f6sKOPQ%2FoZthKLyYM7g==" class="side-nav-link-ref"><span>Tochigi SC - Ventforet Kofu</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/N%2F3zTEXyyS65khxQslL2GA==" class="side-nav-link-ref"><span>V-Varen Nagasaki - Giravanz Kitakyushu</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/iqpkBCx4XjNji%2F02Q1RwfQ==" class="side-nav-link-ref"><span>Yamaguchi FC - Blaublitz Akita</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ARGENTINA Primera B</span> <span> (7)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/1BNLcMktDV4Ug4eHqo8qCA==" class="side-nav-link-ref"><span>Acassuso - Defensores Unidos</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/qQZhRNC6nN4ELsvMCEUNqw==" class="side-nav-link-ref"><span>Argentino de Quilmes - Los Andes</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/B9W1UrsPCJT6sJu%2F+xMB7A==" class="side-nav-link-ref"><span>Cañuelas - Villa San Carlos</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/46QUSIVl2AMts2US9Qxb9Q==" class="side-nav-link-ref"><span>Deportivo Merlo - Comunicaciones</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/c04y%2FWSbQh4Nq05fHoErYg==" class="side-nav-link-ref"><span>Justo José Urquiza - Colegiales</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ydO7mn1MtEiB9h8PBkS2nw==" class="side-nav-link-ref"><span>Sacachispas - CA Fénix</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/rNt7RqhHTmgErUTWIq%2FZXg==" class="side-nav-link-ref"><span>San Miguel - UAI Urquiza</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Serie B</span> <span> (7)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/j96SNYQRlr%2FTb6QhqkUGuQ==" class="side-nav-link-ref"><span>Brasil de Pelotas v EC Vitoria Salvador</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Y66GU%2FBn7CQfiKQjqdv2Gg==" class="side-nav-link-ref"><span>Brusque FC v Botafogo</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/jHuFt9k+wFv2+Z9qulw9dg==" class="side-nav-link-ref"><span>Cruzeiro MG v Avai</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/aJLllGxeAum63XG0RQwjQQ==" class="side-nav-link-ref"><span>Goias v Londrina</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/tZKhJwuLY4H3rvoiIZbROA==" class="side-nav-link-ref"><span>Operario PR v CSA</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/HjdRNN%2F3PEeVoyyVzeQwyQ==" class="side-nav-link-ref"><span>Ponte Preta v Remo</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/mGd+W76SxpNtegR8CyDI4Q==" class="side-nav-link-ref"><span>Sampaio Correa FC v Coritiba</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>NORTH &amp; CENTRAL AMERICA Gold Cup</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/LCPd2lbkSMRPLZLS1pWSfg==" class="side-nav-link-ref"><span>Grenada v Qatar</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4YoGgXSqZZEkM5aDRK4X4Q==" class="side-nav-link-ref"><span>Panama v Honduras</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/kVz%2F3giLHVI+1Zs55rS1PA==" class="side-nav-link-ref"><span>Suriname v Costa Rica</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ALGERIA Ligue 1</span> <span> (6)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/BswokMUsM%2FMMH6zS9gijrA==" class="side-nav-link-ref"><span>Aïn Mlila - USM Alger</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/k1HANkMPWHjADdHTnLHMUQ==" class="side-nav-link-ref"><span>ASO Chlef - MC Alger</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/3CFHlV4SidG7aaGWRA7dqg==" class="side-nav-link-ref"><span>Biskra - MC Oran</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Od3z+bi1ESamCnzH7sHBMA==" class="side-nav-link-ref"><span>JSM Skikda - RC Relizane</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/j98lvydnK9m1EjcSJlT9oQ==" class="side-nav-link-ref"><span>NC Magra - CA Bordj Bou Arreridj</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/R7llOuZjtF30sfhdC9Gbpw==" class="side-nav-link-ref"><span>Paradou AC - USM Bel Abbès</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>NORWAY: Division 2</span> <span> (9)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ymdy%2Fm4oR2f99sD4kmTCCA==" class="side-nav-link-ref"><span>Alta - Brattvåg</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/LbRT57HJChrqGTms2fXzBg==" class="side-nav-link-ref"><span>Florø - Frøya</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/1Np27u0tVyvQoKgPLcIg2Q==" class="side-nav-link-ref"><span>Hald - Eidsvold TF</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/+dLfuiRu2d0agRcyAhmaMw==" class="side-nav-link-ref"><span>Kjelsas Fotball - Egersund</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/wbvLiw2VNlY5pR9mjC00EA==" class="side-nav-link-ref"><span>Øygarden - Flekkerøy</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/qnjx+WtPRVqNX6TsXMIfzw==" class="side-nav-link-ref"><span>Senja - Bærum</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/6LB8QIzuw8cehquoS5RyPA==" class="side-nav-link-ref"><span>Skeid Fotball - Fram Larvik</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/R4aw1qTCmSJImpekEhv2Mg==" class="side-nav-link-ref"><span>Tromsdalen - KIL Toppfotball</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/OJ5bUg8wcJBxpAMjoi+mmw==" class="side-nav-link-ref"><span>Vard Haugesund - Levanger</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Serie D</span> <span> (11)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/O+MGizKGrie6NuKRFpKUuQ==" class="side-nav-link-ref"><span>América RN - Sousa</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/h18QexRBmxP3eX24lxWJrw==" class="side-nav-link-ref"><span>Aparecidense - União Rondonópolis</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/vC9YLPQNysDbDSY69GXt2w==" class="side-nav-link-ref"><span>Campinense - ABC</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PE9SbraHEaEd%2Ft0EbrvwlA==" class="side-nav-link-ref"><span>Cianorte - São Bento</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/5vBahVqLAMsAhY6qIfkZ9g==" class="side-nav-link-ref"><span>Guarany de Sobral - Juventude MA</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0KGvavbO9H%2FjA+yW%2F5xhSw==" class="side-nav-link-ref"><span>Juazeirense - Murici</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/EqgQb9kH4pdUnatUpwzvtQ==" class="side-nav-link-ref"><span>Madureira - Santo André</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/a7gtcLPO0JX7XTT9EhdBfg==" class="side-nav-link-ref"><span>Moto club de São Luiz - Palmas</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/vCAotuFmgg6MZRDRCjls8Q==" class="side-nav-link-ref"><span>SE do Gama - Goianésia</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/WCHf3aWscvnRe6QAuIZ7%2Fw==" class="side-nav-link-ref"><span>Tocantinópolis - Imperatriz</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/qgFrErE+8BDvZWXi+lQ4SQ==" class="side-nav-link-ref"><span>Ypiranga AP - Galvez EC</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CROATIA 1. HNL</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/nXkVeTBDQUfjlFVPRYJw2A==" class="side-nav-link-ref"><span>Lokomotiva v Hajduk Split</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/QLWLMkoL+ssIJpZZRL%2FwpQ==" class="side-nav-link-ref"><span>Rijeka v HNK Gorica</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LITHUANIA A Lyga</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/D0OYM5aL6LUjvkzZ7V9Qgw==" class="side-nav-link-ref"><span>Hegelmann Litauen - FK Nevezis</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>SWEDEN Superettan</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/xLS9n5vAoZORdtrxk%2FuuMw==" class="side-nav-link-ref"><span>Akropolis IF v Norrby IF</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/RmrrxXnVrwxA0vfZAE0jDg==" class="side-nav-link-ref"><span>GAIS v IK Brage</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/5OL45v%2Fo2Muirt4P+MwRGQ==" class="side-nav-link-ref"><span>Landskrona v Trelleborgs</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/HiFZ6mJKEShed6ABabwRUw==" class="side-nav-link-ref"><span>Varnamo v Osters</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ARGENTINA Liga Profesional</span> <span> (5)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/AD9cgM8qbOhjT+yMWS46lw==" class="side-nav-link-ref"><span>Aldosivi v Patronato</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/guTE2pg297YpegeUh6S%2FGw==" class="side-nav-link-ref"><span>Gimnasia La Plata v CA Platense</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/t5W2Uz+JXooyfOtjYtQ%2FBA==" class="side-nav-link-ref"><span>Lanus v Atl Tucuman</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/oZLAxzmZFoNsqku8YSzDew==" class="side-nav-link-ref"><span>Newells v Talleres</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/sKRUfOUUsLwtb30P33Y8BQ==" class="side-nav-link-ref"><span>Velez Sarsfield v Racing Club</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Serie C</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/2czCE1LZUNfnYF%2FDcUWXAQ==" class="side-nav-link-ref"><span>Jacuipense - Botafogo PB</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/TTs9IYJmLse+qilz9oxF5w==" class="side-nav-link-ref"><span>Mirassol - Criciúma</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHINA Jia League</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/WOjJzBSRBEn+D%2FseCt8CFw==" class="side-nav-link-ref"><span>Kunshan FC - Zibo Cuju</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0M82D1yBG6OuAM1aeTKY+g==" class="side-nav-link-ref"><span>Shenyang Urban FC - Chengdu Better City</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>SWEDEN  Allsvenskan</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/FIf6XsQs5d7IFFZg0BLVag==" class="side-nav-link-ref"><span>Degerfors v Malmo FF</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/X7FMizaNeKrZ1S%2Fx2%2FrbIQ==" class="side-nav-link-ref"><span>Elfsborg v Ostersunds FK</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ESTONIA Meistriliiga Women</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/uU23A7iMJHPjaCjpbrh4lQ==" class="side-nav-link-ref"><span>Saku Sporting W - Flora W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/+%2FXGMnBFHHpkDCoMq8Vb3A==" class="side-nav-link-ref"><span>Tammeka W - Tallinna Kalev W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AUSTRIA OFB Cup</span> <span> (5)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/fYb6VlFiRQXoNr9ZJwJEeQ==" class="side-nav-link-ref"><span>ASV Siegendorf - St. Jakob</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/8jkc5vZ7cbGrWigxlCZFGg==" class="side-nav-link-ref"><span>FC Wels - SC Austria Lustenau</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4063cQAZatTqqC2Njm2wHg==" class="side-nav-link-ref"><span>SC Bregenz - Union Vöcklamarkt</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/EjQ9conG2GmGWA19yZTjNQ==" class="side-nav-link-ref"><span>TWL Elektra - FC Gleisdorf 09</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/2N%2FlWQPFDNW9QPYPDItD5w==" class="side-nav-link-ref"><span>Union Gurten - FC Lustenau</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>NORWAY Toppserien Women</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/toEKFxEXapK%2FPfDNM%2FIBgQ==" class="side-nav-link-ref"><span>Kolbotn W - Klepp W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/LXof1541r9XmU6%2Fv0BE5Zg==" class="side-nav-link-ref"><span>Rosenborg W - Sandviken W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Serie A</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4VlJU8dkH7CMm2dX4IJ1+w==" class="side-nav-link-ref"><span>Ceara SC Fortaleza v Athletico-PR</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/keB4PqaILO+mAMR3nIpbMQ==" class="side-nav-link-ref"><span>Corinthians v Atletico MG</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/O+fKhpgSx2%2FvUV4XdCP+Hg==" class="side-nav-link-ref"><span>Fluminense v Gremio</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PnvPXllyy3uTqHqzt5iwyQ==" class="side-nav-link-ref"><span>Sao Paulo v Fortaleza EC</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AUSTRALIA NPL South Australian</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/B0ApM6LcEOXwuEj4Xq4jsw==" class="side-nav-link-ref"><span>Adelaide Blue Eagles - Adelaide City</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Rn7kRIr5KKNXzDot3pkPCA==" class="side-nav-link-ref"><span>Cumberland United FC - North Eastern MetroStars</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/VUAZdU963UjSEEaQsSCc8g==" class="side-nav-link-ref"><span>South Adelaide Panthers - Adelaide Olympic FC</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>WORLD Club Friendly</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/%2FA66Q%2FyaRhSRmqD4nZdeuQ==" class="side-nav-link-ref"><span>Braintree Town - Dagenham and Redbridge</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/x29L06r2yPYt7BJLy27VHA==" class="side-nav-link-ref"><span>Ceglédi VSE - Érdi VSE</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Vu1l4Ksa5hMr5Y7rDf9G9g==" class="side-nav-link-ref"><span>Chorley - Morecambe</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ARGENTINA Primera C</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/KTlP6ldxJVkKMzhSP36lQw==" class="side-nav-link-ref"><span>Dock Sud - Ituzaingó</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/rbHMN3oZQ0qblB%2FzRNOi7g==" class="side-nav-link-ref"><span>Luján - El Porvenir</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/2+EanRTqbQPoSSV9+C+PzA==" class="side-nav-link-ref"><span>Victoriano Arenas - CA Central Cordoba de Rosario</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>NORWAY OBOS-ligaen</span> <span> (7)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/OZj3+b8GVB%2FG1rRFuVpVdg==" class="side-nav-link-ref"><span>Aalesund FK - Jerv</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/SY08doni1tRsA0XK7XOE2A==" class="side-nav-link-ref"><span>Asane Fotball - Ranheim</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/en2874iO%2FLc44dlCpdLbEA==" class="side-nav-link-ref"><span>IK Start - Raufoss</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/wVMkIj8PDdgjQy76bH4Cgw==" class="side-nav-link-ref"><span>Sandnes Ulf - HamKam</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ox3kARTDyfWO+iKborVO+w==" class="side-nav-link-ref"><span>Sogndal - Grorud</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4cdjp8QDpfNu5Xp2zCOHdA==" class="side-nav-link-ref"><span>Stjørdals Blink - KFUM Fotball Oslo</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/mSm05F2zL%2FCXdVZSFxDUQA==" class="side-nav-link-ref"><span>Ullensaker/Kisa - Strømmen</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>FINLAND Veikkausliiga</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/6mwRTN7KWfLok7vz9YI3%2Fg==" class="side-nav-link-ref"><span>AC Oulu - HJK</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LATVIA Optibet Virsliga</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/sAMzfyqBAKChveyZM3onPA==" class="side-nav-link-ref"><span>BFC Daugavpils - FK Metta</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/MBZIW3N%2F8jbDQs39iA1RkQ==" class="side-nav-link-ref"><span>FK Spartaks Jurmala - Riga FC</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>KENYA Premier League</span> <span> (5)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PBVcrHpmJnEAvnqFsGtaXA==" class="side-nav-link-ref"><span>Kariobangi Sharks - Vihiga United</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/LVCw+oFD2112fdWWUf%2FE6A==" class="side-nav-link-ref"><span>Nairobi City Stars - Gor Mahia</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Brn13Cs3rlSF1oPGieaB4Q==" class="side-nav-link-ref"><span>Nzoia Sugar - Kakamega Homeboyz</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/hvVuE8XzXeLSMwJt8qrX0w==" class="side-nav-link-ref"><span>Sofapaka Nairobi - Posta Rangers</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/kgjpM3unXUoUxhQUIc5tSQ==" class="side-nav-link-ref"><span>Wazito - KCB Nairobi</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>FINLAND Kakkonen</span> <span> (9)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/v1RrkuXieOXBVktWXxAKtA==" class="side-nav-link-ref"><span>GBK Kokkola - FCV</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/wEX+QWtep3NciIq%2FfrWTsg==" class="side-nav-link-ref"><span>MiPK - FC Kiffen</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/il7OfmQwVj4HldXIbdFcaA==" class="side-nav-link-ref"><span>NJS Nurmijarvi - JäPS</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/wJMO5qWPknplVKfeueIDmA==" class="side-nav-link-ref"><span>OLS - JS Hercules</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/NCZ4tGbw5VbGi08AFBCg5A==" class="side-nav-link-ref"><span>PEPO - KaPa</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/UwDnpffrnm4InoyAJqtggQ==" class="side-nav-link-ref"><span>PK K-U - SC KuFu-98</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/KFR8p804r8AOAqAth9sONg==" class="side-nav-link-ref"><span>PS Kemi - JJK</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/qEheVstucrOaJQDTeZjCPQ==" class="side-nav-link-ref"><span>Reipas - PeKa</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0q3i9tRCp99eN4nLDmhImw==" class="side-nav-link-ref"><span>VIFK - Kraft</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>SLOVENIA Prva liga</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Qu6ELBh1mqzvxfyHEZKeww==" class="side-nav-link-ref"><span>Mura - Tabor Sežana</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/1X0th+hqnlKiB+AnJFFxMA==" class="side-nav-link-ref"><span>NK Aluminij - Luka Koper</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>MALAWI  Super League</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/yNP3AV7fJMb5xPCYPgQ6sg==" class="side-nav-link-ref"><span>Chitipa United - Blue Eagles FC</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/RCnpMyX5YTmDBaP4dQbXWw==" class="side-nav-link-ref"><span>Red Lions (LBR) - Ntopwa</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>GERMANY Regionalliga Bayern</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/JoyZIYTJ8HXvHFwmef5Taw==" class="side-nav-link-ref"><span>1. FC Nürnberg Am. - FC Pipinsried</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Xyz%2FaQcfx2yLwj5QI524Cg==" class="side-nav-link-ref"><span>Augsburg II - Bayern II</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/sEa%2FE4F0RCPU8hhzV5GkNA==" class="side-nav-link-ref"><span>TSV Buchbach - 1. FC Schweinfurt 05</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ELITE : Friendlies</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/QBIjbUp+OP+UMAkkPcrbZQ==" class="side-nav-link-ref"><span>Bayern Munich v FC Koln</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/+2HYkjkexz1F8sKmPjaa4Q==" class="side-nav-link-ref"><span>Paderborn v Mgladbach</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/gwXvSSR9bimiMAODCIG+tg==" class="side-nav-link-ref"><span>Rangers v Arsenal</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/i2nFNPr+IHsLQezhhsvFdA==" class="side-nav-link-ref"><span>RB Leipzig v Az Alkmaar</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Campeonato Carioca 2</span> <span> (5)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/2E%2FA6lDRv8fTuoNmjOk0Nw==" class="side-nav-link-ref"><span>Americano RJ - CFRJ Marica</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/HJP+i%2F0Sq6HyIEDhhttQpg==" class="side-nav-link-ref"><span>Artsul - Audax Rio</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/r1Ccs7Kt4IdudHmT2UcCtw==" class="side-nav-link-ref"><span>Cabofriense - America RJ</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/CcBoAL1GExt%2FGT4gMEsgtQ==" class="side-nav-link-ref"><span>Friburguense - Angra dos Reis</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/l1rYarzF1ITCnoxyYaqhjA==" class="side-nav-link-ref"><span>Gonçalense - Sampaio Corrêa RJ</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>RUSSIA Supreme Division Women</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PLV77SDKMlKyINc7OR%2FZ3w==" class="side-nav-link-ref"><span>FC Ryazan-VDV W - FK Kubanochka Krasnodar W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/z0CpE43Op074Sxp1zgp5kQ==" class="side-nav-link-ref"><span>Rostov W - WFC Zvezda-2005 Perm W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/eVga5%2FxPzJUFgRKlyBRPlA==" class="side-nav-link-ref"><span>Zenit Saint Petersburg W - Yenisey W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>WALES League Cup</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/2rfpzJohmQTN8EXW0uIKkA==" class="side-nav-link-ref"><span>Airbus UK Broughton FC - Guilsfield</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/plVNQYS+fFw8Rgd3er7ztg==" class="side-nav-link-ref"><span>Port Talbot Town - Swansea University</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AUSTRALIA NPL ACT</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/%2Fco%2FQMjYGMIv1QuxH2Kodw==" class="side-nav-link-ref"><span>Belconnen Utd - Woden Weston</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4pWEmrG3fn3VV67q0%2Fjbbg==" class="side-nav-link-ref"><span>Monaro Panthers - Cooma Tigers</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ICELAND Inkasso-deildin</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/tpLLs7yP3KQSH%2FGr0I1eSw==" class="side-nav-link-ref"><span>IF VESTRI - Thróttur Reykjavík</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>JAPAN J1 League</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/0JhBYyeivWE0u9kbhWV+8w==" class="side-nav-link-ref"><span>Avispa Fukuoka - Gamba Osaka</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/LoXxq63f7pjnIqJbnYP6pA==" class="side-nav-link-ref"><span>Cerezo Osaka - Vissel Kobe</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/AVAy3r04Hf3djojy3mWBfw==" class="side-nav-link-ref"><span>Sagan Tosu - Nagoya Grampus</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/9lT8D0RQnZ3SLb6p5x%2FBTQ==" class="side-nav-link-ref"><span>Shimizu S-Pulse - Kawasaki Frontale</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>NORWAY Eliteserien</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/pxHXtzQipM5H+ybbgfclUA==" class="side-nav-link-ref"><span>Odds BK v Viking</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Qxzk5sfUPuR8TKokbKYa5w==" class="side-nav-link-ref"><span>Sarpsborg v Bodo Glimt</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ESTONIA Esiliiga</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/mqmJ1OlZIbkulgGPh1PDRg==" class="side-nav-link-ref"><span>Maardu Linnameeskond FC - Nomme Utd</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/3hGTbIgSgXLRYLefa6nYYg==" class="side-nav-link-ref"><span>Tammeka II - Tartu JK Welco</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>SERBIA Super Liga</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/u6wrKY1C+siy6NmSK4nteA==" class="side-nav-link-ref"><span>FK Backa Topola v FK Novi Pazar</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/glnHkkqX5A+HoWAw4Lhcwg==" class="side-nav-link-ref"><span>FK Proleter v Partizan Belgrade</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>JORDAN Premier League</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/dpdv2Kjl57M%2FfNa1eKGLYQ==" class="side-nav-link-ref"><span>Maan - Al Jalil</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ZyV6Q0llUZdri7iEhJWmAA==" class="side-nav-link-ref"><span>Shabab Al Ordon - Al Ramtha</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>RUSSIA FNL</span> <span> (6)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/eHXHBqE2znxwHDzE%2F+NZXw==" class="side-nav-link-ref"><span>Akron Tolyatti - Volgar Astrakhan</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/NEUE7iyJaSZEvTpWeacrtA==" class="side-nav-link-ref"><span>FC Enisey - Kuban</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/dXS+JoRnxHaaqD1d2st8aQ==" class="side-nav-link-ref"><span>FC Kamaz - Tekstilshik</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/h2B6JXHpy4PeHjf1iJv6Kg==" class="side-nav-link-ref"><span>FC Neftekhimik Nizhnekamsk - FC SKA-Khabarovsk</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/dUI6cr1hnN8E1bAkOUkaaQ==" class="side-nav-link-ref"><span>FC Orenburg - Metallurg Lipetsk</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/hRrXBNJaD2OBh%2F40kl2fCQ==" class="side-nav-link-ref"><span>Torpedo Moscow - Spartak Moskva-2</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AUSTRALIA NPL Northern NSW</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/9Ok7HY4deBg7PxIkcNW9Nw==" class="side-nav-link-ref"><span>Newcastle Olympic Warriors - Lambton Jaffas</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/wkGF93iWSj5+SYouyn64ZA==" class="side-nav-link-ref"><span>Weston Workers Bears - Edgeworth Eagles</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>FINLAND Ykkonen</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/jTfn2j8VNAgP1+GlIgSYdA==" class="side-nav-link-ref"><span>EIF - Mikkeli Palloilijat</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/NoAxX+LPnAmfU3hHi38zTA==" class="side-nav-link-ref"><span>Klubi 04 - FF Jaro</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/twYrNfTwvLVGsVnd1GQLXw==" class="side-nav-link-ref"><span>RoPS Rovaniemi - JIPPO</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BANGLADESH Premier League</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/MKMFea5tvTdseXIDnAsmYQ==" class="side-nav-link-ref"><span>Mohammedan Dhaka - Baridhara</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/CsFl%2FjQPrS71MyOHpNqwew==" class="side-nav-link-ref"><span>Rahmatganj MFS - Bangladesh Police</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>FINLAND Kansallinen Liiga Women</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/b7lBsWymlUAu1rZwKYruyQ==" class="side-nav-link-ref"><span>HJK Helsinki W - KuPS W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/fGmClm%2F29doZDDTJ0OubEg==" class="side-nav-link-ref"><span>TiPS Vantaa W - Ilves Tampere W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AUSTRALIA Brisbane Premier League</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/B6vlUTzZzsuyW9QpTcVQdA==" class="side-nav-link-ref"><span>Acacia Ridge - Toowong</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/nR75uBCqqp2eItRy0vYf8A==" class="side-nav-link-ref"><span>Bayside United FC - St George Willawong</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>SOUTH KOREA K League 2</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/3nIHyo2t%2FZNmcUG9WF59cQ==" class="side-nav-link-ref"><span>Busan Ipark - Ansan Greeners</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/NZecKZLqxkEATW6Do5kuyg==" class="side-nav-link-ref"><span>Jeonnam Dragons - Chungnam Asan FC</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>KAZAKHSTAN Kazakhstan Cup</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4mQougO8dnenSM3zgvhVaA==" class="side-nav-link-ref"><span>Akzhayik - FK Kaysar</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/lFldw6weykt8XHtJGNXgCA==" class="side-nav-link-ref"><span>FC Kairat Almaty - Caspiy Aktau</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/pOGvdfB9MJerr1d+SeCqsA==" class="side-nav-link-ref"><span>FC Makhtaaral - Taraz</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>URUGUAY Primera Division</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/4j%2F4xLywiHgsC9VakdIlrg==" class="side-nav-link-ref"><span>Cerro Largo FC v Wanderers (Uru)</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PT0B1oRyc65C9oAvJt%2Fsbg==" class="side-nav-link-ref"><span>Deportivo Maldonado v Plaza Colonia</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/en0csWqiF8rjyXNOa2RaLQ==" class="side-nav-link-ref"><span>River Plate (Uru) v CA Rentistas</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>USL League Two</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/Dou4RlzkxqbuR8EQp3nZig==" class="side-nav-link-ref"><span>Cedar Stars Rush - Western Mass Pioneers</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ASPkPb6PQF2l95xmDhA6EA==" class="side-nav-link-ref"><span>Long Island Rough Riders - Seacoast United Phantom</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>IRELAND Division 1</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/InyiMCJAA3uvOTFtOARctw==" class="side-nav-link-ref"><span>Cobh Ramblers - Cabinteely</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ESTONIA Meistriliiga</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/PMqV7UQcA+Xvaa6Lzl+tTA==" class="side-nav-link-ref"><span>JK Viljandi Tulevik - Narva Trans</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/qRweqyvwEK7rwFt+En+%2FnQ==" class="side-nav-link-ref"><span>Kuressaare - Tallinna JK Legion</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>AFRICA CAF Champions League</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/%2Fqus4+Skp1I2mWPYvSOh+g==" class="side-nav-link-ref"><span>Kaizer Chiefs - Al Ahly Cairo</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>JAPAN Nadeshiko League Women</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/8AXmRJOMGFpAXcwdM2dh6g==" class="side-nav-link-ref"><span>Speranza Takatsuki W - Harima Albion W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ROMANIA Liga 1</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/BAFY5EZLET%2FhAq24%2FSWP6w==" class="side-nav-link-ref"><span>Gaz Metan Medias v CS Mioveni</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/a3UIvbT%2FWTj8mcDuYeJKhA==" class="side-nav-link-ref"><span>Universitatea Craiova v Arges Pitesti</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BELARUS Vysshaya Liga</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/nZ9KUcGulEFg8dR6Y6+3tg==" class="side-nav-link-ref"><span>FK Gomel - Rukh Brest</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHINA Yi League</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ajD8I7jbXI86LhXBxNlfHQ==" class="side-nav-link-ref"><span>Hunan Xiangtao FC - Qingdao Red Lions</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/43bBHyUO37ByEk3YjNzHIg==" class="side-nav-link-ref"><span>Wuxi Wugou - China U20</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ICELAND Division 2</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/xI%2FrsamnimMkwxg8txl01Q==" class="side-nav-link-ref"><span>Haukar - Leinir Faskrudsfjordur</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/i%2Fp47p4ii155NcY+bxlrDA==" class="side-nav-link-ref"><span>ÍR Reykjavik - Völsungur</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ISkj+MLfF+EXSfA3uu6%2Fcg==" class="side-nav-link-ref"><span>Njardvik - Magni</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CHILE Primera Division</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/+Z4joZ94Va+Os8hczZP+bw==" class="side-nav-link-ref"><span>Union La Calera v Curico Unido</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/eD+H76IbmemK80aFS5Ez9A==" class="side-nav-link-ref"><span>Univ Catolica (Chile) v Colo Colo</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ALGERIA U21 League</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/W7VlXKTAnr0pof8Iowaqrw==" class="side-nav-link-ref"><span>CS Constantine U21 - Js Kabylie U21</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BELGIUM Super Cup</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/5Sf43bYQQKqMLIlsjHS9%2FA==" class="side-nav-link-ref"><span>Club Brugge v Genk</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>IRELAND Premier Division</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/j43BkA1NFHNBh+KSL9HMBw==" class="side-nav-link-ref"><span>Dundalk v Finn Harps</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>ICELAND Pepsideild</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/lcGw+5gPrIfA7I18BQFUsw==/ExZ2N%2FquZvrFb00KNw%2F2SQ==" class="side-nav-link-ref"><span>ÍA Akranes - Valur</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport8"><span>Table Tennis</span> <span> (47)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>MEN Liga Pro (Russia)</span> <span> (31)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/%2FkG8ibQuF+ceQyYSSblIlg==" class="side-nav-link-ref"><span>Aleksandr Merezhko - Oleg Belugin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/RqNkK8k8ikyMLMjaFGrLoQ==" class="side-nav-link-ref"><span>Aleksandr Savinskiy - Oleg Kharlakin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/aB3s6sJW+RoWtVXyN4qlRw==" class="side-nav-link-ref"><span>Alexander Kononenko - Aleksandr Merezhko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/6ZSs3QlILheBQIcvRSqOfQ==" class="side-nav-link-ref"><span>Alexander Kononenko - Igor Sergeevich</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/FS07ixoDP5nawZ8M9MXYFg==" class="side-nav-link-ref"><span>Alexey Innazarov - Maxim Pronkin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/LDRxKFbzm0ZcS8lAJ+AXhg==" class="side-nav-link-ref"><span>Andrey Lisov - Yuriy Gavrilov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/J548GTXwEvwrwAxanN9lwg==" class="side-nav-link-ref"><span>Andrey Suslov - Anton Mohnachev</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/qTJYRnoQ6Z4nfttqxQC%2FGQ==" class="side-nav-link-ref"><span>Anton Mohnachev - Vladimir Dakhin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/F0%2FctFC6ckStPp2r5YK5nw==" class="side-nav-link-ref"><span>Danila Andreev - Andrey Lisov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/Y9umb5I7TphdiCbmUeUrKA==" class="side-nav-link-ref"><span>Danila Andreev - Konstantin Olshakov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/uQjHa8sBjflqKpchZPJp0A==" class="side-nav-link-ref"><span>Dmitry Bogdanov - Aleksandr Savinskiy</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/2NQSm2Vdm3YyTIQxBUNldQ==" class="side-nav-link-ref"><span>Evgeniy Glazun - Maksim Ilichev</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/GSAgPUcY1cMd865dwd5K%2Fg==" class="side-nav-link-ref"><span>Evgeny Masokin - Pavel Semeshin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/Z5zkLimc+Hizr5ncGmEj3w==" class="side-nav-link-ref"><span>Igor Sergeevich - Aleksandr Merezhko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/IU2SWLeFbDENi9zf4Rbk6Q==" class="side-nav-link-ref"><span>Igor Sergeevich - Oleg Belugin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/MgccduLrwZyGvGBYU50E4Q==" class="side-nav-link-ref"><span>Konstantin Olshakov - Andrey Lisov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/nXmcvQLeYtjvH%2Fy%2FyHnrbw==" class="side-nav-link-ref"><span>Konstantin Olshakov - Yuriy Gavrilov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/liT5+gd6ablZr3Ktwq1dYA==" class="side-nav-link-ref"><span>Maksim Ilichev - Evgeny Masokin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/GZEHBzqzwBZuh0uNnAt29A==" class="side-nav-link-ref"><span>Maxim Pronkin - Vasily Shirshov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/pIw+Kzcm8%2FAhrvSLIZsslA==" class="side-nav-link-ref"><span>Mikhail Marchenko - Andrey Suslov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/nAPMaRSO5lErpmwo3uuAqw==" class="side-nav-link-ref"><span>Mikhail Marchenko - Anton Mohnachev</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/DDSGrUi9KE0Rpp6Sq1Ihgw==" class="side-nav-link-ref"><span>Oleg Belugin - Alexander Kononenko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/LUQQMyhJkoRfb76A6AHKEQ==" class="side-nav-link-ref"><span>Oleg Kharlakin - Dmitry Bogdanov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/C0unQCFZu%2FDub630UqGw0Q==" class="side-nav-link-ref"><span>Oleg Kharlakin - Sergey Lanovenko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/JoqYV88nT9f6gT5r41prEw==" class="side-nav-link-ref"><span>Pavel Semeshin - Evgeniy Glazun</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/eleRoN9uDBSnrnPFzr8oZQ==" class="side-nav-link-ref"><span>Sergey Lanovenko - Aleksandr Savinskiy</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/5NB1B2GXouDf0rcN%2FP3VzA==" class="side-nav-link-ref"><span>Sergey Lanovenko - Dmitry Bogdanov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/50LXwahDc7daf7MPd11AdQ==" class="side-nav-link-ref"><span>Vasily Shirshov - Vladimir Petrov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/zPlUE6J0%2Fx+MtnpcOCxuLA==" class="side-nav-link-ref"><span>Vladimir Dakhin - Mikhail Marchenko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/bTK5AORJhmInkGCfkqGsZQ==" class="side-nav-link-ref"><span>Vladimir Petrov - Alexey Innazarov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/r5qJEHinG%2F+E6Bc8WZRdsg==" class="side-nav-link-ref"><span>Yuriy Gavrilov - Danila Andreev</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>MEN TT Cup (Ukraine)</span> <span> (16)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/x6sMdJQHSAlNP3SZz8g8MA==" class="side-nav-link-ref"><span>Aleksandr Levadniy - Lev Kats</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/QP5Nvr0yskbWrs3fGbN%2F+w==" class="side-nav-link-ref"><span>Aleksandr Shitka - Ilya Yanishevskiy</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/YvfzZZZWjC2Bd%2FXcWj%2F5OQ==" class="side-nav-link-ref"><span>Aleksandr Shkurupij - Aleksandr Zhukovskij</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/eOU4b5WifaYZj0XXtCqErA==" class="side-nav-link-ref"><span>Aleksandr Shkurupij - Sergey Filatov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/OKntGsB1TmtI9mXVyNHUnA==" class="side-nav-link-ref"><span>Aleksandr Zhukovskij - Denys Scherbak</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/gHA7LhroYEaxiFDu+8WwMA==" class="side-nav-link-ref"><span>Borys Buryak - Ilya Yanishevskiy</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/hrcsoLWaiRLWeD59j3f9Ng==" class="side-nav-link-ref"><span>Denys Scherbak - Valentyn Korovychenko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/vttpbCt4Z64yVZEluK5qUg==" class="side-nav-link-ref"><span>Evgeniy Pismenny - Sergiy Baranovskyi</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/GzHQLoMl1DOqmOcPUKYphw==" class="side-nav-link-ref"><span>Ilya Yanishevskiy - Igor Kuznetsov</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/wbXCnzJISjFVRbYgDtWjAw==" class="side-nav-link-ref"><span>Sergey Filatov - Denys Scherbak</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/Q9HtlqW7nqXz0uUWVpuTyQ==" class="side-nav-link-ref"><span>Sergey Filatov - Dmitriy Dubrovin</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/lhaqhwRAGH2IZ+rmpp+jwA==" class="side-nav-link-ref"><span>Sergey Gogenko - Sergiy Boyko</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/7Igp5fenKVnrAStae6BSzA==" class="side-nav-link-ref"><span>Sergey Rutskiy - Andrey Zenyuk</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/v0EHBJu+0ZXapR8n4WLOqQ==" class="side-nav-link-ref"><span>Vasiliy Salivonchik - Maxim Naumchuk</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/bzyxeuic1HItdudePsQOUg==" class="side-nav-link-ref"><span>Vitaliy Zharskiy - Andrey Zenyuk</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Mv1qUHVhzVyTEtA2KA7K6w==/rjBvxzVQhnXgg%2FlxKNhsNw==" class="side-nav-link-ref"><span>Yurii Novokhatskyi - Viktor Milishcuk</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport15"><span>Basketball</span> <span> (14)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>EUROPE European Challengers U20 Women</span> <span> (6)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/PMRcCjE8eHOIm8+vdNmyuw==" class="side-nav-link-ref"><span>Croatia U20 W - Bulgaria U20 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/IHY3xezh6HobGLkd3kTMHA==" class="side-nav-link-ref"><span>Germany U20 W - Belgium U20 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/1RviVQpnBXoji8394x7mJQ==" class="side-nav-link-ref"><span>Italy U20 W - Latvia U20 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/h%2FAarhII10XTsR2+IrkEZA==" class="side-nav-link-ref"><span>Russia U20 W - Poland U20 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/74dJTt8KbUPkoq5TyKHFgQ==" class="side-nav-link-ref"><span>Serbia U20 W - Ireland U20 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/4r8S1lIT8hpdwPYFqVgQiw==" class="side-nav-link-ref"><span>Turkey U20 W - Portugal U20 W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>USA NBA</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/GQSPp1he2ISMY+sk8VEPng==" class="side-nav-link-ref"><span>Phoenix Suns - Milwaukee Bucks</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Campeonato Carioca U19</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/zQEMkOe91Z12CerqDFbk1A==" class="side-nav-link-ref"><span>Niteroi U19 - Escolinha De Esportes Passo Zero U19</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>PHILIPPINES Philippine Cup</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/T7FP9O2Ak0d9LS+2aCgDng==" class="side-nav-link-ref"><span>Magnolia Hotshots - Phoenix Fuelmasters</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/OiVrW8wSQu9BgCMOge9yMg==" class="side-nav-link-ref"><span>Talk N Text Tropang Texters - Terra Firma Dyip</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>MALI Première Division</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/epmCm71HL7TIWpGlU4dz6g==" class="side-nav-link-ref"><span>AS Police - Mande</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>MOROCCO : 1DNH</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/7RwWJg9tWbzXeplDoHI2vA==" class="side-nav-link-ref"><span>RBM Beni Mellal  - AMB Tamouda</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>NEW ZEALAND NBL</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/imE5yIjYvGn0WiYKKiwBlQ==" class="side-nav-link-ref"><span>Auckland Huskies - Wellington Saints</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/%2Fc+BohE+k+EfApXiDXod7A==/1ZygExXNJLTwiq8vt4P41w==" class="side-nav-link-ref"><span>Otago Nuggets - Canterbury Rams</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport18"><span>Volleyball</span> <span> (6)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>RUSSIA Liga Pro</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/wEN2Sr3uW+4yxJQHhrtUrg==/q5YnBKV%2FAbl8kS4qo50txQ==" class="side-nav-link-ref"><span>Imperiya-Pro - Lions-Pro</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/wEN2Sr3uW+4yxJQHhrtUrg==/noSDQfC7uRqQvWooDgRc9Q==" class="side-nav-link-ref"><span>Legion-pro - Energy-pro</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/wEN2Sr3uW+4yxJQHhrtUrg==/v%2FgRWNMr9Nc949KfNxwaKg==" class="side-nav-link-ref"><span>Orly Eagles-Pro - Vityaz-Pro</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/wEN2Sr3uW+4yxJQHhrtUrg==/zP0kJAe6621KUgeyuTsAsw==" class="side-nav-link-ref"><span>Plamya-Pro - Molot-pro</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>RUSSIA Liga Pro Women</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/wEN2Sr3uW+4yxJQHhrtUrg==/kr7Gp1llpsOpD2bWBlV77w==" class="side-nav-link-ref"><span>Foksi-pro W - Pantery-pro W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/wEN2Sr3uW+4yxJQHhrtUrg==/cspSpTO%2FsVKETAL4ZL6+EQ==" class="side-nav-link-ref"><span>Sovy-pro W - Foksi-pro W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport39"><span>Handball</span> <span> (11)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>EUROPE European Championship U19 Women</span> <span> (11)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/jBx4vo1Labp7OKfczO3KuQ==" class="side-nav-link-ref"><span>Austria U19 W - Montenegro U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/6sVnjjGYRuI0L8fC16Tpsw==" class="side-nav-link-ref"><span>Belarus-U19 W - Netherlands U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/2pO7BQRfxW3lfLEJQwJ9jQ==" class="side-nav-link-ref"><span>Croatia U19 W - Germany U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/OhH9be+yKjw72bWFa1RV%2FQ==" class="side-nav-link-ref"><span>Denmark U19 W - Romania U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/Bn1ZKBbjHz+ymbjhvGlrBg==" class="side-nav-link-ref"><span>Faroe Islands U19 W - Poland U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/BUQaA2QJF3bleqy%2FJLFlCQ==" class="side-nav-link-ref"><span>Iceland U19 W - Kosovo U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/e7cHs1Vfkjao43CAIAQZOQ==" class="side-nav-link-ref"><span>North Macedonia U19 W - Finland U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/DNS+cA8hsvFR+4JUQJICew==" class="side-nav-link-ref"><span>North Macedonia U19 W - Kosovo U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/4cNu7LqdHBgEw95d9U+5bw==" class="side-nav-link-ref"><span>Norway U19 W - Czech Republic U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/93z2MjViYbPpb5ZFQRSTRQ==" class="side-nav-link-ref"><span>Serbia U19 W - Lithuania-U19 W</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/O8n0++EdxaIPQ5Ws%2Fxh+dA==/8%2FjqPbjFZ8tCJRvWy+HW%2Fw==" class="side-nav-link-ref"><span>Spain U19 W - Italy U19 W</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport19"><span>Ice Hockey</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport11"><span>E Games</span> <span> (28)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS LoL Pro League (China)</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/pLgHyNqreNMCk6d0iKMHdg==" class="side-nav-link-ref"><span>Rogue Warriors - Rare Atom</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/BpV5yWBI0qRHbPTu5PW+TQ==" class="side-nav-link-ref"><span>Top Esports - FunPlus Phoenix</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/2hf1RTdJ1QvW5heRWWHKOg==" class="side-nav-link-ref"><span>Ultra Prime - Team We</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS: CBOL segunda etapa</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/xDJXmpAFTehiMC2AYQNuCg==" class="side-nav-link-ref"><span>Flamengo eSports - Intz Esports</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/SyfvdE%2FdKotsziCKosJEUA==" class="side-nav-link-ref"><span>KaBuM! eSports - Loud</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/zWp2n5rslEclzXvH1is7qw==" class="side-nav-link-ref"><span>paiN Gaming - Rensga eSports</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/Y4e3YONTSqV1g8tkxaulcQ==" class="side-nav-link-ref"><span>Vorax - FURIA</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CS:GO. European Development Championship Season 5:</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/DE7UiWVSFD7eNSGtttkl+w==" class="side-nav-link-ref"><span>Boys without Pyjamas - Infinite</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/3JJiR7DgtBUQmQIEiXYjjw==" class="side-nav-link-ref"><span>Spirit Academy - Team Finest</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS TCL</span> <span> (5)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/iTvEMLh0zT3RrEn3NwIavQ==" class="side-nav-link-ref"><span>1907 Fenerbahçe eSports - Super Massive</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/LIoqm7eKsuPd%2FPeOUcTPtA==" class="side-nav-link-ref"><span>5 Ronin - Team AURORA</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/cgaa4qroQb3ZbZ8mVj+pNA==" class="side-nav-link-ref"><span>Galatasaray eSports - Besiktas</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/c4y8wqv20AZG%2FT9YCaIf4w==" class="side-nav-link-ref"><span>Istanbul Wild Cats - Galakticos</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/tBUkRK+Y5YTjNyaSJ8AJzA==" class="side-nav-link-ref"><span>NASR - Dark Passage</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>CS:GO. Redragon Latam Series 2021: Southern Cone</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/NzZMdPIDbAt3%2F2a5Ztt78A==" class="side-nav-link-ref"><span>9z  - Coscu Army</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS Champions Korea (South Korea)</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/LOS2eyZRMv0%2FEJsRhDf5uw==" class="side-nav-link-ref"><span>Gen.G - Hanwha Life Esports</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/cUWqC42fvApLpnJXS4Qd5Q==" class="side-nav-link-ref"><span>T1 Esports - Damwon Gaming</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS LoL Continental League (Russia)</span> <span> (4)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/JYR88pfNpCFsJ9ZE%2F9Uxhw==" class="side-nav-link-ref"><span>Black Star Gaming - ROX TEAM</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/lJ0+6mwxJ3CP1efB352Abw==" class="side-nav-link-ref"><span>CTRL PLAY - Unicorns Of Love</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/xDMQzsULazoYp+5hzrvmDw==" class="side-nav-link-ref"><span>Dragon Army - CrowCrowd</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/WYEXVWgmpeZDgIem5cQjvg==" class="side-nav-link-ref"><span>Vega Squadron - One Breath Gaming</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS European Championship (Europe)</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/JBAW6uUylzPKTVEy2bM1WQ==" class="side-nav-link-ref"><span>FC Schalke 04 Esports - Misfits</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/DHY%2FwGHvltHcGlhES5L29A==" class="side-nav-link-ref"><span>Rogue - Astralis </span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/tzaPhDGE%2FgsxPY56PGz6KQ==" class="side-nav-link-ref"><span>SK Gaming - ExceL eSports</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>COUNTER-STRIKE IEM - Cologne (Germany)</span> <span> (3)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/HtSjO+iOV4yQo6p92LTAOQ==" class="side-nav-link-ref"><span>G2 Esports - Astralis </span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/eibAtn9aj8P%2FB3WyPpyDJQ==" class="side-nav-link-ref"><span>Gambit - FaZe Clan</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/%2FvSrMNOi6uWVUwXWgnAPPQ==" class="side-nav-link-ref"><span>Natus Vincere - FaZe Clan</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>LEAGUE OF LEGENDS : LCS Summer</span> <span> (1)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/Epv0oZEJe1SyR+AgATTDGg==/udGJqlaXnyj+UldRvCcucQ==" class="side-nav-link-ref"><span>Team SoloMid - FlyQuest</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="has-arrow sport9"><span>Futsal</span> <span> (2)</span></a>
                                <ul class="sub-menu mm-collapse">
                                    <li class="text-dark accordion-box"><a href="javascript:void(0)" class="has-arrow"><span>BRAZIL Campeonato Paranaense</span> <span> (2)</span></a>
                                        <ul class="sub-menu mm-collapse">
                                            <li class="text-dark"><a href="/admin/game/details/3dL3mzDVrgW%2F4LmJbGFxtg==/OP3KItOYUDhguLDYIL0hiw==" class="side-nav-link-ref"><span>Sao Jose Dos Pinhais - Siqueira Campos/Pro Tork</span></a>

                                            </li>
                                            <li class="text-dark"><a href="/admin/game/details/3dL3mzDVrgW%2F4LmJbGFxtg==/IUBtT6hZq0QHFRbPKdS7Gw==" class="side-nav-link-ref"><span>Toledo - PalmasNet/Palmas</span></a>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport55"><span>Rugby League</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport6"><span>Boxing</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport7"><span>Beach Volleyball</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport3"><span>Mixed Martial Arts</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport10"><span>Horse Racing</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport12"><span>Greyhounds</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport16"><span>MotoGP</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport17"><span>Chess</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport22"><span>Badminton</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport29"><span>Cycling</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport32"><span>Motorbikes</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport33"><span>Athletics</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport35"><span>Basketball 3X3</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport37"><span>Sumo</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport38"><span>Virtual sports</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport52"><span>Motor Sports</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport53"><span>Baseball</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport54"><span>Rugby Union</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport57"><span>Darts</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport58"><span>American Football</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport59"><span>Snooker</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport62"><span>Soccer</span> </a>

                            </li>
                            <li class=""><a href="javascript:void(0)" class="sport64"><span>Esports</span> </a>

                            </li>
                        </ul>
                    </li> */}
                </ul>

            </div>

        );
    }
}

export default AdminHeader;
import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Link,
    Route,
    withRouter,
} from "react-router-dom";
import $ from "jquery";
import { ThemeColor } from '../assets/js/customreact';
import { ApiExecute } from "../Api";

class TopHeader extends Component {

    state = {
        our_live_casinos: [
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race17.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teenmuf.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teensin.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/patti2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/trap.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/superover.jpg'
            }, {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen8.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen9.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/poker6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/baccarat2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dtl20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/dt202.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/card32eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/ab20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/abj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lucky7eu.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/3cardj.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/war.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/worli2.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/aaa.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/btable.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/lottcard.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cricketv3.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmatch20.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/cmeter.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/teen6.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/queen.jpg'
            },
            {
                image: 'https://sitethemedata.com/casino_icons/lc/race20.jpg'
            }
        ],
        upcoming_fixtures: [
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
            {
                name: 'Marchenko v Anderson',
                date: '13/07/2021 00:30:00'
            },
        ],
        sidebarAddclass: false,
        searchInput: '',
        searchBlock: '',
        userDropdownAddclass: false,
        GraphtoggleAddclass: false,
        GraphtoggleAddclass_b: false,
        isOpened: false,
        user: {},
        categories: [],
    }
    logoutSubmit = async () => {
        sessionStorage.removeItem('@token');
        sessionStorage.removeItem('@user');
        // let url = `/${Admin_prifix}dashboard`;
        // this.props.history.push(url);
    }

    theme(color) {
        ThemeColor(color);
    }

    categoryList = async () => {
        let api_response = await ApiExecute(`category?category_id=0`, { method: 'GET' });
        console.log('lists', api_response.data.results);
        this.setState({ categories: api_response.data.results })
    }

    async componentDidMount() {
        this.categoryList();
        let user = sessionStorage.getItem('@front_user');
        this.setState({
            user: JSON.parse(user)
        });
    }
    render() {
        return (
            <div class="header-top">
                <ul>
                    {
                        this.state.categories
                            .sort((a, b) => a.id > b.id ? 1 : -1)
                            .map((c, index) => {
                                return (
                                    <li> <Link to={`/${c.slug}`}>{c.name}</Link>
                                    </li>)
                            })
                    }
                </ul>
                <div class="color-box-container">
                    <div class="color-box dark-box" onClick={() => this.theme('dark')}></div>
                    <div class="color-box light-box" onClick={() => this.theme('light')}></div>
                    <div class="color-box blue-box" onClick={() => this.theme('blue')}></div>
                </div>
            </div >
        );
    }
}

export default withRouter(TopHeader);

import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Link,
    Route,
    withRouter,
} from "react-router-dom";
import $ from "jquery";
import { ThemeColor } from '../../assets/js/customreact';
import { Carousel } from "react-responsive-carousel";
import FrontFooter from "../FrontFooter";
import { ApiExecute1 } from "../../Api";

class Teen20 extends Component {

    state = {
        user: {},
        game_slug: '',
        category_id: '',
        game_detail: {},
        t1: {},
        t2: [],
        t3: [],
        ar: [],
        br: [],
        loading: false,
    }

    getGame = async (slug = null) => {
        let s = slug ? slug : this.state.game_slug;
        let api_response = await ApiExecute1(`getdata/${s}`, { method: 'GET' });
        console.log('game_detail', api_response.data.data);
        // this.setState({
        //     game_detail: api_response.data.data
        // });
        // this.setState({ loading: true });

        api_response.data.data.t1.forEach((element, index) => {
            // console.log('game_detail', element);
            this.setState({ t1: element });

            // if (element.autotime != '0') {
            // console.log(element.autotime);
            // this.toggleClassTimer();
            // this.toggleClassTimer();
            // }
        });
        if (api_response.data.data.t2) {
            this.setState({ t2: api_response.data.data.t2 });
        }
        if (api_response.data.data.t3) {
            api_response.data.data.t3.forEach((element, index) => {
                var ar = [];
                ar = element.ar.split(',');
                this.setState({ ar: ar });
                var br = [];
                br = element.br.split(',');
                this.setState({ br: br });
            });
        }

    }
    async componentWillUnmount() {
        clearInterval(this.getGame);
    }

    async componentDidMount() {
        // console.log('props', this.props);
        // console.log('slugs dfg', this.props.match.params.slug);
        this.getGame(this.props.match.params.slug);
        setInterval(this.getGame, 1000);

        let user = sessionStorage.getItem('@front_user');
        let user_token = sessionStorage.getItem('@front_token');

        this.setState({
            user: JSON.parse(user)
        });

        let game_slug = this.props.match.params.slug;

        if (game_slug) {
            this.state.game_slug = game_slug;
            // this.getCategory(game_slug);
        }
    }

    toggleClassTimer = async () => {
        $('.base-timer').children('.base-timer__label').children('span').toggleClass('component-fade-leave-to');
    }


    render() {
        return (
            <>
                <div class="casino-center">
                    <div id="element" class="casino-container">
                        <div class="casino-table teenpatti20">
                            <div class="casino-video">
                                {/* <div class="disconnected-box">
                                                <div class="disconnected-message">
                                                    <div class="text-center"><i class="fas fa-exclamation-triangle mr-1"></i> <b>Disconnection due to inactivity</b></div>
                                                    <div class="mt-1">Are you there? You have been disconnected. Please go back to home or start playing again</div>
                                                    <div class="disconnected-buttons mt-2"><button type="button" class="btn btn-outline-primary">Reconnect</button> <a href="/casino" class="btn btn-outline-danger router-link-active">
                                                        Home
                                                    </a>
                                                    </div>
                                                </div>
                                            </div> */}
                                <div class="casino-video-title"><span class="casino-name">{this.state.game_slug}</span> <span class="casino-video-rid">Round ID: {this.state.t1.mid}</span></div>
                                <div class="video-box-container">
                                    <div class="video-box">
                                        <iframe src={`http://139.162.213.154/demo-dvideo/${this.state.game_slug}.html`}></iframe>
                                    </div>
                                </div>
                                <div class="casino-video-cards hide-cards">
                                    <div class="casino-cards-shuffle"><i class="fas fa-grip-lines-vertical"></i></div>
                                    <div class="casino-video-cards-container">
                                        <div>
                                            <div class="dealer-name w-100 mb-1">Player</div>
                                            <div><span><span><img src="https://sitethemedata.com/v4/static/front/img/cards/1.png" /></span></span></div>
                                        </div>
                                        <div>
                                            <div class="dealer-name w-100 mb-1">Dealer</div>
                                            <div><span><span><img src="https://sitethemedata.com/v4/static/front/img/cards/1.png" /></span></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="casino-timer d-none-mobile">
                                    <div class="base-timer">
                                        <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" class="base-timer__svg">
                                            <g class="base-timer__circle">
                                                <circle cx="50" cy="50" r="45" class="base-timer__path-elapsed"></circle>
                                                <path stroke-dasharray="30 283" d="M 50, 50
                                                                m -45, 0
                                                                a 45,45 0 1,0 90,0
                                                                a 45,45 0 1,0 -90,0
                                                                " class="base-timer__path-remaining red">
                                                </path>
                                            </g>
                                        </svg>
                                        <span class="base-timer__label red"><span class="component-fade-leave-active">
                                            {this.state.t1.autotime}
                                        </span></span>
                                    </div>
                                </div>
                                <div class="casino-time-digit d-none-desktop">3</div>
                                <div class="casino-video-right-icons">
                                    <div title="Home" class="casino-video-home-icon"><a href="/casino" class="router-link-active" ><i class="fas fa-home"></i></a></div>
                                    <div title="Rules" class="casino-video-rules-icon"><i class="fas fa-info-circle"></i></div>
                                </div>
                                <div id="casino-vieo-rules" aria-modal="true" class="casino-vieo-rules show d-none-small show-rules d-none">
                                    <div class="rules-header">
                                        <div>Rules</div>
                                        <i class="fas fa-times"></i>
                                    </div>
                                    <div class="rules-body">
                                        <div>
                                            <div class="rules-section">
                                                <ul class="pl-4 pr-4 list-style">
                                                    <li>Kaun Banega Crorepati  (KBC ) is a unique and a new concept game played with a regular 52 cards deck.</li>
                                                    <li>As the name itself suggests there are very high returns on your bets.</li>
                                                    <li><b>How to play KBC :</b> There is a set of five questions and each question has options</li>
                                                    <li>5 cards will be drawn one by one from the deck as the answers to this questions 1 to 5 respectively.</li>
                                                    <li><b>Q1. </b><b>RED</b>(Hearts &amp; Diamonds) or <b>BLACK</b>(Spades &amp; Clubs )</li>
                                                    <li><b>Q2. </b><b>ODD</b>(A,3,5,7,9,J,K) or <b>EVEN</b>(2,4,6,8,10,Q)</li>
                                                    <li><b>Q3. </b><b>7UP</b>(8,9,10,J,Q,K) or <b>7DOWN</b>(A,2,3,4,5,6)</li>
                                                    <li>
                                                        <b>Q4. </b><b>3 CARD JUDGEMENT</b>(A,2,3   or  4,5,6   or  8,9,10  or  J,Q,K)
                                                        <div>Any 1card from the set of 3cards you choose.</div>
                                                    </li>
                                                    <li>
                                                        <b>Q5. </b><b>SUITS ( COLOR )</b>(Spades  or   Hearts   or   Clubs    or   Diamonds)
                                                        <div>You have to select your choice of answer from given options for all the questions.</div>
                                                        <div>At the start of the game you have three choices to play this game .</div>
                                                    </li>
                                                    <li><b>1. Five Questions :</b> Going for all the 5 questions</li>
                                                    <li><b>2. Four questions  (Four card quit )  :</b>   Going for the 1st  4 questions .</li>
                                                    <li><b>3. 50-50 Quit :</b> Going for 5 questions but 50-50 quit after the 4th question.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="rules-section">
                                                <h6 class="rules-highlight">About the Odds :  </h6>
                                                <h6 class="rules-sub-highlight">1. Five questions : </h6>
                                                <ul class="pl-4 pr-4 list-style">
                                                    <li>a. If you are going with an ODD card as your 2nd answer your winning odds will be 101 times of your betting amount.</li>
                                                    <li>eg: bet amount : 1000 x  101 odds  = 1,01,000 net winning amount.</li>
                                                    <li>b. If you are going with an EVEN card as your 2nd answer your winning odds will be 111 times of your betting amount.</li>
                                                    <li>eg: bet amount : 1000 x  111 odds  = 1,11,000 net winning amount.</li>
                                                </ul>
                                                <h6 class="rules-sub-highlight">2. Four Questions (Four card quit ) : </h6>
                                                <ul class="pl-4 pr-4 list-style">
                                                    <li>a. If you are going with an ODD card as your 2nd answer your winning odds will be 26.5 times of your betting amount.</li>
                                                    <li>eg : bet amount : 1000 x  26.5 odds  = 26,500 net winning amount.</li>
                                                    <li>b. If you are going with an EVEN card as your 2nd answer your winning odds will be 29 times of your betting amount.</li>
                                                    <li>eg : bet amount : 1000 x  29 odds  =  29,000 net winning amount.</li>
                                                </ul>
                                                <h6 class="rules-sub-highlight">3. 50-50 Quit : </h6>
                                                <ul class="pl-4 pr-4 list-style">
                                                    <li>In this you will get half of your winning amount after the 4th card and the remaining half of your winning amount + half of your initial bet amount will be placed on the 5th card as your betting amount.</li>
                                                    <li><b>If all the five answers are correct :</b></li>
                                                    <li>eg 1(a) ODD CARD :bet amount : 1000 x 63.76 odds   = 63,760 net winning amount.</li>
                                                    <li>eg 1(b) EVEN CARD :bet amount : 1000 x  69.65 odds  = 69,650 net winning amount.</li>
                                                    <li><b>If the 5th answer is incorrect :</b></li>
                                                    <li>eg 2(a) ODD CARD: bet amount : 1000 x  12.75 odds  = 12,750 net winning amount.</li>
                                                    <li>eg 2(b) EVEN CARD: bet amount : 1000 x  14 odds  = 14,000 net winning amount.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="casino-detail">
                                <div class="teen20casino-container d-none-small">
                                    <div class="teen20left">
                                        <div class="casino-box-row">
                                            <div class="casino-nation-name no-border casino-bl-box-title">
                                                <div class="playera">Player A</div>
                                            </div>
                                        </div>
                                        <div class="casino-bl-box casino-bl-box-title">
                                            <div class="casino-bl-box-item"><span>Player A</span></div>
                                            <div class="casino-bl-box-item"><span>3 Baccarat A</span></div>
                                            <div class="casino-bl-box-item"><span>Total A</span></div>
                                            <div class="casino-bl-box-item"><span>Pair Plus A</span></div>
                                        </div>
                                        <div class="casino-bl-box">
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">0</span> <span class="d-none">0</span></div>
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">0</span> <span class="d-none">0</span></div>
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">0</span> <span class="d-none">0</span></div>
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">A</span> <span class="d-none">0</span></div>
                                        </div>
                                        <div class="casino-rb-box-container">
                                            <div class="casino-rb-box blackcontainer">
                                                <div class="casino-rb-box-player blackbox back suspended">
                                                    <div class="text-right"><img src="https://sitethemedata.com/v5/static/front/img/cards/spade.png" /> <img src="https://sitethemedata.com/v5/static/front/img/cards/club.png" /></div>
                                                    <div class="text-right"><span class="d-block casino-box-odd">0</span> <span class="text-right d-none">0</span></div>
                                                </div>
                                            </div>
                                            <div class="casino-rb-box redcontainer">
                                                <div class="casino-rb-box-player redbox back suspended">
                                                    <div><img src="https://sitethemedata.com/v5/static/front/img/cards/heart.png" /> <img src="https://sitethemedata.com/v5/static/front/img/cards/diamond.png" class="diamond-icon" /></div>
                                                    <div class="text-right"><span class="d-block casino-box-odd">0</span> <span class="text-right d-none">0</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="teen20center"></div>
                                    <div class="teen20right">
                                        <div class="casino-box-row">
                                            <div class="casino-nation-name no-border casino-bl-box-title">
                                                <div class="playerb">Player B</div>
                                            </div>
                                        </div>
                                        <div class="casino-bl-box casino-bl-box-title">
                                            <div class="casino-bl-box-item"><span>Player B</span></div>
                                            <div class="casino-bl-box-item"><span>3 Baccarat B</span></div>
                                            <div class="casino-bl-box-item"><span>Total B</span></div>
                                            <div class="casino-bl-box-item"><span>Pair Plus B</span></div>
                                        </div>
                                        <div class="casino-bl-box">
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">0</span> <span class="d-none">0</span></div>
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">0</span> <span class="d-none">0</span></div>
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">0</span> <span class="d-none">0</span></div>
                                            <div class="back casino-bl-box-item suspended"><span class="casino-box-odd">B</span> <span class="d-none">0</span></div>
                                        </div>
                                        <div class="casino-rb-box-container">
                                            <div class="casino-rb-box blackcontainer">
                                                <div class="casino-rb-box-player blackbox back suspended">
                                                    <div><img src="https://sitethemedata.com/v5/static/front/img/cards/spade.png" /> <img src="https://sitethemedata.com/v5/static/front/img/cards/club.png" /></div>
                                                    <div class="text-right"><span class="d-block casino-box-odd">0</span> <span class="text-right d-none">0</span></div>
                                                </div>
                                            </div>
                                            <div class="casino-rb-box redcontainer">
                                                <div class="casino-rb-box-player redbox back suspended">
                                                    <div><img src="https://sitethemedata.com/v5/static/front/img/cards/heart.png" /> <img src="https://sitethemedata.com/v5/static/front/img/cards/diamond.png" class="diamond-icon" /></div>
                                                    <div class="text-right"><span class="d-block casino-box-odd">0</span> <span class="text-right d-none">0</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="casino-remark mt-1">
                                    <div class="remark-icon"><img src="https://sitethemedata.com/v5/static/front/img/icons/remark.png" /></div>
                                    <marquee>Note: After 21st july,2021 18:30:00 IST all result of Khal will be consider with 3 Baccarat rule and Before 21st july,2021 18:30:00 IST all result will be consider with khal rule.</marquee>
                                </div>
                            </div>
                        </div>
                    </div>
                    <FrontFooter {...this.props} />

                </div>
                <div id="right-sidebar-id" class="right-sidebar casino-right-sidebar teen2sidebar">
                    <span></span>
                    <div class="casino-place-bet">
                        <div class="casino-place-bet-title"><span>Last Results</span></div>
                        <div class="casino-video-last-results"><span class="resulta">P</span><span class="resulta">P</span><span class="resulta">P</span><span class="resulta">P</span><span class="resultb">D</span><span class="resulta">P</span><span class="resulta">P</span><span class="resulta">P</span><span class="resultb">D</span><span class="resultb">D</span> <a href="/report/casinoresult/teen1" class="result-more">
                            ...
                        </a>
                        </div>
                    </div>
                    <span></span>
                    <span>
                        <div class="casino-place-bet">
                            <div class="casino-place-bet-title"><span>Place Bet</span> <span class="float-right casino-min-max">
                                Range: <span>100</span>-<span>50K</span></span>
                            </div>
                            <div class="casino-place-bet-header">
                                <div>(Bet for)</div>
                                <div>Odds</div>
                                <div>Stake</div>
                                <div>Profit</div>
                            </div>
                            <div class="casino-place-bet-box">

                                <div class="casino-place-bet-info">
                                    <div class="bet-player"><span>Tie</span></div>
                                    <div class="odds-box"><input type="text" disabled="disabled" class="form-control" /> <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="arrow-up" /> <img src="https://sitethemedata.com/v4/static/front/img/arrow-down.svg" class="arrow-down" /></div>
                                    <div class="bet-input back-border"><input type="text" id="placebetAmountWeb" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control input-stake" /></div>
                                    <div>0</div>
                                </div>
                                <div class="casino-place-bet-button-container"><button class="btn btn-bet"><span>25</span></button><button class="btn btn-bet"><span>50</span></button><button class="btn btn-bet"><span>100</span></button><button class="btn btn-bet"><span>200</span></button><button class="btn btn-bet"><span>500</span></button><button class="btn btn-bet"><span>1000</span></button></div>
                                <div class="casino-place-bet-action-buttons"><button class="btn btn-reset">Reset</button> <button class="btn btn-primary" disabled="disabled">Submit</button></div>
                            </div>
                        </div>
                    </span>
                </div>
                <div id="resultdemo" style={{ position: 'absolute', zIndex: '1040' }}>
                    <div id="__BVID__68" role="dialog" aria-labelledby="__BVID__68___BV_modal_title_" aria-describedby="__BVID__68___BV_modal_body_" class="modal fade show casino-result d-none" aria-modal="true" >
                        <div class="modal-dialog modal-xl">
                            <span tabindex="0"></span>
                            <div id="__BVID__68___BV_modal_content_" tabindex="-1" class="modal-content">
                                <header id="__BVID__68___BV_modal_header_" class="modal-header">
                                    <h5 id="__BVID__68___BV_modal_title_" class="modal-title">
                                        K.B.C Result
                                    </h5>
                                    <button type="button" aria-label="Close" class="close">×</button>
                                </header>
                                <div id="__BVID__68___BV_modal_body_" class="modal-body">
                                    <div>
                                        <div class="casino-result-round">
                                            <div>Round-Id: 7972943735540</div>
                                            <div>Match Time: 10/09/2021 12: 26: 07</div>
                                        </div>

                                        <div class="row row5 worli-result">
                                            <div class="col-12 col-lg-6">
                                                <div class="casino-result-content">
                                                    <div class="casino-result-content-item text-center w-100">
                                                        <div class="casino-result-cards">
                                                            <div class="casino-result-cards-item"><img src="https://sitethemedata.com/v4/static/front/img/cards/KCC.png" /></div>
                                                            <div class="casino-result-cards-item"><img src="https://sitethemedata.com/v4/static/front/img/cards/10CC.png" /></div>
                                                            <div class="casino-result-cards-item"><img src="https://sitethemedata.com/v4/static/front/img/cards/3CC.png" /></div>
                                                            <div class="casino-result-cards-item"><img src="https://sitethemedata.com/v4/static/front/img/cards/KDD.png" /></div>
                                                            <div class="casino-result-cards-item"><img src="https://sitethemedata.com/v4/static/front/img/cards/2SS.png" /></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="casino-result-desc">
                                                    <div class="casino-result-desc-item">
                                                        <div>[Q1]Red-Black</div>
                                                        <div>Black</div>
                                                    </div>
                                                    <div class="casino-result-desc-item">
                                                        <div>[Q2]Odd-Even</div>
                                                        <div>Even</div>
                                                    </div>
                                                    <div class="casino-result-desc-item">
                                                        <div>[Q3]7 Up-7 Down</div>
                                                        <div>Down</div>
                                                    </div>
                                                    <div class="casino-result-desc-item">
                                                        <div>[Q4]3 Card Judgement</div>
                                                        <div>J  Q  K</div>
                                                    </div>
                                                    <div class="casino-result-desc-item">
                                                        <div>[Q5]Suits</div>
                                                        <div>Spade</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <span tabindex="0"></span>
                        </div>
                    </div>
                    <div id="resultdemo_backdrop" class="modal-backdrop d-none"></div>
                </div>
            </>
        );
    }
}

export default withRouter(Teen20);
